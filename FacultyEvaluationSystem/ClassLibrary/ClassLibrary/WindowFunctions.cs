﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClassLibrary
{
    public class WindowFunctions
    {
        //Closes the window
        public static void WindowClose()
        {
            Application.Exit();
        }

        //Minimizes the window in the parameter
        public static void WindowMinimize(Form frm)
        {
            frm.WindowState = FormWindowState.Minimized;
        }
    }
}
