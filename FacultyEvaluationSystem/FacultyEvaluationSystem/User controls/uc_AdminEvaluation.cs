﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FacultyEvaluationSystem
{
    public partial class uc_AdminEvaluation : UserControl
    {
        dataClass_FacultyEvaluationSystemDataContext db = new dataClass_FacultyEvaluationSystemDataContext();

        public uc_AdminEvaluation()
        {
            InitializeComponent();
        }

        private void btn_QuestionPrincipal_Click(object sender, EventArgs e)
        {
            frm_Admin_PrinEvalQuestions frm_Eval = new frm_Admin_PrinEvalQuestions();
            frm_Eval.ShowDialog();
        }

        private void btn_QuestionStudent_Click(object sender, EventArgs e)
        {
            frm_Admin_StudEval frm_StudeEval = new frm_Admin_StudEval();
            frm_StudeEval.ShowDialog();
        }
        //studentSchedbtn
        private void btn_StudentSchedule_Click(object sender, EventArgs e)
        {
            frm_Admin_PromptSchoolYear frm_SchoolYear = new frm_Admin_PromptSchoolYear();
            frm_SchoolYear.ShowDialog();
            if (frm_SchoolYear.set == false)
            {
                //MessageBox.Show("No school year selected.");
                return;
            }
            //frm_Admin_StudentScheduleEvaluation frm_StudSched = new frm_Admin_StudentScheduleEvaluation();
            //frm_StudSched.schoolyear = frmSchoolYear.schoolyear;
            //frm_StudSched.ShowDialog();
            //added paramater to determine what type of evaluation schedule
            frm_Admin_ScheduleEvaluation frm_Sched = new frm_Admin_ScheduleEvaluation("Student");
            frm_Sched.schoolyear = frm_SchoolYear.schoolyear;
            //added this code here pra makuha niya ang "starting YEAR only" para mao sad ang year sa dtp (9/17/2018)
            frm_Sched.startYear = frm_SchoolYear.startYear;
            frm_Sched.endYear = frm_SchoolYear.endYear;
            frm_Sched.ShowDialog();
        }

        private void btn_PrincipalSchedule_Click(object sender, EventArgs e)
        {
            frm_Admin_PromptSchoolYear frm_SchoolYear = new frm_Admin_PromptSchoolYear();
            frm_SchoolYear.ShowDialog();
            if (frm_SchoolYear.set == false) 
            { 
                //MessageBox.Show("No school year selected."); 
                return; 
            }
            //added paramater to determine what type of evaluation schedule
            frm_Admin_ScheduleEvaluation frm_Sched = new frm_Admin_ScheduleEvaluation("Principal");
            frm_Sched.schoolyear = frm_SchoolYear.schoolyear;
            //added this code here pra makuha niya ang "starting YEAR only" para mao sad ang year sa dtp (9/17/2018)
            frm_Sched.startYear = frm_SchoolYear.startYear;
            frm_Sched.endYear = frm_SchoolYear.endYear;
            frm_Sched.ShowDialog();
        }

        private void uc_AdminEvaluation_Load(object sender, EventArgs e)
        {

        }

        private void btn_EvaluateTeacher_Click(object sender, EventArgs e)
        {
            //check for ongoing evaluation
            //if naa, opens form. otherwise,
            //prompt an error.
            if(db.sp_SelectOngoingPrincipalEval().Any())
            {
                string schoolyear="";
                int idSched=0;
                foreach (var items in db.sp_SelectOngoingPrincipalEval())
                {
                    schoolyear = items.School_Year;
                    idSched = items.ID;
                }

                frm_EvaluatePrincipalSelect frm = new frm_EvaluatePrincipalSelect(schoolyear, idSched);
                frm.ShowDialog();

                if (frm.evaluate == false) return;

                frm_PrincipalEvaluation frmEval = new frm_PrincipalEvaluation(idSched, frm.id_Teacher);
                frmEval.label6.Text = frm.teacherName;
                frmEval.Show();
            }
            else
            {
                MessageBox.Show("There are no ongoing evaluation for teachers right now.");
            }
        }
    }
}