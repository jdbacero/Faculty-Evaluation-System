﻿namespace FacultyEvaluationSystem
{
    partial class uc_PanelTeacher
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.rdo_StronglyDisagree = new System.Windows.Forms.RadioButton();
            this.rdo_Disagree = new System.Windows.Forms.RadioButton();
            this.rdo_Agree = new System.Windows.Forms.RadioButton();
            this.rdo_StronglyAgree = new System.Windows.Forms.RadioButton();
            this.lbl_Name = new System.Windows.Forms.RichTextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 5;
            this.bunifuElipse1.TargetControl = this;
            // 
            // rdo_StronglyDisagree
            // 
            this.rdo_StronglyDisagree.AutoSize = true;
            this.rdo_StronglyDisagree.BackColor = System.Drawing.Color.Transparent;
            this.rdo_StronglyDisagree.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdo_StronglyDisagree.ForeColor = System.Drawing.Color.White;
            this.rdo_StronglyDisagree.Location = new System.Drawing.Point(55, 212);
            this.rdo_StronglyDisagree.Name = "rdo_StronglyDisagree";
            this.rdo_StronglyDisagree.Size = new System.Drawing.Size(136, 21);
            this.rdo_StronglyDisagree.TabIndex = 8;
            this.rdo_StronglyDisagree.TabStop = true;
            this.rdo_StronglyDisagree.Text = "Strongly Disagree";
            this.rdo_StronglyDisagree.UseVisualStyleBackColor = false;
            // 
            // rdo_Disagree
            // 
            this.rdo_Disagree.AutoSize = true;
            this.rdo_Disagree.BackColor = System.Drawing.Color.Transparent;
            this.rdo_Disagree.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdo_Disagree.ForeColor = System.Drawing.Color.White;
            this.rdo_Disagree.Location = new System.Drawing.Point(55, 186);
            this.rdo_Disagree.Name = "rdo_Disagree";
            this.rdo_Disagree.Size = new System.Drawing.Size(82, 21);
            this.rdo_Disagree.TabIndex = 7;
            this.rdo_Disagree.TabStop = true;
            this.rdo_Disagree.Text = "Disagree";
            this.rdo_Disagree.UseVisualStyleBackColor = false;
            // 
            // rdo_Agree
            // 
            this.rdo_Agree.AutoSize = true;
            this.rdo_Agree.BackColor = System.Drawing.Color.Transparent;
            this.rdo_Agree.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdo_Agree.ForeColor = System.Drawing.Color.White;
            this.rdo_Agree.Location = new System.Drawing.Point(55, 160);
            this.rdo_Agree.Name = "rdo_Agree";
            this.rdo_Agree.Size = new System.Drawing.Size(64, 21);
            this.rdo_Agree.TabIndex = 6;
            this.rdo_Agree.TabStop = true;
            this.rdo_Agree.Text = "Agree";
            this.rdo_Agree.UseVisualStyleBackColor = false;
            // 
            // rdo_StronglyAgree
            // 
            this.rdo_StronglyAgree.AutoSize = true;
            this.rdo_StronglyAgree.BackColor = System.Drawing.Color.Transparent;
            this.rdo_StronglyAgree.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdo_StronglyAgree.ForeColor = System.Drawing.Color.White;
            this.rdo_StronglyAgree.Location = new System.Drawing.Point(55, 134);
            this.rdo_StronglyAgree.Name = "rdo_StronglyAgree";
            this.rdo_StronglyAgree.Size = new System.Drawing.Size(118, 21);
            this.rdo_StronglyAgree.TabIndex = 5;
            this.rdo_StronglyAgree.TabStop = true;
            this.rdo_StronglyAgree.Text = "Strongly Agree";
            this.rdo_StronglyAgree.UseVisualStyleBackColor = false;
            // 
            // lbl_Name
            // 
            this.lbl_Name.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(89)))), ((int)(((byte)(131)))));
            this.lbl_Name.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbl_Name.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbl_Name.Font = new System.Drawing.Font("Book Antiqua", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbl_Name.ForeColor = System.Drawing.Color.White;
            this.lbl_Name.Location = new System.Drawing.Point(125, 3);
            this.lbl_Name.Name = "lbl_Name";
            this.lbl_Name.ReadOnly = true;
            this.lbl_Name.Size = new System.Drawing.Size(138, 115);
            this.lbl_Name.TabIndex = 10;
            this.lbl_Name.Text = "";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(116, 115);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            // 
            // uc_PanelTeacher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(89)))), ((int)(((byte)(131)))));
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lbl_Name);
            this.Controls.Add(this.rdo_StronglyDisagree);
            this.Controls.Add(this.rdo_Disagree);
            this.Controls.Add(this.rdo_Agree);
            this.Controls.Add(this.rdo_StronglyAgree);
            this.Name = "uc_PanelTeacher";
            this.Size = new System.Drawing.Size(270, 237);
            this.Load += new System.EventHandler(this.uc_PanelTeacher_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        public System.Windows.Forms.RadioButton rdo_StronglyDisagree;
        public System.Windows.Forms.RadioButton rdo_Disagree;
        public System.Windows.Forms.RadioButton rdo_Agree;
        public System.Windows.Forms.RadioButton rdo_StronglyAgree;
        private System.Windows.Forms.RichTextBox lbl_Name;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}
