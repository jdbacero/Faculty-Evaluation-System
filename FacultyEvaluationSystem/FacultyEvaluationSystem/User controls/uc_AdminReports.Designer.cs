﻿namespace FacultyEvaluationSystem
{
    partial class uc_AdminReports
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(uc_AdminReports));
            this.btn_EvaluationReport = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btn_Students = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.bunifuCustomLabel1 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.SuspendLayout();
            // 
            // btn_EvaluationReport
            // 
            this.btn_EvaluationReport.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(89)))), ((int)(((byte)(131)))));
            this.btn_EvaluationReport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_EvaluationReport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(89)))), ((int)(((byte)(131)))));
            this.btn_EvaluationReport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_EvaluationReport.BorderRadius = 0;
            this.btn_EvaluationReport.ButtonText = "Evaluation Reports";
            this.btn_EvaluationReport.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_EvaluationReport.DisabledColor = System.Drawing.Color.Gray;
            this.btn_EvaluationReport.Iconcolor = System.Drawing.Color.Transparent;
            this.btn_EvaluationReport.Iconimage = ((System.Drawing.Image)(resources.GetObject("btn_EvaluationReport.Iconimage")));
            this.btn_EvaluationReport.Iconimage_right = null;
            this.btn_EvaluationReport.Iconimage_right_Selected = null;
            this.btn_EvaluationReport.Iconimage_Selected = null;
            this.btn_EvaluationReport.IconMarginLeft = 0;
            this.btn_EvaluationReport.IconMarginRight = 0;
            this.btn_EvaluationReport.IconRightVisible = false;
            this.btn_EvaluationReport.IconRightZoom = 0D;
            this.btn_EvaluationReport.IconVisible = true;
            this.btn_EvaluationReport.IconZoom = 90D;
            this.btn_EvaluationReport.IsTab = false;
            this.btn_EvaluationReport.Location = new System.Drawing.Point(3, 183);
            this.btn_EvaluationReport.Name = "btn_EvaluationReport";
            this.btn_EvaluationReport.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(89)))), ((int)(((byte)(131)))));
            this.btn_EvaluationReport.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(142)))), ((int)(((byte)(176)))));
            this.btn_EvaluationReport.OnHoverTextColor = System.Drawing.Color.White;
            this.btn_EvaluationReport.selected = false;
            this.btn_EvaluationReport.Size = new System.Drawing.Size(881, 61);
            this.btn_EvaluationReport.TabIndex = 6;
            this.btn_EvaluationReport.Text = "Evaluation Reports";
            this.btn_EvaluationReport.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn_EvaluationReport.Textcolor = System.Drawing.Color.White;
            this.btn_EvaluationReport.TextFont = new System.Drawing.Font("Century Gothic", 11F);
            this.btn_EvaluationReport.Click += new System.EventHandler(this.btn_EvaluationReport_Click);
            // 
            // btn_Students
            // 
            this.btn_Students.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(89)))), ((int)(((byte)(131)))));
            this.btn_Students.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Students.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(89)))), ((int)(((byte)(131)))));
            this.btn_Students.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_Students.BorderRadius = 0;
            this.btn_Students.ButtonText = "Students\' Evaluation Status";
            this.btn_Students.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Students.DisabledColor = System.Drawing.Color.Gray;
            this.btn_Students.Iconcolor = System.Drawing.Color.Transparent;
            this.btn_Students.Iconimage = ((System.Drawing.Image)(resources.GetObject("btn_Students.Iconimage")));
            this.btn_Students.Iconimage_right = null;
            this.btn_Students.Iconimage_right_Selected = null;
            this.btn_Students.Iconimage_Selected = null;
            this.btn_Students.IconMarginLeft = 0;
            this.btn_Students.IconMarginRight = 0;
            this.btn_Students.IconRightVisible = false;
            this.btn_Students.IconRightZoom = 0D;
            this.btn_Students.IconVisible = true;
            this.btn_Students.IconZoom = 70D;
            this.btn_Students.IsTab = false;
            this.btn_Students.Location = new System.Drawing.Point(3, 250);
            this.btn_Students.Name = "btn_Students";
            this.btn_Students.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(89)))), ((int)(((byte)(131)))));
            this.btn_Students.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(142)))), ((int)(((byte)(176)))));
            this.btn_Students.OnHoverTextColor = System.Drawing.Color.White;
            this.btn_Students.selected = false;
            this.btn_Students.Size = new System.Drawing.Size(881, 61);
            this.btn_Students.TabIndex = 7;
            this.btn_Students.Text = "Students\' Evaluation Status";
            this.btn_Students.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn_Students.Textcolor = System.Drawing.Color.White;
            this.btn_Students.TextFont = new System.Drawing.Font("Century Gothic", 11F);
            this.btn_Students.Click += new System.EventHandler(this.btn_Students_Click);
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 5;
            this.bunifuElipse1.TargetControl = this;
            // 
            // bunifuCustomLabel1
            // 
            this.bunifuCustomLabel1.AutoSize = true;
            this.bunifuCustomLabel1.Font = new System.Drawing.Font("Century Gothic", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bunifuCustomLabel1.ForeColor = System.Drawing.Color.White;
            this.bunifuCustomLabel1.Location = new System.Drawing.Point(25, 30);
            this.bunifuCustomLabel1.Name = "bunifuCustomLabel1";
            this.bunifuCustomLabel1.Size = new System.Drawing.Size(211, 27);
            this.bunifuCustomLabel1.TabIndex = 8;
            this.bunifuCustomLabel1.Text = "Data and Reports";
            // 
            // uc_AdminReports
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.Controls.Add(this.bunifuCustomLabel1);
            this.Controls.Add(this.btn_Students);
            this.Controls.Add(this.btn_EvaluationReport);
            this.Name = "uc_AdminReports";
            this.Size = new System.Drawing.Size(887, 514);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuFlatButton btn_EvaluationReport;
        private Bunifu.Framework.UI.BunifuFlatButton btn_Students;
        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel1;
    }
}
