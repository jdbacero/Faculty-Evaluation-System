﻿namespace FacultyEvaluationSystem
{
    partial class uc_StudentEvaluation
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(uc_StudentEvaluation));
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.panel11 = new System.Windows.Forms.Panel();
            this.lbl_Catergory = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.btn_Previous = new System.Windows.Forms.PictureBox();
            this.btn_Next = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.bunifuTrackbar1 = new Bunifu.Framework.UI.BunifuTrackbar();
            this.lbl_Question = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.btn_Evaluate = new Bunifu.Framework.UI.BunifuFlatButton();
            this.butangananSaPangutana = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_Previous)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_Next)).BeginInit();
            this.SuspendLayout();
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 5;
            this.bunifuElipse1.TargetControl = this;
            // 
            // panel11
            // 
            this.panel11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(89)))), ((int)(((byte)(131)))));
            this.panel11.Controls.Add(this.lbl_Catergory);
            this.panel11.Location = new System.Drawing.Point(0, 0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(887, 58);
            this.panel11.TabIndex = 6;
            // 
            // lbl_Catergory
            // 
            this.lbl_Catergory.AutoSize = true;
            this.lbl_Catergory.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Catergory.Font = new System.Drawing.Font("Century Gothic", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Catergory.ForeColor = System.Drawing.Color.Transparent;
            this.lbl_Catergory.Location = new System.Drawing.Point(21, 12);
            this.lbl_Catergory.Name = "lbl_Catergory";
            this.lbl_Catergory.Size = new System.Drawing.Size(41, 33);
            this.lbl_Catergory.TabIndex = 20;
            this.lbl_Catergory.Text = "ID";
            // 
            // btn_Previous
            // 
            this.btn_Previous.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btn_Previous.BackColor = System.Drawing.Color.Transparent;
            this.btn_Previous.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Previous.Image = ((System.Drawing.Image)(resources.GetObject("btn_Previous.Image")));
            this.btn_Previous.Location = new System.Drawing.Point(3, 257);
            this.btn_Previous.Name = "btn_Previous";
            this.btn_Previous.Size = new System.Drawing.Size(42, 41);
            this.btn_Previous.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btn_Previous.TabIndex = 14;
            this.btn_Previous.TabStop = false;
            this.btn_Previous.Click += new System.EventHandler(this.btn_Previous_Click);
            // 
            // btn_Next
            // 
            this.btn_Next.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btn_Next.BackColor = System.Drawing.Color.Transparent;
            this.btn_Next.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Next.Image = ((System.Drawing.Image)(resources.GetObject("btn_Next.Image")));
            this.btn_Next.Location = new System.Drawing.Point(842, 257);
            this.btn_Next.Name = "btn_Next";
            this.btn_Next.Size = new System.Drawing.Size(42, 41);
            this.btn_Next.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btn_Next.TabIndex = 15;
            this.btn_Next.TabStop = false;
            this.btn_Next.Click += new System.EventHandler(this.btn_Next_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.AutoScroll = true;
            this.panel1.Location = new System.Drawing.Point(51, 133);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(785, 302);
            this.panel1.TabIndex = 18;
            // 
            // bunifuTrackbar1
            // 
            this.bunifuTrackbar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuTrackbar1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuTrackbar1.BackgroudColor = System.Drawing.Color.DarkGray;
            this.bunifuTrackbar1.BorderRadius = 0;
            this.bunifuTrackbar1.IndicatorColor = System.Drawing.Color.SeaGreen;
            this.bunifuTrackbar1.Location = new System.Drawing.Point(64, 468);
            this.bunifuTrackbar1.MaximumValue = 10;
            this.bunifuTrackbar1.MinimumValue = 0;
            this.bunifuTrackbar1.Name = "bunifuTrackbar1";
            this.bunifuTrackbar1.Size = new System.Drawing.Size(785, 30);
            this.bunifuTrackbar1.SliderRadius = 0;
            this.bunifuTrackbar1.TabIndex = 19;
            this.bunifuTrackbar1.Value = 2;
            this.bunifuTrackbar1.Visible = false;
            // 
            // lbl_Question
            // 
            this.lbl_Question.AutoSize = true;
            this.lbl_Question.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Question.Font = new System.Drawing.Font("Century Gothic", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Question.ForeColor = System.Drawing.Color.Black;
            this.lbl_Question.Location = new System.Drawing.Point(81, 65);
            this.lbl_Question.Name = "lbl_Question";
            this.lbl_Question.Size = new System.Drawing.Size(160, 25);
            this.lbl_Question.TabIndex = 21;
            this.lbl_Question.Text = "Question here";
            this.lbl_Question.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_Evaluate
            // 
            this.btn_Evaluate.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(89)))), ((int)(((byte)(131)))));
            this.btn_Evaluate.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btn_Evaluate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(89)))), ((int)(((byte)(131)))));
            this.btn_Evaluate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_Evaluate.BorderRadius = 3;
            this.btn_Evaluate.ButtonText = "Submit";
            this.btn_Evaluate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Evaluate.DisabledColor = System.Drawing.Color.Gray;
            this.btn_Evaluate.Iconcolor = System.Drawing.Color.Transparent;
            this.btn_Evaluate.Iconimage = ((System.Drawing.Image)(resources.GetObject("btn_Evaluate.Iconimage")));
            this.btn_Evaluate.Iconimage_right = null;
            this.btn_Evaluate.Iconimage_right_Selected = null;
            this.btn_Evaluate.Iconimage_Selected = null;
            this.btn_Evaluate.IconMarginLeft = 0;
            this.btn_Evaluate.IconMarginRight = 0;
            this.btn_Evaluate.IconRightVisible = true;
            this.btn_Evaluate.IconRightZoom = 0D;
            this.btn_Evaluate.IconVisible = false;
            this.btn_Evaluate.IconZoom = 90D;
            this.btn_Evaluate.IsTab = false;
            this.btn_Evaluate.Location = new System.Drawing.Point(324, 450);
            this.btn_Evaluate.Name = "btn_Evaluate";
            this.btn_Evaluate.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(89)))), ((int)(((byte)(131)))));
            this.btn_Evaluate.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(142)))), ((int)(((byte)(176)))));
            this.btn_Evaluate.OnHoverTextColor = System.Drawing.Color.White;
            this.btn_Evaluate.selected = false;
            this.btn_Evaluate.Size = new System.Drawing.Size(241, 48);
            this.btn_Evaluate.TabIndex = 22;
            this.btn_Evaluate.Text = "Submit";
            this.btn_Evaluate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn_Evaluate.Textcolor = System.Drawing.Color.White;
            this.btn_Evaluate.TextFont = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Evaluate.Click += new System.EventHandler(this.btn_Evaluate_Click_1);
            // 
            // butangananSaPangutana
            // 
            this.butangananSaPangutana.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.butangananSaPangutana.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.butangananSaPangutana.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.butangananSaPangutana.Font = new System.Drawing.Font("Century Gothic", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butangananSaPangutana.ForeColor = System.Drawing.Color.White;
            this.butangananSaPangutana.Location = new System.Drawing.Point(64, 65);
            this.butangananSaPangutana.Name = "butangananSaPangutana";
            this.butangananSaPangutana.ReadOnly = true;
            this.butangananSaPangutana.Size = new System.Drawing.Size(769, 62);
            this.butangananSaPangutana.TabIndex = 23;
            this.butangananSaPangutana.Text = "";
            this.butangananSaPangutana.Click += new System.EventHandler(this.butangananSaPangutana_Click);
            this.butangananSaPangutana.Enter += new System.EventHandler(this.butangananSaPangutana_Enter);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 90);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 24;
            this.label1.Text = "label1";
            this.label1.Visible = false;
            // 
            // uc_StudentEvaluation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.Controls.Add(this.label1);
            this.Controls.Add(this.butangananSaPangutana);
            this.Controls.Add(this.btn_Evaluate);
            this.Controls.Add(this.lbl_Question);
            this.Controls.Add(this.bunifuTrackbar1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btn_Next);
            this.Controls.Add(this.btn_Previous);
            this.Controls.Add(this.panel11);
            this.Name = "uc_StudentEvaluation";
            this.Size = new System.Drawing.Size(887, 514);
            this.Load += new System.EventHandler(this.uc_StudentEvaluation_Load);
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_Previous)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_Next)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.PictureBox btn_Next;
        private System.Windows.Forms.PictureBox btn_Previous;
        private Bunifu.Framework.UI.BunifuTrackbar bunifuTrackbar1;
        public System.Windows.Forms.Panel panel1;
        private Bunifu.Framework.UI.BunifuCustomLabel lbl_Question;
        private Bunifu.Framework.UI.BunifuFlatButton btn_Evaluate;
        private System.Windows.Forms.RichTextBox butangananSaPangutana;
        private System.Windows.Forms.Label label1;
        public Bunifu.Framework.UI.BunifuCustomLabel lbl_Catergory;
    }
}
