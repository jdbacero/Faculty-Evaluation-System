﻿namespace FacultyEvaluationSystem
{
    partial class uc_AdminMasterlist
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(uc_AdminMasterlist));
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.dgv_Masterlist = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.btn_MasterListStudents = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btn_MasterListTeachers = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btn_UploadMasterlist = new Bunifu.Framework.UI.BunifuThinButton2();
            this.lbl_Masterlist = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.btn_Excel = new Bunifu.Framework.UI.BunifuThinButton2();
            this.bunifuSeparator1 = new Bunifu.Framework.UI.BunifuSeparator();
            this.txt_Search = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.bunifuCustomLabel1 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.cmb_SchoolYear = new Bunifu.Framework.UI.BunifuDropdown();
            this.cmb_Section = new Bunifu.Framework.UI.BunifuDropdown();
            this.cmb_GradeLevel = new Bunifu.Framework.UI.BunifuDropdown();
            this.bunifuCustomLabel2 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel3 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel4 = new Bunifu.Framework.UI.BunifuCustomLabel();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Masterlist)).BeginInit();
            this.SuspendLayout();
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 5;
            this.bunifuElipse1.TargetControl = this;
            // 
            // dgv_Masterlist
            // 
            this.dgv_Masterlist.AllowUserToAddRows = false;
            this.dgv_Masterlist.AllowUserToDeleteRows = false;
            this.dgv_Masterlist.AllowUserToResizeColumns = false;
            this.dgv_Masterlist.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgv_Masterlist.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_Masterlist.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_Masterlist.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_Masterlist.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.dgv_Masterlist.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv_Masterlist.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(89)))), ((int)(((byte)(131)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_Masterlist.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_Masterlist.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Century Gothic", 9F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_Masterlist.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgv_Masterlist.DoubleBuffered = true;
            this.dgv_Masterlist.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgv_Masterlist.EnableHeadersVisualStyles = false;
            this.dgv_Masterlist.HeaderBgColor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(89)))), ((int)(((byte)(131)))));
            this.dgv_Masterlist.HeaderForeColor = System.Drawing.Color.White;
            this.dgv_Masterlist.Location = new System.Drawing.Point(2, 134);
            this.dgv_Masterlist.MultiSelect = false;
            this.dgv_Masterlist.Name = "dgv_Masterlist";
            this.dgv_Masterlist.ReadOnly = true;
            this.dgv_Masterlist.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_Masterlist.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgv_Masterlist.RowHeadersVisible = false;
            this.dgv_Masterlist.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgv_Masterlist.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_Masterlist.Size = new System.Drawing.Size(881, 331);
            this.dgv_Masterlist.TabIndex = 1;
            this.dgv_Masterlist.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_Masterlist_CellDoubleClick);
            // 
            // btn_MasterListStudents
            // 
            this.btn_MasterListStudents.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(0)))), ((int)(((byte)(34)))));
            this.btn_MasterListStudents.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(89)))), ((int)(((byte)(131)))));
            this.btn_MasterListStudents.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_MasterListStudents.BorderRadius = 0;
            this.btn_MasterListStudents.ButtonText = "Students";
            this.btn_MasterListStudents.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_MasterListStudents.DisabledColor = System.Drawing.Color.Gray;
            this.btn_MasterListStudents.Iconcolor = System.Drawing.Color.Transparent;
            this.btn_MasterListStudents.Iconimage = null;
            this.btn_MasterListStudents.Iconimage_right = null;
            this.btn_MasterListStudents.Iconimage_right_Selected = null;
            this.btn_MasterListStudents.Iconimage_Selected = null;
            this.btn_MasterListStudents.IconMarginLeft = 0;
            this.btn_MasterListStudents.IconMarginRight = 0;
            this.btn_MasterListStudents.IconRightVisible = true;
            this.btn_MasterListStudents.IconRightZoom = 0D;
            this.btn_MasterListStudents.IconVisible = true;
            this.btn_MasterListStudents.IconZoom = 90D;
            this.btn_MasterListStudents.IsTab = true;
            this.btn_MasterListStudents.Location = new System.Drawing.Point(3, 3);
            this.btn_MasterListStudents.Name = "btn_MasterListStudents";
            this.btn_MasterListStudents.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(89)))), ((int)(((byte)(131)))));
            this.btn_MasterListStudents.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(142)))), ((int)(((byte)(176)))));
            this.btn_MasterListStudents.OnHoverTextColor = System.Drawing.Color.White;
            this.btn_MasterListStudents.selected = false;
            this.btn_MasterListStudents.Size = new System.Drawing.Size(300, 49);
            this.btn_MasterListStudents.TabIndex = 2;
            this.btn_MasterListStudents.Text = "Students";
            this.btn_MasterListStudents.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn_MasterListStudents.Textcolor = System.Drawing.Color.White;
            this.btn_MasterListStudents.TextFont = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_MasterListStudents.Click += new System.EventHandler(this.btn_MasterListStudents_Click);
            // 
            // btn_MasterListTeachers
            // 
            this.btn_MasterListTeachers.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(0)))), ((int)(((byte)(34)))));
            this.btn_MasterListTeachers.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_MasterListTeachers.AutoSize = true;
            this.btn_MasterListTeachers.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(89)))), ((int)(((byte)(131)))));
            this.btn_MasterListTeachers.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_MasterListTeachers.BorderRadius = 0;
            this.btn_MasterListTeachers.ButtonText = "Teachers";
            this.btn_MasterListTeachers.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_MasterListTeachers.DisabledColor = System.Drawing.Color.Gray;
            this.btn_MasterListTeachers.Iconcolor = System.Drawing.Color.Transparent;
            this.btn_MasterListTeachers.Iconimage = null;
            this.btn_MasterListTeachers.Iconimage_right = null;
            this.btn_MasterListTeachers.Iconimage_right_Selected = null;
            this.btn_MasterListTeachers.Iconimage_Selected = null;
            this.btn_MasterListTeachers.IconMarginLeft = 0;
            this.btn_MasterListTeachers.IconMarginRight = 0;
            this.btn_MasterListTeachers.IconRightVisible = true;
            this.btn_MasterListTeachers.IconRightZoom = 0D;
            this.btn_MasterListTeachers.IconVisible = true;
            this.btn_MasterListTeachers.IconZoom = 90D;
            this.btn_MasterListTeachers.IsTab = true;
            this.btn_MasterListTeachers.Location = new System.Drawing.Point(582, 3);
            this.btn_MasterListTeachers.Name = "btn_MasterListTeachers";
            this.btn_MasterListTeachers.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(89)))), ((int)(((byte)(131)))));
            this.btn_MasterListTeachers.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(142)))), ((int)(((byte)(176)))));
            this.btn_MasterListTeachers.OnHoverTextColor = System.Drawing.Color.White;
            this.btn_MasterListTeachers.selected = false;
            this.btn_MasterListTeachers.Size = new System.Drawing.Size(300, 49);
            this.btn_MasterListTeachers.TabIndex = 3;
            this.btn_MasterListTeachers.Text = "Teachers";
            this.btn_MasterListTeachers.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn_MasterListTeachers.Textcolor = System.Drawing.Color.White;
            this.btn_MasterListTeachers.TextFont = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_MasterListTeachers.Click += new System.EventHandler(this.btn_MasterListTeachers_Click);
            // 
            // btn_UploadMasterlist
            // 
            this.btn_UploadMasterlist.ActiveBorderThickness = 1;
            this.btn_UploadMasterlist.ActiveCornerRadius = 20;
            this.btn_UploadMasterlist.ActiveFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(20)))), ((int)(((byte)(32)))));
            this.btn_UploadMasterlist.ActiveForecolor = System.Drawing.Color.White;
            this.btn_UploadMasterlist.ActiveLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(163)))), ((int)(((byte)(173)))));
            this.btn_UploadMasterlist.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_UploadMasterlist.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_UploadMasterlist.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_UploadMasterlist.BackgroundImage")));
            this.btn_UploadMasterlist.ButtonText = "Upload";
            this.btn_UploadMasterlist.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_UploadMasterlist.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_UploadMasterlist.ForeColor = System.Drawing.Color.SeaGreen;
            this.btn_UploadMasterlist.IdleBorderThickness = 3;
            this.btn_UploadMasterlist.IdleCornerRadius = 20;
            this.btn_UploadMasterlist.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(163)))), ((int)(((byte)(173)))));
            this.btn_UploadMasterlist.IdleForecolor = System.Drawing.Color.Black;
            this.btn_UploadMasterlist.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(20)))), ((int)(((byte)(32)))));
            this.btn_UploadMasterlist.Location = new System.Drawing.Point(100, 467);
            this.btn_UploadMasterlist.Margin = new System.Windows.Forms.Padding(5);
            this.btn_UploadMasterlist.Name = "btn_UploadMasterlist";
            this.btn_UploadMasterlist.Size = new System.Drawing.Size(230, 45);
            this.btn_UploadMasterlist.TabIndex = 5;
            this.btn_UploadMasterlist.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn_UploadMasterlist.Visible = false;
            this.btn_UploadMasterlist.Click += new System.EventHandler(this.btn_UploadMasterlist_Click);
            // 
            // lbl_Masterlist
            // 
            this.lbl_Masterlist.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lbl_Masterlist.AutoSize = true;
            this.lbl_Masterlist.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbl_Masterlist.ForeColor = System.Drawing.Color.White;
            this.lbl_Masterlist.Location = new System.Drawing.Point(395, 3);
            this.lbl_Masterlist.Name = "lbl_Masterlist";
            this.lbl_Masterlist.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.lbl_Masterlist.Size = new System.Drawing.Size(98, 25);
            this.lbl_Masterlist.TabIndex = 6;
            this.lbl_Masterlist.Text = "Placeholder";
            this.lbl_Masterlist.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lbl_Masterlist.Visible = false;
            // 
            // btn_Excel
            // 
            this.btn_Excel.ActiveBorderThickness = 1;
            this.btn_Excel.ActiveCornerRadius = 20;
            this.btn_Excel.ActiveFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(20)))), ((int)(((byte)(32)))));
            this.btn_Excel.ActiveForecolor = System.Drawing.Color.White;
            this.btn_Excel.ActiveLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(163)))), ((int)(((byte)(173)))));
            this.btn_Excel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Excel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_Excel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_Excel.BackgroundImage")));
            this.btn_Excel.ButtonText = "Open Excel";
            this.btn_Excel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Excel.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Excel.ForeColor = System.Drawing.Color.SeaGreen;
            this.btn_Excel.IdleBorderThickness = 3;
            this.btn_Excel.IdleCornerRadius = 20;
            this.btn_Excel.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(163)))), ((int)(((byte)(173)))));
            this.btn_Excel.IdleForecolor = System.Drawing.Color.Black;
            this.btn_Excel.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(20)))), ((int)(((byte)(32)))));
            this.btn_Excel.Location = new System.Drawing.Point(552, 467);
            this.btn_Excel.Margin = new System.Windows.Forms.Padding(5);
            this.btn_Excel.Name = "btn_Excel";
            this.btn_Excel.Size = new System.Drawing.Size(230, 45);
            this.btn_Excel.TabIndex = 7;
            this.btn_Excel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn_Excel.Visible = false;
            this.btn_Excel.Click += new System.EventHandler(this.btn_Excel_Click);
            // 
            // bunifuSeparator1
            // 
            this.bunifuSeparator1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuSeparator1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuSeparator1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))));
            this.bunifuSeparator1.LineThickness = 1;
            this.bunifuSeparator1.Location = new System.Drawing.Point(3, 43);
            this.bunifuSeparator1.Name = "bunifuSeparator1";
            this.bunifuSeparator1.Size = new System.Drawing.Size(879, 35);
            this.bunifuSeparator1.TabIndex = 10;
            this.bunifuSeparator1.Transparency = 255;
            this.bunifuSeparator1.Vertical = false;
            // 
            // txt_Search
            // 
            this.txt_Search.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_Search.BorderColorFocused = System.Drawing.Color.Gray;
            this.txt_Search.BorderColorIdle = System.Drawing.Color.White;
            this.txt_Search.BorderColorMouseHover = System.Drawing.Color.Gray;
            this.txt_Search.BorderThickness = 3;
            this.txt_Search.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_Search.Enabled = false;
            this.txt_Search.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txt_Search.ForeColor = System.Drawing.Color.White;
            this.txt_Search.isPassword = false;
            this.txt_Search.Location = new System.Drawing.Point(415, 92);
            this.txt_Search.Margin = new System.Windows.Forms.Padding(4);
            this.txt_Search.Name = "txt_Search";
            this.txt_Search.Size = new System.Drawing.Size(250, 34);
            this.txt_Search.TabIndex = 28;
            this.txt_Search.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // bunifuCustomLabel1
            // 
            this.bunifuCustomLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuCustomLabel1.AutoSize = true;
            this.bunifuCustomLabel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.bunifuCustomLabel1.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bunifuCustomLabel1.ForeColor = System.Drawing.Color.White;
            this.bunifuCustomLabel1.Location = new System.Drawing.Point(684, 65);
            this.bunifuCustomLabel1.Name = "bunifuCustomLabel1";
            this.bunifuCustomLabel1.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.bunifuCustomLabel1.Size = new System.Drawing.Size(98, 25);
            this.bunifuCustomLabel1.TabIndex = 24;
            this.bunifuCustomLabel1.Text = "School Year:";
            this.bunifuCustomLabel1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // cmb_SchoolYear
            // 
            this.cmb_SchoolYear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmb_SchoolYear.BackColor = System.Drawing.Color.Transparent;
            this.cmb_SchoolYear.BorderRadius = 3;
            this.cmb_SchoolYear.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmb_SchoolYear.DisabledColor = System.Drawing.Color.Gray;
            this.cmb_SchoolYear.Enabled = false;
            this.cmb_SchoolYear.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_SchoolYear.ForeColor = System.Drawing.Color.White;
            this.cmb_SchoolYear.Items = new string[0];
            this.cmb_SchoolYear.Location = new System.Drawing.Point(688, 92);
            this.cmb_SchoolYear.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmb_SchoolYear.Name = "cmb_SchoolYear";
            this.cmb_SchoolYear.NomalColor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(89)))), ((int)(((byte)(131)))));
            this.cmb_SchoolYear.onHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(142)))), ((int)(((byte)(176)))));
            this.cmb_SchoolYear.selectedIndex = -1;
            this.cmb_SchoolYear.Size = new System.Drawing.Size(185, 35);
            this.cmb_SchoolYear.TabIndex = 23;
            this.cmb_SchoolYear.onItemSelected += new System.EventHandler(this.cmb_SchoolYear_onItemSelected_1);
            // 
            // cmb_Section
            // 
            this.cmb_Section.BackColor = System.Drawing.Color.Transparent;
            this.cmb_Section.BorderRadius = 3;
            this.cmb_Section.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmb_Section.DisabledColor = System.Drawing.Color.Gray;
            this.cmb_Section.Enabled = false;
            this.cmb_Section.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_Section.ForeColor = System.Drawing.Color.White;
            this.cmb_Section.Items = new string[0];
            this.cmb_Section.Location = new System.Drawing.Point(212, 92);
            this.cmb_Section.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmb_Section.Name = "cmb_Section";
            this.cmb_Section.NomalColor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(89)))), ((int)(((byte)(131)))));
            this.cmb_Section.onHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(142)))), ((int)(((byte)(176)))));
            this.cmb_Section.selectedIndex = -1;
            this.cmb_Section.Size = new System.Drawing.Size(185, 35);
            this.cmb_Section.TabIndex = 22;
            this.cmb_Section.onItemSelected += new System.EventHandler(this.cmb_Section_onItemSelected_1);
            // 
            // cmb_GradeLevel
            // 
            this.cmb_GradeLevel.BackColor = System.Drawing.Color.Transparent;
            this.cmb_GradeLevel.BorderRadius = 3;
            this.cmb_GradeLevel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmb_GradeLevel.DisabledColor = System.Drawing.Color.Gray;
            this.cmb_GradeLevel.Enabled = false;
            this.cmb_GradeLevel.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_GradeLevel.ForeColor = System.Drawing.Color.White;
            this.cmb_GradeLevel.Items = new string[] {
        "All",
        "Grade 1",
        "Grade 2",
        "Grade 3",
        "Grade 4",
        "Grade 5",
        "Grade 6",
        "Grade 7",
        "Grade 8",
        "Grade 9",
        "Grade 10",
        "Grade 11"};
            this.cmb_GradeLevel.Location = new System.Drawing.Point(6, 92);
            this.cmb_GradeLevel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmb_GradeLevel.Name = "cmb_GradeLevel";
            this.cmb_GradeLevel.NomalColor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(89)))), ((int)(((byte)(131)))));
            this.cmb_GradeLevel.onHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(142)))), ((int)(((byte)(176)))));
            this.cmb_GradeLevel.selectedIndex = 0;
            this.cmb_GradeLevel.Size = new System.Drawing.Size(185, 35);
            this.cmb_GradeLevel.TabIndex = 21;
            this.cmb_GradeLevel.onItemSelected += new System.EventHandler(this.cmb_GradeLevel_onItemSelected_1);
            // 
            // bunifuCustomLabel2
            // 
            this.bunifuCustomLabel2.AutoSize = true;
            this.bunifuCustomLabel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.bunifuCustomLabel2.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bunifuCustomLabel2.ForeColor = System.Drawing.Color.White;
            this.bunifuCustomLabel2.Location = new System.Drawing.Point(3, 65);
            this.bunifuCustomLabel2.Name = "bunifuCustomLabel2";
            this.bunifuCustomLabel2.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.bunifuCustomLabel2.Size = new System.Drawing.Size(105, 25);
            this.bunifuCustomLabel2.TabIndex = 25;
            this.bunifuCustomLabel2.Text = "Grade Level:";
            this.bunifuCustomLabel2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // bunifuCustomLabel3
            // 
            this.bunifuCustomLabel3.AutoSize = true;
            this.bunifuCustomLabel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.bunifuCustomLabel3.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bunifuCustomLabel3.ForeColor = System.Drawing.Color.White;
            this.bunifuCustomLabel3.Location = new System.Drawing.Point(208, 65);
            this.bunifuCustomLabel3.Name = "bunifuCustomLabel3";
            this.bunifuCustomLabel3.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.bunifuCustomLabel3.Size = new System.Drawing.Size(67, 25);
            this.bunifuCustomLabel3.TabIndex = 26;
            this.bunifuCustomLabel3.Text = "Section:";
            this.bunifuCustomLabel3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // bunifuCustomLabel4
            // 
            this.bunifuCustomLabel4.AutoSize = true;
            this.bunifuCustomLabel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.bunifuCustomLabel4.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bunifuCustomLabel4.ForeColor = System.Drawing.Color.White;
            this.bunifuCustomLabel4.Location = new System.Drawing.Point(411, 65);
            this.bunifuCustomLabel4.Name = "bunifuCustomLabel4";
            this.bunifuCustomLabel4.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.bunifuCustomLabel4.Size = new System.Drawing.Size(64, 25);
            this.bunifuCustomLabel4.TabIndex = 27;
            this.bunifuCustomLabel4.Text = "Search:";
            this.bunifuCustomLabel4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // uc_AdminMasterlist
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.Controls.Add(this.txt_Search);
            this.Controls.Add(this.bunifuCustomLabel1);
            this.Controls.Add(this.cmb_SchoolYear);
            this.Controls.Add(this.cmb_Section);
            this.Controls.Add(this.cmb_GradeLevel);
            this.Controls.Add(this.bunifuCustomLabel2);
            this.Controls.Add(this.bunifuCustomLabel3);
            this.Controls.Add(this.bunifuCustomLabel4);
            this.Controls.Add(this.btn_Excel);
            this.Controls.Add(this.lbl_Masterlist);
            this.Controls.Add(this.btn_UploadMasterlist);
            this.Controls.Add(this.btn_MasterListTeachers);
            this.Controls.Add(this.btn_MasterListStudents);
            this.Controls.Add(this.dgv_Masterlist);
            this.Controls.Add(this.bunifuSeparator1);
            this.Name = "uc_AdminMasterlist";
            this.Size = new System.Drawing.Size(887, 514);
            this.Load += new System.EventHandler(this.uc_AdminMasterlist_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Masterlist)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private Bunifu.Framework.UI.BunifuFlatButton btn_MasterListTeachers;
        private Bunifu.Framework.UI.BunifuFlatButton btn_MasterListStudents;
        private Bunifu.Framework.UI.BunifuCustomDataGrid dgv_Masterlist;
        private Bunifu.Framework.UI.BunifuThinButton2 btn_UploadMasterlist;
        private Bunifu.Framework.UI.BunifuCustomLabel lbl_Masterlist;
        private Bunifu.Framework.UI.BunifuThinButton2 btn_Excel;
        private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator1;
        private Bunifu.Framework.UI.BunifuMetroTextbox txt_Search;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel1;
        private Bunifu.Framework.UI.BunifuDropdown cmb_SchoolYear;
        private Bunifu.Framework.UI.BunifuDropdown cmb_Section;
        private Bunifu.Framework.UI.BunifuDropdown cmb_GradeLevel;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel2;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel3;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel4;
    }
}
