﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FacultyEvaluationSystem
{
    public partial class uc_PanelTeacher : UserControl
    {
        string pangan;
        public int id;
        dataClass_FacultyEvaluationSystemDataContext db = new dataClass_FacultyEvaluationSystemDataContext();

        public uc_PanelTeacher(string name)
        {
            InitializeComponent();
            pangan = name;
        }

        private void uc_PanelTeacher_Load(object sender, EventArgs e)
        {
            lbl_Name.Text = pangan;
            if(db.sp_SelectPic(id).Any())
            {
                foreach(var asdf in db.sp_SelectPic(id))
                {
                    pictureBox1.ImageLocation = asdf.Picture;
                }
            }
        }
    }
}
