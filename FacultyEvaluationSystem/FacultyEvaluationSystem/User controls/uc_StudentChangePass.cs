﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FacultyEvaluationSystem
{
    public partial class uc_StudentChangePass : UserControl
    {
        public frm_Admin asdf;
        public string username;
        dataClass_FacultyEvaluationSystemDataContext db = new dataClass_FacultyEvaluationSystemDataContext();

        public uc_StudentChangePass()
        {
            InitializeComponent();
        }

        private void uc_StudentChangePass_Load(object sender, EventArgs e)
        {
            this.ActiveControl = lbl_ChangePassword;
        }
        //this code hides the password txt
        private void txt_CurrentPassword_Enter(object sender, EventArgs e)
        {
            txt_CurrentPassword.isPassword = true;
            txt_CurrentPassword.ForeColor = Color.White;
        }
        //this code hides the password txt
        private void txt_NewPassword_Enter(object sender, EventArgs e)
        {
            txt_NewPassword.isPassword = true;
            txt_NewPassword.ForeColor = Color.White;
        }
        //this code hides the password txt
        private void txt_ConfirmPassword_Enter(object sender, EventArgs e)
        {
            txt_ConfirmPassword.isPassword = true;
            txt_ConfirmPassword.ForeColor = Color.White;
        }

        private void btn_ChangePassword_Click(object sender, EventArgs e)
        {
            if(txt_ConfirmPassword.Text =="" || txt_CurrentPassword.Text==""||txt_NewPassword.Text =="")
            {
                MessageBox.Show("One or more inputs are empty.");
            }
            else if(txt_NewPassword.Text != txt_ConfirmPassword.Text)
            {
                MessageBox.Show("New password does not match.");
            }
            else
            {
                if(db.sp_Login(username,txt_CurrentPassword.Text).Any())
                {
                    db.sp_UpdateAccount_ByUsername(username, txt_Username.Text, txt_NewPassword.Text);
                    MessageBox.Show("Successfully changed account details.");
                    frm_Login frm = new frm_Login();
                    frm.Show();
                    asdf.Hide();
                }
                else
                {
                    MessageBox.Show("Incorrect password.");
                }
            }
        }
    }
}
