﻿namespace FacultyEvaluationSystem
{
    partial class uc_StudentChangePass
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.bunifuElipse2 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.lbl_ChangePassword = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.btn_ChangePassword = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuCustomLabel4 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel3 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel2 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.txt_ConfirmPassword = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.txt_NewPassword = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.txt_CurrentPassword = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.bunifuCustomLabel1 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.txt_Username = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.SuspendLayout();
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 5;
            this.bunifuElipse1.TargetControl = this;
            // 
            // bunifuElipse2
            // 
            this.bunifuElipse2.ElipseRadius = 5;
            this.bunifuElipse2.TargetControl = this;
            // 
            // lbl_ChangePassword
            // 
            this.lbl_ChangePassword.AutoSize = true;
            this.lbl_ChangePassword.BackColor = System.Drawing.Color.Transparent;
            this.lbl_ChangePassword.Font = new System.Drawing.Font("Century Gothic", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_ChangePassword.ForeColor = System.Drawing.Color.White;
            this.lbl_ChangePassword.Location = new System.Drawing.Point(25, 25);
            this.lbl_ChangePassword.Name = "lbl_ChangePassword";
            this.lbl_ChangePassword.Size = new System.Drawing.Size(217, 27);
            this.lbl_ChangePassword.TabIndex = 29;
            this.lbl_ChangePassword.Text = "Change Password";
            // 
            // btn_ChangePassword
            // 
            this.btn_ChangePassword.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(8)))), ((int)(((byte)(86)))), ((int)(((byte)(86)))));
            this.btn_ChangePassword.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btn_ChangePassword.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(89)))), ((int)(((byte)(131)))));
            this.btn_ChangePassword.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_ChangePassword.BorderRadius = 3;
            this.btn_ChangePassword.ButtonText = "Change";
            this.btn_ChangePassword.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_ChangePassword.DisabledColor = System.Drawing.Color.Gray;
            this.btn_ChangePassword.Iconcolor = System.Drawing.Color.Transparent;
            this.btn_ChangePassword.Iconimage = null;
            this.btn_ChangePassword.Iconimage_right = null;
            this.btn_ChangePassword.Iconimage_right_Selected = null;
            this.btn_ChangePassword.Iconimage_Selected = null;
            this.btn_ChangePassword.IconMarginLeft = 0;
            this.btn_ChangePassword.IconMarginRight = 0;
            this.btn_ChangePassword.IconRightVisible = true;
            this.btn_ChangePassword.IconRightZoom = 0D;
            this.btn_ChangePassword.IconVisible = false;
            this.btn_ChangePassword.IconZoom = 90D;
            this.btn_ChangePassword.IsTab = false;
            this.btn_ChangePassword.Location = new System.Drawing.Point(344, 442);
            this.btn_ChangePassword.Name = "btn_ChangePassword";
            this.btn_ChangePassword.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(89)))), ((int)(((byte)(131)))));
            this.btn_ChangePassword.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(142)))), ((int)(((byte)(176)))));
            this.btn_ChangePassword.OnHoverTextColor = System.Drawing.Color.White;
            this.btn_ChangePassword.selected = false;
            this.btn_ChangePassword.Size = new System.Drawing.Size(200, 50);
            this.btn_ChangePassword.TabIndex = 36;
            this.btn_ChangePassword.Text = "Change";
            this.btn_ChangePassword.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn_ChangePassword.Textcolor = System.Drawing.Color.White;
            this.btn_ChangePassword.TextFont = new System.Drawing.Font("Century Gothic", 10F);
            this.btn_ChangePassword.Click += new System.EventHandler(this.btn_ChangePassword_Click);
            // 
            // bunifuCustomLabel4
            // 
            this.bunifuCustomLabel4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.bunifuCustomLabel4.AutoSize = true;
            this.bunifuCustomLabel4.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel4.ForeColor = System.Drawing.Color.White;
            this.bunifuCustomLabel4.Location = new System.Drawing.Point(274, 345);
            this.bunifuCustomLabel4.Name = "bunifuCustomLabel4";
            this.bunifuCustomLabel4.Size = new System.Drawing.Size(187, 21);
            this.bunifuCustomLabel4.TabIndex = 34;
            this.bunifuCustomLabel4.Text = "Confirm New Password";
            // 
            // bunifuCustomLabel3
            // 
            this.bunifuCustomLabel3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.bunifuCustomLabel3.AutoSize = true;
            this.bunifuCustomLabel3.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel3.ForeColor = System.Drawing.Color.White;
            this.bunifuCustomLabel3.Location = new System.Drawing.Point(274, 248);
            this.bunifuCustomLabel3.Name = "bunifuCustomLabel3";
            this.bunifuCustomLabel3.Size = new System.Drawing.Size(122, 21);
            this.bunifuCustomLabel3.TabIndex = 35;
            this.bunifuCustomLabel3.Text = "New Password";
            // 
            // bunifuCustomLabel2
            // 
            this.bunifuCustomLabel2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.bunifuCustomLabel2.AutoSize = true;
            this.bunifuCustomLabel2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel2.ForeColor = System.Drawing.Color.White;
            this.bunifuCustomLabel2.Location = new System.Drawing.Point(274, 64);
            this.bunifuCustomLabel2.Name = "bunifuCustomLabel2";
            this.bunifuCustomLabel2.Size = new System.Drawing.Size(146, 21);
            this.bunifuCustomLabel2.TabIndex = 33;
            this.bunifuCustomLabel2.Text = "Current Password";
            // 
            // txt_ConfirmPassword
            // 
            this.txt_ConfirmPassword.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txt_ConfirmPassword.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_ConfirmPassword.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_ConfirmPassword.ForeColor = System.Drawing.Color.White;
            this.txt_ConfirmPassword.HintForeColor = System.Drawing.Color.Empty;
            this.txt_ConfirmPassword.HintText = "";
            this.txt_ConfirmPassword.isPassword = true;
            this.txt_ConfirmPassword.LineFocusedColor = System.Drawing.Color.LightGray;
            this.txt_ConfirmPassword.LineIdleColor = System.Drawing.Color.DarkGray;
            this.txt_ConfirmPassword.LineMouseHoverColor = System.Drawing.Color.Silver;
            this.txt_ConfirmPassword.LineThickness = 4;
            this.txt_ConfirmPassword.Location = new System.Drawing.Point(279, 367);
            this.txt_ConfirmPassword.Margin = new System.Windows.Forms.Padding(6);
            this.txt_ConfirmPassword.Name = "txt_ConfirmPassword";
            this.txt_ConfirmPassword.Size = new System.Drawing.Size(321, 47);
            this.txt_ConfirmPassword.TabIndex = 32;
            this.txt_ConfirmPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txt_ConfirmPassword.Enter += new System.EventHandler(this.txt_ConfirmPassword_Enter);
            // 
            // txt_NewPassword
            // 
            this.txt_NewPassword.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txt_NewPassword.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_NewPassword.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_NewPassword.ForeColor = System.Drawing.Color.White;
            this.txt_NewPassword.HintForeColor = System.Drawing.Color.Empty;
            this.txt_NewPassword.HintText = "";
            this.txt_NewPassword.isPassword = true;
            this.txt_NewPassword.LineFocusedColor = System.Drawing.Color.LightGray;
            this.txt_NewPassword.LineIdleColor = System.Drawing.Color.DarkGray;
            this.txt_NewPassword.LineMouseHoverColor = System.Drawing.Color.Silver;
            this.txt_NewPassword.LineThickness = 4;
            this.txt_NewPassword.Location = new System.Drawing.Point(279, 270);
            this.txt_NewPassword.Margin = new System.Windows.Forms.Padding(6);
            this.txt_NewPassword.Name = "txt_NewPassword";
            this.txt_NewPassword.Size = new System.Drawing.Size(321, 47);
            this.txt_NewPassword.TabIndex = 31;
            this.txt_NewPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txt_NewPassword.Enter += new System.EventHandler(this.txt_NewPassword_Enter);
            // 
            // txt_CurrentPassword
            // 
            this.txt_CurrentPassword.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txt_CurrentPassword.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_CurrentPassword.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_CurrentPassword.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txt_CurrentPassword.HintForeColor = System.Drawing.Color.Empty;
            this.txt_CurrentPassword.HintText = "";
            this.txt_CurrentPassword.isPassword = true;
            this.txt_CurrentPassword.LineFocusedColor = System.Drawing.Color.LightGray;
            this.txt_CurrentPassword.LineIdleColor = System.Drawing.Color.DarkGray;
            this.txt_CurrentPassword.LineMouseHoverColor = System.Drawing.Color.Silver;
            this.txt_CurrentPassword.LineThickness = 4;
            this.txt_CurrentPassword.Location = new System.Drawing.Point(279, 86);
            this.txt_CurrentPassword.Margin = new System.Windows.Forms.Padding(6);
            this.txt_CurrentPassword.Name = "txt_CurrentPassword";
            this.txt_CurrentPassword.Size = new System.Drawing.Size(321, 47);
            this.txt_CurrentPassword.TabIndex = 30;
            this.txt_CurrentPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txt_CurrentPassword.Enter += new System.EventHandler(this.txt_CurrentPassword_Enter);
            // 
            // bunifuCustomLabel1
            // 
            this.bunifuCustomLabel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.bunifuCustomLabel1.AutoSize = true;
            this.bunifuCustomLabel1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel1.ForeColor = System.Drawing.Color.White;
            this.bunifuCustomLabel1.Location = new System.Drawing.Point(274, 153);
            this.bunifuCustomLabel1.Name = "bunifuCustomLabel1";
            this.bunifuCustomLabel1.Size = new System.Drawing.Size(128, 21);
            this.bunifuCustomLabel1.TabIndex = 38;
            this.bunifuCustomLabel1.Text = "New Username";
            // 
            // txt_Username
            // 
            this.txt_Username.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txt_Username.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_Username.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Username.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txt_Username.HintForeColor = System.Drawing.Color.Empty;
            this.txt_Username.HintText = "";
            this.txt_Username.isPassword = false;
            this.txt_Username.LineFocusedColor = System.Drawing.Color.LightGray;
            this.txt_Username.LineIdleColor = System.Drawing.Color.DarkGray;
            this.txt_Username.LineMouseHoverColor = System.Drawing.Color.Silver;
            this.txt_Username.LineThickness = 4;
            this.txt_Username.Location = new System.Drawing.Point(279, 175);
            this.txt_Username.Margin = new System.Windows.Forms.Padding(6);
            this.txt_Username.Name = "txt_Username";
            this.txt_Username.Size = new System.Drawing.Size(321, 47);
            this.txt_Username.TabIndex = 37;
            this.txt_Username.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // uc_StudentChangePass
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.Controls.Add(this.bunifuCustomLabel1);
            this.Controls.Add(this.txt_Username);
            this.Controls.Add(this.lbl_ChangePassword);
            this.Controls.Add(this.btn_ChangePassword);
            this.Controls.Add(this.bunifuCustomLabel4);
            this.Controls.Add(this.bunifuCustomLabel3);
            this.Controls.Add(this.bunifuCustomLabel2);
            this.Controls.Add(this.txt_ConfirmPassword);
            this.Controls.Add(this.txt_NewPassword);
            this.Controls.Add(this.txt_CurrentPassword);
            this.Name = "uc_StudentChangePass";
            this.Size = new System.Drawing.Size(887, 514);
            this.Load += new System.EventHandler(this.uc_StudentChangePass_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private Bunifu.Framework.UI.BunifuCustomLabel lbl_ChangePassword;
        private Bunifu.Framework.UI.BunifuFlatButton btn_ChangePassword;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel4;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel3;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel2;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txt_ConfirmPassword;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txt_NewPassword;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txt_CurrentPassword;
        private Bunifu.Framework.UI.BunifuElipse bunifuElipse2;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel1;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txt_Username;
    }
}
