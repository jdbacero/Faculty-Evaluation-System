﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FacultyEvaluationSystem
{
    public partial class uc_AdminReports : UserControl
    {
        public uc_AdminReports()
        {
            InitializeComponent();
        }

        private void btn_EvaluationReport_Click(object sender, EventArgs e)
        {
            frm_EvaluationReports frm = new frm_EvaluationReports();
            frm.ShowDialog();
        }

        private void btn_Students_Click(object sender, EventArgs e)
        {
            frm_EvaluationStudentsStatus frm = new frm_EvaluationStudentsStatus();
            frm.ShowDialog();
        }
    }
}
