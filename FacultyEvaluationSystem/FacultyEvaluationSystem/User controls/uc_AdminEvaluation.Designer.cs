﻿namespace FacultyEvaluationSystem
{
    partial class uc_AdminEvaluation
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(uc_AdminEvaluation));
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.btn_StudentSchedule = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btn_QuestionPrincipal = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btn_QuestionStudent = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuCustomLabel1 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel2 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel3 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel4 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomDataGrid1 = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.btn_EvaluateTeacher = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btn_TeacherSchedule = new Bunifu.Framework.UI.BunifuFlatButton();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuCustomDataGrid1)).BeginInit();
            this.SuspendLayout();
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 5;
            this.bunifuElipse1.TargetControl = this;
            // 
            // btn_StudentSchedule
            // 
            this.btn_StudentSchedule.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(89)))), ((int)(((byte)(131)))));
            this.btn_StudentSchedule.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_StudentSchedule.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(89)))), ((int)(((byte)(131)))));
            this.btn_StudentSchedule.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_StudentSchedule.BorderRadius = 0;
            this.btn_StudentSchedule.ButtonText = "Students\' Evaluation Schedule";
            this.btn_StudentSchedule.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_StudentSchedule.DisabledColor = System.Drawing.Color.Gray;
            this.btn_StudentSchedule.Iconcolor = System.Drawing.Color.Transparent;
            this.btn_StudentSchedule.Iconimage = ((System.Drawing.Image)(resources.GetObject("btn_StudentSchedule.Iconimage")));
            this.btn_StudentSchedule.Iconimage_right = null;
            this.btn_StudentSchedule.Iconimage_right_Selected = null;
            this.btn_StudentSchedule.Iconimage_Selected = null;
            this.btn_StudentSchedule.IconMarginLeft = 0;
            this.btn_StudentSchedule.IconMarginRight = 0;
            this.btn_StudentSchedule.IconRightVisible = false;
            this.btn_StudentSchedule.IconRightZoom = 0D;
            this.btn_StudentSchedule.IconVisible = true;
            this.btn_StudentSchedule.IconZoom = 90D;
            this.btn_StudentSchedule.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btn_StudentSchedule.IsTab = false;
            this.btn_StudentSchedule.Location = new System.Drawing.Point(3, 70);
            this.btn_StudentSchedule.Name = "btn_StudentSchedule";
            this.btn_StudentSchedule.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(89)))), ((int)(((byte)(131)))));
            this.btn_StudentSchedule.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(142)))), ((int)(((byte)(176)))));
            this.btn_StudentSchedule.OnHoverTextColor = System.Drawing.Color.White;
            this.btn_StudentSchedule.selected = false;
            this.btn_StudentSchedule.Size = new System.Drawing.Size(881, 61);
            this.btn_StudentSchedule.TabIndex = 0;
            this.btn_StudentSchedule.Text = "Students\' Evaluation Schedule";
            this.btn_StudentSchedule.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn_StudentSchedule.Textcolor = System.Drawing.Color.White;
            this.btn_StudentSchedule.TextFont = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_StudentSchedule.Click += new System.EventHandler(this.btn_StudentSchedule_Click);
            // 
            // btn_QuestionPrincipal
            // 
            this.btn_QuestionPrincipal.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(89)))), ((int)(((byte)(131)))));
            this.btn_QuestionPrincipal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_QuestionPrincipal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(89)))), ((int)(((byte)(131)))));
            this.btn_QuestionPrincipal.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_QuestionPrincipal.BorderRadius = 0;
            this.btn_QuestionPrincipal.ButtonText = "Principal\'s Evaluation Questions";
            this.btn_QuestionPrincipal.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_QuestionPrincipal.DisabledColor = System.Drawing.Color.Gray;
            this.btn_QuestionPrincipal.Iconcolor = System.Drawing.Color.Transparent;
            this.btn_QuestionPrincipal.Iconimage = ((System.Drawing.Image)(resources.GetObject("btn_QuestionPrincipal.Iconimage")));
            this.btn_QuestionPrincipal.Iconimage_right = null;
            this.btn_QuestionPrincipal.Iconimage_right_Selected = null;
            this.btn_QuestionPrincipal.Iconimage_Selected = null;
            this.btn_QuestionPrincipal.IconMarginLeft = 0;
            this.btn_QuestionPrincipal.IconMarginRight = 0;
            this.btn_QuestionPrincipal.IconRightVisible = true;
            this.btn_QuestionPrincipal.IconRightZoom = 0D;
            this.btn_QuestionPrincipal.IconVisible = true;
            this.btn_QuestionPrincipal.IconZoom = 90D;
            this.btn_QuestionPrincipal.IsTab = false;
            this.btn_QuestionPrincipal.Location = new System.Drawing.Point(3, 137);
            this.btn_QuestionPrincipal.Name = "btn_QuestionPrincipal";
            this.btn_QuestionPrincipal.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(89)))), ((int)(((byte)(131)))));
            this.btn_QuestionPrincipal.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(142)))), ((int)(((byte)(176)))));
            this.btn_QuestionPrincipal.OnHoverTextColor = System.Drawing.Color.White;
            this.btn_QuestionPrincipal.selected = false;
            this.btn_QuestionPrincipal.Size = new System.Drawing.Size(881, 61);
            this.btn_QuestionPrincipal.TabIndex = 1;
            this.btn_QuestionPrincipal.Text = "Principal\'s Evaluation Questions";
            this.btn_QuestionPrincipal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn_QuestionPrincipal.Textcolor = System.Drawing.Color.White;
            this.btn_QuestionPrincipal.TextFont = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_QuestionPrincipal.Click += new System.EventHandler(this.btn_QuestionPrincipal_Click);
            // 
            // btn_QuestionStudent
            // 
            this.btn_QuestionStudent.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(89)))), ((int)(((byte)(131)))));
            this.btn_QuestionStudent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_QuestionStudent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(89)))), ((int)(((byte)(131)))));
            this.btn_QuestionStudent.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_QuestionStudent.BorderRadius = 0;
            this.btn_QuestionStudent.ButtonText = "Students\' Evaluation Questions";
            this.btn_QuestionStudent.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_QuestionStudent.DisabledColor = System.Drawing.Color.Gray;
            this.btn_QuestionStudent.Iconcolor = System.Drawing.Color.Transparent;
            this.btn_QuestionStudent.Iconimage = ((System.Drawing.Image)(resources.GetObject("btn_QuestionStudent.Iconimage")));
            this.btn_QuestionStudent.Iconimage_right = null;
            this.btn_QuestionStudent.Iconimage_right_Selected = null;
            this.btn_QuestionStudent.Iconimage_Selected = null;
            this.btn_QuestionStudent.IconMarginLeft = 0;
            this.btn_QuestionStudent.IconMarginRight = 0;
            this.btn_QuestionStudent.IconRightVisible = true;
            this.btn_QuestionStudent.IconRightZoom = 0D;
            this.btn_QuestionStudent.IconVisible = true;
            this.btn_QuestionStudent.IconZoom = 90D;
            this.btn_QuestionStudent.IsTab = false;
            this.btn_QuestionStudent.Location = new System.Drawing.Point(3, 3);
            this.btn_QuestionStudent.Name = "btn_QuestionStudent";
            this.btn_QuestionStudent.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(89)))), ((int)(((byte)(131)))));
            this.btn_QuestionStudent.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(142)))), ((int)(((byte)(176)))));
            this.btn_QuestionStudent.OnHoverTextColor = System.Drawing.Color.White;
            this.btn_QuestionStudent.selected = false;
            this.btn_QuestionStudent.Size = new System.Drawing.Size(881, 61);
            this.btn_QuestionStudent.TabIndex = 2;
            this.btn_QuestionStudent.Text = "Students\' Evaluation Questions";
            this.btn_QuestionStudent.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn_QuestionStudent.Textcolor = System.Drawing.Color.White;
            this.btn_QuestionStudent.TextFont = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_QuestionStudent.Click += new System.EventHandler(this.btn_QuestionStudent_Click);
            // 
            // bunifuCustomLabel1
            // 
            this.bunifuCustomLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuCustomLabel1.AutoSize = true;
            this.bunifuCustomLabel1.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bunifuCustomLabel1.ForeColor = System.Drawing.Color.White;
            this.bunifuCustomLabel1.Location = new System.Drawing.Point(4, 339);
            this.bunifuCustomLabel1.Name = "bunifuCustomLabel1";
            this.bunifuCustomLabel1.Size = new System.Drawing.Size(171, 22);
            this.bunifuCustomLabel1.TabIndex = 3;
            this.bunifuCustomLabel1.Text = "Evaluation status:";
            this.bunifuCustomLabel1.Visible = false;
            // 
            // bunifuCustomLabel2
            // 
            this.bunifuCustomLabel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuCustomLabel2.AutoSize = true;
            this.bunifuCustomLabel2.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bunifuCustomLabel2.ForeColor = System.Drawing.Color.White;
            this.bunifuCustomLabel2.Location = new System.Drawing.Point(182, 341);
            this.bunifuCustomLabel2.Name = "bunifuCustomLabel2";
            this.bunifuCustomLabel2.Size = new System.Drawing.Size(437, 20);
            this.bunifuCustomLabel2.TabIndex = 4;
            this.bunifuCustomLabel2.Text = "Ongoing/No evaluation scheduled/ x days until evaluation";
            this.bunifuCustomLabel2.Visible = false;
            // 
            // bunifuCustomLabel3
            // 
            this.bunifuCustomLabel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuCustomLabel3.AutoSize = true;
            this.bunifuCustomLabel3.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bunifuCustomLabel3.ForeColor = System.Drawing.Color.White;
            this.bunifuCustomLabel3.Location = new System.Drawing.Point(182, 363);
            this.bunifuCustomLabel3.Name = "bunifuCustomLabel3";
            this.bunifuCustomLabel3.Size = new System.Drawing.Size(339, 20);
            this.bunifuCustomLabel3.TabIndex = 5;
            this.bunifuCustomLabel3.Text = "x of x students have evaluated their teachers";
            this.bunifuCustomLabel3.Visible = false;
            // 
            // bunifuCustomLabel4
            // 
            this.bunifuCustomLabel4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuCustomLabel4.AutoSize = true;
            this.bunifuCustomLabel4.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bunifuCustomLabel4.ForeColor = System.Drawing.Color.White;
            this.bunifuCustomLabel4.Location = new System.Drawing.Point(182, 385);
            this.bunifuCustomLabel4.Name = "bunifuCustomLabel4";
            this.bunifuCustomLabel4.Size = new System.Drawing.Size(399, 20);
            this.bunifuCustomLabel4.TabIndex = 6;
            this.bunifuCustomLabel4.Text = "x of x teachers have been evaluated by the principal";
            this.bunifuCustomLabel4.Visible = false;
            // 
            // bunifuCustomDataGrid1
            // 
            this.bunifuCustomDataGrid1.AllowUserToAddRows = false;
            this.bunifuCustomDataGrid1.AllowUserToDeleteRows = false;
            this.bunifuCustomDataGrid1.AllowUserToResizeColumns = false;
            this.bunifuCustomDataGrid1.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bunifuCustomDataGrid1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.bunifuCustomDataGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuCustomDataGrid1.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.bunifuCustomDataGrid1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.bunifuCustomDataGrid1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.SeaGreen;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.SeaGreen;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.bunifuCustomDataGrid1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.bunifuCustomDataGrid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.bunifuCustomDataGrid1.DoubleBuffered = true;
            this.bunifuCustomDataGrid1.EnableHeadersVisualStyles = false;
            this.bunifuCustomDataGrid1.HeaderBgColor = System.Drawing.Color.SeaGreen;
            this.bunifuCustomDataGrid1.HeaderForeColor = System.Drawing.Color.SeaGreen;
            this.bunifuCustomDataGrid1.Location = new System.Drawing.Point(8, 416);
            this.bunifuCustomDataGrid1.MultiSelect = false;
            this.bunifuCustomDataGrid1.Name = "bunifuCustomDataGrid1";
            this.bunifuCustomDataGrid1.ReadOnly = true;
            this.bunifuCustomDataGrid1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.bunifuCustomDataGrid1.RowHeadersVisible = false;
            this.bunifuCustomDataGrid1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.bunifuCustomDataGrid1.Size = new System.Drawing.Size(869, 90);
            this.bunifuCustomDataGrid1.TabIndex = 7;
            this.bunifuCustomDataGrid1.Visible = false;
            // 
            // btn_EvaluateTeacher
            // 
            this.btn_EvaluateTeacher.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(89)))), ((int)(((byte)(131)))));
            this.btn_EvaluateTeacher.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_EvaluateTeacher.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(89)))), ((int)(((byte)(131)))));
            this.btn_EvaluateTeacher.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_EvaluateTeacher.BorderRadius = 0;
            this.btn_EvaluateTeacher.ButtonText = "Evaluate Teachers";
            this.btn_EvaluateTeacher.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_EvaluateTeacher.DisabledColor = System.Drawing.Color.Gray;
            this.btn_EvaluateTeacher.Iconcolor = System.Drawing.Color.Transparent;
            this.btn_EvaluateTeacher.Iconimage = ((System.Drawing.Image)(resources.GetObject("btn_EvaluateTeacher.Iconimage")));
            this.btn_EvaluateTeacher.Iconimage_right = null;
            this.btn_EvaluateTeacher.Iconimage_right_Selected = null;
            this.btn_EvaluateTeacher.Iconimage_Selected = null;
            this.btn_EvaluateTeacher.IconMarginLeft = 0;
            this.btn_EvaluateTeacher.IconMarginRight = 0;
            this.btn_EvaluateTeacher.IconRightVisible = true;
            this.btn_EvaluateTeacher.IconRightZoom = 0D;
            this.btn_EvaluateTeacher.IconVisible = true;
            this.btn_EvaluateTeacher.IconZoom = 90D;
            this.btn_EvaluateTeacher.IsTab = false;
            this.btn_EvaluateTeacher.Location = new System.Drawing.Point(3, 271);
            this.btn_EvaluateTeacher.Name = "btn_EvaluateTeacher";
            this.btn_EvaluateTeacher.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(89)))), ((int)(((byte)(131)))));
            this.btn_EvaluateTeacher.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(142)))), ((int)(((byte)(176)))));
            this.btn_EvaluateTeacher.OnHoverTextColor = System.Drawing.Color.White;
            this.btn_EvaluateTeacher.selected = false;
            this.btn_EvaluateTeacher.Size = new System.Drawing.Size(881, 61);
            this.btn_EvaluateTeacher.TabIndex = 8;
            this.btn_EvaluateTeacher.Text = "Evaluate Teachers";
            this.btn_EvaluateTeacher.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn_EvaluateTeacher.Textcolor = System.Drawing.Color.White;
            this.btn_EvaluateTeacher.TextFont = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_EvaluateTeacher.Click += new System.EventHandler(this.btn_EvaluateTeacher_Click);
            // 
            // btn_TeacherSchedule
            // 
            this.btn_TeacherSchedule.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(89)))), ((int)(((byte)(131)))));
            this.btn_TeacherSchedule.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_TeacherSchedule.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(89)))), ((int)(((byte)(131)))));
            this.btn_TeacherSchedule.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_TeacherSchedule.BorderRadius = 0;
            this.btn_TeacherSchedule.ButtonText = "Principal\'s Evaluation Schedule ";
            this.btn_TeacherSchedule.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_TeacherSchedule.DisabledColor = System.Drawing.Color.Gray;
            this.btn_TeacherSchedule.Iconcolor = System.Drawing.Color.Transparent;
            this.btn_TeacherSchedule.Iconimage = ((System.Drawing.Image)(resources.GetObject("btn_TeacherSchedule.Iconimage")));
            this.btn_TeacherSchedule.Iconimage_right = null;
            this.btn_TeacherSchedule.Iconimage_right_Selected = null;
            this.btn_TeacherSchedule.Iconimage_Selected = null;
            this.btn_TeacherSchedule.IconMarginLeft = 0;
            this.btn_TeacherSchedule.IconMarginRight = 0;
            this.btn_TeacherSchedule.IconRightVisible = false;
            this.btn_TeacherSchedule.IconRightZoom = 0D;
            this.btn_TeacherSchedule.IconVisible = true;
            this.btn_TeacherSchedule.IconZoom = 90D;
            this.btn_TeacherSchedule.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btn_TeacherSchedule.IsTab = false;
            this.btn_TeacherSchedule.Location = new System.Drawing.Point(3, 204);
            this.btn_TeacherSchedule.Name = "btn_TeacherSchedule";
            this.btn_TeacherSchedule.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(89)))), ((int)(((byte)(131)))));
            this.btn_TeacherSchedule.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(142)))), ((int)(((byte)(176)))));
            this.btn_TeacherSchedule.OnHoverTextColor = System.Drawing.Color.White;
            this.btn_TeacherSchedule.selected = false;
            this.btn_TeacherSchedule.Size = new System.Drawing.Size(881, 61);
            this.btn_TeacherSchedule.TabIndex = 9;
            this.btn_TeacherSchedule.Text = "Principal\'s Evaluation Schedule ";
            this.btn_TeacherSchedule.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn_TeacherSchedule.Textcolor = System.Drawing.Color.White;
            this.btn_TeacherSchedule.TextFont = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TeacherSchedule.Click += new System.EventHandler(this.btn_PrincipalSchedule_Click);
            // 
            // uc_AdminEvaluation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.Controls.Add(this.btn_TeacherSchedule);
            this.Controls.Add(this.btn_EvaluateTeacher);
            this.Controls.Add(this.bunifuCustomDataGrid1);
            this.Controls.Add(this.bunifuCustomLabel4);
            this.Controls.Add(this.bunifuCustomLabel3);
            this.Controls.Add(this.bunifuCustomLabel2);
            this.Controls.Add(this.bunifuCustomLabel1);
            this.Controls.Add(this.btn_QuestionStudent);
            this.Controls.Add(this.btn_QuestionPrincipal);
            this.Controls.Add(this.btn_StudentSchedule);
            this.Name = "uc_AdminEvaluation";
            this.Size = new System.Drawing.Size(887, 514);
            this.Load += new System.EventHandler(this.uc_AdminEvaluation_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bunifuCustomDataGrid1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private Bunifu.Framework.UI.BunifuFlatButton btn_StudentSchedule;
        private Bunifu.Framework.UI.BunifuFlatButton btn_QuestionStudent;
        private Bunifu.Framework.UI.BunifuFlatButton btn_QuestionPrincipal;
        private Bunifu.Framework.UI.BunifuCustomDataGrid bunifuCustomDataGrid1;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel4;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel3;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel2;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel1;
        private Bunifu.Framework.UI.BunifuFlatButton btn_EvaluateTeacher;
        private Bunifu.Framework.UI.BunifuFlatButton btn_TeacherSchedule;
    }
}
