﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FacultyEvaluationSystem
{
    public partial class uc_AdminMasterlist : UserControl
    {
        dataClass_FacultyEvaluationSystemDataContext db = new dataClass_FacultyEvaluationSystemDataContext(); //instantiate dataclass
        cls_Excel Excel = new cls_Excel(); //excell class for opening/uploading excel
        string uploadMasterlist; //variable that determines what the user wants to upload
        DataTable data = new DataTable();

        public uc_AdminMasterlist()
        {
            InitializeComponent();
        }

        private void btn_MasterListStudents_Click(object sender, EventArgs e)
        {
            txt_Search.Text = "";
            uploadMasterlist = "Student";
            lbl_Masterlist.Text = "Students\nMasterlist";
            lbl_Masterlist.Visible = true;
            btn_UploadMasterlist.Visible = true;
            btn_Excel.Visible = true;
            btn_UploadMasterlist.ButtonText = "Upload Students Masterlist";
            uploadMasterlist = "Student";
            cmb_GradeLevel.Enabled = true;
            txt_Search.Enabled = true;
            //inig click sa student ky mo enable ang school year
            cmb_SchoolYear.Enabled = true;
            cmb_SchoolYear.selectedIndex = 0;
            cmb_GradeLevel.selectedIndex = 0;

            //cmb_GradeLevel.Cursor = Cursors.Hand;
            //txt_Search.Cursor = Cursors.IBeam;
            this.ActiveControl = lbl_Masterlist;
            dgv_Masterlist.DataSource = db.sp_ViewStudents();
            dgv_Masterlist.ClearSelection();
        }

        private void btn_MasterListTeachers_Click(object sender, EventArgs e)
        {
            cmb_GradeLevel.Enabled = true;
            txt_Search.Text = "";
            uploadMasterlist = "Teacher";
            lbl_Masterlist.Text = "Teachers\nMasterlist";
            lbl_Masterlist.Visible = true;
            btn_Excel.Visible = true;
            btn_UploadMasterlist.Visible = true;
            txt_Search.Enabled = true;
            btn_UploadMasterlist.ButtonText = "Upload Teachers Masterlist";
            uploadMasterlist = "Teacher";
            cmb_GradeLevel.selectedIndex = 0;
            //cmb_GradeLevel.Cursor = Cursors.Hand;
            //txt_Search.Cursor = Cursors.IBeam;
            this.ActiveControl = lbl_Masterlist;
            dgv_Masterlist.DataSource = db.sp_ViewTeachers();
            dgv_Masterlist.ClearSelection();
        }

        private void btn_Excel_Click(object sender, EventArgs e)
        {
            string[] columnHeader = null;
            switch (uploadMasterlist)
            {
                case "Teacher":
                    columnHeader = new string[] { "LastName", "FirstName"};
                    break;
                case "Student":
                    //added Gradelevel and Section
                    columnHeader = new string[] { "StudentNo", "LastName", "FirstName", "MiddleInitial", "GradeLevel", "Section" };
                    break;
                default:
                    MessageBox.Show("Error.");
                    break;
            }
            
            Excel.openExcel(columnHeader);
        }

        private void btn_UploadMasterlist_Click(object sender, EventArgs e)
        {
            frm_Admin_PromptSchoolYear frmSchoolYear = new frm_Admin_PromptSchoolYear();
            frmSchoolYear.ShowDialog();
            if (frmSchoolYear.set == false) { MessageBox.Show("No school year selected."); return; }
            Excel.uploadExcel(dgv_Masterlist);
            if (Excel.checkFilepath() == false) return;
            DialogResult dialog = MessageBox.Show("Import this excel file in the database?", "Confirmation", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (dialog == DialogResult.OK)
            {
                string[] columnHeader = null;
                switch (uploadMasterlist)
                {
                    case "Teacher":
                        columnHeader = new string[]{ "LastName", "FirstName"};
                        if (Excel.checker(columnHeader, dgv_Masterlist) == true)
                        {
                            for (int i = 0; i < dgv_Masterlist.RowCount; i++)
                            {
                                db.sp_InsertTeachers(dgv_Masterlist.Rows[i].Cells["FirstName"].Value.ToString(), dgv_Masterlist.Rows[i].Cells["LastName"].Value.ToString(), frmSchoolYear.schoolyear);
                                //string[] subjects = dgv_Masterlist.Rows[i].Cells["Subject"].Value.ToString().Split(',').ToArray();
                                //foreach (string sabjek in subjects)
                                //{
                                //    db.sp_InsertTeachersHandledSection(db.sp_ReturnTeacherID(dgv_Masterlist.Rows[i].Cells["FirstName"].Value.ToString(), dgv_Masterlist.Rows[i].Cells["LastName"].Value.ToString()),
                                //        dgv_Masterlist.Rows[i].Cells["Section"].Value.ToString(), sabjek);
                                //}
                            }
                            MessageBox.Show("Successfully uploaded teacher's masterlist.");
                            dgv_Masterlist.DataSource = db.sp_ViewTeachers();
                            break;
                        }
                        else
                        {
                            break;
                        }
                        

                    case "Student":
                        //Removed selectsection option
                        //frm_SelectSection frm_Section = new frm_SelectSection();
                        //frm_Section.ShowDialog();
                        //added gradelvl & section

                        columnHeader = new string[] { "StudentNo", "LastName", "FirstName", "MiddleInitial" ,"GradeLevel", "Section"};
                        if(Excel.checker(columnHeader, dgv_Masterlist)==true)
                        {
                            //GET DISTINCT DATA OF SECTIONS INTO A LIST
                            List<string> listSection = dgv_Masterlist.Rows.Cast<DataGridViewRow>()
                                            .Select(data => data.Cells["Section"].Value.ToString())
                                            .Distinct()
                                            .ToList();

                            foreach (var entry in listSection)
                            {
                                //Get gradelevel of the sections in listsection
                                string gradeLevel = dgv_Masterlist.Rows.Cast<DataGridViewRow>()
                                            .Where(section => section.Cells["Section"].Value.ToString() == entry)
                                            .Select(data => data.Cells["GradeLevel"].Value.ToString())
                                            .First();

                                ///////////////////////////////////
                                IEnumerable<tbl_Section> checkSection = db.ExecuteQuery<tbl_Section>
                                ("SELECT * FROM tbl_Section WHERE Section = {0}"
                                    , entry);

                                if (checkSection.Any() == false)
                                {
                                    //insert section
                                    db.sp_InsertSection(entry, gradeLevel);
                                }
                            }

                            IEnumerable<tbl_UserAccount> checkExistingAcc;

                            for (int i = 0; i < dgv_Masterlist.RowCount; i++)
                            {
                                //checks for existing account
                                checkExistingAcc = db.ExecuteQuery<tbl_UserAccount>
                                ("SELECT * FROM tbl_UserAccounts WHERE Username = {0}"
                                    , dgv_Masterlist.Rows[i].Cells["StudentNo"].Value.ToString());
                                if(checkExistingAcc.Any()==false)
                                {
                                    db.sp_InsertAccount(dgv_Masterlist.Rows[i].Cells["StudentNo"].Value.ToString(), dgv_Masterlist.Rows[i].Cells["LastName"].Value.ToString(), "Student");
                                }
                                
                                db.sp_InsertStudents(dgv_Masterlist.Rows[i].Cells["StudentNo"].Value.ToString(), dgv_Masterlist.Rows[i].Cells["FirstName"].Value.ToString(), 
                                                        dgv_Masterlist.Rows[i].Cells["MiddleInitial"].Value.ToString(), dgv_Masterlist.Rows[i].Cells["LastName"].Value.ToString(),
                                                            dgv_Masterlist.Rows[i].Cells["StudentNo"].Value.ToString(), 
                                                                 dgv_Masterlist.Rows[i].Cells["GradeLevel"].Value.ToString(), dgv_Masterlist.Rows[i].Cells["Section"].Value.ToString(),
                                                                 frmSchoolYear.schoolyear);
                                                                    //frm_Section.grade_level, frm_Section.section);
                            }

                            MessageBox.Show("Successfully uploaded students' masterlist.");
                            dgv_Masterlist.DataSource = db.sp_ViewStudents();
                            break;
                        }
                        else
                        {
                            break;
                        }
                }
            }
            else
            {
                return;
            }

        }

        private void cmb_GradeLevel_onItemSelected(object sender, EventArgs e)
        {
                       
        }

        private void cmb_Section_onItemSelected(object sender, EventArgs e)
        {
           
        }

        private void dgv_Masterlist_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (uploadMasterlist == "Teacher")
            {
                frm_Admin_Teacher frm = new frm_Admin_Teacher();
                frm.teacherID = db.sp_ReturnTeacherID(dgv_Masterlist.CurrentRow.Cells["First_Name"].Value.ToString(), dgv_Masterlist.CurrentRow.Cells["Last_Name"].Value.ToString());
                frm.fullName = dgv_Masterlist.CurrentRow.Cells["First_Name"].Value.ToString() + " " + dgv_Masterlist.CurrentRow.Cells["Last_Name"].Value.ToString();
                frm.ShowDialog();
            }
        }

        private void txt_Search_OnTextChange(object sender, EventArgs e)
        {
            switch(uploadMasterlist)
            {
                case "Student":
                    switch(cmb_GradeLevel.selectedValue.ToString())
                    {
                        case "All":
                            dgv_Masterlist.DataSource = db.sp_SearchAllStudents(txt_Search.Text);
                            dgv_Masterlist.ClearSelection();
                            break;

                        default:
                            if(cmb_Section.selectedIndex <= 0)
                            {
                                dgv_Masterlist.DataSource = db.sp_SearchAllStudentsByGradeLevel(txt_Search.Text, cmb_GradeLevel.selectedValue.ToString());
                                dgv_Masterlist.ClearSelection();
                                break;
                            }
                            else
                            {
                                dgv_Masterlist.DataSource = db.sp_SearchAllStudentsBySection(txt_Search.Text, cmb_Section.selectedValue.ToString());
                                dgv_Masterlist.ClearSelection();
                                break;
                            }
                    }
                    break;

                default:
                    switch (cmb_GradeLevel.selectedValue.ToString())
                    {
                        case "All":
                            dgv_Masterlist.DataSource = db.sp_SearchAllTeachers(txt_Search.Text);
                            dgv_Masterlist.ClearSelection();
                            break;

                        default:
                            if (cmb_Section.selectedIndex <= 0)
                            {
                                dgv_Masterlist.DataSource = db.sp_SearchTeachersByGradeLevel(cmb_GradeLevel.selectedValue.ToString(), txt_Search.Text);
                                dgv_Masterlist.ClearSelection();
                                break;
                            }
                            else
                            {
                                dgv_Masterlist.DataSource = db.sp_SearchTeachersBySection(cmb_Section.selectedValue.ToString(), txt_Search.Text);
                                dgv_Masterlist.ClearSelection();
                                break;
                            }
                    }
                    break;
            }
        }

        private void uc_AdminMasterlist_Load(object sender, EventArgs e)
        {
            //bunifuedddd
            cmb_SchoolYear.AddItem("All");
            //cmb_SchoolYear.Items.Add("All");
            for (int x = DateTime.Today.Year - 5; x <= DateTime.Today.Year + 1; x++)
            {
                cmb_SchoolYear.AddItem(x + " - " + (x+1));
            }
        }

        private void cmb_SchoolYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void cmb_SchoolYear_onItemSelected(object sender, EventArgs e)
        {
            if (cmb_GradeLevel.selectedValue.ToString() == "All")
            {
                if (cmb_SchoolYear.selectedValue.ToString() == "All")
                {
                    dgv_Masterlist.DataSource = db.sp_ViewStudents();
                    cmb_Section.Enabled = false;
                    dgv_Masterlist.ClearSelection();
                }
                else
                {
                    dgv_Masterlist.DataSource = db.sp_ViewStudentsBySchoolYear(cmb_SchoolYear.selectedValue.ToString());
                    dgv_Masterlist.ClearSelection();
                }
            }
            else
            {
                if (cmb_Section.selectedIndex <= 0)
                {
                    dgv_Masterlist.DataSource = db.sp_ViewStudentsByGradeLevel_SchoolYear(cmb_SchoolYear.selectedValue.ToString(), cmb_GradeLevel.selectedValue.ToString());
                }
                else
                {
                    dgv_Masterlist.DataSource = db.sp_ViewStudentsByGradeLevel_Section_SchoolYear(cmb_SchoolYear.selectedValue.ToString(), cmb_GradeLevel.selectedValue.ToString(), cmb_Section.selectedValue.ToString());
                }
                dgv_Masterlist.DataSource = db.sp_ViewStudentsByGradeLevel(cmb_GradeLevel.selectedValue.ToString());
                cmb_Section.Enabled = true;
                dgv_Masterlist.ClearSelection();
            }
        }

        private void cmb_GradeLevel_onItemSelected_1(object sender, EventArgs e)
        {
            cmb_Section.Enabled = true;
            cmb_Section.Clear();
            cmb_SchoolYear.Enabled = true;
            cmb_SchoolYear.selectedIndex = 0;

            switch (uploadMasterlist)
            {
                case "Student":
                    switch (cmb_GradeLevel.selectedValue.ToString())
                    {
                        case "All":
                            dgv_Masterlist.DataSource = db.sp_ViewStudents();
                            cmb_Section.Enabled = false;
                            dgv_Masterlist.ClearSelection();
                            break;

                        default:
                            dgv_Masterlist.DataSource = db.sp_ViewStudentsByGradeLevel(cmb_GradeLevel.selectedValue.ToString());
                            List<string> listSections = new List<string>();
                            listSections.Add("All");
                            foreach (var result in db.sp_ViewSectionByGradeLevel(cmb_GradeLevel.selectedValue.ToString()))
                            {
                                listSections.Add(result.Section);
                            }
                            cmb_Section.Enabled = true;
                            cmb_Section.Items = listSections.ToArray();
                            cmb_Section.selectedIndex = 0;
                            dgv_Masterlist.ClearSelection();
                            //cmb_Section.Cursor = Cursors.Hand;
                            break;
                    }
                    data = dgv_Masterlist.DataSource as DataTable;
                    break;

                case "Teacher":
                    switch (cmb_GradeLevel.selectedValue.ToString())
                    {
                        case "All":
                            dgv_Masterlist.DataSource = db.sp_ViewTeachers();
                            cmb_Section.Enabled = false;
                            dgv_Masterlist.ClearSelection();
                            break;

                        default:
                            dgv_Masterlist.DataSource = db.sp_ViewTeachersByGradeLevel(cmb_GradeLevel.selectedValue.ToString());
                            List<string> listSections = new List<string>();
                            listSections.Add("All");
                            foreach (var result in db.sp_ViewSectionByGradeLevel(cmb_GradeLevel.selectedValue.ToString()))
                            {
                                listSections.Add(result.Section);
                            }
                            cmb_Section.Enabled = true;
                            cmb_Section.Items = listSections.ToArray();
                            cmb_Section.selectedIndex = 0;
                            dgv_Masterlist.ClearSelection();
                            //cmb_Section.Cursor = Cursors.Hand;
                            break;
                    }
                    break;

                default:
                    MessageBox.Show("Error.");
                    break;
            }
        }

        private void cmb_Section_onItemSelected_1(object sender, EventArgs e)
        {
            cmb_SchoolYear.selectedIndex = 0;
            switch (uploadMasterlist)
            {
                case "Student":
                    switch (cmb_Section.selectedIndex)
                    {
                        case 0:
                            dgv_Masterlist.DataSource = db.sp_ViewStudentsByGradeLevel(cmb_GradeLevel.selectedValue.ToString());
                            dgv_Masterlist.ClearSelection();
                            break;

                        default:
                            dgv_Masterlist.DataSource = db.sp_ViewStudentsBySection(cmb_Section.selectedValue.ToString());
                            dgv_Masterlist.ClearSelection();
                            break;
                    }
                    break;

                case "Teacher":
                    switch (cmb_Section.selectedIndex)
                    {
                        case -1:
                            dgv_Masterlist.DataSource = db.sp_ViewTeachersByGradeLevel(cmb_GradeLevel.selectedValue.ToString());
                            dgv_Masterlist.ClearSelection();
                            break;

                        case 0:
                            dgv_Masterlist.DataSource = db.sp_ViewTeachersByGradeLevel(cmb_GradeLevel.selectedValue.ToString());
                            dgv_Masterlist.ClearSelection();
                            break;

                        default:
                            dgv_Masterlist.DataSource = db.sp_ViewTeachersBySection(cmb_Section.selectedValue.ToString());
                            dgv_Masterlist.ClearSelection();
                            break;
                    }
                    break;

                default:
                    MessageBox.Show("Error.");
                    break;
            }
        }


        private void cmb_SchoolYear_onItemSelected_1(object sender, EventArgs e)
        {
            if (cmb_GradeLevel.selectedValue.ToString() == "All")
            {
                if (cmb_SchoolYear.selectedValue.ToString() == "All")
                {
                    dgv_Masterlist.DataSource = db.sp_ViewStudents();
                    cmb_Section.Enabled = false;
                    dgv_Masterlist.ClearSelection();
                }
                else
                {
                    dgv_Masterlist.DataSource = db.sp_ViewStudentsBySchoolYear(cmb_SchoolYear.selectedValue.ToString());
                    dgv_Masterlist.ClearSelection();
                }
            }
            else
            {
                if (cmb_Section.selectedIndex <= 0)
                {
                    dgv_Masterlist.DataSource = db.sp_ViewStudentsByGradeLevel_SchoolYear(cmb_SchoolYear.selectedValue.ToString(), cmb_GradeLevel.selectedValue.ToString());
                }
                else
                {
                    dgv_Masterlist.DataSource = db.sp_ViewStudentsByGradeLevel_Section_SchoolYear(cmb_SchoolYear.selectedValue.ToString(), cmb_GradeLevel.selectedValue.ToString(), cmb_Section.selectedValue.ToString());
                }
                dgv_Masterlist.DataSource = db.sp_ViewStudentsByGradeLevel(cmb_GradeLevel.selectedValue.ToString());
                cmb_Section.Enabled = true;
                dgv_Masterlist.ClearSelection();
            }
        }
    }
}