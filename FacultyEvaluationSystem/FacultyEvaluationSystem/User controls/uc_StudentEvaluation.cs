﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FacultyEvaluationSystem
{
    public partial class uc_StudentEvaluation : UserControl
    {
        public frm_Student asdf;
        //stud eval id
        public int id;
        //List<string> Teachers = new List<string>();
        public List<uc_PanelTeacher> uc_Panel;
        //setting questionnaires col 0 - id     col 1 - question        col 2 - answer      col 3 - teacher id
        public string[,] questionnaire;
        public int id_sched;
        public string id_stud;
        dataClass_FacultyEvaluationSystemDataContext db = new dataClass_FacultyEvaluationSystemDataContext();
        //EXPERIMENTAL: FIRST COL = TEACHER ID, SECOND COL = QUESTION ID, 
        //int[][] answer = new int[3][];
        int ctr = 0;
        List<cls_studEvaluation> listStudAnswer = new List<cls_studEvaluation>();

        public uc_StudentEvaluation()
        {
            InitializeComponent();
        }

        private void bunifuDropdown1_onItemSelected(object sender, EventArgs e)
        {

        }

        private void uc_StudentEvaluation_Load(object sender, EventArgs e)
        {
            //the 2 codes below are the reason nganong naay mo prompt na error sa design sa studentfrm
            // butangananSaPangutana.Text= $"{ctr+1}). {questionnaire[ctr, 1]}";
            //the code below is just daniel ver cuz his VS ver is 2012
             butangananSaPangutana.Text= (ctr + 1) + ".)" + questionnaire[ctr, 1];
            //answer[0] = new int[uc_Panel.Count()];
            //for (int i = 0; i < uc_Panel.Count(); i++)
            //{
            //    answer[0][i] = uc_Panel[i].id;
            //}
        }

        private void dosomething()
        {
            //List<string> Teachers = new List<string>();
            //Teachers.Add("Jorex");
            //Teachers.Add("Klyde");
            //Teachers.Add("Clyde");
            //List<uc_PanelTeacher> list = new List<uc_PanelTeacher>();

            //int locationX = 0;
            //int ctr = 0;
            //foreach (var items in Teachers)
            //{
            //    list.Add(new uc_PanelTeacher());
            //    list[ctr].Location = new Point(locationX, 0);
            //    list[ctr].Parent = panel1;
            //    list[ctr].Show();
            //    ctr++;
            //    locationX += 500;
            //}
        }

        private void btn_Next_Click(object sender, EventArgs e)
        {
            lbl_Catergory.Focus();
            if (ctr == questionnaire.GetLength(0) - 1)
            {
                return;
            }
            if (ctr + 1 < questionnaire.GetLength(0))
            {
                recordAnswers();
                ctr++;
                nextPrev();
                // butangananSaPangutana.Text= $"{ctr+1}). {questionnaire[ctr, 1]}";
                 butangananSaPangutana.Text= ctr+1 + ".)" + questionnaire[ctr, 1];
            }
        }

        private void btn_Previous_Click(object sender, EventArgs e)
        {
            lbl_Catergory.Focus();
            if (ctr == 0)
            {
                return;
            }
            if (ctr > 0)
            {
                recordAnswers();
                ctr--;
                nextPrev();
                // butangananSaPangutana.Text= $"{ctr + 1}). {questionnaire[ctr, 1]}";
                 butangananSaPangutana.Text= ctr+1 + ".)" + questionnaire[ctr, 1];
            }
        }


        private void addToList()
        {

        }

        //This is to record the answers of the students and store it along with the teachers ID
        private void recordAnswers()
        {
            foreach (var item in uc_Panel)
            {
                int _answer;
                //if wala pay sulod ang list, mo add siya ditso
                if (listStudAnswer.Count() == 0)
                {
                    if (item.rdo_StronglyAgree.Checked)
                    {
                        _answer = 4;
                    }
                    else if (item.rdo_Agree.Checked)
                    {
                        _answer = 3;
                    }
                    else if (item.rdo_Disagree.Checked)
                    {
                        _answer = 2;
                    }
                    else if (item.rdo_StronglyDisagree.Checked)
                    {
                        _answer = 1;
                    }
                    else
                    {
                        _answer = 0;
                    }

                    listStudAnswer.Add(new cls_studEvaluation
                    {
                        id_Teacher = item.id,
                        id_Question = int.Parse(questionnaire[ctr, 0]),
                        answer = _answer
                    });
                }
                else
                {
                    bool verify = false;
                    //verify if na anseran na ba (nag exist na ba sa list). if naa, i-update ang nag exist na sa list
                    foreach (var items in listStudAnswer)
                    {
                        if (item.id == items.id_Teacher && items.id_Question == int.Parse(questionnaire[ctr, 0]))
                        {
                            if (item.rdo_StronglyAgree.Checked)
                            {
                                _answer = 4;
                            }
                            else if (item.rdo_Agree.Checked)
                            {
                                _answer = 3;
                            }
                            else if (item.rdo_Disagree.Checked)
                            {
                                _answer = 2;
                            }
                            else if (item.rdo_StronglyDisagree.Checked)
                            {
                                _answer = 1;
                            }
                            else
                            {
                                _answer = 0;
                            }
                            items.answer = _answer;
                            verify = true;
                        }
                    }

                    if(verify == false)
                    {
                        if (item.rdo_StronglyAgree.Checked)
                        {
                            _answer = 4;
                        }
                        else if (item.rdo_Agree.Checked)
                        {
                            _answer = 3;
                        }
                        else if (item.rdo_Disagree.Checked)
                        {
                            _answer = 2;
                        }
                        else if (item.rdo_StronglyDisagree.Checked)
                        {
                            _answer = 1;
                        }
                        else
                        {
                            _answer = 0;
                        }

                        listStudAnswer.Add(new cls_studEvaluation
                        {
                            id_Teacher = item.id,
                            id_Question = int.Parse(questionnaire[ctr, 0]),
                            answer = _answer
                        });
                    }
                }
            }

            //foreach (var item in uc_Panel)
            //{
            //    questionnaire[ctr, 3] = item.id + "";

            //    if (item.rdo_StronglyAgree.Checked)
            //    {
            //        questionnaire[ctr, 2] = 4+"";
            //    }
            //    else if (item.rdo_Agree.Checked)
            //    {
            //        questionnaire[ctr, 2] = 3 + "";
            //    }
            //    else if (item.rdo_Disagree.Checked)
            //    {
            //        questionnaire[ctr, 2] = 2 + "";
            //    }
            //    else if (item.rdo_StronglyDisagree.Checked)
            //    {
            //        questionnaire[ctr, 2] = 1 + "";
            //    }
            //    else
            //    {
            //        questionnaire[ctr, 2] = 0 + "";
            //    }
            //}
        }

        private void nextPrev()
        {
            foreach (var item in uc_Panel)
            {
                item.rdo_StronglyAgree.Checked = false;
                item.rdo_Agree.Checked = false;
                item.rdo_Disagree.Checked = false;
                item.rdo_StronglyDisagree.Checked = false;
                
                foreach(var items in listStudAnswer)
                {
                    if(items.id_Teacher == item.id && int.Parse(questionnaire[ctr,0]) == items.id_Question)
                    {
                        switch (items.answer)
                        {
                            case 4:
                                item.rdo_StronglyAgree.Checked = true;
                                break;

                            case 3:
                                item.rdo_Agree.Checked = true;
                                break;

                            case 2:
                                item.rdo_Disagree.Checked = true;
                                break;

                            case 1:
                                item.rdo_StronglyDisagree.Checked = true;
                                break;
                        }
                    }
                }




                //if(questionnaire[ctr, 2] == null || int.Parse(questionnaire[ctr, 2]) == 0)
                //{
                //    return;
                //}
                //else
                //{
                //    switch (int.Parse(questionnaire[ctr, 2]))
                //    {
                //        case 4:
                //            item.rdo_StronglyAgree.Checked = true;
                //            break;

                //        case 3:
                //            item.rdo_Agree.Checked = true;
                //            break;

                //        case 2:
                //            item.rdo_Disagree.Checked = true;
                //            break;

                //        case 1:
                //            item.rdo_StronglyDisagree.Checked = true;
                //            break;
                //    }
                //}
            }
        }

        private bool checkForUnanswered()
        {
            if(listStudAnswer.Count() < (questionnaire.GetLength(0) * uc_Panel.Count()))
            {
                MessageBox.Show("One or more questions were left unanswered.");
                return false;
            }
            else
            {
                bool ifAnswerIs0 = false;
                foreach (var item in listStudAnswer)
                {
                    if (item.answer == 0) { ifAnswerIs0 = true; }
                }
                if(ifAnswerIs0)
                {
                    MessageBox.Show("One or more questions were left unanswered.");
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        private void btn_Evaluate_Click_1(object sender, EventArgs e)
        {
            recordAnswers();
            if (checkForUnanswered())
            {
                //code nga i-save ang 
                foreach (var item in listStudAnswer)
                {
                    db.sp_InsertEvaluationStudentResult(item.id_Teacher, id_stud, id_sched, item.id_Question, item.answer);
                }
                frm_StudentComments frm_comm = new frm_StudentComments();
                frm_comm.id_schedeval = id_sched;
                frm_comm.username = id_stud;
                foreach (var item in uc_Panel)
                {
                    frm_comm.id_Teacher.Add(item.id);
                }
                frm_comm.doThis();
                frm_comm.ShowDialog();
                asdf.Close();
            }
        }

        private void butangananSaPangutana_Click(object sender, EventArgs e)
        {
            label1.Focus();
        }

        private void butangananSaPangutana_Enter(object sender, EventArgs e)
        {
            label1.Focus();
        }
    }
}
