﻿namespace FacultyEvaluationSystem
{
    partial class frm_Student
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Student));
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.pnl_Header = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.btn_RestoreDown = new Bunifu.Framework.UI.BunifuImageButton();
            this.btn_Maximize2 = new Bunifu.Framework.UI.BunifuImageButton();
            this.btn_Minimize = new Bunifu.Framework.UI.BunifuImageButton();
            this.btn_Maximize = new Bunifu.Framework.UI.BunifuImageButton();
            this.btn_Close = new Bunifu.Framework.UI.BunifuImageButton();
            this.btn_Sidebar = new System.Windows.Forms.PictureBox();
            this.lbl_SchoolName = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.pnl_Sidebar = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.btn_ChangePassword = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btn_Evaluation = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btn_SignOut = new Bunifu.Framework.UI.BunifuFlatButton();
            this.pb_Logo = new System.Windows.Forms.PictureBox();
            this.separator = new Bunifu.Framework.UI.BunifuSeparator();
            this.bunifuDragControl1 = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.bunifuDragControl2 = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.uc_StudentEvaluation1 = new FacultyEvaluationSystem.uc_StudentEvaluation();
            this.pnl_Header.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_RestoreDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_Maximize2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_Minimize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_Maximize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_Close)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_Sidebar)).BeginInit();
            this.pnl_Sidebar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Logo)).BeginInit();
            this.SuspendLayout();
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 5;
            this.bunifuElipse1.TargetControl = this;
            // 
            // pnl_Header
            // 
            this.pnl_Header.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnl_Header.BackgroundImage")));
            this.pnl_Header.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnl_Header.Controls.Add(this.btn_RestoreDown);
            this.pnl_Header.Controls.Add(this.btn_Maximize2);
            this.pnl_Header.Controls.Add(this.btn_Minimize);
            this.pnl_Header.Controls.Add(this.btn_Maximize);
            this.pnl_Header.Controls.Add(this.btn_Close);
            this.pnl_Header.Controls.Add(this.btn_Sidebar);
            this.pnl_Header.Controls.Add(this.lbl_SchoolName);
            this.pnl_Header.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnl_Header.GradientBottomLeft = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(42)))), ((int)(((byte)(114)))));
            this.pnl_Header.GradientBottomRight = System.Drawing.Color.FromArgb(((int)(((byte)(113)))), ((int)(((byte)(76)))), ((int)(((byte)(143)))));
            this.pnl_Header.GradientTopLeft = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(19)))), ((int)(((byte)(86)))));
            this.pnl_Header.GradientTopRight = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(240)))), ((int)(((byte)(160)))));
            this.pnl_Header.Location = new System.Drawing.Point(0, 0);
            this.pnl_Header.Name = "pnl_Header";
            this.pnl_Header.Quality = 10;
            this.pnl_Header.Size = new System.Drawing.Size(1150, 50);
            this.pnl_Header.TabIndex = 2;
            // 
            // btn_RestoreDown
            // 
            this.btn_RestoreDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_RestoreDown.BackColor = System.Drawing.Color.Transparent;
            this.btn_RestoreDown.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_RestoreDown.Image = ((System.Drawing.Image)(resources.GetObject("btn_RestoreDown.Image")));
            this.btn_RestoreDown.ImageActive = null;
            this.btn_RestoreDown.Location = new System.Drawing.Point(1019, 10);
            this.btn_RestoreDown.Name = "btn_RestoreDown";
            this.btn_RestoreDown.Size = new System.Drawing.Size(30, 30);
            this.btn_RestoreDown.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btn_RestoreDown.TabIndex = 17;
            this.btn_RestoreDown.TabStop = false;
            this.btn_RestoreDown.Visible = false;
            this.btn_RestoreDown.Zoom = 10;
            // 
            // btn_Maximize2
            // 
            this.btn_Maximize2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Maximize2.BackColor = System.Drawing.Color.Transparent;
            this.btn_Maximize2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Maximize2.Image = ((System.Drawing.Image)(resources.GetObject("btn_Maximize2.Image")));
            this.btn_Maximize2.ImageActive = null;
            this.btn_Maximize2.Location = new System.Drawing.Point(988, 10);
            this.btn_Maximize2.Name = "btn_Maximize2";
            this.btn_Maximize2.Size = new System.Drawing.Size(30, 30);
            this.btn_Maximize2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btn_Maximize2.TabIndex = 16;
            this.btn_Maximize2.TabStop = false;
            this.btn_Maximize2.Visible = false;
            this.btn_Maximize2.Zoom = 10;
            // 
            // btn_Minimize
            // 
            this.btn_Minimize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Minimize.BackColor = System.Drawing.Color.Transparent;
            this.btn_Minimize.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Minimize.Image = ((System.Drawing.Image)(resources.GetObject("btn_Minimize.Image")));
            this.btn_Minimize.ImageActive = null;
            this.btn_Minimize.Location = new System.Drawing.Point(1050, 10);
            this.btn_Minimize.Name = "btn_Minimize";
            this.btn_Minimize.Size = new System.Drawing.Size(30, 30);
            this.btn_Minimize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btn_Minimize.TabIndex = 15;
            this.btn_Minimize.TabStop = false;
            this.btn_Minimize.Zoom = 10;
            this.btn_Minimize.Click += new System.EventHandler(this.btn_Minimize_Click);
            // 
            // btn_Maximize
            // 
            this.btn_Maximize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Maximize.BackColor = System.Drawing.Color.Transparent;
            this.btn_Maximize.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Maximize.Image = ((System.Drawing.Image)(resources.GetObject("btn_Maximize.Image")));
            this.btn_Maximize.ImageActive = null;
            this.btn_Maximize.Location = new System.Drawing.Point(1081, 10);
            this.btn_Maximize.Name = "btn_Maximize";
            this.btn_Maximize.Size = new System.Drawing.Size(30, 30);
            this.btn_Maximize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btn_Maximize.TabIndex = 14;
            this.btn_Maximize.TabStop = false;
            this.btn_Maximize.Zoom = 10;
            this.btn_Maximize.Click += new System.EventHandler(this.btn_Maximize_Click);
            // 
            // btn_Close
            // 
            this.btn_Close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Close.BackColor = System.Drawing.Color.Transparent;
            this.btn_Close.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Close.Image = ((System.Drawing.Image)(resources.GetObject("btn_Close.Image")));
            this.btn_Close.ImageActive = null;
            this.btn_Close.Location = new System.Drawing.Point(1112, 10);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(30, 30);
            this.btn_Close.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btn_Close.TabIndex = 13;
            this.btn_Close.TabStop = false;
            this.btn_Close.Zoom = 10;
            this.btn_Close.Click += new System.EventHandler(this.btn_Close_Click);
            // 
            // btn_Sidebar
            // 
            this.btn_Sidebar.BackColor = System.Drawing.Color.Transparent;
            this.btn_Sidebar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Sidebar.Image = ((System.Drawing.Image)(resources.GetObject("btn_Sidebar.Image")));
            this.btn_Sidebar.Location = new System.Drawing.Point(1, 2);
            this.btn_Sidebar.Name = "btn_Sidebar";
            this.btn_Sidebar.Size = new System.Drawing.Size(45, 45);
            this.btn_Sidebar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btn_Sidebar.TabIndex = 12;
            this.btn_Sidebar.TabStop = false;
            this.btn_Sidebar.Click += new System.EventHandler(this.btn_Sidebar_Click);
            this.btn_Sidebar.MouseEnter += new System.EventHandler(this.btn_Sidebar_MouseEnter);
            this.btn_Sidebar.MouseLeave += new System.EventHandler(this.btn_Sidebar_MouseLeave);
            // 
            // lbl_SchoolName
            // 
            this.lbl_SchoolName.AutoSize = true;
            this.lbl_SchoolName.BackColor = System.Drawing.Color.Transparent;
            this.lbl_SchoolName.Font = new System.Drawing.Font("Georgia", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_SchoolName.ForeColor = System.Drawing.Color.White;
            this.lbl_SchoolName.Location = new System.Drawing.Point(51, 6);
            this.lbl_SchoolName.Name = "lbl_SchoolName";
            this.lbl_SchoolName.Size = new System.Drawing.Size(304, 38);
            this.lbl_SchoolName.TabIndex = 9;
            this.lbl_SchoolName.Text = "Schildknecht School";
            // 
            // pnl_Sidebar
            // 
            this.pnl_Sidebar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pnl_Sidebar.BackColor = System.Drawing.Color.LemonChiffon;
            this.pnl_Sidebar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnl_Sidebar.BackgroundImage")));
            this.pnl_Sidebar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnl_Sidebar.Controls.Add(this.btn_ChangePassword);
            this.pnl_Sidebar.Controls.Add(this.btn_Evaluation);
            this.pnl_Sidebar.Controls.Add(this.btn_SignOut);
            this.pnl_Sidebar.Controls.Add(this.pb_Logo);
            this.pnl_Sidebar.GradientBottomLeft = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(42)))), ((int)(((byte)(114)))));
            this.pnl_Sidebar.GradientBottomRight = System.Drawing.Color.FromArgb(((int)(((byte)(113)))), ((int)(((byte)(76)))), ((int)(((byte)(143)))));
            this.pnl_Sidebar.GradientTopLeft = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(19)))), ((int)(((byte)(86)))));
            this.pnl_Sidebar.GradientTopRight = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(118)))), ((int)(((byte)(171)))));
            this.pnl_Sidebar.Location = new System.Drawing.Point(0, 49);
            this.pnl_Sidebar.Name = "pnl_Sidebar";
            this.pnl_Sidebar.Quality = 10;
            this.pnl_Sidebar.Size = new System.Drawing.Size(241, 521);
            this.pnl_Sidebar.TabIndex = 3;
            // 
            // btn_ChangePassword
            // 
            this.btn_ChangePassword.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(19)))), ((int)(((byte)(85)))));
            this.btn_ChangePassword.BackColor = System.Drawing.Color.Transparent;
            this.btn_ChangePassword.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_ChangePassword.BorderRadius = 0;
            this.btn_ChangePassword.ButtonText = "Change password";
            this.btn_ChangePassword.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_ChangePassword.DisabledColor = System.Drawing.Color.Gray;
            this.btn_ChangePassword.Iconcolor = System.Drawing.Color.Transparent;
            this.btn_ChangePassword.Iconimage = ((System.Drawing.Image)(resources.GetObject("btn_ChangePassword.Iconimage")));
            this.btn_ChangePassword.Iconimage_right = null;
            this.btn_ChangePassword.Iconimage_right_Selected = null;
            this.btn_ChangePassword.Iconimage_Selected = null;
            this.btn_ChangePassword.IconMarginLeft = 0;
            this.btn_ChangePassword.IconMarginRight = 0;
            this.btn_ChangePassword.IconRightVisible = true;
            this.btn_ChangePassword.IconRightZoom = 0D;
            this.btn_ChangePassword.IconVisible = true;
            this.btn_ChangePassword.IconZoom = 90D;
            this.btn_ChangePassword.IsTab = false;
            this.btn_ChangePassword.Location = new System.Drawing.Point(1, 207);
            this.btn_ChangePassword.Name = "btn_ChangePassword";
            this.btn_ChangePassword.Normalcolor = System.Drawing.Color.Transparent;
            this.btn_ChangePassword.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(115)))), ((int)(((byte)(76)))), ((int)(((byte)(142)))));
            this.btn_ChangePassword.OnHoverTextColor = System.Drawing.Color.White;
            this.btn_ChangePassword.selected = false;
            this.btn_ChangePassword.Size = new System.Drawing.Size(240, 48);
            this.btn_ChangePassword.TabIndex = 16;
            this.btn_ChangePassword.Text = "Change password";
            this.btn_ChangePassword.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_ChangePassword.Textcolor = System.Drawing.Color.White;
            this.btn_ChangePassword.TextFont = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ChangePassword.Visible = false;
            // 
            // btn_Evaluation
            // 
            this.btn_Evaluation.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(19)))), ((int)(((byte)(85)))));
            this.btn_Evaluation.BackColor = System.Drawing.Color.Transparent;
            this.btn_Evaluation.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_Evaluation.BorderRadius = 0;
            this.btn_Evaluation.ButtonText = "Evaluate teacher";
            this.btn_Evaluation.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Evaluation.DisabledColor = System.Drawing.Color.Gray;
            this.btn_Evaluation.Iconcolor = System.Drawing.Color.Transparent;
            this.btn_Evaluation.Iconimage = ((System.Drawing.Image)(resources.GetObject("btn_Evaluation.Iconimage")));
            this.btn_Evaluation.Iconimage_right = null;
            this.btn_Evaluation.Iconimage_right_Selected = null;
            this.btn_Evaluation.Iconimage_Selected = null;
            this.btn_Evaluation.IconMarginLeft = 0;
            this.btn_Evaluation.IconMarginRight = 0;
            this.btn_Evaluation.IconRightVisible = true;
            this.btn_Evaluation.IconRightZoom = 0D;
            this.btn_Evaluation.IconVisible = true;
            this.btn_Evaluation.IconZoom = 90D;
            this.btn_Evaluation.IsTab = false;
            this.btn_Evaluation.Location = new System.Drawing.Point(1, 153);
            this.btn_Evaluation.Name = "btn_Evaluation";
            this.btn_Evaluation.Normalcolor = System.Drawing.Color.Transparent;
            this.btn_Evaluation.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(115)))), ((int)(((byte)(76)))), ((int)(((byte)(142)))));
            this.btn_Evaluation.OnHoverTextColor = System.Drawing.Color.White;
            this.btn_Evaluation.selected = false;
            this.btn_Evaluation.Size = new System.Drawing.Size(240, 48);
            this.btn_Evaluation.TabIndex = 15;
            this.btn_Evaluation.Text = "Evaluate teacher";
            this.btn_Evaluation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_Evaluation.Textcolor = System.Drawing.Color.White;
            this.btn_Evaluation.TextFont = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Evaluation.Click += new System.EventHandler(this.btn_Evaluation_Click_1);
            // 
            // btn_SignOut
            // 
            this.btn_SignOut.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(19)))), ((int)(((byte)(85)))));
            this.btn_SignOut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_SignOut.BackColor = System.Drawing.Color.Transparent;
            this.btn_SignOut.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_SignOut.BorderRadius = 0;
            this.btn_SignOut.ButtonText = "Signout";
            this.btn_SignOut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_SignOut.DisabledColor = System.Drawing.Color.Gray;
            this.btn_SignOut.Iconcolor = System.Drawing.Color.Transparent;
            this.btn_SignOut.Iconimage = ((System.Drawing.Image)(resources.GetObject("btn_SignOut.Iconimage")));
            this.btn_SignOut.Iconimage_right = null;
            this.btn_SignOut.Iconimage_right_Selected = null;
            this.btn_SignOut.Iconimage_Selected = null;
            this.btn_SignOut.IconMarginLeft = 0;
            this.btn_SignOut.IconMarginRight = 0;
            this.btn_SignOut.IconRightVisible = true;
            this.btn_SignOut.IconRightZoom = 0D;
            this.btn_SignOut.IconVisible = true;
            this.btn_SignOut.IconZoom = 90D;
            this.btn_SignOut.IsTab = false;
            this.btn_SignOut.Location = new System.Drawing.Point(1, 465);
            this.btn_SignOut.Name = "btn_SignOut";
            this.btn_SignOut.Normalcolor = System.Drawing.Color.Transparent;
            this.btn_SignOut.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(115)))), ((int)(((byte)(76)))), ((int)(((byte)(142)))));
            this.btn_SignOut.OnHoverTextColor = System.Drawing.Color.White;
            this.btn_SignOut.selected = false;
            this.btn_SignOut.Size = new System.Drawing.Size(240, 48);
            this.btn_SignOut.TabIndex = 14;
            this.btn_SignOut.Text = "Signout";
            this.btn_SignOut.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_SignOut.Textcolor = System.Drawing.Color.White;
            this.btn_SignOut.TextFont = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_SignOut.Click += new System.EventHandler(this.btn_SignOut_Click);
            // 
            // pb_Logo
            // 
            this.pb_Logo.BackColor = System.Drawing.Color.Transparent;
            this.pb_Logo.Image = ((System.Drawing.Image)(resources.GetObject("pb_Logo.Image")));
            this.pb_Logo.Location = new System.Drawing.Point(54, 7);
            this.pb_Logo.Name = "pb_Logo";
            this.pb_Logo.Size = new System.Drawing.Size(135, 140);
            this.pb_Logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_Logo.TabIndex = 11;
            this.pb_Logo.TabStop = false;
            // 
            // separator
            // 
            this.separator.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.separator.BackColor = System.Drawing.Color.Transparent;
            this.separator.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))));
            this.separator.LineThickness = 6;
            this.separator.Location = new System.Drawing.Point(233, 42);
            this.separator.Name = "separator";
            this.separator.Size = new System.Drawing.Size(20, 530);
            this.separator.TabIndex = 7;
            this.separator.Transparency = 255;
            this.separator.Vertical = true;
            // 
            // bunifuDragControl1
            // 
            this.bunifuDragControl1.Fixed = true;
            this.bunifuDragControl1.Horizontal = true;
            this.bunifuDragControl1.TargetControl = this.pnl_Header;
            this.bunifuDragControl1.Vertical = true;
            // 
            // bunifuDragControl2
            // 
            this.bunifuDragControl2.Fixed = true;
            this.bunifuDragControl2.Horizontal = true;
            this.bunifuDragControl2.TargetControl = this.lbl_SchoolName;
            this.bunifuDragControl2.Vertical = true;
            // 
            // uc_StudentEvaluation1
            // 
            this.uc_StudentEvaluation1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.uc_StudentEvaluation1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.uc_StudentEvaluation1.Location = new System.Drawing.Point(247, 51);
            this.uc_StudentEvaluation1.Name = "uc_StudentEvaluation1";
            this.uc_StudentEvaluation1.Size = new System.Drawing.Size(902, 518);
            this.uc_StudentEvaluation1.TabIndex = 8;
            // 
            // frm_Student
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LemonChiffon;
            this.ClientSize = new System.Drawing.Size(1150, 570);
            this.Controls.Add(this.uc_StudentEvaluation1);
            this.Controls.Add(this.pnl_Sidebar);
            this.Controls.Add(this.pnl_Header);
            this.Controls.Add(this.separator);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frm_Student";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Student Evaluation Main Form";
            this.Load += new System.EventHandler(this.frm_Student_Load);
            this.pnl_Header.ResumeLayout(false);
            this.pnl_Header.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_RestoreDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_Maximize2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_Minimize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_Maximize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_Close)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_Sidebar)).EndInit();
            this.pnl_Sidebar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pb_Logo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private Bunifu.Framework.UI.BunifuGradientPanel pnl_Sidebar;
        private System.Windows.Forms.PictureBox pb_Logo;
        private Bunifu.Framework.UI.BunifuGradientPanel pnl_Header;
        private System.Windows.Forms.PictureBox btn_Sidebar;
        private Bunifu.Framework.UI.BunifuCustomLabel lbl_SchoolName;
        private Bunifu.Framework.UI.BunifuSeparator separator;
        private Bunifu.Framework.UI.BunifuImageButton btn_Minimize;
        private Bunifu.Framework.UI.BunifuImageButton btn_Maximize;
        private Bunifu.Framework.UI.BunifuImageButton btn_Close;
        private Bunifu.Framework.UI.BunifuDragControl bunifuDragControl1;
        private Bunifu.Framework.UI.BunifuDragControl bunifuDragControl2;
        private Bunifu.Framework.UI.BunifuImageButton btn_RestoreDown;
        private Bunifu.Framework.UI.BunifuImageButton btn_Maximize2;
        private uc_StudentEvaluation uc_StudentEvaluation1;
        private Bunifu.Framework.UI.BunifuFlatButton btn_ChangePassword;
        private Bunifu.Framework.UI.BunifuFlatButton btn_Evaluation;
        private Bunifu.Framework.UI.BunifuFlatButton btn_SignOut;
    }
}