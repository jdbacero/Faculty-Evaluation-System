﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClassLibrary;

namespace FacultyEvaluationSystem
{
    public partial class frm_Student : Form
    {
        List<UserControl> studUserControls = new List<UserControl>();
        List<int> id_Teachers = new List<int>();
        int indicator;
        string id;
        int id_sched;
        string schoolyear;
        int id_eval = 0;
        string id_stud;
        dataClass_FacultyEvaluationSystemDataContext db = new dataClass_FacultyEvaluationSystemDataContext();
        public frm_Student(string ID, string SchoolYear, int id_studEval, int id_Sched, string id_Stud, int Indicator)
        {
            id = ID;
            id_sched = id_Sched;
            id_eval = id_studEval;
            id_stud = id_Stud;
            schoolyear = SchoolYear;
            InitializeComponent();
            //added the uc
            studUserControls.Add(uc_StudentEvaluation1);
            uc_StudentEvaluation1.id = id_studEval;
            indicator = Indicator;
            doThis();
        }

        private void doThis()
        {
            if (id_eval == 0 || indicator == 1)
            {
                uc_StudentEvaluation1.Hide();
                btn_Evaluation.Enabled = false;
                if (indicator == 1) MessageBox.Show("You have already evaluated for this school year's evaluation.");
            }
            else
            {
                this.ActiveControl = btn_Evaluation;
                frm_IntroEval intro = new frm_IntroEval();
                intro.ShowDialog();
                uc_StudentEvaluation1.asdf = this;
                uc_StudentEvaluation1.lbl_Catergory.Text = "ID# " + id_stud;
                //setting questionnaires col 0 - id     col 1 - question        col 2 - answer      col 3 - teacher id
                string[,] questionnaire = new string[db.sp_ViewEvaluationStudentQuestions_ByID(id_eval).Count(), 4];
                int x = 0;
                foreach (var results in db.sp_ViewEvaluationStudentQuestions_ByID(id_eval))
                {
                    questionnaire[x, 0] = results.ID_Question + "";
                    questionnaire[x, 1] = results.Question;
                    questionnaire[x, 2] = null;
                    x++;
                }

                uc_StudentEvaluation1.id_sched = id_sched;
                uc_StudentEvaluation1.questionnaire = questionnaire;
                uc_StudentEvaluation1.id_stud = id_stud;

                foreach (var result in db.sp_ViewTeachers_Distinct_BySchoolID_SchoolYear(id, schoolyear))
                {
                    id_Teachers.Add(result.ID);
                }
                List<uc_PanelTeacher> list = new List<uc_PanelTeacher>();

                int locationX = 0;
                int ctr = 0;

                for (int i = 0; i < id_Teachers.Count(); i++)
                {
                    foreach (var res in db.sp_ViewTeachers_ByID(id_Teachers[ctr]))
                    {
                        list.Add(new uc_PanelTeacher(res.Last_Name + ", " + res.First_Name));
                        list[ctr].id = res.ID;
                        list[ctr].Parent = uc_StudentEvaluation1.panel1;
                        list[ctr].Location = new Point(locationX, 0);
                    }
                    ctr++;
                    locationX += 270;
                }
                uc_StudentEvaluation1.uc_Panel = list;
            }
            
            //foreach (var items in id_Teachers)
            //{
            //    foreach (var res in db.sp_ViewTeachers_ByID(id_Teachers[ctr]))
            //    {
            //        list.Add(new uc_PanelTeacher(res.Last_Name + ", " + res.First_Name));
            //        list[ctr].Parent = uc_StudentEvaluation1.panel1;
            //        list[ctr].Location = new Point(locationX, 0);
            //    }
            //    ctr++;
            //    locationX += 100;
            //    list[ctr].Show();
            //}
        }

        private void frm_Student_Load(object sender, EventArgs e)
        {
            //doThis();
            
            

            /* the godly code
            List<string> Teachers = new List<string>();
            Teachers.Add("Jorex");
            Teachers.Add("Klyde");
            Teachers.Add("Clyde");
            List<uc_PanelTeacher> list = new List<uc_PanelTeacher>();

            int locationX = 0;
            int ctr = 0;
            foreach(var items in Teachers)
            {
                list.Add(new uc_PanelTeacher());
                list[ctr].Location = new Point(locationX, 0);
                ctr++;
                locationX += 200;
            }

            list[0].Parent = panel1;
            list[0].Show();
            list[1].Parent = panel1;
            list[1].Show();
            */
        }

        private void btn_Sidebar_Click(object sender, EventArgs e)
        {
            switch (pnl_Sidebar.Width)
            {
                case 241:
                    pb_Logo.Height = 45;
                    pb_Logo.Width = 45;
                    separator.Location = new Point(39, 49);
                    pb_Logo.Location = new Point(1, 33);

                    //Ease animation
                    while (pnl_Sidebar.Width != 47)
                    {
                        if (pnl_Sidebar.Width > 150)
                        {
                            pnl_Sidebar.Width -= 20;
                        }
                        else if (pnl_Sidebar.Width <= 150 && pnl_Sidebar.Width > 102)
                        {
                            pnl_Sidebar.Width -= 10;
                        }
                        else if (pnl_Sidebar.Width <= 102 && pnl_Sidebar.Width > 63)
                        {
                            pnl_Sidebar.Width -= 5;
                        }
                        else if (pnl_Sidebar.Width <= 63)
                        {
                            pnl_Sidebar.Width -= 2;
                        }
                    }

                    foreach (UserControl frms in studUserControls)
                    {
                        //frms.Size = new Size(1081, 514);
                        frms.Location = new Point(53, 51);
                        frms.Width = frms.Width + 194;
                        frms.Anchor = (AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top);
                    }

                    break;
                default:
                    pnl_Sidebar.Width = 241;
                    separator.Location = new Point(233, 49);
                    pb_Logo.Height = 135;
                    pb_Logo.Width = 140;
                    pb_Logo.Location = new Point(58, 7);

                    foreach (UserControl frms in studUserControls)
                    {
                        //frms.Size = new Size(887, 514);
                        frms.Location = new Point(247, 51);
                        frms.Width = frms.Width - 194;
                        frms.Anchor = (AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top);
                    }

                    break;
            }
        }

        private void btn_Maximize_Click(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Normal)
            {
                btn_Maximize.Image = btn_RestoreDown.Image;
                WindowState = FormWindowState.Maximized;
            }
            else
            {
                if (WindowState == FormWindowState.Maximized)
                {
                    btn_Maximize.Image = btn_Maximize2.Image;
                    WindowState = FormWindowState.Normal;
                }
            }
        }

        private void btn_Minimize_Click(object sender, EventArgs e)
        {
            WindowFunctions.WindowMinimize(this);
        }
        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_Evaluation_Click(object sender, EventArgs e)
        {
            
        }

        private void btn_Sidebar_MouseEnter(object sender, EventArgs e)
        {
            btn_Sidebar.BackColor = ColorTranslator.FromHtml("#734C8E");
        }

        private void btn_Sidebar_MouseLeave(object sender, EventArgs e)
        {
            btn_Sidebar.BackColor = Color.Transparent;
        }

        private void btn_SignOut_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_Evaluation_Click_1(object sender, EventArgs e)
        {

        }
    }
}
