﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FacultyEvaluationSystem
{
    public partial class frm_StudentComments : Form
    {
        public List<int> id_Teacher = new List<int>();
        public string username;
        public int id_schedeval;
        dataClass_FacultyEvaluationSystemDataContext db = new dataClass_FacultyEvaluationSystemDataContext();
        List<uc_Comments> list_uc_Comments = new List<uc_Comments>();

        public frm_StudentComments()
        {
            InitializeComponent();
        }

        private void frm_StudentComments_Load(object sender, EventArgs e)
        {
            
        }

        public void doThis()
        {
            int locationY = 0;
            int ctr = 0;

            for (int i = 0; i < id_Teacher.Count(); i++)
            {
                foreach (var res in db.sp_ViewTeachers_ByID(id_Teacher[ctr]))
                {
                    list_uc_Comments.Add(new uc_Comments());
                    list_uc_Comments[ctr].id_teacher = res.ID;
                    list_uc_Comments[ctr].Parent = panel;
                    list_uc_Comments[ctr].lbl_Teacher.Text = $"{res.Last_Name}, {res.First_Name}";
                    list_uc_Comments[ctr].Location = new Point(0, locationY);
                }
                ctr++;
                locationY += 200;
            }
        }

        private void btn_Submit_Click(object sender, EventArgs e)
        {
            int responded = 0;
            foreach (var item in list_uc_Comments)
            {
                if(item.txt_Positive.Text.Trim() == "" || item.txt_Negative.Text.Trim() == "")
                {
                    //MessageBox.Show("One or more comments is blank.");
                    //return;

                    DialogResult dialog = MessageBox.Show("One or more comments is left blank. Are you sure you want to submit?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    if(dialog == DialogResult.Yes)
                    {
                        responded = 2;
                    }
                    else
                    {
                        responded = 1;
                    }
                }
            }

            if (responded == 1) return;

            foreach (var item in list_uc_Comments)
            {
                db.sp_InsertEvaluationStudentComment(item.id_teacher, username, id_schedeval, item.txt_Positive.Text, item.txt_Negative.Text, DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm")));
                MessageBox.Show("Thank you for giving an honest evaluation to your teachers.");
            }
            this.Hide();
        }
    }
}
