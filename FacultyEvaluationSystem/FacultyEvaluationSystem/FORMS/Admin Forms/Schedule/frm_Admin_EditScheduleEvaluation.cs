﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FacultyEvaluationSystem
{
    public partial class frm_Admin_EditScheduleEvaluation : Form
    {
        dataClass_FacultyEvaluationSystemDataContext db = new dataClass_FacultyEvaluationSystemDataContext();
        
        public Bunifu.Framework.UI.BunifuCustomDataGrid dgv, dgv1;
        int id;
        string type;
        int id_Eval;

        public frm_Admin_EditScheduleEvaluation(int ID, string Type, int ID_EVAL)
        {
            InitializeComponent();
            id = ID;
            type = Type;
            id_Eval = ID_EVAL;
        }

        private void frm_Admin_EditScheduleEvaluation_Load(object sender, EventArgs e)
        {
            // this switches base on the evaluation type being called
            // this is because principal and student evaluation shares the same form in editing schedule
            switch (type)
            {
                case "Principal":
                    dgv_EvaluationID.DataSource = db.sp_ViewEvaluationPrincipalID();
                    dtp_Start.Value = DateTime.Parse(dgv1.CurrentRow.Cells[2].Value.ToString());
                    dtp_End.Value = DateTime.Parse(dgv1.CurrentRow.Cells[3].Value.ToString());
                    foreach (DataGridViewRow row in dgv_EvaluationID.Rows)
                    {
                        if (int.Parse(row.Cells["ID_Evaluation"].Value.ToString()) == id)
                        {
                            row.Selected = true;
                        }
                    }
                    break;
                case "Student":
                    dgv_EvaluationID.DataSource = db.sp_ViewEvaluationStudentID();
                    dtp_Start.Value = DateTime.Parse(dgv1.CurrentRow.Cells[2].Value.ToString());
                    dtp_End.Value = DateTime.Parse(dgv1.CurrentRow.Cells[3].Value.ToString());
                    foreach (DataGridViewRow row in dgv_EvaluationID.Rows)
                    {
                        if (int.Parse(row.Cells["ID_Evaluation"].Value.ToString()) == id)
                        {
                            row.Selected = true;
                        }
                    }
                    break;
                default:
                    break;
            }
            dgv_EvaluationID.ClearSelection();
           
        }
        //------------------------------------------BUTTONS---------------------------------------------//

        private void btn_Edit_Click(object sender, EventArgs e)
        {
            //setting if the evaluation is on going or inactive
            string status;
            if (dtp_Start.Value < DateTime.Today && dtp_End.Value > DateTime.Today)
            {
                status = "Ongoing";
            }
            else
            {
                status = "Inactive";
            }

            switch (type)
            {
                // updating according to their evaluation type
                case "Principal":
                    db.sp_UpdateEvaluationPrincipalID_Status(id_Eval, "Inactive");
                    db.sp_UpdatePrincipalEvaluationSchedule(id, DateTime.Parse(dtp_Start.Value.ToString("yyyy/MM/dd")), DateTime.Parse(dtp_End.Value.ToString("yyyy/MM/dd")), status, "Active");
                    MessageBox.Show("Successfully Updated Evaluation Schedule.");
                    dgv1.DataSource = db.sp_ViewEvaluationSchedulePrincipal();
                    dgv_EvaluationID.DataSource = db.sp_ViewEvaluationPrincipalID();
                    break;
                case "Student":
                    db.sp_UpdateEvaluationStudentID_Status(id_Eval, "Inactive");
                    db.sp_UpdateStudentEvaluationSchedule(id, DateTime.Parse(dtp_Start.Value.ToString("yyyy/MM/dd")), DateTime.Parse(dtp_End.Value.ToString("yyyy/MM/dd")), status, "Active");
                    MessageBox.Show("Successfully Updated Evaluation Schedule.");
                    dgv1.DataSource = db.sp_ViewEvaluationScheduleStudent();
                    dgv_EvaluationID.DataSource = db.sp_ViewEvaluationStudentID();
                    break;
            }
        }
        // btn close
        private void btn_Close_Click(object sender, EventArgs e)
        {
            switch (type)
            {
                case "Principal":
                    frm_Admin_ScheduleEvaluation schedeval = new frm_Admin_ScheduleEvaluation("Principal");
                    schedeval.dgv_Schedule.DataSource = db.sp_ViewEvaluationSchedulePrincipal();
                    this.Close();
                    break;
                case "Student":
                    //to be coded
                    this.Close();
                    break;
                default:
                    break;
            }
        }

        //------------------------------------------EVENTS---------------------------------------------//

        private void dgv_EvalTeacherID_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            // this is to set the data source in the dgv of frm_Admin_ScheduleEvaluation form in viewing evaluation questions
            switch (type)
            {
                case "Principal":
                    dgv.DataSource = db.sp_ViewEvalutationQuestionsByID(int.Parse(dgv_EvaluationID.CurrentRow.Cells["ID_Evaluation"].Value.ToString()));
                    break;
                case "Student":
                    dgv.DataSource = db.sp_ViewEvaluationStudentQuestionsByID(int.Parse(dgv_EvaluationID.CurrentRow.Cells["ID_Evaluation"].Value.ToString()));
                    break;
                default:
                    break;
            }
        }

        private void dtp_End_onValueChanged(object sender, EventArgs e)
        {
            // error trapping on date inputs
            if (dtp_End.Value < dtp_Start.Value)
            {
                MessageBox.Show("End date must be greater than starting date.", "Date Conflict Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                MessageBox.Show("The end date has been automatically set five (5) days after starting date.", "Automatic End Date Setting", MessageBoxButtons.OK, MessageBoxIcon.Information);
                dtp_End.Value = dtp_Start.Value.AddDays(5);
            }
        }

        private void dtp_Start_onValueChanged(object sender, EventArgs e)
        {
            // auto setting the end date to five days after the start date
            dtp_End.Value = dtp_Start.Value.AddDays(5);
        }


    }
}
