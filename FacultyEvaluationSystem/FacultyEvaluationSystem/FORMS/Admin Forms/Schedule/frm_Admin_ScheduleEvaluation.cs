﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FacultyEvaluationSystem
{
    public partial class frm_Admin_ScheduleEvaluation : Form
    {
        dataClass_FacultyEvaluationSystemDataContext db = new dataClass_FacultyEvaluationSystemDataContext();
        //school year is the whole thing
        public string schoolyear;
        //added start year pra makuha ang START year only
        public string startYear;
        //added end year pra makuha ang end YEAR only
        public string endYear;
        string type;

        public frm_Admin_ScheduleEvaluation(string Type)
        {
            InitializeComponent();
            type = Type;
        }

        private void frm_Admin_ScheduleEvaluation_Load(object sender, EventArgs e)
        {
            //lbl_SchoolYear.Text = $"A.Y. {schoolyear}";
            lbl_SchoolYear.Text = "A.Y. " + schoolyear;
            //added this code here pra makuha niya ang "starting YEAR only" para mao sad ang year sa dtp - daniel(9/17/2018)
            int month = DateTime.Now.Month;
            int day = DateTime.Now.Day;
            dtp_Start.Value = new DateTime(int.Parse(startYear), month, day);
            //added this switch case to 
            switch (type)
            {
                case "Principal":
                    lbl_Header.Text = "Teacher Evaluation Schedule";
                    gb_Questions.Text = "Teacher Questions";

                    dgv_Schedule.DataSource = db.sp_ViewEvaluationSchedulePrincipal();
                    dgv_EvaluationID.DataSource = db.sp_ViewEvaluationPrincipalID();

                    break;
                case "Student":
                    lbl_Header.Text = "Student Evaluation Schedule";
                    gb_Questions.Text = "Student Questions";

                    dgv_Schedule.DataSource = db.sp_ViewEvaluationScheduleStudent();
                    dgv_EvaluationID.DataSource = db.sp_ViewEvaluationStudentID();
                    break;
                default:
                    break;
            }
            dgv_Schedule.ClearSelection();
            dgv_EvaluationID.ClearSelection();

            //this code sets dtp_end
            DateTime new_dtend = dtp_End.Value.AddDays(5);
            dtp_End.Value = new_dtend;


            //This code lets the dgv to not inherit the forecolor & font size of the groupbox header
            foreach (Control gb in gb_Questions.Controls)
            {
                gb.ForeColor = Color.Black;
                gb.Font = new Font(gb_Questions.Font.FontFamily, 9);
            }
        }

        //------------------------------------------BUTTONS---------------------------------------------//

        private void btn_Add_Click(object sender, EventArgs e)
        {
           
            //added restriction to addsched
            if (dgv_EvaluationQuestions.Rows.Count == 0)
            {
                MessageBox.Show("Select the set first.");
                return;
            }

            if (dgv_EvaluationID.CurrentRow.Cells["Status"].Value.ToString().ToLower() == "active")
            {
                MessageBox.Show("Cannot use a currently active set.");
                return;
            }

            if (dtp_Start.Value < DateTime.Today)
            {
                MessageBox.Show("Start date must not be less than the date today.");
                dtp_Start.ResetText();
                return;
            }

            //added restriction pra dile mka add if ang end year nilapas based on the chosen school year -9/17/2018
            if (dtp_End.Value.Year > int.Parse(endYear))
            {
                MessageBox.Show("Date must only be within the chosen school year.");
                return;
            }

            //NOTE: Current form needs to be closed first inorder to see the change of the statusu of schedule -9/17/2018
            string status;
            if (dtp_Start.Value < DateTime.Today && dtp_End.Value > DateTime.Today)
            {
                status = "Ongoing";
            } 
            else
            {
                status = "Inactive";
            }
            //added this to determin what type of schedule eval
            switch (type)
            {
                case "Principal":
                    //insert sp needs to be changed cuz of             this param below
                    //db.sp_InsertEvaluationSchedule(int.Parse(dgv_EvalStudentID.CurrentRow.Cells["ID_Evaluation"].Value.ToString())
                    //    , int.Parse(dgv_EvalTeacherID.CurrentRow.Cells["ID_Evaluation"].Value.ToString())
                    //    , DateTime.Parse(dtp_Start.Value.ToString("yyyy/MM/dd")), DateTime.Parse(dtp_End.Value.ToString("yyyy/MM/dd")), status, schoolyear);
                    db.sp_InsertEvaluationSchedulePrincipal(int.Parse(dgv_EvaluationID.CurrentRow.Cells["ID_Evaluation"].Value.ToString()), DateTime.Parse(dtp_Start.Value.ToString("yyyy/MM/dd")), DateTime.Parse(dtp_End.Value.ToString("yyyy/MM/dd")), status, schoolyear);
                    db.sp_UpdateEvaluationPrincipalID_Status(int.Parse(dgv_EvaluationID.CurrentRow.Cells["ID_Evaluation"].Value.ToString()), "Active");
                    dgv_Schedule.DataSource = db.sp_ViewEvaluationSchedulePrincipal();
                    dgv_EvaluationID.DataSource = db.sp_ViewEvaluationPrincipalID();
                    dgv_Schedule.ClearSelection();
                    MessageBox.Show("Successfuly added schedule!");
                    break;
                case "Student":
                    //wala ni sud kay wala sad juy sulod ang naasa orig kay gi una pasa sa principal & tungod sad atong mga changes na gi ingun sir jee
                    db.sp_InsertEvaluationScheduleStudent(int.Parse(dgv_EvaluationID.CurrentRow.Cells["ID_Evaluation"].Value.ToString()), DateTime.Parse(dtp_Start.Value.ToString("yyyy/MM/dd")), DateTime.Parse(dtp_End.Value.ToString("yyyy/MM/dd")), status, schoolyear);
                    db.sp_UpdateEvaluationStudentID_Status(int.Parse(dgv_EvaluationID.CurrentRow.Cells["ID_Evaluation"].Value.ToString()), "Active");
                    dgv_EvaluationID.DataSource = db.sp_ViewEvaluationStudentID();
                    dgv_Schedule.DataSource = db.sp_ViewEvaluationScheduleStudent();
                    dgv_Schedule.ClearSelection();
                    MessageBox.Show("Successfuly added schedule!");
                    break;
                default:
                    break;
            }      
        }

        //if possible kailangan mawala ang VIEW sa dgv teacher questions inig click sa edit
        private void btn_Edit_Click(object sender, EventArgs e)
        {
            String start = DateTime.Parse(dgv_Schedule.CurrentRow.Cells[2].Value.ToString()).ToString("yyyy/MM/dd");
            String end = DateTime.Parse(dgv_Schedule.CurrentRow.Cells[3].Value.ToString()).ToString("yyyy/MM/dd");
            String now = DateTime.Now.ToString("yyyy/MM/dd");
            if (DateTime.Parse(now) >= DateTime.Parse(start) && DateTime.Parse(now) <= DateTime.Parse(end))
            {
                MessageBox.Show("Cannot edit on-going schedules.");
            }
            else
            {
                //added this to determining what type of schedule eval
                switch (type)
                {
                    case "Principal":
                        frm_Admin_EditScheduleEvaluation editSchedEval = new frm_Admin_EditScheduleEvaluation(int.Parse(dgv_Schedule.CurrentRow.Cells["ID"].Value.ToString()), "Principal", int.Parse(dgv_EvaluationID.CurrentRow.Cells["ID_Evaluation"].Value.ToString()));
                        Disable("Edit");
                        editSchedEval.dgv = dgv_EvaluationQuestions;
                        editSchedEval.dgv1 = dgv_Schedule;
                        editSchedEval.ShowDialog();
                        break;
                    case "Student":
                        //i was tapol2 to find another wae so i just added 1 to the name
                        MessageBox.Show(dgv_Schedule.CurrentRow.Cells["ID"].Value.ToString());
                        frm_Admin_EditScheduleEvaluation editSchedEval1 = new frm_Admin_EditScheduleEvaluation(int.Parse(dgv_Schedule.CurrentRow.Cells["ID"].Value.ToString()), "Student", int.Parse(dgv_EvaluationID.CurrentRow.Cells["ID_Evaluation"].Value.ToString()));
                        Disable("Edit");
                        editSchedEval1.dgv = dgv_EvaluationQuestions;
                        editSchedEval1.dgv1 = dgv_Schedule;
                        editSchedEval1.ShowDialog();
                        break;
                    default:
                        break;
                }           
            }
        }
        //rip in peace
        private void btn_Delete_Click(object sender, EventArgs e)
        {

        }

        //close bont
        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //------------------------------------------EVENTS---------------------------------------------//

        private void dgv_EvalPrincipalID_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            switch (type)
            {
                case "Principal":
                    dgv_EvaluationQuestions.DataSource = db.sp_ViewEvalutationQuestionsByID(int.Parse(dgv_EvaluationID.CurrentRow.Cells["ID_Evaluation"].Value.ToString()));
                    dgv_EvaluationQuestions.Columns["ID_Question"].Visible = false;
                    dgv_EvaluationQuestions.Columns["ID_Evaluation"].Visible = false;
                    dgv_EvaluationQuestions.ClearSelection();
                    Enable("Add");
                    Disable("Edit");
                    break;
                case "Student":
                    //                                      im not sure if mao ni na stored proc ako ra gi copy sa old form
                    dgv_EvaluationQuestions.DataSource = db.sp_ViewEvaluationStudentQuestionsByID(int.Parse(dgv_EvaluationID.CurrentRow.Cells["ID_Evaluation"].Value.ToString()));
                    dgv_EvaluationQuestions.Columns["ID_Question"].Visible = false;
                    dgv_EvaluationQuestions.Columns["ID_Criteria"].Visible = false;
                    dgv_EvaluationQuestions.ClearSelection();
                    Enable("Add");
                    Disable("Edit");
                    break;
                default:
                    break;
            }      
        }

        private void dgv_EvalPrincipalQuestion_SelectionChanged(object sender, EventArgs e)
        {
            dgv_EvaluationQuestions.ClearSelection();
        }

        private void dgv_Schedule_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Enable("Edit");
            Disable("Add");
            dgv_EvaluationID.ClearSelection();
        }

        private void dtp_Start_onValueChanged(object sender, EventArgs e)
        {
            //this is to set the date end 5 days from the date start
            dtp_End.Value = dtp_Start.Value.AddDays(5);
        }

        private void dtp_End_onValueChanged(object sender, EventArgs e)
        {
            if (dtp_End.Value < dtp_Start.Value)
            {
                MessageBox.Show("End date must be greater than starting date.", "Date Conflict Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                MessageBox.Show("The end date has been automatically set five (5) days after starting date.", "Automatic End Date Setting", MessageBoxButtons.OK, MessageBoxIcon.Information);
                dtp_End.Value = dtp_Start.Value.AddDays(5);
            }
        }

        //------------------------------------------METHODS---------------------------------------------//
        public void Enable(string identifier)
        {
            switch (identifier)
            {
                case "Add":
                    btn_Add.Enabled = true;
                    btn_Add.Cursor = Cursors.Hand;
                    break;
                case "Edit":
                    btn_Edit.Enabled = true;
                    btn_Edit.Cursor = Cursors.Hand;
                    break;
                default:
                    break;
            }
        }

        public void Disable(string identifier)
        {
            switch (identifier)
            {
                case "Add":
                    btn_Add.Enabled = false;
                    btn_Add.Cursor = Cursors.No;
                    break;
                case "Edit":
                    btn_Edit.Enabled = false;
                    btn_Edit.Cursor = Cursors.No;
                    dgv_Schedule.ClearSelection();
                    break;
                default:
                    break;
            }
        }
    }
}
