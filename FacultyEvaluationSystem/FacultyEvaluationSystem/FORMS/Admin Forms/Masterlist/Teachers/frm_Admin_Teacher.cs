﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace FacultyEvaluationSystem
{


    public partial class frm_Admin_Teacher : Form
    {
        public static string currentDir = Environment.CurrentDirectory;
        public int teacherID;
        public string fullName;
        dataClass_FacultyEvaluationSystemDataContext db = new dataClass_FacultyEvaluationSystemDataContext();

        public frm_Admin_Teacher()
        {
            InitializeComponent();
        }
        
        private void frm_Admin_Teacher_Load(object sender, EventArgs e)
        {
            this.Text = fullName;
            dgv_HandledSection.DataSource = db.sp_ViewTeachersHandledSectionByTeachersID(teacherID);
            dgv_HandledSection.ClearSelection();
        }

        public void btn_Add_Click(object sender, EventArgs e)
        {
            if (btn_Add.ButtonText == "Cancel")
            {
                // if cancelled, goes back to orig UI
                txt_Subject.Text = ""; //changed it to this cuz bunifuMetro textbox doesnt have .Clear()
                cmb_Section.SelectedIndex = -1;
                cmb_GradeLevel.SelectedIndex= -1;
                txt_Subject.Enabled = false;
                cmb_Section.Enabled = false;
                cmb_GradeLevel.Enabled = false;
                btn_Edit.Enabled = false;
                btn_Delete.Enabled = false;
                dgv_HandledSection.ClearSelection();
                btn_Add.ButtonText = "Add";
                btn_Edit.Text = "Edit";
                btn_Edit.Cursor = Cursors.No;
                btn_Delete.Cursor = Cursors.No;
            }
            else
            {
                // triggers to another form (frm_Admin_AddSubSec), to execute adding of handled subject in the selected teacher
                frm_Admin_AddSubSec frm_add = new frm_Admin_AddSubSec();
                frm_add.ShowDialog();
                if (frm_add.add == true)
                {
                    db.sp_InsertTeachersHandledSection(teacherID, frm_add.cmb_Section.SelectedItem.ToString(), frm_add.txt_Subject.Text);
                }
                // showing purposes and resetting all controls
                dgv_HandledSection.DataSource = db.sp_ViewTeachersHandledSectionByTeachersID(teacherID);
                dgv_HandledSection.ClearSelection();
                btn_Edit.Enabled = false;
                btn_Delete.Enabled = false;
                //added clearing of textboxes & cmb here cuz if dgv is clicked & user clicks addBtn textboxes cmb values r still ther
                txt_Subject.Text = "";
                cmb_Section.SelectedIndex = -1;
                cmb_GradeLevel.SelectedIndex = -1;
                btn_Edit.Cursor = Cursors.No;
                btn_Delete.Cursor = Cursors.No;
            }
        }

        private void dgv_HandledSection_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            // passing all data of the selected row to its corresponding controls/
            // also enables editting and deleting
            btn_Edit.Enabled = true;
            btn_Edit.Cursor = Cursors.Hand;
            //enabling btnDelete upon cellclick and hcanging its cursor to hand from Disabled/No
            btn_Delete.Enabled = true;
            btn_Delete.Cursor = Cursors.Hand;
            foreach(var result in db.sp_ViewTeachersHandledSectionByID(db.sp_ReturnTeachersHandledSectionID(teacherID, dgv_HandledSection.CurrentRow.Cells["Section"].Value.ToString(), dgv_HandledSection.CurrentRow.Cells["Subject"].Value.ToString())))
            {
                txt_Subject.Text = result.Subject;
                cmb_GradeLevel.SelectedItem = result.Grade_Level;
                List<string> listSections = new List<string>();
                foreach (var entry in db.sp_ViewSectionByGradeLevel(cmb_GradeLevel.SelectedItem.ToString()))
                {
                    listSections.Add(entry.Section);
                }
                cmb_Section.DataSource = listSections;
                cmb_Section.SelectedItem = result.Section;
            }
        }

        private void btn_Edit_Click(object sender, EventArgs e)
        {
            if (btn_Edit.Text == "Edit")
            {
                // enabling controls to accept user inputs for editing
                btn_Edit.Text = "Confirm";
                txt_Subject.Enabled = true;
                cmb_GradeLevel.Enabled = true;
                cmb_Section.Enabled = true;
                btn_Add.ButtonText = "Cancel";
                //dile dpat mka delete kung nka pislit na sa edit
                btn_Delete.Enabled = false;
                btn_Delete.Cursor = Cursors.No;
            }
            else
            {
                //update code
                db.sp_UpdateTeachersHandledSection(cmb_Section.SelectedItem.ToString(), txt_Subject.Text, db.sp_ReturnTeachersHandledSectionID(teacherID, dgv_HandledSection.CurrentRow.Cells["Section"].Value.ToString(), dgv_HandledSection.CurrentRow.Cells["Subject"].Value.ToString()));
                
                txt_Subject.Text = "";
                btn_Edit.Text = "Edit";
                btn_Add.ButtonText = "Add";
                cmb_Section.SelectedIndex = -1;
                cmb_GradeLevel.SelectedIndex = -1;
                txt_Subject.Enabled = false;
                cmb_GradeLevel.Enabled = false;
                cmb_Section.Enabled = false;
                btn_Edit.Enabled = false;
                btn_Delete.Enabled = false;
                dgv_HandledSection.DataSource = db.sp_ViewTeachersHandledSectionByTeachersID(teacherID);
                dgv_HandledSection.ClearSelection();
                btn_Edit.Cursor = Cursors.No;
                btn_Delete.Cursor = Cursors.No;
            }
        }
        //i have not found a solution for bunifu's cmb/dropdownlist lack of datasource so ang default cmb lang sa akong gibutang
        //private void cmb_GradeLevel_onItemSelected(object sender, EventArgs e)
        //{
        //    if (cmb_GradeLevel.selectedIndex < 0)
        //    {
        //        return;
        //    }
        //    else
        //    {
        //        List<string> listSections = new List<string>();
        //        foreach (var entry in db.sp_ViewSectionByGradeLevel(cmb_GradeLevel.selectedValue.ToString()))
        //        {
        //            listSections.Add(entry.Section);
        //        }
        //        cmb_Section.DataSource = listSections;
        //    }
        //}

        private void cmb_GradeLevel1_SelectedIndexChanged(object sender, EventArgs e)
        {
            // this gets the selected grade level and generates the sections under that grade level
            if (cmb_GradeLevel.SelectedIndex < 0)
            {
                return;
            }
            else
            {
                List<string> listSections = new List<string>();
                foreach (var entry in db.sp_ViewSectionByGradeLevel(cmb_GradeLevel.SelectedItem.ToString()))
                {
                    listSections.Add(entry.Section);
                }
                cmb_Section.DataSource = listSections;
            }
        }
        // this is delete, but remember, delete is not an option
        private void btn_Delete_Click(object sender, EventArgs e)
        {

        }

        //close boton
        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bunifuFlatButton1_Click(object sender, EventArgs e)
        {
            string file = "";
            foreach(var test in db.sp_Count())
            {
                file = test.Column1.ToString();
            }
            OpenFileDialog upload = new OpenFileDialog(); //Instantiate new dialog
            upload.Filter = "Image | *.jpg; *.png; *.bmp"; //This line of code makes the user upload the excel files only
            upload.ShowDialog(); //Shows the upload dialog
            if (string.IsNullOrEmpty(upload.FileName)) return;
            File.Copy(upload.FileName, currentDir + "\\" + file, true);
            pictureBox1.ImageLocation = upload.FileName;
            db.sp_StorePic(teacherID, currentDir + "\\" + file, DateTime.Now);
            
            //Image picSaTeach = upload.File;
            
        }
    }
}
