﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FacultyEvaluationSystem
{
    public partial class frm_Admin_AddSubSec : Form
    {
        public bool add = false;
        dataClass_FacultyEvaluationSystemDataContext db = new dataClass_FacultyEvaluationSystemDataContext();

        public frm_Admin_AddSubSec()
        {
            InitializeComponent();
        }

        private void btn_Add_Click(object sender, EventArgs e)
        {
            if(txt_Subject.Text.Trim() == null || cmb_GradeLevel.SelectedIndex < 0)
            {
                // checks null inputs
                MessageBox.Show("Please fill all inputs.");
                return;
            }
            // setting true to trigger add procedure in frm_Admin_Teacher
            add = true;
            this.Close();
        }

        private void cmb_GradeLevel_SelectedIndexChanged(object sender, EventArgs e)
        {
            // generates all sections from the selected grade level
            cmb_Section.Enabled = true;
            List<string> listSections = new List<string>();
            foreach (var result in db.sp_ViewSectionByGradeLevel(cmb_GradeLevel.SelectedItem.ToString()))
            {
                listSections.Add(result.Section);
            }
            cmb_Section.DataSource = listSections;
        }
        // btn close
        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
