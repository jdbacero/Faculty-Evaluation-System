﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FacultyEvaluationSystem
{
    public partial class frm_Admin_PromptSchoolYear : Form
    {
        public string schoolyear;
        public string startYear;
        public string endYear;
        public bool set = false;

        public frm_Admin_PromptSchoolYear()
        {
            InitializeComponent();
            
        }

        private void frm_Admin_PromptSchoolYear_Load(object sender, EventArgs e)
        {
            // this is to show the starting year of every school year, (edited: showing latest to oldest)
            for (int x = DateTime.Today.Year + 1; x >= DateTime.Today.Year - 1; x--)
            {
                //changed from cmb_YearStart.Items.Add(x); to below cuz bunifu dropdown is array
                cmb_YearStart.AddItem(x.ToString());
            }
        }

        private void btn_Confirm_Click(object sender, EventArgs e)
        {
            if (cmb_YearStart.selectedIndex.ToString() == "-1")
            {
                // checks null values
                MessageBox.Show("Please select school year.");
            }
            else
            {
                // merging the selected value and its end year to get the school year
                startYear = cmb_YearStart.selectedValue.ToString();
                endYear = txt_EndYear.Text;
                schoolyear = cmb_YearStart.selectedValue.ToString() + " - " + txt_EndYear.Text;
                //changed set = true from cmbOnitemselected to here cuz if form is closed while user has selected a year, select upload prompts
                set = true;
                this.Close();
            }
        }

        private void cmb_YearStart_onItemSelected(object sender, EventArgs e)
        {
            // auto generating of end year once start year get selected
            txt_EndYear.Text = "" + (int.Parse(cmb_YearStart.selectedValue.ToString()) + 1);
        }
        // btn close
        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
