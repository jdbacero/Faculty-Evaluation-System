﻿namespace FacultyEvaluationSystem
{
    partial class frm_Admin_PromptSchoolYear
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Admin_PromptSchoolYear));
            this.label1 = new System.Windows.Forms.Label();
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.cmb_YearStart = new Bunifu.Framework.UI.BunifuDropdown();
            this.txt_EndYear = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.btn_Confirm = new Bunifu.Framework.UI.BunifuThinButton2();
            this.bunifuDragControl1 = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.lbl_PrompSchoolYear = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.btn_Close = new Bunifu.Framework.UI.BunifuImageButton();
            this.pnl_Header = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.btn_Close)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(158, 87);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "___";
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 3;
            this.bunifuElipse1.TargetControl = this;
            // 
            // cmb_YearStart
            // 
            this.cmb_YearStart.BackColor = System.Drawing.Color.Transparent;
            this.cmb_YearStart.BorderRadius = 3;
            this.cmb_YearStart.DisabledColor = System.Drawing.Color.Gray;
            this.cmb_YearStart.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_YearStart.ForeColor = System.Drawing.Color.White;
            this.cmb_YearStart.Items = new string[0];
            this.cmb_YearStart.Location = new System.Drawing.Point(43, 80);
            this.cmb_YearStart.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmb_YearStart.Name = "cmb_YearStart";
            this.cmb_YearStart.NomalColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.cmb_YearStart.onHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(88)))), ((int)(((byte)(130)))));
            this.cmb_YearStart.selectedIndex = -1;
            this.cmb_YearStart.Size = new System.Drawing.Size(100, 35);
            this.cmb_YearStart.TabIndex = 4;
            this.cmb_YearStart.onItemSelected += new System.EventHandler(this.cmb_YearStart_onItemSelected);
            // 
            // txt_EndYear
            // 
            this.txt_EndYear.BorderColorFocused = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(88)))), ((int)(((byte)(130)))));
            this.txt_EndYear.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.txt_EndYear.BorderColorMouseHover = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(88)))), ((int)(((byte)(130)))));
            this.txt_EndYear.BorderThickness = 3;
            this.txt_EndYear.Cursor = System.Windows.Forms.Cursors.Default;
            this.txt_EndYear.Enabled = false;
            this.txt_EndYear.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txt_EndYear.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_EndYear.isPassword = false;
            this.txt_EndYear.Location = new System.Drawing.Point(198, 80);
            this.txt_EndYear.Margin = new System.Windows.Forms.Padding(4);
            this.txt_EndYear.Name = "txt_EndYear";
            this.txt_EndYear.Size = new System.Drawing.Size(103, 35);
            this.txt_EndYear.TabIndex = 5;
            this.txt_EndYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btn_Confirm
            // 
            this.btn_Confirm.ActiveBorderThickness = 1;
            this.btn_Confirm.ActiveCornerRadius = 5;
            this.btn_Confirm.ActiveFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_Confirm.ActiveForecolor = System.Drawing.Color.White;
            this.btn_Confirm.ActiveLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_Confirm.BackColor = System.Drawing.SystemColors.Control;
            this.btn_Confirm.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_Confirm.BackgroundImage")));
            this.btn_Confirm.ButtonText = "Confirm";
            this.btn_Confirm.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Confirm.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Confirm.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_Confirm.IdleBorderThickness = 1;
            this.btn_Confirm.IdleCornerRadius = 5;
            this.btn_Confirm.IdleFillColor = System.Drawing.Color.White;
            this.btn_Confirm.IdleForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_Confirm.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_Confirm.Location = new System.Drawing.Point(118, 133);
            this.btn_Confirm.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn_Confirm.Name = "btn_Confirm";
            this.btn_Confirm.Size = new System.Drawing.Size(107, 44);
            this.btn_Confirm.TabIndex = 6;
            this.btn_Confirm.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn_Confirm.Click += new System.EventHandler(this.btn_Confirm_Click);
            // 
            // bunifuDragControl1
            // 
            this.bunifuDragControl1.Fixed = true;
            this.bunifuDragControl1.Horizontal = true;
            this.bunifuDragControl1.TargetControl = this.pnl_Header;
            this.bunifuDragControl1.Vertical = true;
            // 
            // lbl_PrompSchoolYear
            // 
            this.lbl_PrompSchoolYear.AutoSize = true;
            this.lbl_PrompSchoolYear.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_PrompSchoolYear.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.lbl_PrompSchoolYear.Location = new System.Drawing.Point(65, 37);
            this.lbl_PrompSchoolYear.Name = "lbl_PrompSchoolYear";
            this.lbl_PrompSchoolYear.Size = new System.Drawing.Size(222, 20);
            this.lbl_PrompSchoolYear.TabIndex = 7;
            this.lbl_PrompSchoolYear.Text = "Select the school year below";
            // 
            // btn_Close
            // 
            this.btn_Close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Close.BackColor = System.Drawing.Color.Transparent;
            this.btn_Close.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Close.Image = ((System.Drawing.Image)(resources.GetObject("btn_Close.Image")));
            this.btn_Close.ImageActive = null;
            this.btn_Close.Location = new System.Drawing.Point(318, 1);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(30, 30);
            this.btn_Close.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btn_Close.TabIndex = 8;
            this.btn_Close.TabStop = false;
            this.btn_Close.Zoom = 10;
            this.btn_Close.Click += new System.EventHandler(this.btn_Close_Click);
            // 
            // pnl_Header
            // 
            this.pnl_Header.Location = new System.Drawing.Point(2, 2);
            this.pnl_Header.Name = "pnl_Header";
            this.pnl_Header.Size = new System.Drawing.Size(346, 30);
            this.pnl_Header.TabIndex = 62;
            // 
            // frm_Admin_PromptSchoolYear
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(350, 200);
            this.Controls.Add(this.btn_Close);
            this.Controls.Add(this.lbl_PrompSchoolYear);
            this.Controls.Add(this.btn_Confirm);
            this.Controls.Add(this.txt_EndYear);
            this.Controls.Add(this.cmb_YearStart);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pnl_Header);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frm_Admin_PromptSchoolYear";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "School Year Selection";
            this.Load += new System.EventHandler(this.frm_Admin_PromptSchoolYear_Load);
            ((System.ComponentModel.ISupportInitialize)(this.btn_Close)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private Bunifu.Framework.UI.BunifuThinButton2 btn_Confirm;
        private Bunifu.Framework.UI.BunifuMetroTextbox txt_EndYear;
        private Bunifu.Framework.UI.BunifuDropdown cmb_YearStart;
        private Bunifu.Framework.UI.BunifuDragControl bunifuDragControl1;
        private Bunifu.Framework.UI.BunifuCustomLabel lbl_PrompSchoolYear;
        private Bunifu.Framework.UI.BunifuImageButton btn_Close;
        private System.Windows.Forms.Panel pnl_Header;
    }
}