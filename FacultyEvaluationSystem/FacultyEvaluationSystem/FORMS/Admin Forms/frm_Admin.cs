﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClassLibrary;

namespace FacultyEvaluationSystem
{
    public partial class frm_Admin : Form
    {
        //Initialize an empty list of usercontrols
        List<UserControl> adminUserControls = new List<UserControl>();
        public frm_Login login;
        public frm_Admin(string username)
        {
            InitializeComponent();
            
            //Add all usercontrols to the list that is being used in frm_Admin
            adminUserControls.Add(frm_AdminEvaluation1);
            adminUserControls.Add(frm_Masterlist1);
            adminUserControls.Add(frm_Reports1);
            adminUserControls.Add(frm_StudentChangePass1);
            frm_StudentChangePass1.username = username;
            frm_StudentChangePass1.asdf = this;
        }

        dataClass_FacultyEvaluationSystemDataContext db = new dataClass_FacultyEvaluationSystemDataContext();
        cls_reuseablecode asdf;

        private void frm_Admin_Load(object sender, EventArgs e)
        {
            //To be coded
            asdf = new cls_reuseablecode();
        }

        //Collapse or expand side bar
        private void btn_Sidebar_Click(object sender, EventArgs e)
        {
            switch (pnl_Sidebar.Width)
            {
                case 241:
                    pb_Logo.Height = 45;
                    pb_Logo.Width = 45;
                    separator.Location = new Point(39, 49);
                    pb_Logo.Location = new Point(1, 33);

                    //Ease animation
                    while (pnl_Sidebar.Width != 47)
                    {
                        if (pnl_Sidebar.Width > 150)
                        {
                            pnl_Sidebar.Width -= 20;
                        }
                        else if (pnl_Sidebar.Width <= 150 && pnl_Sidebar.Width > 102)
                        {
                            pnl_Sidebar.Width -= 10;
                        }
                        else if (pnl_Sidebar.Width <= 102 && pnl_Sidebar.Width > 63)
                        {
                            pnl_Sidebar.Width -= 5;
                        }
                        else if (pnl_Sidebar.Width <= 63)
                        {
                            pnl_Sidebar.Width -= 2;
                        }
                    }

                    foreach (UserControl frms in adminUserControls)
                    {
                        //frms.Size = new Size(1081, 514);
                        frms.Location = new Point(53, 51);
                        frms.Width = frms.Width + 194;
                        frms.Anchor = (AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top);
                    }
                    break;
                default:
                    pnl_Sidebar.Width = 241;
                    separator.Location = new Point(233, 49);
                    pb_Logo.Height = 135;
                    pb_Logo.Width = 140;
                    pb_Logo.Location = new Point(58, 7);

                    foreach (UserControl frms in adminUserControls)
                    {
                        //frms.Size = new Size(887, 514);
                        frms.Location = new Point(247, 51);
                        frms.Width = frms.Width - 194;
                        frms.Anchor = (AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top);
                    }
                    break;
            }
        }
        //sidebar cursor hover/enter & leave
        private void btn_Sidebar_MouseEnter(object sender, EventArgs e)
        {
            btn_Sidebar.BackColor = ColorTranslator.FromHtml("#734C8E");
        }

        private void btn_Sidebar_MouseLeave(object sender, EventArgs e)
        {
            btn_Sidebar.BackColor = Color.Transparent;
        }
        //sidebar button
        private void btn_Reports_Click(object sender, EventArgs e)
        {
            setUserControl(frm_Reports1);
        }

        private void btn_Masterlist_Click(object sender, EventArgs e)
        {
            setUserControl(frm_Masterlist1);
        }

        private void btn_Evaluation_Click(object sender, EventArgs e)
        {
            setUserControl(frm_AdminEvaluation1);
        }
        private void btn_ChangePassword_Click(object sender, EventArgs e)
        {
            setUserControl(frm_StudentChangePass1);
        }

        /////////////FUNCTIONS/////////////

        //--Displays the UI of the admin's desired usercontrol. Function for each main button of the Admin UI
        private void setUserControl(UserControl userControl)
        {
            userControl.BringToFront();
            userControl.Visible = true;
            foreach (UserControl frms in adminUserControls)
            {
                if (userControl.Name != frms.Name)
                {
                    frms.Visible = false;
                    frms.SendToBack();
                }
            }
        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            WindowFunctions.WindowClose();
        }

        private void btn_Minimize_Click(object sender, EventArgs e)
        {
            WindowFunctions.WindowMinimize(this);
        }
        //maximize btn
        private void btn_Maximize_Click(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Normal)
            {
                btn_Maximize.Image = btn_RestoreDown.Image;
                WindowState = FormWindowState.Maximized;
            }
            else
            {
                if (WindowState == FormWindowState.Maximized)
                {
                    btn_Maximize.Image = btn_Maximize2.Image;
                    WindowState = FormWindowState.Normal;
                }
            }
        }

        private void btn_SignOut_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("Are you sure you want to sign out?", "Sign Out", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            
            this.Hide();
            login.Show();
        }
    }
}
