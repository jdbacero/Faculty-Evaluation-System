﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FacultyEvaluationSystem
{
    public partial class frm_EvaluationPrincipalResult : Form
    {
        dataClass_FacultyEvaluationSystemDataContext db = new dataClass_FacultyEvaluationSystemDataContext();
        string schoolYear;
        int idSched;

        public frm_EvaluationPrincipalResult(string SchoolYear, int IdSched)
        {
            InitializeComponent();
            schoolYear = SchoolYear;
            idSched = IdSched;

            this.ActiveControl = label1;
        }

        private void frm_EvaluationPrincipalResult_Load(object sender, EventArgs e)
        {
            dgv_ScheduleFinished.DataSource = db.sp_ViewTeachersBySchoolYear(schoolYear);
            dgv_ScheduleFinished.Columns[0].Visible = false;

            dgv_ScheduleFinished.ClearSelection();
        }

        private void dgv_ScheduleFinished_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int idTeacher = (int)dgv_ScheduleFinished.CurrentRow.Cells[0].Value;
            int teacherTotal = 0;
            int total = 0;
            btn_Print.Enabled = true;
            lblScoreAssess.Visible = true;
            lblScoreAttributes.Visible = true;
            lblScoreClass.Visible = true;
            lblScoreInstruction.Visible = true;
            lblTotalAssess.Visible = true;
            lblTotalAttr.Visible = true;
            lblTotalClass.Visible = true;
            lblTotalInst.Visible = true;
            //lblWeightedScoreAssess.Visible = true;
            //lblWeightedScoreClass.Visible = true;
            //lblWeightedScoreInst.Visible = true;
            //lblWeightedTotalAssess.Visible = true;
            //lblWeightedTotalAttr.Visible = true;
            //lblWeightedTotalClass.Visible = true;
            //lblWeightedTotalInst.Visible = true;
            //lblWeigtedScroreAttr.Visible = true;

            foreach (var item in db.sp_ViewPrincipalEvaluationResultFinishedByTeacherID(idTeacher, idSched))
            {
                switch(item.Criteria)
                {
                    case "ASSESSMENT OF LEARNING":
                        lblScoreAssess.Text = Math.Round(((decimal)item.Score/(decimal)item.Total) * 0.1m, 2) + "";
                        lblTotalAssess.Text = "" + item.Score;
                        //lblWeightedScoreAssess.Text = "" + Math.Round((decimal)item.Score * 0.1m, 2);
                        //lblWeightedTotalAssess.Text = "" + Math.Round((decimal)item.Total * 0.1m, 2);
                        break;

                    case "CLASSROOM MANAGEMENT AND LEARNING ENVIRONMENT":
                        lblScoreClass.Text = Math.Round(((decimal)item.Score / (decimal)item.Total) * 0.2m, 2) + "";
                        lblTotalClass.Text = "" + item.Score;
                        //lblWeightedScoreClass.Text = "" + Math.Round((decimal)item.Score * 0.2m, 2);
                        //lblWeightedTotalClass.Text = "" + Math.Round((decimal)item.Total * 0.2m, 2);
                        break;

                    case "INSTRUCTIONAL DELIVERY":
                        lblScoreInstruction.Text = Math.Round(((decimal)item.Score / (decimal)item.Total) * 0.5m, 2) + "";
                        lblTotalInst.Text = "" + item.Score;
                        //lblWeightedScoreInst.Text = "" + Math.Round((decimal)item.Score * 0.5m, 2);
                        //lblWeightedTotalInst.Text = "" + Math.Round((decimal)item.Total * 0.5m, 2);
                        break;

                    default:
                        lblScoreAttributes.Text = Math.Round(((decimal)item.Score / (decimal)item.Total) * 0.2m, 2) + "";
                        lblTotalAttr.Text = "" + item.Score;
                        //lblWeigtedScroreAttr.Text = "" + Math.Round((decimal)item.Score * 0.2m, 2);
                        //lblWeightedTotalAttr.Text = "" + Math.Round((decimal)item.Total * 0.2m, 2);
                        break;
                }
            }

            if (db.sp_ViewEvaluationPrincipalResultOverall_BySchedIDTeacherID(idSched, idTeacher).Any())
            {

            }
            else
            {
                //db.sp_InsertEvaluationPrincipalResultOverall(idSched, idTeacher, decimal.Parse(lblScoreAttributes.Text),
                //    decimal.Parse(lblScoreInstruction.Text), decimal.Parse(lblScoreClass.Text), decimal.Parse(lblScoreAssess.Text),
                //    decimal.Parse(lblWeigtedScroreAttr.Text)/ decimal.Parse(lblWeightedTotalAttr.Text) * 100*0.2m, decimal.Parse(lblWeightedScoreInst.Text)/ decimal.Parse(lblWeightedTotalInst.Text) * 100*0.5m,
                //    decimal.Parse(lblWeightedScoreClass.Text)/ decimal.Parse(lblWeightedTotalClass.Text) * 100*0.2m, decimal.Parse(lblWeightedScoreAssess.Text)/ decimal.Parse(lblWeightedTotalAssess.Text) * 100*0.1m);
            }

            //txt_Assessment.Text = Math.Round(decimal.Parse(lblWeightedScoreAssess.Text) / decimal.Parse(lblWeightedTotalAssess.Text) * 100 * 0.1m,2)+"%";
            //txt_Attributes.Text = Math.Round(decimal.Parse(lblWeigtedScroreAttr.Text) / decimal.Parse(lblWeightedTotalAttr.Text) * 100 * 0.2m,2) + "%";
            //txt_Class.Text = Math.Round(decimal.Parse(lblWeightedScoreClass.Text) / decimal.Parse(lblWeightedTotalClass.Text) * 100 * 0.2m,2)+"%";
            //txt_Instructional.Text = Math.Round(decimal.Parse(lblWeightedScoreInst.Text) / decimal.Parse(lblWeightedTotalInst.Text) * 100 * 0.5m,2) + "%";
        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
            
        }

        private void btn_Print_Click(object sender, EventArgs e)
        {
            User_controls.frm_ReportsPrincipalEvaluation frm = new User_controls.frm_ReportsPrincipalEvaluation(idSched, (int)dgv_ScheduleFinished.CurrentRow.Cells[0].Value, "Individual");
            frm.ShowDialog();
        }

        private void btn_AllTeach_Click(object sender, EventArgs e)
        {
            User_controls.frm_ReportsPrincipalEvaluation frm = new User_controls.frm_ReportsPrincipalEvaluation(idSched, 0, "Jorex Gwapo");
            frm.ShowDialog();
        }

        private void lblWeigtedScroreAttr_Click(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void lblWeightedTotalInst_Click(object sender, EventArgs e)
        {

        }

        private void lblWeightedScoreInst_Click(object sender, EventArgs e)
        {

        }

        private void label19_Click(object sender, EventArgs e)
        {

        }

        private void label21_Click(object sender, EventArgs e)
        {

        }

        private void lblWeightedTotalClass_Click(object sender, EventArgs e)
        {

        }

        private void lblWeightedScoreClass_Click(object sender, EventArgs e)
        {

        }

        private void label35_Click(object sender, EventArgs e)
        {

        }

        private void lblWeightedTotalAttr_Click(object sender, EventArgs e)
        {

        }

        private void lblWeightedScoreAssess_Click(object sender, EventArgs e)
        {

        }

        private void lblWeightedTotalAssess_Click(object sender, EventArgs e)
        {

        }

        private void label37_Click(object sender, EventArgs e)
        {

        }

        private void label27_Click(object sender, EventArgs e)
        {

        }

        private void label29_Click(object sender, EventArgs e)
        {

        }
    }
}
