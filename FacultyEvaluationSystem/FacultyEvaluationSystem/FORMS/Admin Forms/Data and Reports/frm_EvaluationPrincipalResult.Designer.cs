﻿namespace FacultyEvaluationSystem
{
    partial class frm_EvaluationPrincipalResult
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_EvaluationPrincipalResult));
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.dgv_ScheduleFinished = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.bunifuSeparator1 = new Bunifu.Framework.UI.BunifuSeparator();
            this.bunifuSeparator2 = new Bunifu.Framework.UI.BunifuSeparator();
            this.bunifuSeparator3 = new Bunifu.Framework.UI.BunifuSeparator();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lblWeightedTotalAttr = new System.Windows.Forms.Label();
            this.lblTotalAttr = new System.Windows.Forms.Label();
            this.lblWeigtedScroreAttr = new System.Windows.Forms.Label();
            this.lblScoreAttributes = new System.Windows.Forms.Label();
            this.lblWeightedTotalInst = new System.Windows.Forms.Label();
            this.lblTotalInst = new System.Windows.Forms.Label();
            this.lblWeightedScoreInst = new System.Windows.Forms.Label();
            this.lblScoreInstruction = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.lblWeightedTotalAssess = new System.Windows.Forms.Label();
            this.lblTotalAssess = new System.Windows.Forms.Label();
            this.lblWeightedScoreAssess = new System.Windows.Forms.Label();
            this.lblScoreAssess = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.lblWeightedTotalClass = new System.Windows.Forms.Label();
            this.lblTotalClass = new System.Windows.Forms.Label();
            this.lblWeightedScoreClass = new System.Windows.Forms.Label();
            this.lblScoreClass = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.bunifuDragControl1 = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.btn_Close = new Bunifu.Framework.UI.BunifuImageButton();
            this.btn_Print = new Bunifu.Framework.UI.BunifuThinButton2();
            this.txt_Attributes = new System.Windows.Forms.TextBox();
            this.txt_Instructional = new System.Windows.Forms.TextBox();
            this.txt_Class = new System.Windows.Forms.TextBox();
            this.txt_Assessment = new System.Windows.Forms.TextBox();
            this.btn_AllTeach = new Bunifu.Framework.UI.BunifuThinButton2();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ScheduleFinished)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_Close)).BeginInit();
            this.SuspendLayout();
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 5;
            this.bunifuElipse1.TargetControl = this;
            // 
            // dgv_ScheduleFinished
            // 
            this.dgv_ScheduleFinished.AllowUserToAddRows = false;
            this.dgv_ScheduleFinished.AllowUserToDeleteRows = false;
            this.dgv_ScheduleFinished.AllowUserToResizeColumns = false;
            this.dgv_ScheduleFinished.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgv_ScheduleFinished.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_ScheduleFinished.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dgv_ScheduleFinished.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_ScheduleFinished.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.dgv_ScheduleFinished.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv_ScheduleFinished.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_ScheduleFinished.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_ScheduleFinished.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_ScheduleFinished.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgv_ScheduleFinished.DoubleBuffered = true;
            this.dgv_ScheduleFinished.EnableHeadersVisualStyles = false;
            this.dgv_ScheduleFinished.HeaderBgColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.dgv_ScheduleFinished.HeaderForeColor = System.Drawing.Color.White;
            this.dgv_ScheduleFinished.Location = new System.Drawing.Point(0, 35);
            this.dgv_ScheduleFinished.MultiSelect = false;
            this.dgv_ScheduleFinished.Name = "dgv_ScheduleFinished";
            this.dgv_ScheduleFinished.ReadOnly = true;
            this.dgv_ScheduleFinished.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_ScheduleFinished.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgv_ScheduleFinished.RowHeadersVisible = false;
            this.dgv_ScheduleFinished.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgv_ScheduleFinished.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_ScheduleFinished.Size = new System.Drawing.Size(1039, 185);
            this.dgv_ScheduleFinished.TabIndex = 3;
            this.dgv_ScheduleFinished.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_ScheduleFinished_CellClick);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(4, 253);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(210, 18);
            this.label1.TabIndex = 4;
            this.label1.Text = "TEACHER’S ATTRIBUTES (20%)";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(238, 253);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(236, 18);
            this.label2.TabIndex = 5;
            this.label2.Text = "INSTRUCTIONAL DELIVERY (50%)";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(784, 253);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(200, 18);
            this.label3.TabIndex = 9;
            this.label3.Text = "ASSESSMENT OF LEARNING";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(520, 239);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(170, 48);
            this.label4.TabIndex = 8;
            this.label4.Text = "CLASSROOM MANAGEMENT\nAND\nLEARNING ENVIRONMENT";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bunifuSeparator1
            // 
            this.bunifuSeparator1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.bunifuSeparator1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuSeparator1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))));
            this.bunifuSeparator1.LineThickness = 1;
            this.bunifuSeparator1.Location = new System.Drawing.Point(211, 236);
            this.bunifuSeparator1.Name = "bunifuSeparator1";
            this.bunifuSeparator1.Size = new System.Drawing.Size(21, 178);
            this.bunifuSeparator1.TabIndex = 10;
            this.bunifuSeparator1.Transparency = 255;
            this.bunifuSeparator1.Vertical = true;
            // 
            // bunifuSeparator2
            // 
            this.bunifuSeparator2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.bunifuSeparator2.BackColor = System.Drawing.Color.Transparent;
            this.bunifuSeparator2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))));
            this.bunifuSeparator2.LineThickness = 1;
            this.bunifuSeparator2.Location = new System.Drawing.Point(468, 236);
            this.bunifuSeparator2.Name = "bunifuSeparator2";
            this.bunifuSeparator2.Size = new System.Drawing.Size(30, 178);
            this.bunifuSeparator2.TabIndex = 11;
            this.bunifuSeparator2.Transparency = 255;
            this.bunifuSeparator2.Vertical = true;
            // 
            // bunifuSeparator3
            // 
            this.bunifuSeparator3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.bunifuSeparator3.BackColor = System.Drawing.Color.Transparent;
            this.bunifuSeparator3.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))));
            this.bunifuSeparator3.LineThickness = 1;
            this.bunifuSeparator3.Location = new System.Drawing.Point(759, 236);
            this.bunifuSeparator3.Name = "bunifuSeparator3";
            this.bunifuSeparator3.Size = new System.Drawing.Size(30, 178);
            this.bunifuSeparator3.TabIndex = 12;
            this.bunifuSeparator3.Transparency = 255;
            this.bunifuSeparator3.Vertical = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(32, 307);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 12);
            this.label5.TabIndex = 13;
            this.label5.Text = "Score";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(684, 253);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 18);
            this.label6.TabIndex = 14;
            this.label6.Text = "(20%)";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(990, 251);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 18);
            this.label7.TabIndex = 15;
            this.label7.Text = "(10%)";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(12, 365);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 12);
            this.label8.TabIndex = 16;
            this.label8.Text = "Weighted Score";
            this.label8.Visible = false;
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(145, 309);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(25, 12);
            this.label9.TabIndex = 17;
            this.label9.Text = "Total";
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.Location = new System.Drawing.Point(127, 367);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(66, 12);
            this.label10.TabIndex = 18;
            this.label10.Text = "Weighted Total";
            this.label10.Visible = false;
            this.label10.Click += new System.EventHandler(this.label10_Click);
            // 
            // lblWeightedTotalAttr
            // 
            this.lblWeightedTotalAttr.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblWeightedTotalAttr.AutoSize = true;
            this.lblWeightedTotalAttr.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblWeightedTotalAttr.Location = new System.Drawing.Point(154, 354);
            this.lblWeightedTotalAttr.Name = "lblWeightedTotalAttr";
            this.lblWeightedTotalAttr.Size = new System.Drawing.Size(80, 13);
            this.lblWeightedTotalAttr.TabIndex = 22;
            this.lblWeightedTotalAttr.Text = "Weighted Total";
            this.lblWeightedTotalAttr.Visible = false;
            this.lblWeightedTotalAttr.Click += new System.EventHandler(this.lblWeightedTotalAttr_Click);
            // 
            // lblTotalAttr
            // 
            this.lblTotalAttr.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblTotalAttr.AutoSize = true;
            this.lblTotalAttr.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblTotalAttr.Location = new System.Drawing.Point(149, 296);
            this.lblTotalAttr.Name = "lblTotalAttr";
            this.lblTotalAttr.Size = new System.Drawing.Size(31, 13);
            this.lblTotalAttr.TabIndex = 21;
            this.lblTotalAttr.Text = "Total";
            this.lblTotalAttr.Visible = false;
            // 
            // lblWeigtedScroreAttr
            // 
            this.lblWeigtedScroreAttr.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblWeigtedScroreAttr.AutoSize = true;
            this.lblWeigtedScroreAttr.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblWeigtedScroreAttr.Location = new System.Drawing.Point(37, 354);
            this.lblWeigtedScroreAttr.Name = "lblWeigtedScroreAttr";
            this.lblWeigtedScroreAttr.Size = new System.Drawing.Size(84, 13);
            this.lblWeigtedScroreAttr.TabIndex = 20;
            this.lblWeigtedScroreAttr.Text = "Weighted Score";
            this.lblWeigtedScroreAttr.Visible = false;
            this.lblWeigtedScroreAttr.Click += new System.EventHandler(this.lblWeigtedScroreAttr_Click);
            // 
            // lblScoreAttributes
            // 
            this.lblScoreAttributes.AutoSize = true;
            this.lblScoreAttributes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblScoreAttributes.Location = new System.Drawing.Point(37, 294);
            this.lblScoreAttributes.Name = "lblScoreAttributes";
            this.lblScoreAttributes.Size = new System.Drawing.Size(35, 13);
            this.lblScoreAttributes.TabIndex = 19;
            this.lblScoreAttributes.Text = "Score";
            this.lblScoreAttributes.Visible = false;
            // 
            // lblWeightedTotalInst
            // 
            this.lblWeightedTotalInst.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblWeightedTotalInst.AutoSize = true;
            this.lblWeightedTotalInst.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblWeightedTotalInst.Location = new System.Drawing.Point(399, 354);
            this.lblWeightedTotalInst.Name = "lblWeightedTotalInst";
            this.lblWeightedTotalInst.Size = new System.Drawing.Size(80, 13);
            this.lblWeightedTotalInst.TabIndex = 30;
            this.lblWeightedTotalInst.Text = "Weighted Total";
            this.lblWeightedTotalInst.Visible = false;
            this.lblWeightedTotalInst.Click += new System.EventHandler(this.lblWeightedTotalInst_Click);
            // 
            // lblTotalInst
            // 
            this.lblTotalInst.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblTotalInst.AutoSize = true;
            this.lblTotalInst.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblTotalInst.Location = new System.Drawing.Point(394, 296);
            this.lblTotalInst.Name = "lblTotalInst";
            this.lblTotalInst.Size = new System.Drawing.Size(31, 13);
            this.lblTotalInst.TabIndex = 29;
            this.lblTotalInst.Text = "Total";
            this.lblTotalInst.Visible = false;
            // 
            // lblWeightedScoreInst
            // 
            this.lblWeightedScoreInst.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblWeightedScoreInst.AutoSize = true;
            this.lblWeightedScoreInst.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblWeightedScoreInst.Location = new System.Drawing.Point(286, 354);
            this.lblWeightedScoreInst.Name = "lblWeightedScoreInst";
            this.lblWeightedScoreInst.Size = new System.Drawing.Size(84, 13);
            this.lblWeightedScoreInst.TabIndex = 28;
            this.lblWeightedScoreInst.Text = "Weighted Score";
            this.lblWeightedScoreInst.Visible = false;
            this.lblWeightedScoreInst.Click += new System.EventHandler(this.lblWeightedScoreInst_Click);
            // 
            // lblScoreInstruction
            // 
            this.lblScoreInstruction.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblScoreInstruction.AutoSize = true;
            this.lblScoreInstruction.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblScoreInstruction.Location = new System.Drawing.Point(282, 296);
            this.lblScoreInstruction.Name = "lblScoreInstruction";
            this.lblScoreInstruction.Size = new System.Drawing.Size(35, 13);
            this.lblScoreInstruction.TabIndex = 27;
            this.lblScoreInstruction.Text = "Score";
            this.lblScoreInstruction.Visible = false;
            // 
            // label19
            // 
            this.label19.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label19.Location = new System.Drawing.Point(372, 367);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(66, 12);
            this.label19.TabIndex = 26;
            this.label19.Text = "Weighted Total";
            this.label19.Visible = false;
            this.label19.Click += new System.EventHandler(this.label19_Click);
            // 
            // label20
            // 
            this.label20.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label20.Location = new System.Drawing.Point(390, 309);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(25, 12);
            this.label20.TabIndex = 25;
            this.label20.Text = "Total";
            // 
            // label21
            // 
            this.label21.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label21.Location = new System.Drawing.Point(257, 367);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(70, 12);
            this.label21.TabIndex = 24;
            this.label21.Text = "Weighted Score";
            this.label21.Visible = false;
            this.label21.Click += new System.EventHandler(this.label21_Click);
            // 
            // label22
            // 
            this.label22.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label22.Location = new System.Drawing.Point(277, 309);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(29, 12);
            this.label22.TabIndex = 23;
            this.label22.Text = "Score";
            // 
            // lblWeightedTotalAssess
            // 
            this.lblWeightedTotalAssess.AutoSize = true;
            this.lblWeightedTotalAssess.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblWeightedTotalAssess.Location = new System.Drawing.Point(963, 362);
            this.lblWeightedTotalAssess.Name = "lblWeightedTotalAssess";
            this.lblWeightedTotalAssess.Size = new System.Drawing.Size(80, 13);
            this.lblWeightedTotalAssess.TabIndex = 38;
            this.lblWeightedTotalAssess.Text = "Weighted Total";
            this.lblWeightedTotalAssess.Visible = false;
            this.lblWeightedTotalAssess.Click += new System.EventHandler(this.lblWeightedTotalAssess_Click);
            // 
            // lblTotalAssess
            // 
            this.lblTotalAssess.AutoSize = true;
            this.lblTotalAssess.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblTotalAssess.Location = new System.Drawing.Point(958, 304);
            this.lblTotalAssess.Name = "lblTotalAssess";
            this.lblTotalAssess.Size = new System.Drawing.Size(31, 13);
            this.lblTotalAssess.TabIndex = 37;
            this.lblTotalAssess.Text = "Total";
            this.lblTotalAssess.Visible = false;
            // 
            // lblWeightedScoreAssess
            // 
            this.lblWeightedScoreAssess.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblWeightedScoreAssess.AutoSize = true;
            this.lblWeightedScoreAssess.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblWeightedScoreAssess.Location = new System.Drawing.Point(850, 354);
            this.lblWeightedScoreAssess.Name = "lblWeightedScoreAssess";
            this.lblWeightedScoreAssess.Size = new System.Drawing.Size(84, 13);
            this.lblWeightedScoreAssess.TabIndex = 36;
            this.lblWeightedScoreAssess.Text = "Weighted Score";
            this.lblWeightedScoreAssess.Visible = false;
            this.lblWeightedScoreAssess.Click += new System.EventHandler(this.lblWeightedScoreAssess_Click);
            // 
            // lblScoreAssess
            // 
            this.lblScoreAssess.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblScoreAssess.AutoSize = true;
            this.lblScoreAssess.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblScoreAssess.Location = new System.Drawing.Point(846, 296);
            this.lblScoreAssess.Name = "lblScoreAssess";
            this.lblScoreAssess.Size = new System.Drawing.Size(35, 13);
            this.lblScoreAssess.TabIndex = 35;
            this.lblScoreAssess.Text = "Score";
            this.lblScoreAssess.Visible = false;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label27.Location = new System.Drawing.Point(936, 375);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(66, 12);
            this.label27.TabIndex = 34;
            this.label27.Text = "Weighted Total";
            this.label27.Visible = false;
            this.label27.Click += new System.EventHandler(this.label27_Click);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label28.Location = new System.Drawing.Point(954, 317);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(25, 12);
            this.label28.TabIndex = 33;
            this.label28.Text = "Total";
            // 
            // label29
            // 
            this.label29.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label29.Location = new System.Drawing.Point(821, 367);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(70, 12);
            this.label29.TabIndex = 32;
            this.label29.Text = "Weighted Score";
            this.label29.Visible = false;
            this.label29.Click += new System.EventHandler(this.label29_Click);
            // 
            // label30
            // 
            this.label30.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label30.Location = new System.Drawing.Point(841, 309);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(29, 12);
            this.label30.TabIndex = 31;
            this.label30.Text = "Score";
            // 
            // lblWeightedTotalClass
            // 
            this.lblWeightedTotalClass.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblWeightedTotalClass.AutoSize = true;
            this.lblWeightedTotalClass.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblWeightedTotalClass.Location = new System.Drawing.Point(674, 354);
            this.lblWeightedTotalClass.Name = "lblWeightedTotalClass";
            this.lblWeightedTotalClass.Size = new System.Drawing.Size(80, 13);
            this.lblWeightedTotalClass.TabIndex = 46;
            this.lblWeightedTotalClass.Text = "Weighted Total";
            this.lblWeightedTotalClass.Visible = false;
            this.lblWeightedTotalClass.Click += new System.EventHandler(this.lblWeightedTotalClass_Click);
            // 
            // lblTotalClass
            // 
            this.lblTotalClass.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblTotalClass.AutoSize = true;
            this.lblTotalClass.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblTotalClass.Location = new System.Drawing.Point(669, 296);
            this.lblTotalClass.Name = "lblTotalClass";
            this.lblTotalClass.Size = new System.Drawing.Size(31, 13);
            this.lblTotalClass.TabIndex = 45;
            this.lblTotalClass.Text = "Total";
            this.lblTotalClass.Visible = false;
            // 
            // lblWeightedScoreClass
            // 
            this.lblWeightedScoreClass.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblWeightedScoreClass.AutoSize = true;
            this.lblWeightedScoreClass.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblWeightedScoreClass.Location = new System.Drawing.Point(561, 354);
            this.lblWeightedScoreClass.Name = "lblWeightedScoreClass";
            this.lblWeightedScoreClass.Size = new System.Drawing.Size(84, 13);
            this.lblWeightedScoreClass.TabIndex = 44;
            this.lblWeightedScoreClass.Text = "Weighted Score";
            this.lblWeightedScoreClass.Visible = false;
            this.lblWeightedScoreClass.Click += new System.EventHandler(this.lblWeightedScoreClass_Click);
            // 
            // lblScoreClass
            // 
            this.lblScoreClass.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblScoreClass.AutoSize = true;
            this.lblScoreClass.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblScoreClass.Location = new System.Drawing.Point(557, 296);
            this.lblScoreClass.Name = "lblScoreClass";
            this.lblScoreClass.Size = new System.Drawing.Size(35, 13);
            this.lblScoreClass.TabIndex = 43;
            this.lblScoreClass.Text = "Score";
            this.lblScoreClass.Visible = false;
            // 
            // label35
            // 
            this.label35.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label35.Location = new System.Drawing.Point(647, 367);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(66, 12);
            this.label35.TabIndex = 42;
            this.label35.Text = "Weighted Total";
            this.label35.Visible = false;
            this.label35.Click += new System.EventHandler(this.label35_Click);
            // 
            // label36
            // 
            this.label36.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label36.Location = new System.Drawing.Point(665, 309);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(25, 12);
            this.label36.TabIndex = 41;
            this.label36.Text = "Total";
            // 
            // label37
            // 
            this.label37.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label37.Location = new System.Drawing.Point(532, 367);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(70, 12);
            this.label37.TabIndex = 40;
            this.label37.Text = "Weighted Score";
            this.label37.Visible = false;
            this.label37.Click += new System.EventHandler(this.label37_Click);
            // 
            // label38
            // 
            this.label38.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label38.Location = new System.Drawing.Point(552, 309);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(29, 12);
            this.label38.TabIndex = 39;
            this.label38.Text = "Score";
            // 
            // bunifuDragControl1
            // 
            this.bunifuDragControl1.Fixed = true;
            this.bunifuDragControl1.Horizontal = true;
            this.bunifuDragControl1.TargetControl = this;
            this.bunifuDragControl1.Vertical = true;
            // 
            // btn_Close
            // 
            this.btn_Close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Close.BackColor = System.Drawing.Color.Transparent;
            this.btn_Close.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Close.Image = ((System.Drawing.Image)(resources.GetObject("btn_Close.Image")));
            this.btn_Close.ImageActive = null;
            this.btn_Close.InitialImage = null;
            this.btn_Close.Location = new System.Drawing.Point(1008, 1);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(30, 30);
            this.btn_Close.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btn_Close.TabIndex = 47;
            this.btn_Close.TabStop = false;
            this.btn_Close.Zoom = 10;
            this.btn_Close.Click += new System.EventHandler(this.btn_Close_Click);
            // 
            // btn_Print
            // 
            this.btn_Print.ActiveBorderThickness = 1;
            this.btn_Print.ActiveCornerRadius = 20;
            this.btn_Print.ActiveFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_Print.ActiveForecolor = System.Drawing.Color.White;
            this.btn_Print.ActiveLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_Print.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btn_Print.BackColor = System.Drawing.SystemColors.Control;
            this.btn_Print.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_Print.BackgroundImage")));
            this.btn_Print.ButtonText = "Print";
            this.btn_Print.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Print.Enabled = false;
            this.btn_Print.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Print.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_Print.IdleBorderThickness = 1;
            this.btn_Print.IdleCornerRadius = 20;
            this.btn_Print.IdleFillColor = System.Drawing.Color.White;
            this.btn_Print.IdleForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_Print.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_Print.Location = new System.Drawing.Point(411, 439);
            this.btn_Print.Margin = new System.Windows.Forms.Padding(5);
            this.btn_Print.Name = "btn_Print";
            this.btn_Print.Size = new System.Drawing.Size(181, 41);
            this.btn_Print.TabIndex = 48;
            this.btn_Print.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn_Print.Click += new System.EventHandler(this.btn_Print_Click);
            // 
            // txt_Attributes
            // 
            this.txt_Attributes.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txt_Attributes.Enabled = false;
            this.txt_Attributes.Location = new System.Drawing.Point(99, 384);
            this.txt_Attributes.Name = "txt_Attributes";
            this.txt_Attributes.Size = new System.Drawing.Size(43, 20);
            this.txt_Attributes.TabIndex = 49;
            // 
            // txt_Instructional
            // 
            this.txt_Instructional.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txt_Instructional.Enabled = false;
            this.txt_Instructional.Location = new System.Drawing.Point(327, 394);
            this.txt_Instructional.Name = "txt_Instructional";
            this.txt_Instructional.Size = new System.Drawing.Size(43, 20);
            this.txt_Instructional.TabIndex = 50;
            // 
            // txt_Class
            // 
            this.txt_Class.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txt_Class.Enabled = false;
            this.txt_Class.Location = new System.Drawing.Point(602, 394);
            this.txt_Class.Name = "txt_Class";
            this.txt_Class.Size = new System.Drawing.Size(43, 20);
            this.txt_Class.TabIndex = 51;
            // 
            // txt_Assessment
            // 
            this.txt_Assessment.Enabled = false;
            this.txt_Assessment.Location = new System.Drawing.Point(900, 402);
            this.txt_Assessment.Name = "txt_Assessment";
            this.txt_Assessment.Size = new System.Drawing.Size(43, 20);
            this.txt_Assessment.TabIndex = 52;
            // 
            // btn_AllTeach
            // 
            this.btn_AllTeach.ActiveBorderThickness = 1;
            this.btn_AllTeach.ActiveCornerRadius = 20;
            this.btn_AllTeach.ActiveFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_AllTeach.ActiveForecolor = System.Drawing.Color.White;
            this.btn_AllTeach.ActiveLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_AllTeach.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btn_AllTeach.BackColor = System.Drawing.SystemColors.Control;
            this.btn_AllTeach.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_AllTeach.BackgroundImage")));
            this.btn_AllTeach.ButtonText = "Print All Teachers";
            this.btn_AllTeach.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_AllTeach.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_AllTeach.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_AllTeach.IdleBorderThickness = 1;
            this.btn_AllTeach.IdleCornerRadius = 20;
            this.btn_AllTeach.IdleFillColor = System.Drawing.Color.White;
            this.btn_AllTeach.IdleForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_AllTeach.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_AllTeach.Location = new System.Drawing.Point(880, 469);
            this.btn_AllTeach.Margin = new System.Windows.Forms.Padding(5);
            this.btn_AllTeach.Name = "btn_AllTeach";
            this.btn_AllTeach.Size = new System.Drawing.Size(158, 27);
            this.btn_AllTeach.TabIndex = 53;
            this.btn_AllTeach.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn_AllTeach.Click += new System.EventHandler(this.btn_AllTeach_Click);
            // 
            // frm_EvaluationPrincipalResult
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1040, 500);
            this.Controls.Add(this.btn_AllTeach);
            this.Controls.Add(this.txt_Assessment);
            this.Controls.Add(this.txt_Class);
            this.Controls.Add(this.txt_Instructional);
            this.Controls.Add(this.txt_Attributes);
            this.Controls.Add(this.btn_Print);
            this.Controls.Add(this.btn_Close);
            this.Controls.Add(this.lblWeightedTotalClass);
            this.Controls.Add(this.lblTotalClass);
            this.Controls.Add(this.lblWeightedScoreClass);
            this.Controls.Add(this.lblScoreClass);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.lblWeightedTotalAssess);
            this.Controls.Add(this.lblTotalAssess);
            this.Controls.Add(this.lblWeightedScoreAssess);
            this.Controls.Add(this.lblScoreAssess);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.lblWeightedTotalInst);
            this.Controls.Add(this.lblTotalInst);
            this.Controls.Add(this.lblWeightedScoreInst);
            this.Controls.Add(this.lblScoreInstruction);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.lblWeightedTotalAttr);
            this.Controls.Add(this.lblTotalAttr);
            this.Controls.Add(this.lblWeigtedScroreAttr);
            this.Controls.Add(this.lblScoreAttributes);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.bunifuSeparator3);
            this.Controls.Add(this.bunifuSeparator2);
            this.Controls.Add(this.bunifuSeparator1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgv_ScheduleFinished);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frm_EvaluationPrincipalResult";
            this.Text = "frm_EvaluationPrincipalResult";
            this.Load += new System.EventHandler(this.frm_EvaluationPrincipalResult_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ScheduleFinished)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_Close)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private Bunifu.Framework.UI.BunifuCustomDataGrid dgv_ScheduleFinished;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator3;
        private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator2;
        private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblWeightedTotalClass;
        private System.Windows.Forms.Label lblTotalClass;
        private System.Windows.Forms.Label lblWeightedScoreClass;
        private System.Windows.Forms.Label lblScoreClass;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label lblWeightedTotalAssess;
        private System.Windows.Forms.Label lblTotalAssess;
        private System.Windows.Forms.Label lblWeightedScoreAssess;
        private System.Windows.Forms.Label lblScoreAssess;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label lblWeightedTotalInst;
        private System.Windows.Forms.Label lblTotalInst;
        private System.Windows.Forms.Label lblWeightedScoreInst;
        private System.Windows.Forms.Label lblScoreInstruction;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label lblWeightedTotalAttr;
        private System.Windows.Forms.Label lblTotalAttr;
        private System.Windows.Forms.Label lblWeigtedScroreAttr;
        private System.Windows.Forms.Label lblScoreAttributes;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private Bunifu.Framework.UI.BunifuDragControl bunifuDragControl1;
        private Bunifu.Framework.UI.BunifuImageButton btn_Close;
        private Bunifu.Framework.UI.BunifuThinButton2 btn_Print;
        private System.Windows.Forms.TextBox txt_Assessment;
        private System.Windows.Forms.TextBox txt_Class;
        private System.Windows.Forms.TextBox txt_Instructional;
        private System.Windows.Forms.TextBox txt_Attributes;
        private Bunifu.Framework.UI.BunifuThinButton2 btn_AllTeach;
    }
}