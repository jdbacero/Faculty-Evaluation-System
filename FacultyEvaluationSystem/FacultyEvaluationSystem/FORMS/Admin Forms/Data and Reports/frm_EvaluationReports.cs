﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FacultyEvaluationSystem
{
    public partial class frm_EvaluationReports : Form
    {
        public frm_EvaluationReports()
        {
            InitializeComponent();
        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_Evaluation_Click(object sender, EventArgs e)
        {
            frm_SelectSchedule frm = new frm_SelectSchedule("Student");
            frm.ShowDialog();
            this.Close();
        }

        private void btn_Principal_Click(object sender, EventArgs e)
        {
            frm_SelectSchedule frm = new frm_SelectSchedule("Principal");
            frm.ShowDialog();
            this.Close();
        }

        private void btn_Comments_Click(object sender, EventArgs e)
        {
            frm_SelectSchedule frm = new frm_SelectSchedule("Comment");
            frm.ShowDialog();
            this.Close();
        }
    }
}
