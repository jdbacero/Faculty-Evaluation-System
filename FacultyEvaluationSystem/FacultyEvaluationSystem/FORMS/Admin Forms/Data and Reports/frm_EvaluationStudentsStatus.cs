﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FacultyEvaluationSystem
{
    public partial class frm_EvaluationStudentsStatus : Form
    {
        dataClass_FacultyEvaluationSystemDataContext db = new dataClass_FacultyEvaluationSystemDataContext();

        public frm_EvaluationStudentsStatus()
        {
            InitializeComponent();
        }

        private void btn_Evaluate_Click(object sender, EventArgs e)
        {
            bool ongoing = false;
            int idSched = 0;

            foreach (var sched in db.sp_ViewEvaluationScheduleStudent())
            {
                if(sched.Status == "Ongoing")
                {
                    ongoing = true;
                    idSched = sched.ID;
                }
            }

            if(ongoing)
            {
                frm_ReportStudentsThatEvaluated frm_report = new frm_ReportStudentsThatEvaluated(idSched);
                frm_report.ShowDialog();
                this.Close();
            }
            else
            {
                MessageBox.Show("There are no ongoing student evaluation at the moment.");
            }
        }

        private void btn_Evaluatent_Click(object sender, EventArgs e)
        {
            bool ongoing = false;
            int idSched = 0;

            foreach (var sched in db.sp_ViewEvaluationScheduleStudent())
            {
                if (sched.Status == "Ongoing")
                {
                    ongoing = true;
                    idSched = sched.ID;
                }
            }

            if (ongoing)
            {
                frm_ReportStudentsThatHaveEvaluatednt frm_report = new frm_ReportStudentsThatHaveEvaluatednt(idSched);
                frm_report.ShowDialog();
                this.Close();
            }
            else
            {
                MessageBox.Show("There are no ongoing student evaluation at the moment.");
            }
        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}