﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FacultyEvaluationSystem
{
    public partial class frm_SelectSchedule : Form
    {
        dataClass_FacultyEvaluationSystemDataContext db = new dataClass_FacultyEvaluationSystemDataContext();
        string eval;
        public frm_SelectSchedule(string Eval)
        {
            InitializeComponent();
            eval = Eval;
        }

        private void frm_SelectSchedule_Load(object sender, EventArgs e)
        {
            if (eval == "Principal")
            {
                dgv_ScheduleFinished.DataSource = db.sp_ViewFinishedPrincipalEval();
                dgv_ScheduleFinished.Columns[0].Visible = false;
                dgv_ScheduleFinished.Columns[1].Visible = false;
                dgv_ScheduleFinished.Columns[5].Visible = false;
            }
            else if (eval == "Comment")
            {
                dgv_ScheduleFinished.DataSource = db.sp_ViewFinishedStudentEval();
                dgv_ScheduleFinished.Columns[0].Visible = false;
                dgv_ScheduleFinished.Columns[1].Visible = false;
                dgv_ScheduleFinished.Columns[5].Visible = false;
            } else
            {
                dgv_ScheduleFinished.DataSource = db.sp_ViewFinishedStudentEval();
                dgv_ScheduleFinished.Columns[0].Visible = false;
                dgv_ScheduleFinished.Columns[1].Visible = false;
                dgv_ScheduleFinished.Columns[5].Visible = false;
            }
        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgv_ScheduleFinished_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if(eval=="Principal")
            {
                frm_EvaluationPrincipalResult frm = new frm_EvaluationPrincipalResult(dgv_ScheduleFinished.CurrentRow.Cells[4].Value.ToString(), (int)dgv_ScheduleFinished.CurrentRow.Cells[0].Value);
                frm.Show();
            }
            else if (eval == "Comment")
            {
                frm_SelectTeacher frm = new frm_SelectTeacher(dgv_ScheduleFinished.CurrentRow.Cells[4].Value.ToString(), (int)dgv_ScheduleFinished.CurrentRow.Cells[0].Value, "Comments");
                frm.Show();
            }
            else
            {
                frm_SelectTeacher frm = new frm_SelectTeacher(dgv_ScheduleFinished.CurrentRow.Cells[4].Value.ToString(), (int)dgv_ScheduleFinished.CurrentRow.Cells[0].Value, "Students");
                frm.Show();
            }
        }
    }
}
