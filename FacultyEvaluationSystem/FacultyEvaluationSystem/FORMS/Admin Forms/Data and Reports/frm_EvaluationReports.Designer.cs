﻿namespace FacultyEvaluationSystem
{
    partial class frm_EvaluationReports
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_EvaluationReports));
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.btn_Evaluation = new Bunifu.Framework.UI.BunifuTileButton();
            this.btn_Principal = new Bunifu.Framework.UI.BunifuTileButton();
            this.btn_Comments = new Bunifu.Framework.UI.BunifuTileButton();
            this.btn_Close = new Bunifu.Framework.UI.BunifuImageButton();
            this.bunifuDragControl1 = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.pnl_Header = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.btn_Close)).BeginInit();
            this.SuspendLayout();
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 3;
            this.bunifuElipse1.TargetControl = this;
            // 
            // btn_Evaluation
            // 
            this.btn_Evaluation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_Evaluation.color = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_Evaluation.colorActive = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(88)))), ((int)(((byte)(130)))));
            this.btn_Evaluation.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Evaluation.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_Evaluation.ForeColor = System.Drawing.Color.White;
            this.btn_Evaluation.Image = ((System.Drawing.Image)(resources.GetObject("btn_Evaluation.Image")));
            this.btn_Evaluation.ImagePosition = 17;
            this.btn_Evaluation.ImageZoom = 40;
            this.btn_Evaluation.LabelPosition = 50;
            this.btn_Evaluation.LabelText = "Student\'s Evaluation";
            this.btn_Evaluation.Location = new System.Drawing.Point(13, 39);
            this.btn_Evaluation.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Evaluation.Name = "btn_Evaluation";
            this.btn_Evaluation.Size = new System.Drawing.Size(131, 117);
            this.btn_Evaluation.TabIndex = 0;
            this.btn_Evaluation.Click += new System.EventHandler(this.btn_Evaluation_Click);
            // 
            // btn_Principal
            // 
            this.btn_Principal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_Principal.color = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_Principal.colorActive = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(88)))), ((int)(((byte)(130)))));
            this.btn_Principal.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Principal.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_Principal.ForeColor = System.Drawing.Color.White;
            this.btn_Principal.Image = ((System.Drawing.Image)(resources.GetObject("btn_Principal.Image")));
            this.btn_Principal.ImagePosition = 17;
            this.btn_Principal.ImageZoom = 40;
            this.btn_Principal.LabelPosition = 50;
            this.btn_Principal.LabelText = "Principal\'s Evaluation";
            this.btn_Principal.Location = new System.Drawing.Point(152, 39);
            this.btn_Principal.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Principal.Name = "btn_Principal";
            this.btn_Principal.Size = new System.Drawing.Size(131, 117);
            this.btn_Principal.TabIndex = 1;
            this.btn_Principal.Click += new System.EventHandler(this.btn_Principal_Click);
            // 
            // btn_Comments
            // 
            this.btn_Comments.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_Comments.color = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_Comments.colorActive = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(88)))), ((int)(((byte)(130)))));
            this.btn_Comments.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Comments.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_Comments.ForeColor = System.Drawing.Color.White;
            this.btn_Comments.Image = ((System.Drawing.Image)(resources.GetObject("btn_Comments.Image")));
            this.btn_Comments.ImagePosition = 17;
            this.btn_Comments.ImageZoom = 40;
            this.btn_Comments.LabelPosition = 35;
            this.btn_Comments.LabelText = "Comments";
            this.btn_Comments.Location = new System.Drawing.Point(291, 39);
            this.btn_Comments.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Comments.Name = "btn_Comments";
            this.btn_Comments.Size = new System.Drawing.Size(131, 117);
            this.btn_Comments.TabIndex = 2;
            this.btn_Comments.Click += new System.EventHandler(this.btn_Comments_Click);
            // 
            // btn_Close
            // 
            this.btn_Close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Close.BackColor = System.Drawing.Color.Transparent;
            this.btn_Close.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Close.Image = ((System.Drawing.Image)(resources.GetObject("btn_Close.Image")));
            this.btn_Close.ImageActive = null;
            this.btn_Close.InitialImage = null;
            this.btn_Close.Location = new System.Drawing.Point(408, 1);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(30, 30);
            this.btn_Close.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btn_Close.TabIndex = 33;
            this.btn_Close.TabStop = false;
            this.btn_Close.Zoom = 10;
            this.btn_Close.Click += new System.EventHandler(this.btn_Close_Click);
            // 
            // bunifuDragControl1
            // 
            this.bunifuDragControl1.Fixed = true;
            this.bunifuDragControl1.Horizontal = true;
            this.bunifuDragControl1.TargetControl = this.pnl_Header;
            this.bunifuDragControl1.Vertical = true;
            // 
            // pnl_Header
            // 
            this.pnl_Header.Location = new System.Drawing.Point(1, 1);
            this.pnl_Header.Name = "pnl_Header";
            this.pnl_Header.Size = new System.Drawing.Size(437, 30);
            this.pnl_Header.TabIndex = 62;
            // 
            // frm_EvaluationReports
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(440, 175);
            this.Controls.Add(this.btn_Close);
            this.Controls.Add(this.btn_Comments);
            this.Controls.Add(this.btn_Principal);
            this.Controls.Add(this.btn_Evaluation);
            this.Controls.Add(this.pnl_Header);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frm_EvaluationReports";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frm_FinishedEvaluation";
            ((System.ComponentModel.ISupportInitialize)(this.btn_Close)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private Bunifu.Framework.UI.BunifuTileButton btn_Principal;
        private Bunifu.Framework.UI.BunifuTileButton btn_Evaluation;
        private Bunifu.Framework.UI.BunifuTileButton btn_Comments;
        private Bunifu.Framework.UI.BunifuImageButton btn_Close;
        private Bunifu.Framework.UI.BunifuDragControl bunifuDragControl1;
        private System.Windows.Forms.Panel pnl_Header;
    }
}