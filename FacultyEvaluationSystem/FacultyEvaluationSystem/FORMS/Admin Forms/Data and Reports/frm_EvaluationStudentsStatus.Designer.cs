﻿namespace FacultyEvaluationSystem
{
    partial class frm_EvaluationStudentsStatus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_EvaluationStudentsStatus));
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.btn_Evaluatent = new Bunifu.Framework.UI.BunifuTileButton();
            this.btn_Evaluate = new Bunifu.Framework.UI.BunifuTileButton();
            this.btn_Close = new Bunifu.Framework.UI.BunifuImageButton();
            this.pnl_Header = new System.Windows.Forms.Panel();
            this.bunifuDragControl1 = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.btn_Close)).BeginInit();
            this.SuspendLayout();
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 5;
            this.bunifuElipse1.TargetControl = this;
            // 
            // btn_Evaluatent
            // 
            this.btn_Evaluatent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_Evaluatent.color = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_Evaluatent.colorActive = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(88)))), ((int)(((byte)(130)))));
            this.btn_Evaluatent.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Evaluatent.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_Evaluatent.ForeColor = System.Drawing.Color.White;
            this.btn_Evaluatent.Image = ((System.Drawing.Image)(resources.GetObject("btn_Evaluatent.Image")));
            this.btn_Evaluatent.ImagePosition = 15;
            this.btn_Evaluatent.ImageZoom = 40;
            this.btn_Evaluatent.LabelPosition = 43;
            this.btn_Evaluatent.LabelText = "Students that are yet to Evaluate";
            this.btn_Evaluatent.Location = new System.Drawing.Point(237, 41);
            this.btn_Evaluatent.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn_Evaluatent.Name = "btn_Evaluatent";
            this.btn_Evaluatent.Size = new System.Drawing.Size(131, 117);
            this.btn_Evaluatent.TabIndex = 3;
            this.btn_Evaluatent.Click += new System.EventHandler(this.btn_Evaluatent_Click);
            // 
            // btn_Evaluate
            // 
            this.btn_Evaluate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_Evaluate.color = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_Evaluate.colorActive = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(88)))), ((int)(((byte)(130)))));
            this.btn_Evaluate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Evaluate.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_Evaluate.ForeColor = System.Drawing.Color.White;
            this.btn_Evaluate.Image = ((System.Drawing.Image)(resources.GetObject("btn_Evaluate.Image")));
            this.btn_Evaluate.ImagePosition = 15;
            this.btn_Evaluate.ImageZoom = 40;
            this.btn_Evaluate.LabelPosition = 50;
            this.btn_Evaluate.LabelText = "Students that have Evaluated";
            this.btn_Evaluate.Location = new System.Drawing.Point(60, 41);
            this.btn_Evaluate.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Evaluate.Name = "btn_Evaluate";
            this.btn_Evaluate.Size = new System.Drawing.Size(131, 117);
            this.btn_Evaluate.TabIndex = 2;
            this.btn_Evaluate.Click += new System.EventHandler(this.btn_Evaluate_Click);
            // 
            // btn_Close
            // 
            this.btn_Close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Close.BackColor = System.Drawing.Color.Transparent;
            this.btn_Close.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Close.Image = ((System.Drawing.Image)(resources.GetObject("btn_Close.Image")));
            this.btn_Close.ImageActive = null;
            this.btn_Close.Location = new System.Drawing.Point(408, 1);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(30, 30);
            this.btn_Close.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btn_Close.TabIndex = 63;
            this.btn_Close.TabStop = false;
            this.btn_Close.Zoom = 10;
            this.btn_Close.Click += new System.EventHandler(this.btn_Close_Click);
            // 
            // pnl_Header
            // 
            this.pnl_Header.Location = new System.Drawing.Point(1, 2);
            this.pnl_Header.Name = "pnl_Header";
            this.pnl_Header.Size = new System.Drawing.Size(437, 30);
            this.pnl_Header.TabIndex = 64;
            // 
            // bunifuDragControl1
            // 
            this.bunifuDragControl1.Fixed = true;
            this.bunifuDragControl1.Horizontal = true;
            this.bunifuDragControl1.TargetControl = this.pnl_Header;
            this.bunifuDragControl1.Vertical = true;
            // 
            // frm_EvaluationStudentsStatus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(440, 175);
            this.Controls.Add(this.btn_Close);
            this.Controls.Add(this.pnl_Header);
            this.Controls.Add(this.btn_Evaluatent);
            this.Controls.Add(this.btn_Evaluate);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frm_EvaluationStudentsStatus";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frm_EvaluationStudentsStatus";
            ((System.ComponentModel.ISupportInitialize)(this.btn_Close)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private Bunifu.Framework.UI.BunifuTileButton btn_Evaluatent;
        private Bunifu.Framework.UI.BunifuTileButton btn_Evaluate;
        private Bunifu.Framework.UI.BunifuImageButton btn_Close;
        private System.Windows.Forms.Panel pnl_Header;
        private Bunifu.Framework.UI.BunifuDragControl bunifuDragControl1;
    }
}