﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FacultyEvaluationSystem.Reports;

namespace FacultyEvaluationSystem
{
    public partial class frm_SelectTeacher : Form
    {
        dataClass_FacultyEvaluationSystemDataContext db = new dataClass_FacultyEvaluationSystemDataContext();
        string schoolYear;
        int idSched;
        string mode;

        public frm_SelectTeacher(string SchoolYear, int IdSched, string Mode)
        {
            InitializeComponent();
            schoolYear = SchoolYear;
            idSched = IdSched;
            mode = Mode;

            this.ActiveControl = bunifuCustomLabel2;
        }

        private void frm_SelectTeacher_Load(object sender, EventArgs e)
        {
            dgv_Teachers.DataSource = db.sp_ViewTeachersBySchoolYear(schoolYear);
            dgv_Teachers.Columns[0].Visible = false;
        }

        private void dgv_Teachers_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (mode == "Comments")
            {
                frm_Reports_Comments frm = new frm_Reports_Comments(idSched, (int)dgv_Teachers.CurrentRow.Cells[0].Value);
                frm.ShowDialog();
                this.Close();
            }
            else
            {
                frm_ReportsStudentEvaluation frm = new frm_ReportsStudentEvaluation(idSched, (int)dgv_Teachers.CurrentRow.Cells[0].Value);
                frm.ShowDialog();
                this.Close();
            }
        }
    }
}
