﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FacultyEvaluationSystem
{
    public partial class frm_Admin_AddEditStudentQuestion : Form
    {
        public int id;
        dataClass_FacultyEvaluationSystemDataContext db = new dataClass_FacultyEvaluationSystemDataContext();
        public DataGridView dgv;
        List<int> idcriteria = new List<int>();
        string action;
        public string editCrit;
        public int id_question;

        public frm_Admin_AddEditStudentQuestion(string Action)
        {
            InitializeComponent();
            action = Action;
        }

        private void frm_Admin_AddEditStudentQuestion_Load(object sender, EventArgs e)
        {
            // gets all criteria in an evaluation set (represented by its id, the variable id in the parameter)
            // and also the evaluation set id itself
            foreach (var results in db.sp_ViewEvaluationStudentCriteriaByEvaluationID(id))
            {
                cmb_Criteria.Items.Add(results.Criteria);
                idcriteria.Add(results.ID_Criteria);
            }

            // changes button functionality according to action var
            if(action != "Add")
            {
                btn_Add.Text = "Edit";
                cmb_Criteria.SelectedItem = editCrit;
            }

        }

        private void btn_Add_Click(object sender, EventArgs e)
        {
            //Edit
            if(action!="Add")
            {
                // checks null values
                if (cmb_Criteria.SelectedIndex < 0 || txt_Question.Text.Trim() == "")
                {
                    MessageBox.Show("Please fill all the inputs.");
                    return;
                }
                db.sp_UpdateEvaluationStudentQuestion(id_question, txt_Question.Text, idcriteria[cmb_Criteria.SelectedIndex]);
                MessageBox.Show("Successfully edited question.");
                this.Close();
            }
            //Add
            else
            {
                // checks null values
                if (cmb_Criteria.SelectedIndex < 0 || txt_Question.Text.Trim() == "")
                {
                    MessageBox.Show("Please fill all the inputs.");
                    return;
                }

                db.sp_InsertEvaluationStudentQuestion(idcriteria[cmb_Criteria.SelectedIndex], txt_Question.Text);
                txt_Question.Clear();
                dgv.DataSource = db.sp_ViewEvaluationStudentQuestionsByID(id);
                dgv.ClearSelection();
                MessageBox.Show("Successfully added question.");
            }            
        }
        // btn close
        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
