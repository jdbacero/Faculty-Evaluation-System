﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FacultyEvaluationSystem
{
    public partial class frm_Admin_StudEval : Form
    {
        dataClass_FacultyEvaluationSystemDataContext db = new dataClass_FacultyEvaluationSystemDataContext();

        public frm_Admin_StudEval()
        {
            InitializeComponent();
        }

        private void frm_Admin_StudEval_Load(object sender, EventArgs e)
        {
            dgv_EvaluationID.DataSource = db.sp_ViewEvaluationStudentID();
            dgv_EvaluationID.ClearSelection();
        }
        //------------------------------------BUTTONS------------------------------------------------------
        private void btn_AddNewSet_Click(object sender, EventArgs e)
        {
            // this adds new evaluation set (version)
            // naming depends on the date created
            string date = DateTime.Today.Year + "-" + DateTime.Today.Month + "-" + DateTime.Today.Day;
            db.sp_InsertEvaluationStudentID(DateTime.Parse(date));
            dgv_EvaluationID.DataSource = db.sp_ViewEvaluationStudentID();
            dgv_EvaluationID.ClearSelection();
            MessageBox.Show("Added new empty set of evaluation questions.");
        }

        private void btn_AddCriteria_Click(object sender, EventArgs e)
        {
            // this triggers adding of criteria in another form (frm_Admin_AddEditStudEvalCriteria)

            #region this area gets every criteria percentage in an evaluation set, then add to monitor the total percentage present in an evaluation set

            //if (dgv_Criteria.Rows.Count == 0) { MessageBox.Show("Select a set first."); return; }
            decimal percent = 0;
            foreach (var result in db.sp_ViewEvaluationStudentCriteriaByEvaluationID(int.Parse(dgv_EvaluationID.CurrentRow.Cells["ID_Evaluation"].Value.ToString())))
            {
                percent += result.Percentage;
            }

            #endregion

            #region this section is where everything is pass to the adding form
            // the current total of criteria percentage and the evaluation set id
            frm_Admin_AddEditStudEvalCriteria frm_AddCriteria = new frm_Admin_AddEditStudEvalCriteria("Add");
            frm_AddCriteria.currentPercent = percent;
            frm_AddCriteria.dgv = dgv_Criteria;
            frm_AddCriteria.id_eval = int.Parse(dgv_EvaluationID.CurrentRow.Cells["ID_Evaluation"].Value.ToString());
            frm_AddCriteria.ShowDialog();
            Disable("Criteria");
            Disable("Question");
            #endregion
        }

        private void btn_AddQuestion_Click(object sender, EventArgs e)
        {
            // this triggers adding question in every criteria
            // this only pass evaluation set id to another form (frm_Admin_AddEditStudentQuestion) for adding of question
            //-----
            //if (dgv_Questions.Rows.Count == 0) { MessageBox.Show("Select a set first."); return; }
            frm_Admin_AddEditStudentQuestion frm_StudEval = new frm_Admin_AddEditStudentQuestion("Add");
            frm_StudEval.id = int.Parse(dgv_EvaluationID.CurrentRow.Cells["ID_Evaluation"].Value.ToString());
            frm_StudEval.dgv = dgv_Questions;
            frm_StudEval.ShowDialog();
            Disable("Question");
            Disable("Criteria");
        }
        private void btn_EditCriteria_Click(object sender, EventArgs e)
        {
            #region this area gets every criteria percentage in an evaluation set, then add to monitor the total percentage present in an evaluation set

            decimal percent = 0;
            foreach (var result in db.sp_ViewEvaluationStudentCriteriaByEvaluationID(int.Parse(dgv_EvaluationID.CurrentRow.Cells["ID_Evaluation"].Value.ToString())))
            {
                percent += result.Percentage;
            }

            #endregion

            frm_Admin_AddEditStudEvalCriteria frm_EditCriteria = new frm_Admin_AddEditStudEvalCriteria("Edit");

            #region on other hand, this subtracts the selected criteria percentage from the current total criteria percentage
            // this is to avoid miscalculation of percentage

            foreach (var result in db.sp_ViewEvaluationStudentCriteriaByCriteriaID(int.Parse(dgv_Criteria.CurrentRow.Cells["ID_Criteria"].Value.ToString())))
            {
                percent -= result.Percentage;
                frm_EditCriteria.txt_Criteria.Text = result.Criteria;
                frm_EditCriteria.txt_Percentage.Text = result.Percentage.ToString();
            }

            #endregion

            // this is where values from the selected rows are pass to frm_Admin_AddEditStudEvalCriteria for editing of criteria
            frm_EditCriteria.currentPercent = percent;
            frm_EditCriteria.id_eval = int.Parse(dgv_Criteria.CurrentRow.Cells["ID_Criteria"].Value.ToString());
            frm_EditCriteria.ShowDialog();
            dgv_Criteria.DataSource = db.sp_ViewEvaluationStudentCriteriaByEvaluationID(int.Parse(dgv_EvaluationID.CurrentRow.Cells["ID_Evaluation"].Value.ToString()));
            Disable("Criteria");
            dgv_Questions.DataSource = db.sp_ViewEvaluationStudentQuestionsByID(int.Parse(dgv_EvaluationID.CurrentRow.Cells["ID_Evaluation"].Value.ToString()));
            dgv_Questions.ClearSelection();
        }

        private void btn_EditQuestion_Click(object sender, EventArgs e)
        {
            // this is to pass data about the selected Student Evaluation Question to frm_Admin_AddEditStudentQuestion for editing of question
            frm_Admin_AddEditStudentQuestion frm_EditQuestion = new frm_Admin_AddEditStudentQuestion("Edit");
            frm_EditQuestion.id = int.Parse(dgv_EvaluationID.CurrentRow.Cells["ID_Evaluation"].Value.ToString());
            frm_EditQuestion.id_question = int.Parse(dgv_Questions.CurrentRow.Cells["ID_Question"].Value.ToString());
            frm_EditQuestion.txt_Question.Text = dgv_Questions.CurrentRow.Cells["Question"].Value.ToString();
            frm_EditQuestion.editCrit = dgv_Questions.CurrentRow.Cells["Criteria"].Value.ToString();
            frm_EditQuestion.ShowDialog();
            dgv_Questions.DataSource = db.sp_ViewEvaluationStudentQuestionsByID(int.Parse(dgv_EvaluationID.CurrentRow.Cells["ID_Evaluation"].Value.ToString()));
            Disable("Question");
        }

        #region this area reminds you that deleting is not a choice
        private void btn_DeleteCriteria_Click(object sender, EventArgs e)
        {
            //to be coded
            Disable("Criteria");
        }

        private void btn_DeleteQuestion_Click(object sender, EventArgs e)
        {
            //to be coded
            Disable("Question");
        }
        #endregion

        //btn close
        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //----------------------------------------DGV CELL CLICKS----------------------------------------------
        private void dgv_EvaluationID_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            dgv_Questions.DataSource = db.sp_ViewEvaluationStudentQuestionsByID(int.Parse(dgv_EvaluationID.CurrentRow.Cells["ID_Evaluation"].Value.ToString()));
            dgv_Questions.Columns["ID_Question"].Visible = false;
            dgv_Questions.Columns["ID_Criteria"].Visible = false;
            dgv_Questions.ClearSelection();

            dgv_Criteria.DataSource = db.sp_ViewEvaluationStudentCriteriaByEvaluationID(int.Parse(dgv_EvaluationID.CurrentRow.Cells["ID_Evaluation"].Value.ToString()));
            dgv_Criteria.Columns["ID_Evaluation"].Visible = false;
            dgv_Criteria.Columns["ID_Criteria"].Visible = false;
            dgv_Criteria.ClearSelection();
            Enable("Add");
            Disable("Question");
            Disable("Criteria");
        }

        private void dgv_Criteria_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Enable("Criteria");
            Disable("Question");
        }

        private void dgv_Questions_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Disable("Criteria");
            Enable("Question");
        }

        //----------------------------------------FUNCTIONS----------------------------------------------
        //Function for enabling buttons
        public void Enable(string identifier)
        {
            switch (identifier)
            {
                //enables add/create buttons both from criteria & questions
                case "Add":
                    btn_AddCriteria.Enabled = true;
                    btn_AddQuestion.Enabled = true;
                    btn_AddCriteria.Cursor = Cursors.Hand;
                    btn_AddQuestion.Cursor = Cursors.Hand;
                    break;
                //enables edit & delete CRITERIA buttons
                case "Criteria":
                    btn_EditCriteria.Enabled = true;
                    btn_DeleteCriteria.Enabled = true;
                    btn_EditCriteria.Cursor = Cursors.Hand;
                    btn_DeleteCriteria.Cursor = Cursors.Hand;
                    break;
                //enable edit & delete Questions buttons
                case "Question":
                    btn_EditQuestion.Enabled = true;
                    btn_DeleteQuestion.Enabled = true;
                    btn_EditQuestion.Cursor = Cursors.Hand;
                    btn_DeleteQuestion.Cursor = Cursors.Hand;
                    break;
                default:
                    break;
            }

        }
        //Function for disabling buttons
        public void Disable(string identifier)
        {
            switch (identifier)
            {
                //disables edit & delete CRITERIA buttons
                case "Criteria":
                    btn_EditCriteria.Enabled = false;
                    btn_DeleteCriteria.Enabled = false;
                    btn_EditCriteria.Cursor = Cursors.No;
                    btn_DeleteCriteria.Cursor = Cursors.No;
                    dgv_Criteria.ClearSelection();
                    break;
                //disables edit & delete QUESTION buttons
                case "Question":
                    btn_EditQuestion.Enabled = false;
                    btn_DeleteQuestion.Enabled = false;
                    btn_EditQuestion.Cursor = Cursors.No;
                    btn_DeleteQuestion.Cursor = Cursors.No;
                    dgv_Questions.ClearSelection();
                    break;
                default:
                    break;
            }
        }

       



        

       
    }
}
