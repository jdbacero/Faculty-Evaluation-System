﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FacultyEvaluationSystem
{
    public partial class frm_Admin_AddEditStudEvalCriteria : Form
    {
        public int id_eval;
        dataClass_FacultyEvaluationSystemDataContext db = new dataClass_FacultyEvaluationSystemDataContext();
        public decimal currentPercent;
        public DataGridView dgv;
        string action;

        public frm_Admin_AddEditStudEvalCriteria(string Action)
        {
            InitializeComponent();
            action = Action;
        }

        private void frm_Admin_AddEditStudEvalCriteria_Load(object sender, EventArgs e)
        {
            // showing the current percentage in the selected evaluation set
            lbl_CurrentPercent.Text = currentPercent + "%";
            if (action != "Add")
            {
                btn_AddCriteria.Text = "Edit";
            }
        }

        private void btn_AddCriteria_Click(object sender, EventArgs e)
        {
            // added error trapping in adding criteria

            // this is the main adding of criteria happens
            if (txt_Criteria.Text == "" || txt_Percentage.Text == "")
            {
                // this traps null values
                MessageBox.Show("Please enter criteria and its percentage.");
            }
            else
            {
                // this if statement here restricts user to input non numeric values
                if (!System.Text.RegularExpressions.Regex.IsMatch(txt_Percentage.Text, "^[0-9]*$"))
                {
                    MessageBox.Show("Percentage only accepts numbers.");
                }
                else
                {
                    // this checks percentage value so the system wont accept zero percentage
                    if (txt_Percentage.Text == 0 + "")
                    {
                        MessageBox.Show("Percentage must be greater than zero.");
                    } else
                    {
                        //Edit
                        if (action != "Add")
                        {
                            //Dili mulapas ug 100%
                            if ((decimal.Parse(txt_Percentage.Text) + currentPercent) > 100)
                            {
                                MessageBox.Show("Total percentage of criteria shouldn't go over 100%!");
                                return;
                            }

                            db.sp_UpdateEvaluationStudentCriteria(id_eval, txt_Criteria.Text, decimal.Parse(txt_Percentage.Text));
                            MessageBox.Show("Successfully edited criteria.");
                            this.Close();
                        }
                        //Add
                        else
                        {
                            //Dili mulapas ug 100%
                            if ((decimal.Parse(txt_Percentage.Text) + currentPercent) > 100)
                            {
                                MessageBox.Show("Total percentage of criteria shouldn't go over 100%!");
                                return;
                            }
                            db.sp_InsertEvaluationStudentCriteria(id_eval, txt_Criteria.Text, decimal.Parse(txt_Percentage.Text));
                            MessageBox.Show("Successfully added a new criteria");
                            txt_Percentage.Text = "";
                            txt_Criteria.Text = "";
                            dgv.DataSource = db.sp_ViewEvaluationStudentCriteriaByEvaluationID(id_eval);
                            dgv.ClearSelection();

                            currentPercent = 0;
                            foreach (var result in db.sp_ViewEvaluationStudentCriteriaByEvaluationID(id_eval))
                            {
                                currentPercent += result.Percentage;
                            }

                            lbl_CurrentPercent.Text = currentPercent + "%";
                        }
                    }
                }
            }
        }
        //btn close
        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        // to disable alpha keys in percentage textbox
        private void txt_Percentage_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
    }
}
