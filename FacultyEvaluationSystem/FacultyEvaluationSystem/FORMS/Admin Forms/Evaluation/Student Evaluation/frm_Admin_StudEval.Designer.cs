﻿namespace FacultyEvaluationSystem
{
    partial class frm_Admin_StudEval
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Admin_StudEval));
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.btn_Close = new Bunifu.Framework.UI.BunifuImageButton();
            this.dgv_EvaluationID = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.btn_AddNewSet = new Bunifu.Framework.UI.BunifuFlatButton();
            this.dgv_Criteria = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.dgv_Questions = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.btn_AddCriteria = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btn_EditCriteria = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btn_DeleteCriteria = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btn_AddQuestion = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btn_EditQuestion = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btn_DeleteQuestion = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuDragControl1 = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.bunifuCustomLabel3 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.pnl_Header = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.btn_Close)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_EvaluationID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Criteria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Questions)).BeginInit();
            this.SuspendLayout();
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 3;
            this.bunifuElipse1.TargetControl = this;
            // 
            // btn_Close
            // 
            this.btn_Close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Close.BackColor = System.Drawing.Color.Transparent;
            this.btn_Close.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Close.Image = ((System.Drawing.Image)(resources.GetObject("btn_Close.Image")));
            this.btn_Close.ImageActive = null;
            this.btn_Close.Location = new System.Drawing.Point(938, 1);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(30, 30);
            this.btn_Close.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btn_Close.TabIndex = 21;
            this.btn_Close.TabStop = false;
            this.btn_Close.Zoom = 10;
            this.btn_Close.Click += new System.EventHandler(this.btn_Close_Click);
            // 
            // dgv_EvaluationID
            // 
            this.dgv_EvaluationID.AllowUserToAddRows = false;
            this.dgv_EvaluationID.AllowUserToDeleteRows = false;
            this.dgv_EvaluationID.AllowUserToResizeColumns = false;
            this.dgv_EvaluationID.AllowUserToResizeRows = false;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgv_EvaluationID.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle9;
            this.dgv_EvaluationID.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_EvaluationID.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_EvaluationID.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.dgv_EvaluationID.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv_EvaluationID.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_EvaluationID.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dgv_EvaluationID.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_EvaluationID.DefaultCellStyle = dataGridViewCellStyle11;
            this.dgv_EvaluationID.DoubleBuffered = true;
            this.dgv_EvaluationID.EnableHeadersVisualStyles = false;
            this.dgv_EvaluationID.HeaderBgColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.dgv_EvaluationID.HeaderForeColor = System.Drawing.Color.White;
            this.dgv_EvaluationID.Location = new System.Drawing.Point(12, 93);
            this.dgv_EvaluationID.MultiSelect = false;
            this.dgv_EvaluationID.Name = "dgv_EvaluationID";
            this.dgv_EvaluationID.ReadOnly = true;
            this.dgv_EvaluationID.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_EvaluationID.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.dgv_EvaluationID.RowHeadersVisible = false;
            this.dgv_EvaluationID.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgv_EvaluationID.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_EvaluationID.Size = new System.Drawing.Size(294, 110);
            this.dgv_EvaluationID.TabIndex = 22;
            this.dgv_EvaluationID.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_EvaluationID_CellClick);
            // 
            // btn_AddNewSet
            // 
            this.btn_AddNewSet.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_AddNewSet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_AddNewSet.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_AddNewSet.BorderRadius = 3;
            this.btn_AddNewSet.ButtonText = "Create new set";
            this.btn_AddNewSet.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_AddNewSet.DisabledColor = System.Drawing.Color.Gray;
            this.btn_AddNewSet.ForeColor = System.Drawing.Color.White;
            this.btn_AddNewSet.Iconcolor = System.Drawing.Color.Transparent;
            this.btn_AddNewSet.Iconimage = ((System.Drawing.Image)(resources.GetObject("btn_AddNewSet.Iconimage")));
            this.btn_AddNewSet.Iconimage_right = null;
            this.btn_AddNewSet.Iconimage_right_Selected = null;
            this.btn_AddNewSet.Iconimage_Selected = null;
            this.btn_AddNewSet.IconMarginLeft = 0;
            this.btn_AddNewSet.IconMarginRight = 0;
            this.btn_AddNewSet.IconRightVisible = true;
            this.btn_AddNewSet.IconRightZoom = 0D;
            this.btn_AddNewSet.IconVisible = false;
            this.btn_AddNewSet.IconZoom = 90D;
            this.btn_AddNewSet.IsTab = false;
            this.btn_AddNewSet.Location = new System.Drawing.Point(13, 47);
            this.btn_AddNewSet.Name = "btn_AddNewSet";
            this.btn_AddNewSet.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_AddNewSet.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(88)))), ((int)(((byte)(130)))));
            this.btn_AddNewSet.OnHoverTextColor = System.Drawing.Color.White;
            this.btn_AddNewSet.selected = false;
            this.btn_AddNewSet.Size = new System.Drawing.Size(139, 40);
            this.btn_AddNewSet.TabIndex = 28;
            this.btn_AddNewSet.Text = "Create new set";
            this.btn_AddNewSet.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn_AddNewSet.Textcolor = System.Drawing.Color.White;
            this.btn_AddNewSet.TextFont = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_AddNewSet.Click += new System.EventHandler(this.btn_AddNewSet_Click);
            // 
            // dgv_Criteria
            // 
            this.dgv_Criteria.AllowUserToAddRows = false;
            this.dgv_Criteria.AllowUserToDeleteRows = false;
            this.dgv_Criteria.AllowUserToResizeColumns = false;
            this.dgv_Criteria.AllowUserToResizeRows = false;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgv_Criteria.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgv_Criteria.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_Criteria.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_Criteria.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.dgv_Criteria.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv_Criteria.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_Criteria.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgv_Criteria.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_Criteria.DefaultCellStyle = dataGridViewCellStyle7;
            this.dgv_Criteria.DoubleBuffered = true;
            this.dgv_Criteria.EnableHeadersVisualStyles = false;
            this.dgv_Criteria.HeaderBgColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.dgv_Criteria.HeaderForeColor = System.Drawing.Color.White;
            this.dgv_Criteria.Location = new System.Drawing.Point(331, 93);
            this.dgv_Criteria.MultiSelect = false;
            this.dgv_Criteria.Name = "dgv_Criteria";
            this.dgv_Criteria.ReadOnly = true;
            this.dgv_Criteria.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_Criteria.RowHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.dgv_Criteria.RowHeadersVisible = false;
            this.dgv_Criteria.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgv_Criteria.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_Criteria.Size = new System.Drawing.Size(627, 110);
            this.dgv_Criteria.TabIndex = 29;
            this.dgv_Criteria.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_Criteria_CellClick);
            // 
            // dgv_Questions
            // 
            this.dgv_Questions.AllowUserToAddRows = false;
            this.dgv_Questions.AllowUserToDeleteRows = false;
            this.dgv_Questions.AllowUserToResizeColumns = false;
            this.dgv_Questions.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgv_Questions.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_Questions.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_Questions.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_Questions.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.dgv_Questions.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv_Questions.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_Questions.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_Questions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_Questions.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgv_Questions.DoubleBuffered = true;
            this.dgv_Questions.EnableHeadersVisualStyles = false;
            this.dgv_Questions.HeaderBgColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.dgv_Questions.HeaderForeColor = System.Drawing.Color.White;
            this.dgv_Questions.Location = new System.Drawing.Point(13, 271);
            this.dgv_Questions.MultiSelect = false;
            this.dgv_Questions.Name = "dgv_Questions";
            this.dgv_Questions.ReadOnly = true;
            this.dgv_Questions.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_Questions.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgv_Questions.RowHeadersVisible = false;
            this.dgv_Questions.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgv_Questions.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_Questions.Size = new System.Drawing.Size(945, 249);
            this.dgv_Questions.TabIndex = 30;
            this.dgv_Questions.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_Questions_CellClick);
            // 
            // btn_AddCriteria
            // 
            this.btn_AddCriteria.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_AddCriteria.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_AddCriteria.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_AddCriteria.BorderRadius = 3;
            this.btn_AddCriteria.ButtonText = "Create new criteria";
            this.btn_AddCriteria.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_AddCriteria.DisabledColor = System.Drawing.Color.Gray;
            this.btn_AddCriteria.Enabled = false;
            this.btn_AddCriteria.ForeColor = System.Drawing.Color.White;
            this.btn_AddCriteria.Iconcolor = System.Drawing.Color.Transparent;
            this.btn_AddCriteria.Iconimage = ((System.Drawing.Image)(resources.GetObject("btn_AddCriteria.Iconimage")));
            this.btn_AddCriteria.Iconimage_right = null;
            this.btn_AddCriteria.Iconimage_right_Selected = null;
            this.btn_AddCriteria.Iconimage_Selected = null;
            this.btn_AddCriteria.IconMarginLeft = 0;
            this.btn_AddCriteria.IconMarginRight = 0;
            this.btn_AddCriteria.IconRightVisible = true;
            this.btn_AddCriteria.IconRightZoom = 0D;
            this.btn_AddCriteria.IconVisible = false;
            this.btn_AddCriteria.IconZoom = 90D;
            this.btn_AddCriteria.IsTab = false;
            this.btn_AddCriteria.Location = new System.Drawing.Point(331, 47);
            this.btn_AddCriteria.Name = "btn_AddCriteria";
            this.btn_AddCriteria.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_AddCriteria.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(88)))), ((int)(((byte)(130)))));
            this.btn_AddCriteria.OnHoverTextColor = System.Drawing.Color.White;
            this.btn_AddCriteria.selected = false;
            this.btn_AddCriteria.Size = new System.Drawing.Size(139, 40);
            this.btn_AddCriteria.TabIndex = 31;
            this.btn_AddCriteria.Text = "Create new criteria";
            this.btn_AddCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn_AddCriteria.Textcolor = System.Drawing.Color.White;
            this.btn_AddCriteria.TextFont = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_AddCriteria.Click += new System.EventHandler(this.btn_AddCriteria_Click);
            // 
            // btn_EditCriteria
            // 
            this.btn_EditCriteria.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_EditCriteria.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_EditCriteria.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_EditCriteria.BorderRadius = 3;
            this.btn_EditCriteria.ButtonText = "Edit criteria";
            this.btn_EditCriteria.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_EditCriteria.DisabledColor = System.Drawing.Color.Gray;
            this.btn_EditCriteria.Enabled = false;
            this.btn_EditCriteria.ForeColor = System.Drawing.Color.White;
            this.btn_EditCriteria.Iconcolor = System.Drawing.Color.Transparent;
            this.btn_EditCriteria.Iconimage = ((System.Drawing.Image)(resources.GetObject("btn_EditCriteria.Iconimage")));
            this.btn_EditCriteria.Iconimage_right = null;
            this.btn_EditCriteria.Iconimage_right_Selected = null;
            this.btn_EditCriteria.Iconimage_Selected = null;
            this.btn_EditCriteria.IconMarginLeft = 0;
            this.btn_EditCriteria.IconMarginRight = 0;
            this.btn_EditCriteria.IconRightVisible = true;
            this.btn_EditCriteria.IconRightZoom = 0D;
            this.btn_EditCriteria.IconVisible = false;
            this.btn_EditCriteria.IconZoom = 90D;
            this.btn_EditCriteria.IsTab = false;
            this.btn_EditCriteria.Location = new System.Drawing.Point(519, 47);
            this.btn_EditCriteria.Name = "btn_EditCriteria";
            this.btn_EditCriteria.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_EditCriteria.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(88)))), ((int)(((byte)(130)))));
            this.btn_EditCriteria.OnHoverTextColor = System.Drawing.Color.White;
            this.btn_EditCriteria.selected = false;
            this.btn_EditCriteria.Size = new System.Drawing.Size(139, 40);
            this.btn_EditCriteria.TabIndex = 32;
            this.btn_EditCriteria.Text = "Edit criteria";
            this.btn_EditCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn_EditCriteria.Textcolor = System.Drawing.Color.White;
            this.btn_EditCriteria.TextFont = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_EditCriteria.Click += new System.EventHandler(this.btn_EditCriteria_Click);
            // 
            // btn_DeleteCriteria
            // 
            this.btn_DeleteCriteria.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_DeleteCriteria.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_DeleteCriteria.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_DeleteCriteria.BorderRadius = 3;
            this.btn_DeleteCriteria.ButtonText = "Delete criteria";
            this.btn_DeleteCriteria.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_DeleteCriteria.DisabledColor = System.Drawing.Color.Gray;
            this.btn_DeleteCriteria.Enabled = false;
            this.btn_DeleteCriteria.ForeColor = System.Drawing.Color.White;
            this.btn_DeleteCriteria.Iconcolor = System.Drawing.Color.Transparent;
            this.btn_DeleteCriteria.Iconimage = ((System.Drawing.Image)(resources.GetObject("btn_DeleteCriteria.Iconimage")));
            this.btn_DeleteCriteria.Iconimage_right = null;
            this.btn_DeleteCriteria.Iconimage_right_Selected = null;
            this.btn_DeleteCriteria.Iconimage_Selected = null;
            this.btn_DeleteCriteria.IconMarginLeft = 0;
            this.btn_DeleteCriteria.IconMarginRight = 0;
            this.btn_DeleteCriteria.IconRightVisible = true;
            this.btn_DeleteCriteria.IconRightZoom = 0D;
            this.btn_DeleteCriteria.IconVisible = false;
            this.btn_DeleteCriteria.IconZoom = 90D;
            this.btn_DeleteCriteria.IsTab = false;
            this.btn_DeleteCriteria.Location = new System.Drawing.Point(702, 47);
            this.btn_DeleteCriteria.Name = "btn_DeleteCriteria";
            this.btn_DeleteCriteria.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_DeleteCriteria.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(88)))), ((int)(((byte)(130)))));
            this.btn_DeleteCriteria.OnHoverTextColor = System.Drawing.Color.White;
            this.btn_DeleteCriteria.selected = false;
            this.btn_DeleteCriteria.Size = new System.Drawing.Size(139, 40);
            this.btn_DeleteCriteria.TabIndex = 33;
            this.btn_DeleteCriteria.Text = "Delete criteria";
            this.btn_DeleteCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn_DeleteCriteria.Textcolor = System.Drawing.Color.White;
            this.btn_DeleteCriteria.TextFont = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_DeleteCriteria.Visible = false;
            this.btn_DeleteCriteria.Click += new System.EventHandler(this.btn_DeleteCriteria_Click);
            // 
            // btn_AddQuestion
            // 
            this.btn_AddQuestion.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_AddQuestion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_AddQuestion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_AddQuestion.BorderRadius = 3;
            this.btn_AddQuestion.ButtonText = "Add question";
            this.btn_AddQuestion.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_AddQuestion.DisabledColor = System.Drawing.Color.Gray;
            this.btn_AddQuestion.Enabled = false;
            this.btn_AddQuestion.ForeColor = System.Drawing.Color.White;
            this.btn_AddQuestion.Iconcolor = System.Drawing.Color.Transparent;
            this.btn_AddQuestion.Iconimage = ((System.Drawing.Image)(resources.GetObject("btn_AddQuestion.Iconimage")));
            this.btn_AddQuestion.Iconimage_right = null;
            this.btn_AddQuestion.Iconimage_right_Selected = null;
            this.btn_AddQuestion.Iconimage_Selected = null;
            this.btn_AddQuestion.IconMarginLeft = 0;
            this.btn_AddQuestion.IconMarginRight = 0;
            this.btn_AddQuestion.IconRightVisible = true;
            this.btn_AddQuestion.IconRightZoom = 0D;
            this.btn_AddQuestion.IconVisible = false;
            this.btn_AddQuestion.IconZoom = 90D;
            this.btn_AddQuestion.IsTab = false;
            this.btn_AddQuestion.Location = new System.Drawing.Point(13, 225);
            this.btn_AddQuestion.Name = "btn_AddQuestion";
            this.btn_AddQuestion.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_AddQuestion.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(88)))), ((int)(((byte)(130)))));
            this.btn_AddQuestion.OnHoverTextColor = System.Drawing.Color.White;
            this.btn_AddQuestion.selected = false;
            this.btn_AddQuestion.Size = new System.Drawing.Size(139, 40);
            this.btn_AddQuestion.TabIndex = 34;
            this.btn_AddQuestion.Text = "Add question";
            this.btn_AddQuestion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn_AddQuestion.Textcolor = System.Drawing.Color.White;
            this.btn_AddQuestion.TextFont = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_AddQuestion.Click += new System.EventHandler(this.btn_AddQuestion_Click);
            // 
            // btn_EditQuestion
            // 
            this.btn_EditQuestion.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_EditQuestion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_EditQuestion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_EditQuestion.BorderRadius = 3;
            this.btn_EditQuestion.ButtonText = "Edit question";
            this.btn_EditQuestion.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_EditQuestion.DisabledColor = System.Drawing.Color.Gray;
            this.btn_EditQuestion.Enabled = false;
            this.btn_EditQuestion.ForeColor = System.Drawing.Color.White;
            this.btn_EditQuestion.Iconcolor = System.Drawing.Color.Transparent;
            this.btn_EditQuestion.Iconimage = ((System.Drawing.Image)(resources.GetObject("btn_EditQuestion.Iconimage")));
            this.btn_EditQuestion.Iconimage_right = null;
            this.btn_EditQuestion.Iconimage_right_Selected = null;
            this.btn_EditQuestion.Iconimage_Selected = null;
            this.btn_EditQuestion.IconMarginLeft = 0;
            this.btn_EditQuestion.IconMarginRight = 0;
            this.btn_EditQuestion.IconRightVisible = true;
            this.btn_EditQuestion.IconRightZoom = 0D;
            this.btn_EditQuestion.IconVisible = false;
            this.btn_EditQuestion.IconZoom = 90D;
            this.btn_EditQuestion.IsTab = false;
            this.btn_EditQuestion.Location = new System.Drawing.Point(215, 225);
            this.btn_EditQuestion.Name = "btn_EditQuestion";
            this.btn_EditQuestion.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_EditQuestion.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(88)))), ((int)(((byte)(130)))));
            this.btn_EditQuestion.OnHoverTextColor = System.Drawing.Color.White;
            this.btn_EditQuestion.selected = false;
            this.btn_EditQuestion.Size = new System.Drawing.Size(139, 40);
            this.btn_EditQuestion.TabIndex = 35;
            this.btn_EditQuestion.Text = "Edit question";
            this.btn_EditQuestion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn_EditQuestion.Textcolor = System.Drawing.Color.White;
            this.btn_EditQuestion.TextFont = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_EditQuestion.Click += new System.EventHandler(this.btn_EditQuestion_Click);
            // 
            // btn_DeleteQuestion
            // 
            this.btn_DeleteQuestion.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_DeleteQuestion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_DeleteQuestion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_DeleteQuestion.BorderRadius = 3;
            this.btn_DeleteQuestion.ButtonText = "Delete question";
            this.btn_DeleteQuestion.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_DeleteQuestion.DisabledColor = System.Drawing.Color.Gray;
            this.btn_DeleteQuestion.Enabled = false;
            this.btn_DeleteQuestion.ForeColor = System.Drawing.Color.White;
            this.btn_DeleteQuestion.Iconcolor = System.Drawing.Color.Transparent;
            this.btn_DeleteQuestion.Iconimage = ((System.Drawing.Image)(resources.GetObject("btn_DeleteQuestion.Iconimage")));
            this.btn_DeleteQuestion.Iconimage_right = null;
            this.btn_DeleteQuestion.Iconimage_right_Selected = null;
            this.btn_DeleteQuestion.Iconimage_Selected = null;
            this.btn_DeleteQuestion.IconMarginLeft = 0;
            this.btn_DeleteQuestion.IconMarginRight = 0;
            this.btn_DeleteQuestion.IconRightVisible = true;
            this.btn_DeleteQuestion.IconRightZoom = 0D;
            this.btn_DeleteQuestion.IconVisible = false;
            this.btn_DeleteQuestion.IconZoom = 90D;
            this.btn_DeleteQuestion.IsTab = false;
            this.btn_DeleteQuestion.Location = new System.Drawing.Point(592, 225);
            this.btn_DeleteQuestion.Name = "btn_DeleteQuestion";
            this.btn_DeleteQuestion.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_DeleteQuestion.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(88)))), ((int)(((byte)(130)))));
            this.btn_DeleteQuestion.OnHoverTextColor = System.Drawing.Color.White;
            this.btn_DeleteQuestion.selected = false;
            this.btn_DeleteQuestion.Size = new System.Drawing.Size(139, 40);
            this.btn_DeleteQuestion.TabIndex = 36;
            this.btn_DeleteQuestion.Text = "Delete question";
            this.btn_DeleteQuestion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn_DeleteQuestion.Textcolor = System.Drawing.Color.White;
            this.btn_DeleteQuestion.TextFont = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_DeleteQuestion.Visible = false;
            this.btn_DeleteQuestion.Click += new System.EventHandler(this.btn_DeleteQuestion_Click);
            // 
            // bunifuDragControl1
            // 
            this.bunifuDragControl1.Fixed = true;
            this.bunifuDragControl1.Horizontal = true;
            this.bunifuDragControl1.TargetControl = this.pnl_Header;
            this.bunifuDragControl1.Vertical = true;
            // 
            // bunifuCustomLabel3
            // 
            this.bunifuCustomLabel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuCustomLabel3.AutoSize = true;
            this.bunifuCustomLabel3.Font = new System.Drawing.Font("Century Gothic", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bunifuCustomLabel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.bunifuCustomLabel3.Location = new System.Drawing.Point(8, 9);
            this.bunifuCustomLabel3.Name = "bunifuCustomLabel3";
            this.bunifuCustomLabel3.Size = new System.Drawing.Size(261, 22);
            this.bunifuCustomLabel3.TabIndex = 57;
            this.bunifuCustomLabel3.Text = "Student Evaluation Questions";
            // 
            // pnl_Header
            // 
            this.pnl_Header.Location = new System.Drawing.Point(2, 1);
            this.pnl_Header.Name = "pnl_Header";
            this.pnl_Header.Size = new System.Drawing.Size(966, 30);
            this.pnl_Header.TabIndex = 62;
            // 
            // frm_Admin_StudEval
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(970, 541);
            this.Controls.Add(this.bunifuCustomLabel3);
            this.Controls.Add(this.btn_DeleteQuestion);
            this.Controls.Add(this.btn_EditQuestion);
            this.Controls.Add(this.btn_AddQuestion);
            this.Controls.Add(this.btn_DeleteCriteria);
            this.Controls.Add(this.btn_EditCriteria);
            this.Controls.Add(this.btn_AddCriteria);
            this.Controls.Add(this.dgv_Questions);
            this.Controls.Add(this.dgv_Criteria);
            this.Controls.Add(this.btn_AddNewSet);
            this.Controls.Add(this.dgv_EvaluationID);
            this.Controls.Add(this.btn_Close);
            this.Controls.Add(this.pnl_Header);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frm_Admin_StudEval";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Student Evaluation";
            this.Load += new System.EventHandler(this.frm_Admin_StudEval_Load);
            ((System.ComponentModel.ISupportInitialize)(this.btn_Close)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_EvaluationID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Criteria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Questions)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private Bunifu.Framework.UI.BunifuImageButton btn_Close;
        private Bunifu.Framework.UI.BunifuCustomDataGrid dgv_EvaluationID;
        private Bunifu.Framework.UI.BunifuFlatButton btn_AddNewSet;
        private Bunifu.Framework.UI.BunifuCustomDataGrid dgv_Criteria;
        private Bunifu.Framework.UI.BunifuCustomDataGrid dgv_Questions;
        private Bunifu.Framework.UI.BunifuFlatButton btn_AddCriteria;
        private Bunifu.Framework.UI.BunifuFlatButton btn_DeleteCriteria;
        private Bunifu.Framework.UI.BunifuFlatButton btn_EditCriteria;
        private Bunifu.Framework.UI.BunifuFlatButton btn_AddQuestion;
        private Bunifu.Framework.UI.BunifuFlatButton btn_DeleteQuestion;
        private Bunifu.Framework.UI.BunifuFlatButton btn_EditQuestion;
        private Bunifu.Framework.UI.BunifuDragControl bunifuDragControl1;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel3;
        private System.Windows.Forms.Panel pnl_Header;
    }
}