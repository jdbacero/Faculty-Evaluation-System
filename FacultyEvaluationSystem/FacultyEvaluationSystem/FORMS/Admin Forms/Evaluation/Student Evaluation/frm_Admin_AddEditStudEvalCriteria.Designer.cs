﻿namespace FacultyEvaluationSystem
{
    partial class frm_Admin_AddEditStudEvalCriteria
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Admin_AddEditStudEvalCriteria));
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.bunifuDragControl1 = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.btn_Close = new Bunifu.Framework.UI.BunifuImageButton();
            this.bunifuCustomLabel2 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel1 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.btn_AddCriteria = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuCustomLabel3 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.lbl_CurrentPercent = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.txt_Criteria = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.txt_Percentage = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.pnl_Header = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.btn_Close)).BeginInit();
            this.SuspendLayout();
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 3;
            this.bunifuElipse1.TargetControl = this;
            // 
            // bunifuDragControl1
            // 
            this.bunifuDragControl1.Fixed = true;
            this.bunifuDragControl1.Horizontal = true;
            this.bunifuDragControl1.TargetControl = this.pnl_Header;
            this.bunifuDragControl1.Vertical = true;
            // 
            // btn_Close
            // 
            this.btn_Close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Close.BackColor = System.Drawing.Color.Transparent;
            this.btn_Close.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Close.Image = ((System.Drawing.Image)(resources.GetObject("btn_Close.Image")));
            this.btn_Close.ImageActive = null;
            this.btn_Close.Location = new System.Drawing.Point(368, 1);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(30, 30);
            this.btn_Close.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btn_Close.TabIndex = 25;
            this.btn_Close.TabStop = false;
            this.btn_Close.Zoom = 10;
            this.btn_Close.Click += new System.EventHandler(this.btn_Close_Click);
            // 
            // bunifuCustomLabel2
            // 
            this.bunifuCustomLabel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuCustomLabel2.AutoSize = true;
            this.bunifuCustomLabel2.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bunifuCustomLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.bunifuCustomLabel2.Location = new System.Drawing.Point(72, 53);
            this.bunifuCustomLabel2.Name = "bunifuCustomLabel2";
            this.bunifuCustomLabel2.Size = new System.Drawing.Size(66, 20);
            this.bunifuCustomLabel2.TabIndex = 31;
            this.bunifuCustomLabel2.Text = "Criteria:";
            // 
            // bunifuCustomLabel1
            // 
            this.bunifuCustomLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuCustomLabel1.AutoSize = true;
            this.bunifuCustomLabel1.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bunifuCustomLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.bunifuCustomLabel1.Location = new System.Drawing.Point(113, 91);
            this.bunifuCustomLabel1.Name = "bunifuCustomLabel1";
            this.bunifuCustomLabel1.Size = new System.Drawing.Size(25, 20);
            this.bunifuCustomLabel1.TabIndex = 32;
            this.bunifuCustomLabel1.Text = "%:";
            // 
            // btn_AddCriteria
            // 
            this.btn_AddCriteria.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_AddCriteria.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_AddCriteria.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_AddCriteria.BorderRadius = 3;
            this.btn_AddCriteria.ButtonText = "Add";
            this.btn_AddCriteria.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_AddCriteria.DisabledColor = System.Drawing.Color.Gray;
            this.btn_AddCriteria.ForeColor = System.Drawing.Color.White;
            this.btn_AddCriteria.Iconcolor = System.Drawing.Color.Transparent;
            this.btn_AddCriteria.Iconimage = ((System.Drawing.Image)(resources.GetObject("btn_AddCriteria.Iconimage")));
            this.btn_AddCriteria.Iconimage_right = null;
            this.btn_AddCriteria.Iconimage_right_Selected = null;
            this.btn_AddCriteria.Iconimage_Selected = null;
            this.btn_AddCriteria.IconMarginLeft = 0;
            this.btn_AddCriteria.IconMarginRight = 0;
            this.btn_AddCriteria.IconRightVisible = true;
            this.btn_AddCriteria.IconRightZoom = 0D;
            this.btn_AddCriteria.IconVisible = false;
            this.btn_AddCriteria.IconZoom = 90D;
            this.btn_AddCriteria.IsTab = false;
            this.btn_AddCriteria.Location = new System.Drawing.Point(145, 135);
            this.btn_AddCriteria.Name = "btn_AddCriteria";
            this.btn_AddCriteria.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_AddCriteria.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(88)))), ((int)(((byte)(130)))));
            this.btn_AddCriteria.OnHoverTextColor = System.Drawing.Color.White;
            this.btn_AddCriteria.selected = false;
            this.btn_AddCriteria.Size = new System.Drawing.Size(100, 40);
            this.btn_AddCriteria.TabIndex = 33;
            this.btn_AddCriteria.Text = "Add";
            this.btn_AddCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn_AddCriteria.Textcolor = System.Drawing.Color.White;
            this.btn_AddCriteria.TextFont = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_AddCriteria.Click += new System.EventHandler(this.btn_AddCriteria_Click);
            // 
            // bunifuCustomLabel3
            // 
            this.bunifuCustomLabel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuCustomLabel3.AutoSize = true;
            this.bunifuCustomLabel3.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bunifuCustomLabel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.bunifuCustomLabel3.Location = new System.Drawing.Point(60, 200);
            this.bunifuCustomLabel3.Name = "bunifuCustomLabel3";
            this.bunifuCustomLabel3.Size = new System.Drawing.Size(84, 20);
            this.bunifuCustomLabel3.TabIndex = 34;
            this.bunifuCustomLabel3.Text = "Current %:";
            // 
            // lbl_CurrentPercent
            // 
            this.lbl_CurrentPercent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_CurrentPercent.AutoSize = true;
            this.lbl_CurrentPercent.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbl_CurrentPercent.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.lbl_CurrentPercent.Location = new System.Drawing.Point(150, 200);
            this.lbl_CurrentPercent.Name = "lbl_CurrentPercent";
            this.lbl_CurrentPercent.Size = new System.Drawing.Size(162, 20);
            this.lbl_CurrentPercent.TabIndex = 35;
            this.lbl_CurrentPercent.Text = "Total current percent";
            // 
            // txt_Criteria
            // 
            this.txt_Criteria.BorderColorFocused = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(88)))), ((int)(((byte)(130)))));
            this.txt_Criteria.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.txt_Criteria.BorderColorMouseHover = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(88)))), ((int)(((byte)(130)))));
            this.txt_Criteria.BorderThickness = 3;
            this.txt_Criteria.Cursor = System.Windows.Forms.Cursors.Default;
            this.txt_Criteria.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Criteria.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Criteria.isPassword = false;
            this.txt_Criteria.Location = new System.Drawing.Point(145, 48);
            this.txt_Criteria.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txt_Criteria.Name = "txt_Criteria";
            this.txt_Criteria.Size = new System.Drawing.Size(147, 28);
            this.txt_Criteria.TabIndex = 36;
            this.txt_Criteria.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // txt_Percentage
            // 
            this.txt_Percentage.BorderColorFocused = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(88)))), ((int)(((byte)(130)))));
            this.txt_Percentage.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.txt_Percentage.BorderColorMouseHover = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(88)))), ((int)(((byte)(130)))));
            this.txt_Percentage.BorderThickness = 3;
            this.txt_Percentage.Cursor = System.Windows.Forms.Cursors.Default;
            this.txt_Percentage.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Percentage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Percentage.isPassword = false;
            this.txt_Percentage.Location = new System.Drawing.Point(145, 86);
            this.txt_Percentage.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txt_Percentage.Name = "txt_Percentage";
            this.txt_Percentage.Size = new System.Drawing.Size(69, 28);
            this.txt_Percentage.TabIndex = 37;
            this.txt_Percentage.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txt_Percentage.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_Percentage_KeyPress);
            // 
            // pnl_Header
            // 
            this.pnl_Header.Location = new System.Drawing.Point(2, 1);
            this.pnl_Header.Name = "pnl_Header";
            this.pnl_Header.Size = new System.Drawing.Size(396, 30);
            this.pnl_Header.TabIndex = 62;
            // 
            // frm_Admin_AddEditStudEvalCriteria
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(400, 250);
            this.Controls.Add(this.txt_Percentage);
            this.Controls.Add(this.txt_Criteria);
            this.Controls.Add(this.lbl_CurrentPercent);
            this.Controls.Add(this.bunifuCustomLabel3);
            this.Controls.Add(this.btn_AddCriteria);
            this.Controls.Add(this.bunifuCustomLabel1);
            this.Controls.Add(this.bunifuCustomLabel2);
            this.Controls.Add(this.btn_Close);
            this.Controls.Add(this.pnl_Header);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frm_Admin_AddEditStudEvalCriteria";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add/Edit Student Evaluation Criteria";
            this.Load += new System.EventHandler(this.frm_Admin_AddEditStudEvalCriteria_Load);
            ((System.ComponentModel.ISupportInitialize)(this.btn_Close)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private Bunifu.Framework.UI.BunifuDragControl bunifuDragControl1;
        private Bunifu.Framework.UI.BunifuImageButton btn_Close;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel1;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel2;
        private Bunifu.Framework.UI.BunifuFlatButton btn_AddCriteria;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel3;
        private Bunifu.Framework.UI.BunifuCustomLabel lbl_CurrentPercent;
        public Bunifu.Framework.UI.BunifuMetroTextbox txt_Criteria;
        public Bunifu.Framework.UI.BunifuMetroTextbox txt_Percentage;
        private System.Windows.Forms.Panel pnl_Header;
    }
}