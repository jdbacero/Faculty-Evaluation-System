﻿namespace FacultyEvaluationSystem
{
    partial class frm_PrincipalEvaluation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_PrincipalEvaluation));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.dgv_Instuctional = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.id2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dgv_Attributes = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn4 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dgv_ClassroomManagement = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.id3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn5 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn6 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn7 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn8 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dgv_Assessment = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.id4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn9 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn10 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn11 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn12 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.bunifuDragControl1 = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.btn_Submit = new Bunifu.Framework.UI.BunifuThinButton2();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Instuctional)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Attributes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ClassroomManagement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Assessment)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(375, -2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(165, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "TEACHER’S ATTRIBUTES";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(360, 207);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(191, 18);
            this.label2.TabIndex = 3;
            this.label2.Text = "INSTRUCTIONAL DELIVERY";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(355, 622);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(200, 18);
            this.label3.TabIndex = 7;
            this.label3.Text = "ASSESSMENT OF LEARNING";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(256, 415);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(439, 18);
            this.label4.TabIndex = 5;
            this.label4.Text = "CLASSROOM MANAGEMENT AND LEARNING ENVIRONMENT";
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 3;
            this.bunifuElipse1.TargetControl = this;
            // 
            // dgv_Instuctional
            // 
            this.dgv_Instuctional.AllowUserToAddRows = false;
            this.dgv_Instuctional.AllowUserToDeleteRows = false;
            this.dgv_Instuctional.AllowUserToResizeColumns = false;
            this.dgv_Instuctional.AllowUserToResizeRows = false;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgv_Instuctional.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle10;
            this.dgv_Instuctional.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.dgv_Instuctional.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv_Instuctional.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_Instuctional.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.dgv_Instuctional.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_Instuctional.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id2,
            this.Column6,
            this.Column7,
            this.Column8,
            this.Column9,
            this.Column10});
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Century Gothic", 9F);
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_Instuctional.DefaultCellStyle = dataGridViewCellStyle12;
            this.dgv_Instuctional.DoubleBuffered = true;
            this.dgv_Instuctional.EnableHeadersVisualStyles = false;
            this.dgv_Instuctional.HeaderBgColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.dgv_Instuctional.HeaderForeColor = System.Drawing.Color.White;
            this.dgv_Instuctional.Location = new System.Drawing.Point(5, 223);
            this.dgv_Instuctional.MultiSelect = false;
            this.dgv_Instuctional.Name = "dgv_Instuctional";
            this.dgv_Instuctional.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv_Instuctional.RowHeadersVisible = false;
            this.dgv_Instuctional.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_Instuctional.Size = new System.Drawing.Size(873, 194);
            this.dgv_Instuctional.TabIndex = 10;
            this.dgv_Instuctional.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_Instuctional_CellContentClick);
            this.dgv_Instuctional.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgv_Instuctional_CurrentCellDirtyStateChanged);
            // 
            // id2
            // 
            this.id2.HeaderText = "ID";
            this.id2.Name = "id2";
            this.id2.Visible = false;
            // 
            // Column6
            // 
            this.Column6.DividerWidth = 4;
            this.Column6.HeaderText = "Question";
            this.Column6.MinimumWidth = 510;
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 510;
            // 
            // Column7
            // 
            this.Column7.HeaderText = "Expert";
            this.Column7.MinimumWidth = 90;
            this.Column7.Name = "Column7";
            this.Column7.Width = 90;
            // 
            // Column8
            // 
            this.Column8.HeaderText = "Very Satisafactory";
            this.Column8.MinimumWidth = 90;
            this.Column8.Name = "Column8";
            this.Column8.Width = 90;
            // 
            // Column9
            // 
            this.Column9.HeaderText = "Satisfactory";
            this.Column9.MinimumWidth = 90;
            this.Column9.Name = "Column9";
            this.Column9.Width = 90;
            // 
            // Column10
            // 
            this.Column10.HeaderText = "Unsatisfactory";
            this.Column10.MinimumWidth = 90;
            this.Column10.Name = "Column10";
            this.Column10.Width = 90;
            // 
            // dgv_Attributes
            // 
            this.dgv_Attributes.AllowUserToAddRows = false;
            this.dgv_Attributes.AllowUserToDeleteRows = false;
            this.dgv_Attributes.AllowUserToResizeColumns = false;
            this.dgv_Attributes.AllowUserToResizeRows = false;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgv_Attributes.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle7;
            this.dgv_Attributes.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.dgv_Attributes.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv_Attributes.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_Attributes.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.dgv_Attributes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_Attributes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewCheckBoxColumn1,
            this.dataGridViewCheckBoxColumn2,
            this.dataGridViewCheckBoxColumn3,
            this.dataGridViewCheckBoxColumn4});
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Century Gothic", 9F);
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_Attributes.DefaultCellStyle = dataGridViewCellStyle9;
            this.dgv_Attributes.DoubleBuffered = true;
            this.dgv_Attributes.EnableHeadersVisualStyles = false;
            this.dgv_Attributes.HeaderBgColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.dgv_Attributes.HeaderForeColor = System.Drawing.Color.White;
            this.dgv_Attributes.Location = new System.Drawing.Point(7, 15);
            this.dgv_Attributes.MultiSelect = false;
            this.dgv_Attributes.Name = "dgv_Attributes";
            this.dgv_Attributes.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv_Attributes.RowHeadersVisible = false;
            this.dgv_Attributes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_Attributes.Size = new System.Drawing.Size(873, 194);
            this.dgv_Attributes.TabIndex = 11;
            this.dgv_Attributes.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_Attributes_CellContentClick);
            this.dgv_Attributes.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgv_Attributes_CurrentCellDirtyStateChanged);
            // 
            // id
            // 
            this.id.HeaderText = "ID";
            this.id.Name = "id";
            this.id.Visible = false;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DividerWidth = 4;
            this.dataGridViewTextBoxColumn1.HeaderText = "Question";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 510;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 510;
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.HeaderText = "Expert";
            this.dataGridViewCheckBoxColumn1.MinimumWidth = 90;
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            this.dataGridViewCheckBoxColumn1.Width = 90;
            // 
            // dataGridViewCheckBoxColumn2
            // 
            this.dataGridViewCheckBoxColumn2.HeaderText = "Very Satisafactory";
            this.dataGridViewCheckBoxColumn2.MinimumWidth = 90;
            this.dataGridViewCheckBoxColumn2.Name = "dataGridViewCheckBoxColumn2";
            this.dataGridViewCheckBoxColumn2.Width = 90;
            // 
            // dataGridViewCheckBoxColumn3
            // 
            this.dataGridViewCheckBoxColumn3.HeaderText = "Satisfactory";
            this.dataGridViewCheckBoxColumn3.MinimumWidth = 90;
            this.dataGridViewCheckBoxColumn3.Name = "dataGridViewCheckBoxColumn3";
            this.dataGridViewCheckBoxColumn3.Width = 90;
            // 
            // dataGridViewCheckBoxColumn4
            // 
            this.dataGridViewCheckBoxColumn4.HeaderText = "Unsatisfactory";
            this.dataGridViewCheckBoxColumn4.MinimumWidth = 90;
            this.dataGridViewCheckBoxColumn4.Name = "dataGridViewCheckBoxColumn4";
            this.dataGridViewCheckBoxColumn4.Width = 90;
            // 
            // dgv_ClassroomManagement
            // 
            this.dgv_ClassroomManagement.AllowUserToAddRows = false;
            this.dgv_ClassroomManagement.AllowUserToDeleteRows = false;
            this.dgv_ClassroomManagement.AllowUserToResizeColumns = false;
            this.dgv_ClassroomManagement.AllowUserToResizeRows = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgv_ClassroomManagement.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgv_ClassroomManagement.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.dgv_ClassroomManagement.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv_ClassroomManagement.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_ClassroomManagement.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgv_ClassroomManagement.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_ClassroomManagement.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id3,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewCheckBoxColumn5,
            this.dataGridViewCheckBoxColumn6,
            this.dataGridViewCheckBoxColumn7,
            this.dataGridViewCheckBoxColumn8});
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Century Gothic", 9F);
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_ClassroomManagement.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgv_ClassroomManagement.DoubleBuffered = true;
            this.dgv_ClassroomManagement.EnableHeadersVisualStyles = false;
            this.dgv_ClassroomManagement.HeaderBgColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.dgv_ClassroomManagement.HeaderForeColor = System.Drawing.Color.White;
            this.dgv_ClassroomManagement.Location = new System.Drawing.Point(5, 431);
            this.dgv_ClassroomManagement.MultiSelect = false;
            this.dgv_ClassroomManagement.Name = "dgv_ClassroomManagement";
            this.dgv_ClassroomManagement.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv_ClassroomManagement.RowHeadersVisible = false;
            this.dgv_ClassroomManagement.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_ClassroomManagement.Size = new System.Drawing.Size(873, 194);
            this.dgv_ClassroomManagement.TabIndex = 12;
            this.dgv_ClassroomManagement.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_ClassroomManagement_CellContentClick);
            this.dgv_ClassroomManagement.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgv_ClassroomManagement_CurrentCellDirtyStateChanged);
            // 
            // id3
            // 
            this.id3.HeaderText = "ID";
            this.id3.Name = "id3";
            this.id3.ReadOnly = true;
            this.id3.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DividerWidth = 4;
            this.dataGridViewTextBoxColumn2.HeaderText = "Question";
            this.dataGridViewTextBoxColumn2.MinimumWidth = 510;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 510;
            // 
            // dataGridViewCheckBoxColumn5
            // 
            this.dataGridViewCheckBoxColumn5.HeaderText = "Expert";
            this.dataGridViewCheckBoxColumn5.MinimumWidth = 90;
            this.dataGridViewCheckBoxColumn5.Name = "dataGridViewCheckBoxColumn5";
            this.dataGridViewCheckBoxColumn5.Width = 90;
            // 
            // dataGridViewCheckBoxColumn6
            // 
            this.dataGridViewCheckBoxColumn6.HeaderText = "Very Satisafactory";
            this.dataGridViewCheckBoxColumn6.MinimumWidth = 90;
            this.dataGridViewCheckBoxColumn6.Name = "dataGridViewCheckBoxColumn6";
            this.dataGridViewCheckBoxColumn6.Width = 90;
            // 
            // dataGridViewCheckBoxColumn7
            // 
            this.dataGridViewCheckBoxColumn7.HeaderText = "Satisfactory";
            this.dataGridViewCheckBoxColumn7.MinimumWidth = 90;
            this.dataGridViewCheckBoxColumn7.Name = "dataGridViewCheckBoxColumn7";
            this.dataGridViewCheckBoxColumn7.Width = 90;
            // 
            // dataGridViewCheckBoxColumn8
            // 
            this.dataGridViewCheckBoxColumn8.HeaderText = "Unsatisfactory";
            this.dataGridViewCheckBoxColumn8.MinimumWidth = 90;
            this.dataGridViewCheckBoxColumn8.Name = "dataGridViewCheckBoxColumn8";
            this.dataGridViewCheckBoxColumn8.Width = 90;
            // 
            // dgv_Assessment
            // 
            this.dgv_Assessment.AllowUserToAddRows = false;
            this.dgv_Assessment.AllowUserToDeleteRows = false;
            this.dgv_Assessment.AllowUserToResizeColumns = false;
            this.dgv_Assessment.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgv_Assessment.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_Assessment.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.dgv_Assessment.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv_Assessment.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_Assessment.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_Assessment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_Assessment.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id4,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewCheckBoxColumn9,
            this.dataGridViewCheckBoxColumn10,
            this.dataGridViewCheckBoxColumn11,
            this.dataGridViewCheckBoxColumn12});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Century Gothic", 9F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_Assessment.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgv_Assessment.DoubleBuffered = true;
            this.dgv_Assessment.EnableHeadersVisualStyles = false;
            this.dgv_Assessment.HeaderBgColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.dgv_Assessment.HeaderForeColor = System.Drawing.Color.White;
            this.dgv_Assessment.Location = new System.Drawing.Point(5, 638);
            this.dgv_Assessment.MultiSelect = false;
            this.dgv_Assessment.Name = "dgv_Assessment";
            this.dgv_Assessment.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv_Assessment.RowHeadersVisible = false;
            this.dgv_Assessment.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_Assessment.Size = new System.Drawing.Size(873, 194);
            this.dgv_Assessment.TabIndex = 13;
            this.dgv_Assessment.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_Assessment_CellContentClick);
            this.dgv_Assessment.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgv_Assessment_CurrentCellDirtyStateChanged);
            // 
            // id4
            // 
            this.id4.HeaderText = "ID";
            this.id4.Name = "id4";
            this.id4.Visible = false;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DividerWidth = 4;
            this.dataGridViewTextBoxColumn3.HeaderText = "Question";
            this.dataGridViewTextBoxColumn3.MinimumWidth = 510;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 510;
            // 
            // dataGridViewCheckBoxColumn9
            // 
            this.dataGridViewCheckBoxColumn9.HeaderText = "Expert";
            this.dataGridViewCheckBoxColumn9.MinimumWidth = 90;
            this.dataGridViewCheckBoxColumn9.Name = "dataGridViewCheckBoxColumn9";
            this.dataGridViewCheckBoxColumn9.Width = 90;
            // 
            // dataGridViewCheckBoxColumn10
            // 
            this.dataGridViewCheckBoxColumn10.HeaderText = "Very Satisafactory";
            this.dataGridViewCheckBoxColumn10.MinimumWidth = 90;
            this.dataGridViewCheckBoxColumn10.Name = "dataGridViewCheckBoxColumn10";
            this.dataGridViewCheckBoxColumn10.Width = 90;
            // 
            // dataGridViewCheckBoxColumn11
            // 
            this.dataGridViewCheckBoxColumn11.HeaderText = "Satisfactory";
            this.dataGridViewCheckBoxColumn11.MinimumWidth = 90;
            this.dataGridViewCheckBoxColumn11.Name = "dataGridViewCheckBoxColumn11";
            this.dataGridViewCheckBoxColumn11.Width = 90;
            // 
            // dataGridViewCheckBoxColumn12
            // 
            this.dataGridViewCheckBoxColumn12.HeaderText = "Unsatisfactory";
            this.dataGridViewCheckBoxColumn12.MinimumWidth = 90;
            this.dataGridViewCheckBoxColumn12.Name = "dataGridViewCheckBoxColumn12";
            this.dataGridViewCheckBoxColumn12.Width = 90;
            // 
            // bunifuDragControl1
            // 
            this.bunifuDragControl1.Fixed = true;
            this.bunifuDragControl1.Horizontal = true;
            this.bunifuDragControl1.TargetControl = this;
            this.bunifuDragControl1.Vertical = true;
            // 
            // btn_Submit
            // 
            this.btn_Submit.ActiveBorderThickness = 1;
            this.btn_Submit.ActiveCornerRadius = 20;
            this.btn_Submit.ActiveFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_Submit.ActiveForecolor = System.Drawing.Color.White;
            this.btn_Submit.ActiveLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_Submit.BackColor = System.Drawing.SystemColors.Control;
            this.btn_Submit.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_Submit.BackgroundImage")));
            this.btn_Submit.ButtonText = "Evaluate";
            this.btn_Submit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Submit.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Submit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_Submit.IdleBorderThickness = 1;
            this.btn_Submit.IdleCornerRadius = 20;
            this.btn_Submit.IdleFillColor = System.Drawing.Color.White;
            this.btn_Submit.IdleForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_Submit.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_Submit.Location = new System.Drawing.Point(375, 833);
            this.btn_Submit.Margin = new System.Windows.Forms.Padding(5);
            this.btn_Submit.Name = "btn_Submit";
            this.btn_Submit.Size = new System.Drawing.Size(181, 41);
            this.btn_Submit.TabIndex = 14;
            this.btn_Submit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn_Submit.Click += new System.EventHandler(this.btn_Submit_Click);
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(2, 840);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(159, 18);
            this.label5.TabIndex = 15;
            this.label5.Text = "Currently Evaluating:";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(14, 861);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(123, 18);
            this.label6.TabIndex = 16;
            this.label6.Text = "Teachers Name";
            // 
            // frm_PrincipalEvaluation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(890, 880);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btn_Submit);
            this.Controls.Add(this.dgv_Assessment);
            this.Controls.Add(this.dgv_ClassroomManagement);
            this.Controls.Add(this.dgv_Attributes);
            this.Controls.Add(this.dgv_Instuctional);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frm_PrincipalEvaluation";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frm_PrincipalEvalusation";
            this.Load += new System.EventHandler(this.frm_PrincipalEvaluation_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Instuctional)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Attributes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ClassroomManagement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Assessment)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private Bunifu.Framework.UI.BunifuCustomDataGrid dgv_Instuctional;
        private Bunifu.Framework.UI.BunifuCustomDataGrid dgv_Assessment;
        private Bunifu.Framework.UI.BunifuCustomDataGrid dgv_ClassroomManagement;
        private Bunifu.Framework.UI.BunifuCustomDataGrid dgv_Attributes;
        private Bunifu.Framework.UI.BunifuDragControl bunifuDragControl1;
        private Bunifu.Framework.UI.BunifuThinButton2 btn_Submit;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridViewTextBoxColumn id4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn9;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn10;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn11;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn id3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn5;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn6;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn7;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn id2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column7;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column8;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column9;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column10;
    }
}