﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FacultyEvaluationSystem
{
    public partial class frm_Admin_AddEditPrincipalQuestion : Form
    {
        string action;
        public int id;
        dataClass_FacultyEvaluationSystemDataContext db = new dataClass_FacultyEvaluationSystemDataContext();
        public DataGridView dgv;

        public frm_Admin_AddEditPrincipalQuestion(string Action)
        {
            InitializeComponent();
            //this "action" variable here is to identify if the clicked button from frm_Admin_PrinEvalQuestion is either add or edit
            //this is because the add and edit UI were merge
            action = Action;
        }

        private void btn_Add_Click(object sender, EventArgs e)
        {
            // this checks if the action is add or edit to execute their specific stored procedures
            if(cmb_Criteria.SelectedIndex < 0 || txt_Question.Text.Trim() == "")
            {
                MessageBox.Show("Please fill all the inputs.");
                return;
            }

            if (action == "Add")
            {
                db.sp_InsertPrincipalQuestion(id, cmb_Criteria.SelectedItem.ToString(), txt_Question.Text);
                dgv.DataSource = db.sp_ViewEvalutationQuestionsByID(id);
                dgv.ClearSelection();
                MessageBox.Show("Successfully added question.");
            }
            else
            {
                db.sp_UpdatePrincipalQuestion(id, cmb_Criteria.SelectedItem.ToString(), txt_Question.Text);
                MessageBox.Show("Successfully edited question.");
                this.Close();
            }
        }
        //added close butn
        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frm_Admin_AddPrincipalQuestion_Load(object sender, EventArgs e)
        {
            if (action != "Add")
            {
                btn_Add.Text = "Edit";
            }
        }
    }
}
