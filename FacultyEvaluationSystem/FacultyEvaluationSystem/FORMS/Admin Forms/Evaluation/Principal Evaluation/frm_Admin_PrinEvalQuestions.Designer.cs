﻿namespace FacultyEvaluationSystem
{
    partial class frm_Admin_PrinEvalQuestions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Admin_PrinEvalQuestions));
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.btn_Close = new Bunifu.Framework.UI.BunifuImageButton();
            this.dgv_EvaluationID = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.dgv_Questions = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.btn_CreateSet = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btn_AddQuestion = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btn_EditQuestion = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuCustomLabel2 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.btn_DeleteQuestion = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuDragControl1 = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.cmb_Criteria = new Bunifu.Framework.UI.BunifuDropdown();
            this.pnl_Header = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.btn_Close)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_EvaluationID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Questions)).BeginInit();
            this.SuspendLayout();
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 3;
            this.bunifuElipse1.TargetControl = this;
            // 
            // btn_Close
            // 
            this.btn_Close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Close.BackColor = System.Drawing.Color.Transparent;
            this.btn_Close.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Close.Image = ((System.Drawing.Image)(resources.GetObject("btn_Close.Image")));
            this.btn_Close.ImageActive = null;
            this.btn_Close.Location = new System.Drawing.Point(938, 1);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(30, 30);
            this.btn_Close.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btn_Close.TabIndex = 21;
            this.btn_Close.TabStop = false;
            this.btn_Close.Zoom = 10;
            this.btn_Close.Click += new System.EventHandler(this.btn_Close_Click);
            // 
            // dgv_EvaluationID
            // 
            this.dgv_EvaluationID.AllowUserToAddRows = false;
            this.dgv_EvaluationID.AllowUserToDeleteRows = false;
            this.dgv_EvaluationID.AllowUserToResizeColumns = false;
            this.dgv_EvaluationID.AllowUserToResizeRows = false;
            dataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle33.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgv_EvaluationID.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle33;
            this.dgv_EvaluationID.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_EvaluationID.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_EvaluationID.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.dgv_EvaluationID.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv_EvaluationID.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle34.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            dataGridViewCellStyle34.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle34.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle34.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle34.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle34.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_EvaluationID.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle34;
            this.dgv_EvaluationID.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle35.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle35.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle35.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle35.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle35.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_EvaluationID.DefaultCellStyle = dataGridViewCellStyle35;
            this.dgv_EvaluationID.DoubleBuffered = true;
            this.dgv_EvaluationID.EnableHeadersVisualStyles = false;
            this.dgv_EvaluationID.HeaderBgColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.dgv_EvaluationID.HeaderForeColor = System.Drawing.Color.White;
            this.dgv_EvaluationID.Location = new System.Drawing.Point(16, 91);
            this.dgv_EvaluationID.MultiSelect = false;
            this.dgv_EvaluationID.Name = "dgv_EvaluationID";
            this.dgv_EvaluationID.ReadOnly = true;
            this.dgv_EvaluationID.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle36.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle36.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle36.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle36.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle36.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_EvaluationID.RowHeadersDefaultCellStyle = dataGridViewCellStyle36;
            this.dgv_EvaluationID.RowHeadersVisible = false;
            this.dgv_EvaluationID.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgv_EvaluationID.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_EvaluationID.Size = new System.Drawing.Size(935, 130);
            this.dgv_EvaluationID.TabIndex = 22;
            this.dgv_EvaluationID.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_EvaluationID_CellClick);
            // 
            // dgv_Questions
            // 
            this.dgv_Questions.AllowUserToAddRows = false;
            this.dgv_Questions.AllowUserToDeleteRows = false;
            this.dgv_Questions.AllowUserToResizeColumns = false;
            this.dgv_Questions.AllowUserToResizeRows = false;
            dataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle37.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgv_Questions.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle37;
            this.dgv_Questions.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_Questions.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_Questions.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.dgv_Questions.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv_Questions.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle38.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            dataGridViewCellStyle38.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle38.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle38.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle38.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle38.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_Questions.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle38;
            this.dgv_Questions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle39.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle39.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle39.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle39.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle39.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_Questions.DefaultCellStyle = dataGridViewCellStyle39;
            this.dgv_Questions.DoubleBuffered = true;
            this.dgv_Questions.EnableHeadersVisualStyles = false;
            this.dgv_Questions.HeaderBgColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.dgv_Questions.HeaderForeColor = System.Drawing.Color.White;
            this.dgv_Questions.Location = new System.Drawing.Point(16, 285);
            this.dgv_Questions.MultiSelect = false;
            this.dgv_Questions.Name = "dgv_Questions";
            this.dgv_Questions.ReadOnly = true;
            this.dgv_Questions.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle40.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle40.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle40.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle40.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle40.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_Questions.RowHeadersDefaultCellStyle = dataGridViewCellStyle40;
            this.dgv_Questions.RowHeadersVisible = false;
            this.dgv_Questions.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgv_Questions.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_Questions.Size = new System.Drawing.Size(935, 230);
            this.dgv_Questions.TabIndex = 23;
            this.dgv_Questions.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_Questions_CellClick);
            // 
            // btn_CreateSet
            // 
            this.btn_CreateSet.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_CreateSet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_CreateSet.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_CreateSet.BorderRadius = 3;
            this.btn_CreateSet.ButtonText = "Create new set";
            this.btn_CreateSet.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_CreateSet.DisabledColor = System.Drawing.Color.Gray;
            this.btn_CreateSet.ForeColor = System.Drawing.Color.White;
            this.btn_CreateSet.Iconcolor = System.Drawing.Color.Transparent;
            this.btn_CreateSet.Iconimage = ((System.Drawing.Image)(resources.GetObject("btn_CreateSet.Iconimage")));
            this.btn_CreateSet.Iconimage_right = null;
            this.btn_CreateSet.Iconimage_right_Selected = null;
            this.btn_CreateSet.Iconimage_Selected = null;
            this.btn_CreateSet.IconMarginLeft = 0;
            this.btn_CreateSet.IconMarginRight = 0;
            this.btn_CreateSet.IconRightVisible = true;
            this.btn_CreateSet.IconRightZoom = 0D;
            this.btn_CreateSet.IconVisible = false;
            this.btn_CreateSet.IconZoom = 90D;
            this.btn_CreateSet.IsTab = false;
            this.btn_CreateSet.Location = new System.Drawing.Point(16, 45);
            this.btn_CreateSet.Name = "btn_CreateSet";
            this.btn_CreateSet.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_CreateSet.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(88)))), ((int)(((byte)(130)))));
            this.btn_CreateSet.OnHoverTextColor = System.Drawing.Color.White;
            this.btn_CreateSet.selected = false;
            this.btn_CreateSet.Size = new System.Drawing.Size(139, 40);
            this.btn_CreateSet.TabIndex = 27;
            this.btn_CreateSet.Text = "Create new set";
            this.btn_CreateSet.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn_CreateSet.Textcolor = System.Drawing.Color.White;
            this.btn_CreateSet.TextFont = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_CreateSet.Click += new System.EventHandler(this.btn_CreateSet_Click);
            // 
            // btn_AddQuestion
            // 
            this.btn_AddQuestion.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_AddQuestion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_AddQuestion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_AddQuestion.BorderRadius = 3;
            this.btn_AddQuestion.ButtonText = "Add Question";
            this.btn_AddQuestion.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_AddQuestion.DisabledColor = System.Drawing.Color.Gray;
            this.btn_AddQuestion.ForeColor = System.Drawing.Color.White;
            this.btn_AddQuestion.Iconcolor = System.Drawing.Color.Transparent;
            this.btn_AddQuestion.Iconimage = ((System.Drawing.Image)(resources.GetObject("btn_AddQuestion.Iconimage")));
            this.btn_AddQuestion.Iconimage_right = null;
            this.btn_AddQuestion.Iconimage_right_Selected = null;
            this.btn_AddQuestion.Iconimage_Selected = null;
            this.btn_AddQuestion.IconMarginLeft = 0;
            this.btn_AddQuestion.IconMarginRight = 0;
            this.btn_AddQuestion.IconRightVisible = true;
            this.btn_AddQuestion.IconRightZoom = 0D;
            this.btn_AddQuestion.IconVisible = false;
            this.btn_AddQuestion.IconZoom = 90D;
            this.btn_AddQuestion.IsTab = false;
            this.btn_AddQuestion.Location = new System.Drawing.Point(16, 239);
            this.btn_AddQuestion.Name = "btn_AddQuestion";
            this.btn_AddQuestion.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_AddQuestion.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(88)))), ((int)(((byte)(130)))));
            this.btn_AddQuestion.OnHoverTextColor = System.Drawing.Color.White;
            this.btn_AddQuestion.selected = false;
            this.btn_AddQuestion.Size = new System.Drawing.Size(139, 40);
            this.btn_AddQuestion.TabIndex = 28;
            this.btn_AddQuestion.Text = "Add Question";
            this.btn_AddQuestion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn_AddQuestion.Textcolor = System.Drawing.Color.White;
            this.btn_AddQuestion.TextFont = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_AddQuestion.Click += new System.EventHandler(this.btn_Add_Click);
            // 
            // btn_EditQuestion
            // 
            this.btn_EditQuestion.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_EditQuestion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_EditQuestion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_EditQuestion.BorderRadius = 3;
            this.btn_EditQuestion.ButtonText = "Edit Question";
            this.btn_EditQuestion.Cursor = System.Windows.Forms.Cursors.No;
            this.btn_EditQuestion.DisabledColor = System.Drawing.Color.Gray;
            this.btn_EditQuestion.Enabled = false;
            this.btn_EditQuestion.ForeColor = System.Drawing.Color.White;
            this.btn_EditQuestion.Iconcolor = System.Drawing.Color.Transparent;
            this.btn_EditQuestion.Iconimage = ((System.Drawing.Image)(resources.GetObject("btn_EditQuestion.Iconimage")));
            this.btn_EditQuestion.Iconimage_right = null;
            this.btn_EditQuestion.Iconimage_right_Selected = null;
            this.btn_EditQuestion.Iconimage_Selected = null;
            this.btn_EditQuestion.IconMarginLeft = 0;
            this.btn_EditQuestion.IconMarginRight = 0;
            this.btn_EditQuestion.IconRightVisible = true;
            this.btn_EditQuestion.IconRightZoom = 0D;
            this.btn_EditQuestion.IconVisible = false;
            this.btn_EditQuestion.IconZoom = 90D;
            this.btn_EditQuestion.IsTab = false;
            this.btn_EditQuestion.Location = new System.Drawing.Point(198, 239);
            this.btn_EditQuestion.Name = "btn_EditQuestion";
            this.btn_EditQuestion.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_EditQuestion.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(88)))), ((int)(((byte)(130)))));
            this.btn_EditQuestion.OnHoverTextColor = System.Drawing.Color.White;
            this.btn_EditQuestion.selected = false;
            this.btn_EditQuestion.Size = new System.Drawing.Size(139, 40);
            this.btn_EditQuestion.TabIndex = 29;
            this.btn_EditQuestion.Text = "Edit Question";
            this.btn_EditQuestion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn_EditQuestion.Textcolor = System.Drawing.Color.White;
            this.btn_EditQuestion.TextFont = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_EditQuestion.Click += new System.EventHandler(this.btn_Edit_Click);
            // 
            // bunifuCustomLabel2
            // 
            this.bunifuCustomLabel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuCustomLabel2.AutoSize = true;
            this.bunifuCustomLabel2.Font = new System.Drawing.Font("Century Gothic", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bunifuCustomLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.bunifuCustomLabel2.Location = new System.Drawing.Point(12, 9);
            this.bunifuCustomLabel2.Name = "bunifuCustomLabel2";
            this.bunifuCustomLabel2.Size = new System.Drawing.Size(271, 22);
            this.bunifuCustomLabel2.TabIndex = 30;
            this.bunifuCustomLabel2.Text = "Principal Evaluation Questions";
            // 
            // btn_DeleteQuestion
            // 
            this.btn_DeleteQuestion.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_DeleteQuestion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_DeleteQuestion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_DeleteQuestion.BorderRadius = 3;
            this.btn_DeleteQuestion.ButtonText = "Delete Question";
            this.btn_DeleteQuestion.Cursor = System.Windows.Forms.Cursors.No;
            this.btn_DeleteQuestion.DisabledColor = System.Drawing.Color.Gray;
            this.btn_DeleteQuestion.Enabled = false;
            this.btn_DeleteQuestion.ForeColor = System.Drawing.Color.White;
            this.btn_DeleteQuestion.Iconcolor = System.Drawing.Color.Transparent;
            this.btn_DeleteQuestion.Iconimage = ((System.Drawing.Image)(resources.GetObject("btn_DeleteQuestion.Iconimage")));
            this.btn_DeleteQuestion.Iconimage_right = null;
            this.btn_DeleteQuestion.Iconimage_right_Selected = null;
            this.btn_DeleteQuestion.Iconimage_Selected = null;
            this.btn_DeleteQuestion.IconMarginLeft = 0;
            this.btn_DeleteQuestion.IconMarginRight = 0;
            this.btn_DeleteQuestion.IconRightVisible = true;
            this.btn_DeleteQuestion.IconRightZoom = 0D;
            this.btn_DeleteQuestion.IconVisible = false;
            this.btn_DeleteQuestion.IconZoom = 90D;
            this.btn_DeleteQuestion.IsTab = false;
            this.btn_DeleteQuestion.Location = new System.Drawing.Point(385, 239);
            this.btn_DeleteQuestion.Name = "btn_DeleteQuestion";
            this.btn_DeleteQuestion.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.btn_DeleteQuestion.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(88)))), ((int)(((byte)(130)))));
            this.btn_DeleteQuestion.OnHoverTextColor = System.Drawing.Color.White;
            this.btn_DeleteQuestion.selected = false;
            this.btn_DeleteQuestion.Size = new System.Drawing.Size(139, 40);
            this.btn_DeleteQuestion.TabIndex = 31;
            this.btn_DeleteQuestion.Text = "Delete Question";
            this.btn_DeleteQuestion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn_DeleteQuestion.Textcolor = System.Drawing.Color.White;
            this.btn_DeleteQuestion.TextFont = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_DeleteQuestion.Visible = false;
            this.btn_DeleteQuestion.Click += new System.EventHandler(this.btn_DeleteQuestion_Click);
            // 
            // bunifuDragControl1
            // 
            this.bunifuDragControl1.Fixed = true;
            this.bunifuDragControl1.Horizontal = true;
            this.bunifuDragControl1.TargetControl = this.pnl_Header;
            this.bunifuDragControl1.Vertical = true;
            // 
            // cmb_Criteria
            // 
            this.cmb_Criteria.BackColor = System.Drawing.Color.Transparent;
            this.cmb_Criteria.BorderRadius = 3;
            this.cmb_Criteria.DisabledColor = System.Drawing.Color.Gray;
            this.cmb_Criteria.Enabled = false;
            this.cmb_Criteria.ForeColor = System.Drawing.Color.White;
            this.cmb_Criteria.Items = new string[] {
        "ALL",
        "TEACHER’S ATTRIBUTES",
        "INSTRUCTIONAL DELIVERY",
        "CLASSROOM MANAGEMENT AND LEARNING ENVIRONMENT",
        "ASSESSMENT OF LEARNING"};
            this.cmb_Criteria.Location = new System.Drawing.Point(552, 239);
            this.cmb_Criteria.Name = "cmb_Criteria";
            this.cmb_Criteria.NomalColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(47)))), ((int)(((byte)(92)))));
            this.cmb_Criteria.onHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(88)))), ((int)(((byte)(130)))));
            this.cmb_Criteria.selectedIndex = -1;
            this.cmb_Criteria.Size = new System.Drawing.Size(399, 35);
            this.cmb_Criteria.TabIndex = 32;
            this.cmb_Criteria.onItemSelected += new System.EventHandler(this.cmb_Criteria_onItemSelected);
            // 
            // pnl_Header
            // 
            this.pnl_Header.Location = new System.Drawing.Point(1, 1);
            this.pnl_Header.Name = "pnl_Header";
            this.pnl_Header.Size = new System.Drawing.Size(967, 30);
            this.pnl_Header.TabIndex = 62;
            // 
            // frm_Admin_PrinEvalQuestions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(970, 530);
            this.Controls.Add(this.cmb_Criteria);
            this.Controls.Add(this.btn_DeleteQuestion);
            this.Controls.Add(this.bunifuCustomLabel2);
            this.Controls.Add(this.btn_EditQuestion);
            this.Controls.Add(this.btn_AddQuestion);
            this.Controls.Add(this.btn_CreateSet);
            this.Controls.Add(this.dgv_Questions);
            this.Controls.Add(this.dgv_EvaluationID);
            this.Controls.Add(this.btn_Close);
            this.Controls.Add(this.pnl_Header);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frm_Admin_PrinEvalQuestions";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Principal Evaluation Questions";
            this.Load += new System.EventHandler(this.frm_Admin_PrinEvalQuestions_Load);
            ((System.ComponentModel.ISupportInitialize)(this.btn_Close)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_EvaluationID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Questions)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private Bunifu.Framework.UI.BunifuImageButton btn_Close;
        private Bunifu.Framework.UI.BunifuCustomDataGrid dgv_EvaluationID;
        private Bunifu.Framework.UI.BunifuCustomDataGrid dgv_Questions;
        private Bunifu.Framework.UI.BunifuFlatButton btn_CreateSet;
        private Bunifu.Framework.UI.BunifuFlatButton btn_AddQuestion;
        private Bunifu.Framework.UI.BunifuFlatButton btn_EditQuestion;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel2;
        private Bunifu.Framework.UI.BunifuFlatButton btn_DeleteQuestion;
        private Bunifu.Framework.UI.BunifuDragControl bunifuDragControl1;
        private Bunifu.Framework.UI.BunifuDropdown cmb_Criteria;
        private System.Windows.Forms.Panel pnl_Header;
    }
}