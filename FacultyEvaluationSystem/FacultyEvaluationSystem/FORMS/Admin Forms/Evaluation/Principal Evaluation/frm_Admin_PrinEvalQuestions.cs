﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FacultyEvaluationSystem
{
    public partial class frm_Admin_PrinEvalQuestions : Form
    {
        dataClass_FacultyEvaluationSystemDataContext db = new dataClass_FacultyEvaluationSystemDataContext();

        public frm_Admin_PrinEvalQuestions()
        {
            InitializeComponent();
        }

        private void frm_Admin_PrinEvalQuestions_Load(object sender, EventArgs e)
        {
            dgv_EvaluationID.DataSource = db.sp_ViewEvaluationPrincipalID();
            dgv_EvaluationID.ClearSelection();
        }

        private void btn_CreateSet_Click(object sender, EventArgs e)
        {
            // this creates a new set (version) of evaluation, the name depends on the date created
            string date = DateTime.Today.Year + "-" + DateTime.Today.Month + "-" + DateTime.Today.Day;
            db.sp_InsertEvaluationID(DateTime.Parse(date));
            dgv_EvaluationID.DataSource = db.sp_ViewEvaluationPrincipalID();
            dgv_EvaluationID.ClearSelection();
            MessageBox.Show("Added new empty set of evaluation questions.");
        }

        private void frm_Admin_PrinEvalQuestions_Load_1(object sender, EventArgs e)
        {
            dgv_EvaluationID.DataSource = db.sp_ViewEvaluationPrincipalID();
            dgv_EvaluationID.ClearSelection();
        }

        private void btn_Add_Click(object sender, EventArgs e)
        {
            // this section triggers the adding of evaluation question
            // however, this button only transfer the action and the evaluation id (evaluation set id) to another form
            // to execute the real adding procedure
            if (dgv_EvaluationID.SelectedRows.Count == 0) { MessageBox.Show("Select evaluation ID."); return; }
            cmb_Criteria.selectedIndex = 0;
            frm_Admin_AddEditPrincipalQuestion frm_Add = new frm_Admin_AddEditPrincipalQuestion("Add");
            frm_Add.id = int.Parse(dgv_EvaluationID.CurrentRow.Cells["ID_Evaluation"].Value.ToString());
            frm_Add.dgv = dgv_Questions;
            frm_Add.ShowDialog();
            btn_EditQuestion.Enabled = false;
            btn_DeleteQuestion.Enabled = false;
            btn_EditQuestion.Cursor = Cursors.No;
            btn_DeleteQuestion.Cursor = Cursors.No;
            dgv_Questions.ClearSelection();
            //dgv_Questions.DataSource = db.sp_ViewEvalutationQuestionsByID(int.Parse(cmb_ID.SelectedItem.ToString()));
            //dgv_Questions.ClearSelection();
        }

        private void dgv_Questions_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            // this enables deleting and editing of questions
            btn_EditQuestion.Enabled = true;
            btn_DeleteQuestion.Enabled = true;
            btn_EditQuestion.Cursor = Cursors.Hand;
            btn_DeleteQuestion.Cursor = Cursors.Hand;
        }

        private void btn_Edit_Click(object sender, EventArgs e)
        {
            // same with the adding button, this triggers the editing of evaluation question
            // this transfers all values from the selected row to be edited in the AddEditPrincipalQuestion form
            frm_Admin_AddEditPrincipalQuestion frm_Edit = new frm_Admin_AddEditPrincipalQuestion("Edit");
            frm_Edit.id = int.Parse(dgv_Questions.CurrentRow.Cells["ID_Question"].Value.ToString());
            frm_Edit.cmb_Criteria.SelectedItem = dgv_Questions.CurrentRow.Cells["Criteria"].Value.ToString();
            frm_Edit.txt_Question.Text = dgv_Questions.CurrentRow.Cells["Questions"].Value.ToString();
            frm_Edit.ShowDialog();
            dgv_Questions.DataSource = db.sp_ViewEvalutationQuestionsByID(int.Parse(dgv_EvaluationID.CurrentRow.Cells["ID_Evaluation"].Value.ToString()));
            cmb_Criteria.selectedIndex = 0;
            dgv_Questions.ClearSelection();
            btn_EditQuestion.Enabled = false;
            btn_DeleteQuestion.Enabled = false;
            btn_EditQuestion.Cursor = Cursors.No;
            btn_DeleteQuestion.Cursor = Cursors.No;
        }

        private void dgv_EvaluationID_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            // this event listener is to show all criterias and questions in the selected evaluation set ID
            cmb_Criteria.Enabled = true;
            cmb_Criteria.selectedIndex = 0;
            dgv_Questions.DataSource = db.sp_ViewEvalutationQuestionsByID(int.Parse(dgv_EvaluationID.CurrentRow.Cells["ID_Evaluation"].Value.ToString()));
            dgv_Questions.Columns["ID_Question"].Visible = false;
            dgv_Questions.Columns["ID_Evaluation"].Visible = false;
            dgv_Questions.ClearSelection();
            btn_EditQuestion.Enabled = false;
            btn_DeleteQuestion.Enabled = false;
            btn_EditQuestion.Cursor = Cursors.No;
            btn_DeleteQuestion.Cursor = Cursors.No;
        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_DeleteQuestion_Click(object sender, EventArgs e)
        {
            //to be coded
            btn_EditQuestion.Enabled = false;
            btn_DeleteQuestion.Enabled = false;
            btn_EditQuestion.Cursor = Cursors.No;
            btn_DeleteQuestion.Cursor = Cursors.No;
            dgv_Questions.ClearSelection();
        }

        private void cmb_Criteria_onItemSelected(object sender, EventArgs e)
        {
            // sorting purposes
            switch(cmb_Criteria.selectedValue.ToString())
            {
                case "ALL":
                    dgv_Questions.DataSource = db.sp_ViewEvalutationQuestionsByID(int.Parse(dgv_EvaluationID.CurrentRow.Cells["ID_Evaluation"].Value.ToString()));
                    break;

                default:
                    dgv_Questions.DataSource = db.sp_ViewEvaluationQuestionsByID_ByCriteria(int.Parse(dgv_EvaluationID.CurrentRow.Cells["ID_Evaluation"].Value.ToString()), cmb_Criteria.selectedValue.ToString());
                    break;
            }
        }
    }
}
