﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FacultyEvaluationSystem
{
    public partial class frm_PrincipalEvaluation : Form
    {
        //note: DGV COLUMN INDEX 0 - QUESTION ID INDEX 1 - QUESTION

        dataClass_FacultyEvaluationSystemDataContext db = new dataClass_FacultyEvaluationSystemDataContext();
        List<Bunifu.Framework.UI.BunifuCustomDataGrid> dgv = new List<Bunifu.Framework.UI.BunifuCustomDataGrid>();
        int id_sched, id_teacher, id_evaluation;
        string[] criterias = { "TEACHER’S ATTRIBUTES", "INSTRUCTIONAL DELIVERY",
                                "CLASSROOM MANAGEMENT AND LEARNING ENVIRONMENT",
                                                        "ASSESSMENT OF LEARNING"};
        public frm_PrincipalEvaluation(int Id_sched, int Id_teacher)
        {
            InitializeComponent();
            id_sched = Id_sched;
            id_teacher = Id_teacher;
        }

        private void frm_PrincipalEvaluation_Load(object sender, EventArgs e)
        {
            
            foreach (var item in db.sp_ViewEvaluationPrincipalScheduleByID(id_sched))
            {
                id_evaluation = item.ID_EvaluationPrincipal;
            }

            //Gi-add tanan datagrids in a list
            dgv.Add(dgv_Attributes);dgv.Add(dgv_Instuctional);dgv.Add(dgv_ClassroomManagement);dgv.Add(dgv_Assessment);

            //loop para mabutngan ug questions ang dgv's using the foreach and the dgv list
            int x = 0;
            foreach (var item in criterias)
            {
                int y = 0;
                foreach (var items in db.sp_ViewEvaluationQuestionsByID_ByCriteria(id_evaluation,item))
                {
                    dgv[x].Rows.Add();
                    dgv[x].Rows[y].Cells[0].Value = items.ID_Question;
                    dgv[x].Rows[y].Cells[1].Value = items.Questions;
                    y++;
                }
                x++;
            }
        }

        private void btn_Submit_Click(object sender, EventArgs e)
        {
            #region checks for unanswered quesions
            //check first if there are unanswered questions for every dgv
            //the first for loop, in order to loop in between dgv's
            for (int i = 0; i < dgv.Count(); i++)
            {
                //second for loop, in order to loop in between dgv rows
                for (int x = 0; x < dgv[i].Rows.Count; x++)
                {
                    //Bool used to as means to determine if there's no answer
                    bool unanswered = true;
                    //third for loop, in order to loop in between cells
                    for (int y = 2; y < 6; y++)
                    {
                        DataGridViewCheckBoxCell chkBox = dgv[i].Rows[x].Cells[y] as DataGridViewCheckBoxCell;
                        if (Convert.ToBoolean(chkBox.Value) == true)
                        {
                            unanswered = false;
                        }
                    }

                    if(unanswered == true)
                    {
                        MessageBox.Show("One or more questions are left unanswered.");
                        return;
                    }

                }
            }
            #endregion

            #region insert to database
            for (int i = 0; i < dgv.Count(); i++)
            {
                //second for loop, in order to loop in between dgv rows
                for (int x = 0; x < dgv[i].Rows.Count; x++)
                {
                    int z = 4;
                    //third for loop, in order to loop in between cells
                    for (int y = 2; y < 6; y++)
                    {
                        DataGridViewCheckBoxCell chkBox = dgv[i].Rows[x].Cells[y] as DataGridViewCheckBoxCell;
                        if (Convert.ToBoolean(chkBox.Value) == true)
                        {
                            db.sp_InsertEvaluationPrincipalResult(id_sched, id_teacher, (int)dgv[i].Rows[x].Cells[0].Value, z);
                        }
                        z--;
                    }
                }
            }
            #endregion

            decimal scoreAssess = 0;
            decimal totalAssess = 0;
            decimal scoreClass= 0;
            decimal totalClass= 0;
            decimal scoreInst = 0;
            decimal totalInstn= 0;
            decimal scoreAttr = 0;
            decimal totalAttr= 0;

            foreach (var item in db.sp_ViewPrincipalEvaluationResultFinishedByTeacherID(id_teacher, id_sched))
            {
                switch (item.Criteria)
                {
                    case "ASSESSMENT OF LEARNING":
                        scoreAssess+= Math.Round(((decimal)item.Score / (decimal)item.Total) * 0.1m, 2);
                        totalAssess = (decimal)item.Score;
                        //lblWeightedScoreAssess.Text = "" + Math.Round((decimal)item.Score * 0.1m, 2);
                        //lblWeightedTotalAssess.Text = "" + Math.Round((decimal)item.Total * 0.1m, 2);
                        break;

                    case "CLASSROOM MANAGEMENT AND LEARNING ENVIRONMENT":
                        scoreClass+= Math.Round(((decimal)item.Score / (decimal)item.Total) * 0.2m, 2) ;
                        totalClass= (decimal)item.Score;
                        //lblWeightedScoreClass.Text = "" + Math.Round((decimal)item.Score * 0.2m, 2);
                        //lblWeightedTotalClass.Text = "" + Math.Round((decimal)item.Total * 0.2m, 2);
                        break;

                    case "INSTRUCTIONAL DELIVERY":
                        scoreInst+= Math.Round(((decimal)item.Score / (decimal)item.Total) * 0.5m, 2) ;
                        totalInstn= (decimal)item.Score;
                        //lblWeightedScoreInst.Text = "" + Math.Round((decimal)item.Score * 0.5m, 2);
                        //lblWeightedTotalInst.Text = "" + Math.Round((decimal)item.Total * 0.5m, 2);
                        break;

                    default:
                        scoreAttr+= Math.Round(((decimal)item.Score / (decimal)item.Total) * 0.2m, 2);
                        totalAttr= (decimal)item.Score;
                        //lblWeigtedScroreAttr.Text = "" + Math.Round((decimal)item.Score * 0.2m, 2);
                        //lblWeightedTotalAttr.Text = "" + Math.Round((decimal)item.Total * 0.2m, 2);
                        break;
                }
            }
            db.sp_InsertEvaluationPrincipalResultOverall2(id_sched, id_teacher,
                (float)scoreAttr, (float)scoreInst, (float)scoreClass,(float)scoreAssess,DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm")));
            MessageBox.Show("You have successfully completed the evaluation!");
            this.Close();
        }

        //cellcontent clicks
        private void dgv_Instuctional_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            dgvCheckboxtoRadioBehaviour(dgv_Instuctional, e);
        }
        private void dgv_Assessment_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            dgvCheckboxtoRadioBehaviour(dgv_Assessment, e);
        }
        private void dgv_Attributes_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            dgvCheckboxtoRadioBehaviour(dgv_Attributes, e);
        }
        private void dgv_ClassroomManagement_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            dgvCheckboxtoRadioBehaviour(dgv_ClassroomManagement, e);
        }

        //CurrentCellDirtyStateChanged
        private void dgv_Instuctional_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            dgv_Instuctional.CommitEdit(DataGridViewDataErrorContexts.Commit);
        }
        private void dgv_ClassroomManagement_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            dgv_ClassroomManagement.CommitEdit(DataGridViewDataErrorContexts.Commit);
        }
        private void dgv_Attributes_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            dgv_Attributes.CommitEdit(DataGridViewDataErrorContexts.Commit);
        }
        private void dgv_Assessment_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            dgv_Assessment.CommitEdit(DataGridViewDataErrorContexts.Commit);
        }

        //function for the checkbox behave like radio button
        private void dgvCheckboxtoRadioBehaviour(Bunifu.Framework.UI.BunifuCustomDataGrid dgv, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0) return;
            for (int i = 2; i < 6; i++)
            {
                if (i != e.ColumnIndex)
                {
                    dgv.CurrentRow.Cells[i].Value = false;
                }
            }
        }


    }
}
