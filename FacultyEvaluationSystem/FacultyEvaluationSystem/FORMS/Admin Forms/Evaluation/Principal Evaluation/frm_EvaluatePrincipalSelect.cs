﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FacultyEvaluationSystem
{
    public partial class frm_EvaluatePrincipalSelect : Form
    {
        dataClass_FacultyEvaluationSystemDataContext db = new dataClass_FacultyEvaluationSystemDataContext();
        string schoolYear;
        int idSched;
        public string teacherName;
        public int id_Teacher;
        public bool evaluate = false;
        public frm_EvaluatePrincipalSelect(string SchoolYear, int IdSched)
        {
            InitializeComponent();
            schoolYear = SchoolYear;
            idSched = IdSched;
        }

        private void frm_EvaluatePrincipalSelect_Load(object sender, EventArgs e)
        {
            dgv_Teachers.DataSource = db.sp_ViewTeachersBySchoolYear(schoolYear);
            dgv_Teachers.Columns[0].Visible = false;
            dgv_Teachers.Columns.Add("status", "Status");

            for (int i = 0; i < dgv_Teachers.Rows.Count; i++)
            {
                if(db.sp_ViewEvaluationResultByTeacherID_ScheduleID(((int)dgv_Teachers.Rows[i].Cells[0].Value), idSched).Any())
                {
                    dgv_Teachers.Rows[i].Cells["status"].Value = "Evaluated";
                }
                else
                {
                    dgv_Teachers.Rows[i].Cells["status"].Value = "Ready for Evaluation";
                }
            }
        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgv_Teachers_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgv_Teachers.CurrentRow.Cells["status"].Value.ToString() != "Evaluated")
            {
                evaluate = true;
                id_Teacher = (int)dgv_Teachers.CurrentRow.Cells[0].Value;
                teacherName = dgv_Teachers.CurrentRow.Cells[2].Value.ToString() + ", " + dgv_Teachers.CurrentRow.Cells[1].Value.ToString();
                this.Close();
            }
            else
            {
                MessageBox.Show("This teacher has already been evaluated.");
            }
        }

        private void bunifuCustomLabel2_Click(object sender, EventArgs e)
        {

        }
    }
}
