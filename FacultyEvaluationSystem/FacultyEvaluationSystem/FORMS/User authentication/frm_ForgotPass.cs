﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FacultyEvaluationSystem.FORMS.User_authentication
{
    public partial class frm_ForgotPass : Form
    {
        public frm_ForgotPass()
        {
            InitializeComponent();
        }
        dataClass_FacultyEvaluationSystemDataContext db = new dataClass_FacultyEvaluationSystemDataContext();
        private void btnChange_Click(object sender, EventArgs e)
        {
            if (txtUsername.Text == "" || txtPassword.Text == "" || txtConf.Text == "")
            {
                MessageBox.Show("One or more inputs are empty.");
            }
            else if (txtPassword.Text != txtConf.Text)
            {
                MessageBox.Show("New password does not match.");
            }
            else
            {
                db.sp_ForgotPassword(txtUsername.Text, txtPassword.Text);
                MessageBox.Show("Successfully reset the admin account!");
                this.Close();
            }
        }
    }
}
