﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClassLibrary;

namespace FacultyEvaluationSystem
{
    public partial class frm_Register : Form
    {

        dataClass_FacultyEvaluationSystemDataContext db = new dataClass_FacultyEvaluationSystemDataContext();

        public frm_Register()
        {
            InitializeComponent();
            this.ActiveControl = lbl_Administrator;
        }

        private void btn_Register_Click(object sender, EventArgs e)
        {
           //StoredProcedure Register
           if(txt_Password.Text != txt_PasswordConfirm.Text)
            {
                // password verification
                MessageBox.Show("Password does not match.");
            }
            else
            {
                // one time first user admin
                db.sp_InsertAdmin(txt_Username.Text, txt_Password.Text);
                MessageBox.Show("Successfully registered.");
                frm_Login frmlogin = new frm_Login();
                frmlogin.Show();
                this.Hide(); 
            }
        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            WindowFunctions.WindowClose();
        }

        private void btn_Minimize_Click(object sender, EventArgs e)
        {
            WindowFunctions.WindowMinimize(this);
        }
        //this code hides the password txt
        private void txt_Password_Enter(object sender, EventArgs e)
        {
            txt_Password.isPassword = true;
        }
        //this code hides the password txt
        private void txt_PasswordConfirm_Enter(object sender, EventArgs e)
        {
            txt_PasswordConfirm.isPassword = true;
        }
    }
}
