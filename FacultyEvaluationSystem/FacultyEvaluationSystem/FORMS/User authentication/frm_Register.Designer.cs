﻿namespace FacultyEvaluationSystem
{
    partial class frm_Register
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Register));
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.bunifuDragControl1 = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.bunifuGradientPanel2 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.btn_Minimize = new Bunifu.Framework.UI.BunifuImageButton();
            this.btn_Close = new Bunifu.Framework.UI.BunifuImageButton();
            this.lbl_Administrator = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel2 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.btn_Register = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuCustomLabel4 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel3 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel1 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.txt_PasswordConfirm = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.txt_Password = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.txt_Username = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.bunifuGradientPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_Minimize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_Close)).BeginInit();
            this.SuspendLayout();
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 3;
            this.bunifuElipse1.TargetControl = this;
            // 
            // bunifuDragControl1
            // 
            this.bunifuDragControl1.Fixed = true;
            this.bunifuDragControl1.Horizontal = true;
            this.bunifuDragControl1.TargetControl = this.bunifuGradientPanel2;
            this.bunifuDragControl1.Vertical = true;
            // 
            // bunifuGradientPanel2
            // 
            this.bunifuGradientPanel2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.bunifuGradientPanel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel2.BackgroundImage")));
            this.bunifuGradientPanel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel2.Controls.Add(this.btn_Minimize);
            this.bunifuGradientPanel2.Controls.Add(this.btn_Close);
            this.bunifuGradientPanel2.Controls.Add(this.lbl_Administrator);
            this.bunifuGradientPanel2.Controls.Add(this.bunifuCustomLabel2);
            this.bunifuGradientPanel2.GradientBottomLeft = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(42)))), ((int)(((byte)(114)))));
            this.bunifuGradientPanel2.GradientBottomRight = System.Drawing.Color.FromArgb(((int)(((byte)(113)))), ((int)(((byte)(76)))), ((int)(((byte)(143)))));
            this.bunifuGradientPanel2.GradientTopLeft = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(19)))), ((int)(((byte)(86)))));
            this.bunifuGradientPanel2.GradientTopRight = System.Drawing.Color.MintCream;
            this.bunifuGradientPanel2.Location = new System.Drawing.Point(-3, -1);
            this.bunifuGradientPanel2.Name = "bunifuGradientPanel2";
            this.bunifuGradientPanel2.Quality = 10;
            this.bunifuGradientPanel2.Size = new System.Drawing.Size(485, 50);
            this.bunifuGradientPanel2.TabIndex = 10;
            // 
            // btn_Minimize
            // 
            this.btn_Minimize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Minimize.BackColor = System.Drawing.Color.Transparent;
            this.btn_Minimize.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Minimize.Image = ((System.Drawing.Image)(resources.GetObject("btn_Minimize.Image")));
            this.btn_Minimize.ImageActive = null;
            this.btn_Minimize.Location = new System.Drawing.Point(413, 11);
            this.btn_Minimize.Name = "btn_Minimize";
            this.btn_Minimize.Size = new System.Drawing.Size(30, 30);
            this.btn_Minimize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btn_Minimize.TabIndex = 12;
            this.btn_Minimize.TabStop = false;
            this.btn_Minimize.Zoom = 10;
            this.btn_Minimize.Click += new System.EventHandler(this.btn_Minimize_Click);
            // 
            // btn_Close
            // 
            this.btn_Close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Close.BackColor = System.Drawing.Color.Transparent;
            this.btn_Close.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Close.Image = ((System.Drawing.Image)(resources.GetObject("btn_Close.Image")));
            this.btn_Close.ImageActive = null;
            this.btn_Close.InitialImage = null;
            this.btn_Close.Location = new System.Drawing.Point(444, 11);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(30, 30);
            this.btn_Close.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btn_Close.TabIndex = 11;
            this.btn_Close.TabStop = false;
            this.btn_Close.Zoom = 10;
            this.btn_Close.Click += new System.EventHandler(this.btn_Close_Click);
            // 
            // lbl_Administrator
            // 
            this.lbl_Administrator.AutoSize = true;
            this.lbl_Administrator.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Administrator.Font = new System.Drawing.Font("Georgia", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Administrator.ForeColor = System.Drawing.Color.White;
            this.lbl_Administrator.Location = new System.Drawing.Point(10, 10);
            this.lbl_Administrator.Name = "lbl_Administrator";
            this.lbl_Administrator.Size = new System.Drawing.Size(113, 31);
            this.lbl_Administrator.TabIndex = 0;
            this.lbl_Administrator.Text = "Register";
            // 
            // bunifuCustomLabel2
            // 
            this.bunifuCustomLabel2.AutoSize = true;
            this.bunifuCustomLabel2.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel2.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel2.ForeColor = System.Drawing.Color.White;
            this.bunifuCustomLabel2.Location = new System.Drawing.Point(129, 20);
            this.bunifuCustomLabel2.Name = "bunifuCustomLabel2";
            this.bunifuCustomLabel2.Size = new System.Drawing.Size(108, 18);
            this.bunifuCustomLabel2.TabIndex = 1;
            this.bunifuCustomLabel2.Text = "Administrator";
            // 
            // btn_Register
            // 
            this.btn_Register.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(113)))), ((int)(((byte)(76)))), ((int)(((byte)(143)))));
            this.btn_Register.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(42)))), ((int)(((byte)(114)))));
            this.btn_Register.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_Register.BorderRadius = 3;
            this.btn_Register.ButtonText = "Register";
            this.btn_Register.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Register.DisabledColor = System.Drawing.Color.Gray;
            this.btn_Register.Iconcolor = System.Drawing.Color.Transparent;
            this.btn_Register.Iconimage = ((System.Drawing.Image)(resources.GetObject("btn_Register.Iconimage")));
            this.btn_Register.Iconimage_right = null;
            this.btn_Register.Iconimage_right_Selected = null;
            this.btn_Register.Iconimage_Selected = null;
            this.btn_Register.IconMarginLeft = 0;
            this.btn_Register.IconMarginRight = 0;
            this.btn_Register.IconRightVisible = true;
            this.btn_Register.IconRightZoom = 0D;
            this.btn_Register.IconVisible = false;
            this.btn_Register.IconZoom = 90D;
            this.btn_Register.IsTab = false;
            this.btn_Register.Location = new System.Drawing.Point(174, 289);
            this.btn_Register.Name = "btn_Register";
            this.btn_Register.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(42)))), ((int)(((byte)(114)))));
            this.btn_Register.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(113)))), ((int)(((byte)(76)))), ((int)(((byte)(143)))));
            this.btn_Register.OnHoverTextColor = System.Drawing.Color.White;
            this.btn_Register.selected = false;
            this.btn_Register.Size = new System.Drawing.Size(128, 45);
            this.btn_Register.TabIndex = 14;
            this.btn_Register.Text = "Register";
            this.btn_Register.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn_Register.Textcolor = System.Drawing.Color.White;
            this.btn_Register.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Register.Click += new System.EventHandler(this.btn_Register_Click);
            // 
            // bunifuCustomLabel4
            // 
            this.bunifuCustomLabel4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.bunifuCustomLabel4.AutoSize = true;
            this.bunifuCustomLabel4.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel4.ForeColor = System.Drawing.Color.Black;
            this.bunifuCustomLabel4.Location = new System.Drawing.Point(107, 210);
            this.bunifuCustomLabel4.Name = "bunifuCustomLabel4";
            this.bunifuCustomLabel4.Size = new System.Drawing.Size(151, 21);
            this.bunifuCustomLabel4.TabIndex = 35;
            this.bunifuCustomLabel4.Text = "Confirm Password:";
            // 
            // bunifuCustomLabel3
            // 
            this.bunifuCustomLabel3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.bunifuCustomLabel3.AutoSize = true;
            this.bunifuCustomLabel3.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel3.ForeColor = System.Drawing.Color.Black;
            this.bunifuCustomLabel3.Location = new System.Drawing.Point(107, 138);
            this.bunifuCustomLabel3.Name = "bunifuCustomLabel3";
            this.bunifuCustomLabel3.Size = new System.Drawing.Size(86, 21);
            this.bunifuCustomLabel3.TabIndex = 36;
            this.bunifuCustomLabel3.Text = "Password:";
            // 
            // bunifuCustomLabel1
            // 
            this.bunifuCustomLabel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.bunifuCustomLabel1.AutoSize = true;
            this.bunifuCustomLabel1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel1.ForeColor = System.Drawing.Color.Black;
            this.bunifuCustomLabel1.Location = new System.Drawing.Point(107, 71);
            this.bunifuCustomLabel1.Name = "bunifuCustomLabel1";
            this.bunifuCustomLabel1.Size = new System.Drawing.Size(92, 21);
            this.bunifuCustomLabel1.TabIndex = 34;
            this.bunifuCustomLabel1.Text = "Username:";
            // 
            // txt_PasswordConfirm
            // 
            this.txt_PasswordConfirm.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_PasswordConfirm.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.txt_PasswordConfirm.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_PasswordConfirm.HintForeColor = System.Drawing.Color.Empty;
            this.txt_PasswordConfirm.HintText = "";
            this.txt_PasswordConfirm.isPassword = true;
            this.txt_PasswordConfirm.LineFocusedColor = System.Drawing.Color.LightGray;
            this.txt_PasswordConfirm.LineIdleColor = System.Drawing.Color.Gray;
            this.txt_PasswordConfirm.LineMouseHoverColor = System.Drawing.Color.Silver;
            this.txt_PasswordConfirm.LineThickness = 4;
            this.txt_PasswordConfirm.Location = new System.Drawing.Point(111, 231);
            this.txt_PasswordConfirm.Margin = new System.Windows.Forms.Padding(5);
            this.txt_PasswordConfirm.Name = "txt_PasswordConfirm";
            this.txt_PasswordConfirm.Size = new System.Drawing.Size(262, 41);
            this.txt_PasswordConfirm.TabIndex = 33;
            this.txt_PasswordConfirm.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txt_PasswordConfirm.Enter += new System.EventHandler(this.txt_PasswordConfirm_Enter);
            // 
            // txt_Password
            // 
            this.txt_Password.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_Password.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.txt_Password.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Password.HintForeColor = System.Drawing.Color.Empty;
            this.txt_Password.HintText = "";
            this.txt_Password.isPassword = true;
            this.txt_Password.LineFocusedColor = System.Drawing.Color.LightGray;
            this.txt_Password.LineIdleColor = System.Drawing.Color.Gray;
            this.txt_Password.LineMouseHoverColor = System.Drawing.Color.Silver;
            this.txt_Password.LineThickness = 4;
            this.txt_Password.Location = new System.Drawing.Point(111, 162);
            this.txt_Password.Margin = new System.Windows.Forms.Padding(5);
            this.txt_Password.Name = "txt_Password";
            this.txt_Password.Size = new System.Drawing.Size(262, 41);
            this.txt_Password.TabIndex = 32;
            this.txt_Password.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txt_Password.Enter += new System.EventHandler(this.txt_Password_Enter);
            // 
            // txt_Username
            // 
            this.txt_Username.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_Username.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.txt_Username.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Username.HintForeColor = System.Drawing.Color.Empty;
            this.txt_Username.HintText = "";
            this.txt_Username.isPassword = false;
            this.txt_Username.LineFocusedColor = System.Drawing.Color.LightGray;
            this.txt_Username.LineIdleColor = System.Drawing.Color.Gray;
            this.txt_Username.LineMouseHoverColor = System.Drawing.Color.Silver;
            this.txt_Username.LineThickness = 4;
            this.txt_Username.Location = new System.Drawing.Point(111, 92);
            this.txt_Username.Margin = new System.Windows.Forms.Padding(5);
            this.txt_Username.Name = "txt_Username";
            this.txt_Username.Size = new System.Drawing.Size(262, 41);
            this.txt_Username.TabIndex = 31;
            this.txt_Username.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // frm_Register
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LemonChiffon;
            this.ClientSize = new System.Drawing.Size(480, 350);
            this.Controls.Add(this.bunifuCustomLabel4);
            this.Controls.Add(this.bunifuCustomLabel3);
            this.Controls.Add(this.bunifuCustomLabel1);
            this.Controls.Add(this.txt_PasswordConfirm);
            this.Controls.Add(this.txt_Password);
            this.Controls.Add(this.txt_Username);
            this.Controls.Add(this.btn_Register);
            this.Controls.Add(this.bunifuGradientPanel2);
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frm_Register";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frm_Admin";
            this.bunifuGradientPanel2.ResumeLayout(false);
            this.bunifuGradientPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_Minimize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_Close)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private Bunifu.Framework.UI.BunifuDragControl bunifuDragControl1;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel2;
        private Bunifu.Framework.UI.BunifuImageButton btn_Minimize;
        private Bunifu.Framework.UI.BunifuImageButton btn_Close;
        private Bunifu.Framework.UI.BunifuCustomLabel lbl_Administrator;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel2;
        private Bunifu.Framework.UI.BunifuFlatButton btn_Register;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel4;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel3;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel1;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txt_PasswordConfirm;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txt_Password;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txt_Username;
    }
}