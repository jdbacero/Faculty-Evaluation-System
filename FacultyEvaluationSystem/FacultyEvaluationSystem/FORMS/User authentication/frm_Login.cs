﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClassLibrary;
using FacultyEvaluationSystem.FORMS.User_authentication;

namespace FacultyEvaluationSystem
{
    public partial class frm_Login : Form
    {
        dataClass_FacultyEvaluationSystemDataContext db = new dataClass_FacultyEvaluationSystemDataContext();
        //FORMS.User_authentication.frm_ForgotPass fp = new FORMS.User_authentication.frm_ForgotPass();
        frm_ForgotPass fp = new frm_ForgotPass();
        string staticUser = "MktVobvVOP";
        string staticPass = "SH5c96VK25";
        public frm_Login()
        {
            InitializeComponent();

            //Para dili mu-automatic set as active ang usa sa mga textbox.
            this.ActiveControl = lblLogin;
        }
       
        //Close button code.
        private void btn_Close_Click(object sender, EventArgs e)
        {
            WindowFunctions.WindowClose();
        }

        private void llbl_ForgotPass_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //MessageBox.Show("Too bad :(");
        }

        private void btn_Minimize_Click(object sender, EventArgs e)
        {
            WindowFunctions.WindowMinimize(this);
        }

        private void btn_Login_Click(object sender, EventArgs e)
        {
            login();
        }
        //this code encrypts pass
        private void txt_Password_Enter(object sender, EventArgs e)
        {
            txt_Password.isPassword = true;
        }

        private void frm_Login_Load(object sender, EventArgs e)
        {

        }

        private void txt_Password_KeyPress(object sender, KeyPressEventArgs e)
        {
            
        }

        private void txt_Password_KeyUp(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                login();
            }
        }

        private void login()
        {
            //Login button code
            if (db.sp_Login(txt_Username.Text, txt_Password.Text).Count() == 0)
            {
                // verification of account
                if (txt_Username.Text.Equals(staticUser) && txt_Password.Text.Equals(staticPass) )
                {
                    fp.ShowDialog();
                    txt_Password.Text = "";
                    txt_Username.Text = "";
                }
                    else
                {
                    MessageBox.Show("Invalid username or password.");
                }
            }
            else
            {
                foreach (var res in db.sp_Login(txt_Username.Text, txt_Password.Text))
                {
                    // checks if the user is either student or not
                    string id_student = res.Username;
                    if (res.UserType == "Student")
                    {

                        int id_studeval = 0;
                        int id_schedule = 0;

                        //mu-indicate if naka evaluate na ba ang student.
                        int indicator = 0;
                        if (db.sp_SelectOngoingStudentEval().Any())
                        {
                            foreach (var id in db.sp_SelectOngoingStudentEval())
                            {
                                id_studeval = id.ID_EvaluationStudent;
                                id_schedule = id.ID;
                            }
                        }

                        //this is to make sure nga para makuha ang latest nga schoolyear sa
                        //student, ie: naa na siya daan sa database iyang account, nya gi-add
                        //siya balik sa masterlist, maybe sa sunod na schoolyear
                        foreach (var result in db.sp_ViewStudent_TopOne_BySchoolYear(res.Username))
                        {
                            //mucheck if naka evaluate na ba
                            //ang student
                            if (db.sp_ViewEvaluationResultByUser_ScheduleID(result.Username, id_schedule).Any())
                            {
                                indicator = 1;
                            }
                            frm_Student stud = new frm_Student(result.ID_No, result.School_Year,
                                id_studeval, id_schedule, id_student, indicator);
                            this.Hide();
                            stud.ShowDialog();
                            txt_Password.Text="";
                            this.Show();
                        }
                    }
                    else
                    {
                        frm_Admin admin = new frm_Admin(txt_Username.Text);
                        admin.Show();
                        admin.login = this;
                        this.Hide();
                    }
                }
            }
        }
    }
}
