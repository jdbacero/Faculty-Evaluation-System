﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FacultyEvaluationSystem.reportsDatasetTableAdapters;

namespace FacultyEvaluationSystem
{
    public partial class frm_ReportStudentsThatHaveEvaluatednt : Form
    {
        int id_sched;

        public frm_ReportStudentsThatHaveEvaluatednt(int ID_Sched)
        {
            InitializeComponent();
            id_sched = ID_Sched;
        }

        private void frm_ReportStudentsThatHaveEvaluatednt_Load(object sender, EventArgs e)
        {
            Reports.reports_StudentsThatHaveEvaluatednt report = new Reports.reports_StudentsThatHaveEvaluatednt();
            reportsDataset dataset = new reportsDataset();
            sp_ViewStudentsThatHaventEvaluated_BySchedIDTableAdapter tableAdapter = new sp_ViewStudentsThatHaventEvaluated_BySchedIDTableAdapter();
            tableAdapter.Fill(dataset.sp_ViewStudentsThatHaventEvaluated_BySchedID, id_sched);
            report.SetDataSource(dataset);
            crystalReportViewer1.ReportSource = report;
        }
    }
}
