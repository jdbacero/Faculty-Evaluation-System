﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FacultyEvaluationSystem.reportsDatasetTableAdapters;

namespace FacultyEvaluationSystem
{
    public partial class frm_Reports_Comments : Form
    {
        int id_sched, id_teacher;
        public frm_Reports_Comments(int Id_Sched, int Id_teacher)
        {
            InitializeComponent();
            id_sched = Id_Sched;
            id_teacher = Id_teacher;
        }
        dataClass_FacultyEvaluationSystemDataContext db = new dataClass_FacultyEvaluationSystemDataContext();
        private void frm_Reports_Comments_Load(object sender, EventArgs e)
        {
            Reports.reports_Comments report = new Reports.reports_Comments();
            reportsDataset dataset = new reportsDataset();
            sp_ViewComments_ByTeacherID_ScheduleIDTableAdapter tableAdapter = new sp_ViewComments_ByTeacherID_ScheduleIDTableAdapter();
            tableAdapter.Fill(dataset.sp_ViewComments_ByTeacherID_ScheduleID,id_teacher,id_sched);
            report.SetDataSource(dataset);
            crystalReportViewer1.ReportSource = report;
        }
    }
}
