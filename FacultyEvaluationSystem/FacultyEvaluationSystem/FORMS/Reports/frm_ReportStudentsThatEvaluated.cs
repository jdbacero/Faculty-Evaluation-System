﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FacultyEvaluationSystem.reportsDatasetTableAdapters;

namespace FacultyEvaluationSystem
{
    public partial class frm_ReportStudentsThatEvaluated : Form
    {
        int id_sched;

        public frm_ReportStudentsThatEvaluated(int Id_Sched)
        {
            InitializeComponent();
            id_sched = Id_Sched;
        }

        private void frm_ReportStudentsThatEvaluated_Load(object sender, EventArgs e)
        {
            Reports.reports_StudentsThatHaveEvaluated report = new Reports.reports_StudentsThatHaveEvaluated();
            reportsDataset dataset = new reportsDataset();
            sp_ViewStudentsThatHaveEvaluated_BySchedIDTableAdapter tableAdapter = new sp_ViewStudentsThatHaveEvaluated_BySchedIDTableAdapter();
            tableAdapter.Fill(dataset.sp_ViewStudentsThatHaveEvaluated_BySchedID, id_sched);
            report.SetDataSource(dataset);
            crystalReportViewer1.ReportSource = report;
        }
    }
}
