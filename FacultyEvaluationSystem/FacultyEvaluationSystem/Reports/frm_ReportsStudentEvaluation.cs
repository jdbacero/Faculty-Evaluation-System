﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FacultyEvaluationSystem.reportsDatasetTableAdapters;

namespace FacultyEvaluationSystem.Reports
{
    public partial class frm_ReportsStudentEvaluation : Form
    {
        int id_sched;
        int id_teacher = 0;
        dataClass_FacultyEvaluationSystemDataContext db = new dataClass_FacultyEvaluationSystemDataContext();
        public frm_ReportsStudentEvaluation(int Id_Sched, int Id_teacher)
        {
            InitializeComponent();
            id_sched = Id_Sched;
            id_teacher = Id_teacher;
        }

        private int getNumSec()
        {
            return Convert.ToInt32(db.sp_CountNumOfEvaluatedSection(id_teacher, id_sched));
        }

        private void frm_ReportsStudentEvaluation_Load(object sender, EventArgs e)
        {
            reports_StudentsEvaluation report = new reports_StudentsEvaluation();
            reportsDataset dataset = new reportsDataset();
            sp_ViewStudentEvaluationResultFinishedByTeacherIDBySectionTableAdapter tableAdapter = new sp_ViewStudentEvaluationResultFinishedByTeacherIDBySectionTableAdapter();
            tableAdapter.Fill(dataset.sp_ViewStudentEvaluationResultFinishedByTeacherIDBySection, id_teacher, id_sched, getNumSec());
            report.SetDataSource(dataset);
            crystalReportViewer1.ReportSource = report;
        }
    }
}
