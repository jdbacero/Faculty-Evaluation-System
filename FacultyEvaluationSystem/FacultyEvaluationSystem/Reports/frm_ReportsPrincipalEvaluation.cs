﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FacultyEvaluationSystem.reportsDatasetTableAdapters;

namespace FacultyEvaluationSystem.User_controls
{
    public partial class frm_ReportsPrincipalEvaluation : Form
    {
        int id_sched;
        int id_teacher = 0;
        string mode;
        public frm_ReportsPrincipalEvaluation(int Id_Sched, int Id_teacher, string Mode)
        {
            InitializeComponent();
            id_sched = Id_Sched;
            id_teacher = Id_teacher;
            mode = Mode;
        }

        private void frm_ReportsPrincipalEvaluation_Load(object sender, EventArgs e)
        {
            if(mode == "Individual")
            {
                reports_PrincipalEvaluationResults report = new reports_PrincipalEvaluationResults();
                reportsDataset dataset = new reportsDataset();
                sp_ViewEvaluationPrincipalInnerJoinTableAdapter tableAdapter = new sp_ViewEvaluationPrincipalInnerJoinTableAdapter();
                tableAdapter.Fill(dataset.sp_ViewEvaluationPrincipalInnerJoin, id_sched, id_teacher);
                report.SetDataSource(dataset);
                crystalReportViewer1.ReportSource = report;
            }
            else
            {
                reports_PrincipalAllteachers report = new reports_PrincipalAllteachers();
                reportsDataset dataset = new reportsDataset();
                sp_ViewEvaluationPrincipalInnerJoinTananTableAdapter tableAdapter = new sp_ViewEvaluationPrincipalInnerJoinTananTableAdapter();
                tableAdapter.Fill(dataset.sp_ViewEvaluationPrincipalInnerJoinTanan, id_sched, id_teacher);
                report.SetDataSource(dataset);
                crystalReportViewer1.ReportSource = report;
            }
            
        }
    }
}
