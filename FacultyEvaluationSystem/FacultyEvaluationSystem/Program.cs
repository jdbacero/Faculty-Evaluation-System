﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FacultyEvaluationSystem
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            dataClass_FacultyEvaluationSystemDataContext db = new dataClass_FacultyEvaluationSystemDataContext();

            //cls_checker check = new cls_checker("chkAdmin");
            //if (check.verify())
            //{
            //    Application.Run(new frm_Login());
            //}
            //else
            //{
            //    MessageBox.Show("Wala pa'y admin :-(");
            //    Application.Run(new frm_Register());
            //}

            IEnumerable<tbl_UserAccount> chkAcc = db.ExecuteQuery<tbl_UserAccount>
                       ("SELECT * FROM tbl_UserAccounts WHERE UserType = {0}",
                       "Admin");
            //Returns true if there's an admin. Returns false if there's none.
            if (chkAcc.Any() == true)
            {
                //string[,,] test = new string[,,]
                //{
                //    { { "A", "B"}, { "C", "D"} },
                //    { { "E", "F"}, { "G", "H"} },
                //};

                //MessageBox.Show("" + test[0,1,0]);

                Application.Run(new frm_Login());
                //Application.Run(new frm_Reports_Comments());
            }
            else
            {
                Application.Run(new frm_Register());
            }


        }
    }
}