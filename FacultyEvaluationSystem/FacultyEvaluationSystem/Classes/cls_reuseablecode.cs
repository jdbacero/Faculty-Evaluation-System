﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FacultyEvaluationSystem
{
    class cls_reuseablecode
    {
        //inialization of necessary variables//
        dataClass_FacultyEvaluationSystemDataContext db = new dataClass_FacultyEvaluationSystemDataContext();
        protected string check;
        Timer time = new Timer();

        //-----------------------------------//

        public cls_reuseablecode()
        {
            time.Tick += new EventHandler(time_Tick);
            time.Interval = 10000;
            time.Start();
        }
        int x;
        private void time_Tick(object sender, EventArgs e)
        {
            //code stored proc nga mucheck sa schedule
            foreach(var sched in db.sp_ViewEvaluationSchedulePrincipal())
            {
                x = db.sp_CheckPrincipalSchedule(sched.ID, DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd")));
                if(x == 1)
                {
                    db.sp_UpdatePrincipalEvaluationSchedule(sched.ID, sched.Date_Start, sched.Date_End, "Ongoing", "Active");
                }
                else
                {
                    if(sched.Date_End < DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd")))
                    {
                        db.sp_UpdatePrincipalEvaluationSchedule(sched.ID, sched.Date_Start, sched.Date_End, "Finished", "Inactive");
                    }
                }
            }

            foreach (var sched in db.sp_ViewEvaluationScheduleStudent())
            {
                x = db.sp_CheckStudentSchedule(sched.ID, DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd")));
                if (x == 1)
                {
                    db.sp_UpdateStudentEvaluationSchedule(sched.ID, sched.Date_Start, sched.Date_End, "Ongoing", "Active");
                }
                else
                {
                    if (sched.Date_End < DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd")))
                    {
                        db.sp_UpdateStudentEvaluationSchedule(sched.ID, sched.Date_Start, sched.Date_End, "Finished", "Inactive");
                    }
                }
            }
        }
        //commented for the meantime ky walay table
        //public bool checkForEval()
        //{
        //    IEnumerable<tbl_EvaluationSchedule> chkSched = db.ExecuteQuery<tbl_EvaluationSchedule>
        //                ("SELECT * FROM tbl_EvaluationSchedule WHERE Status = 'Inactive' AND Date_Start >= {0}",
        //                DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day);
        //    if (chkSched.Any() == true)
        //    {
        //        return true;
        //    }
        //    else { return false; }
        //}


        public bool verify()
        {
            switch(check)
            {
                case "chkAdmin":
                    //This line of code executes a query to select a system admin in the table User_Login
                    IEnumerable<tbl_UserAccount> chkAcc = db.ExecuteQuery<tbl_UserAccount>
                        ("SELECT * FROM tbl_UserAccounts WHERE UserType = {0}",
                        "Admin");
                    //Returns true if there's an admin. Returns false if there's none.
                    if (chkAcc.Any() == true)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                default:
                    return true;

            }
        }
    }
}
