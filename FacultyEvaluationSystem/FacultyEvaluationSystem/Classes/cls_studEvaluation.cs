﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacultyEvaluationSystem
{
    class cls_studEvaluation
    {
        public int id_Teacher { get; set; }
        public int id_Question { get; set; }
        public int answer { get; set; }
    }
}
