﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using System.Data.OleDb;
using System.Windows.Forms;

namespace FacultyEvaluationSystem
{
    class cls_Excel
    {
        public string filepath;
        //Opens new excel
        public void openExcel(string[] columnHeader) //Parameters are as follows: Number of columns, Column headers
        {
            int columns = columnHeader.Count();

            Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application(); //Instantiate excel app
            xlApp.Workbooks.Add(); //Add workbook

            //Loop to set all column headers
            for (int y = 0; y < columns; y++)
            {
                xlApp.Cells[1, (y + 1)] = columnHeader[y];
            }

            //Loop to set all to-be-used columns' width
            for (int i = 1; i < (columns + 1); i++)
            {
                xlApp.Columns[i].ColumnWidth = 18;
            }

            xlApp.Visible = true; //show excel
        }

        //Prompts an upload dialog
        public void uploadExcel(Bunifu.Framework.UI.BunifuCustomDataGrid dgv)
        {
            OpenFileDialog upload = new OpenFileDialog(); //Instantiate new dialog
            upload.Filter = "Excel | *.xls; *.xlsx; *.xlsm"; //This line of code makes the user upload the excel files only
            upload.ShowDialog(); //Shows the upload dialog
            filepath = upload.FileName; //this assigns the file path of the excel uploaded to this variable
            string connectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" +
                                        filepath +
                                        ";Extended Properties='Excel 12.0;HDR=YES';"; //acts as a string to connect excel to the filepath
            OleDbConnection conn = new OleDbConnection(connectionString); //turns the above code into a open connection to a datasource
            if (filepath == "")
            {
                MessageBox.Show("No file selected.");
            }
            else
            {
                OleDbDataAdapter data = new OleDbDataAdapter("SELECT DISTINCT * FROM [Sheet1$]", conn); //sql query for selecting sheet1 of the excel
                System.Data.DataTable datatable = new System.Data.DataTable(); //instantiate datatable
                data.Fill(datatable); //fills the datatable with the excel file
                dgv.DataSource = datatable; //connects the dgv datasource
            }
        }

        public bool checker(string[] columnHeader, Bunifu.Framework.UI.BunifuCustomDataGrid dgv)
        {
            if (dgv.Columns.Count != columnHeader.Count())
            {
                MessageBox.Show("Invalid.\nColumns overlapped.");
                dgv.DataSource = null;
                dgv.Refresh();
                return false;
            }
            //This checks if the uploaded excel file (which is transferred to dgv) has the appropriate column headers
            for (int i = 0; i < columnHeader.Count(); i++)
            {
                if (columnHeader[i].caseInsensitiveContains(dgv.Columns[i].Name) == false)
                {
                    MessageBox.Show("Invalid.\nIt seems there's an error in the formatting.\nAre the header names correct?");
                    dgv.DataSource = null;
                    dgv.Refresh();
                    return false;
                }

                //This checks if the uploaded excel file (which is transferred to dgv) has empty of null cell values
                for (int x = 0; x < dgv.Rows.Count; x++)
                {
                    if (string.IsNullOrEmpty(dgv.Rows[x].Cells[i].Value as string))
                    {
                        MessageBox.Show(dgv.Rows[x].Cells[i].Value.ToString());
                        MessageBox.Show("One or more inputs are empty.\nDon't leave anything blank!");
                        dgv.DataSource = null;
                        dgv.Refresh();
                        return false;
                    }
                }
            }

            return true;
        }

        public bool checkFilepath()
        {
            switch(filepath)
            {
                case "":
                    return false;
                default:
                    return true;
            }
        }

    }
    public static class Extensions
    {
        public static bool caseInsensitiveContains(this string text, string value,
            StringComparison stringComparison = StringComparison.CurrentCultureIgnoreCase)
        {
            return text.IndexOf(value, stringComparison) >= 0;
        }
    }



}
