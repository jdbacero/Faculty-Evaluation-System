CREATE DATABASE [db_FacultyEvaluationSystem]

USE [db_FacultyEvaluationSystem]


CREATE TABLE [tbl_UserAccounts] (
    [Username] varchar(100)  NOT NULL PRIMARY KEY CLUSTERED ([Username] ASC),
    [Password] varchar(500)  NOT NULL ,
    [UserType] varchar(100)  NOT NULL
)

CREATE TABLE [tbl_Section] (
    -- ID int identity pk
    [Section] varchar(50)  NOT NULL PRIMARY KEY CLUSTERED ([Section] ASC),
    [Grade_Level] varchar(50)  NOT NULL
)

CREATE TABLE [tbl_Students] (
	[ID_No] varchar(200)  NOT NULL,
    [First_Name] varchar(200)  NOT NULL ,
    [Middle_Initial] varchar(5)  NOT NULL ,
    [Last_Name] varchar(200)  NOT NULL ,
    [Username] varchar(100)  NOT NULL FOREIGN KEY REFERENCES [tbl_UserAccounts]([Username]),
    [Grade_Level] varchar(50)  NOT NULL ,
    [Section] varchar(50)  NOT NULL FOREIGN KEY REFERENCES [tbl_Section]([Section]),
	[School_Year] varchar(100) NOT NULL
)

CREATE TABLE [tbl_Teachers] (
    [ID] int IDENTITY(1,1) NOT NULL PRIMARY KEY CLUSTERED ([ID] ASC),
    [First_Name] varchar(200)  NOT NULL ,
    [Last_Name] varchar(200)  NOT NULL,
	[School_Year] varchar(100) NOT NULL
)

CREATE TABLE [tbl_TeachersHandledSection] (
	[ID_Handler] int identity(1,1),
    [ID_Teacher] int  NOT NULL FOREIGN KEY REFERENCES [tbl_Teachers]([ID]),
    [Section] varchar(50)  NOT NULL FOREIGN KEY REFERENCES [tbl_Section]([Section]),
    [Subject] varchar(100)  NOT NULL 
)
-- Error 1
-- some of the following tables contain variable having DECIMAL data type, they originally have DOUBLE
--CREATE TABLE [tbl_EvaluationStudentCriteria] (
--    [Criteria] varchar(100)  NOT NULL PRIMARY KEY CLUSTERED ([Criteria] ASC),
--    [Percentage] decimal  NOT NULL
--)

-------------------------
CREATE TABLE [tbl_EvaluationPrincipalID] (
	[ID_Evaluation] int identity(1,1) NOT NULL PRIMARY KEY,
	[Date] date NOT NULL,
	[Status] varchar(10) NOT NULL
	)
-------------------------
CREATE TABLE [tbl_EvaluationPrincipalQuestion] (
	[ID_Question] int identity(1,1) NOT NULL PRIMARY KEY,
	[ID_Evaluation] int FOREIGN KEY REFERENCES [tbl_EvaluationPrincipalID]([ID_Evaluation]),
	[Questions] varchar(2000) NOT NULL,
	[Criteria] varchar(100) NOT NULL
	)
-------------------------
CREATE TABLE [tbl_EvaluationStudentID] (
	[ID_Evaluation] int identity(1,1) NOT NULL PRIMARY KEY,
	[Date] date NOT NULL,
	[Status] varchar(10) NOT NULL
)

-------------------------
CREATE TABLE [tbl_EvaluationStudentCriteria] (
	[ID_Evaluation] int NOT NULL FOREIGN KEY REFERENCES [tbl_EvaluationStudentID]([ID_Evaluation]),
	[ID_Criteria] int identity(1,1) NOT NULL PRIMARY KEY,
	[Criteria] VARCHAR (100) NOT NULL,
	[Percentage] decimal NOT NULL
)
-------------------------
CREATE TABLE [tbl_EvaluationStudentQuestions] (
	[ID_Question] int identity(1,1) NOT NULL PRIMARY KEY,
	[ID_Criteria] int NOT NULL FOREIGN KEY REFERENCES [tbl_EvaluationStudentCriteria]([ID_Criteria]),
	[Question] varchar(2000) NOT NULL
)
-------------------------TO BE REDESIGNED-----------------------
--CREATE TABLE [tbl_EvaluationSchedule] (
--	[ID] int identity(1,1) NOT NULL PRIMARY KEY,
--	[ID_EvaluationStudent] int NOT NULL FOREIGN KEY REFERENCES[tbl_EvaluationStudentID]([ID_Evaluation]),
--	[ID_EvaluationPrincipal] int NOT NULL FOREIGN KEY REFERENCES[tbl_EvaluationPrincipalID]([ID_Evaluation]),
--	[Date_Start] date not null,
--	[Date_End] date not null,
--	[School_Year] varchar(100) not null,
--	[Status] varchar (10)
--	)
-----------------------------------------------------------------
CREATE TABLE [tbl_EvaluationPrincipalSchedule] (
	[ID] int identity(1,1) NOT NULL PRIMARY KEY,
	[ID_EvaluationPrincipal] int NOT NULL FOREIGN KEY REFERENCES[tbl_EvaluationPrincipalID]([ID_Evaluation]),
	[Date_Start] date not null,
	[Date_End] date not null,
	[School_Year] varchar(100) not null,
	[Status] varchar (10)
)
-----------------------------------------------------------------
CREATE TABLE [tbl_EvaluationStudentSchedule] (
	[ID] int identity(1,1) NOT NULL PRIMARY KEY,
	[ID_EvaluationStudent] int NOT NULL FOREIGN KEY REFERENCES[tbl_EvaluationStudentID]([ID_Evaluation]),
	[Date_Start] date not null,
	[Date_End] date not null,
	[School_Year] varchar(100) not null,
	[Status] varchar (10)
)
-----------------------------------------------------------------
CREATE TABLE [tbl_EvaluationStudentResult] (
	[ID_Teacher] INT NOT NULL FOREIGN KEY REFERENCES[tbl_Teachers]([ID]),
	[Username] VARCHAR(100) NOT NULL FOREIGN KEY REFERENCES[tbl_UserAccounts]([Username]),
	[ID_Schedule] INT NOT NULL FOREIGN KEY REFERENCES[tbl_EvaluationStudentSchedule]([ID]),
	[ID_Question] INT NOT NULL FOREIGN KEY REFERENCES[tbl_EvaluationStudentQuestions]([ID_Question]),
	[Answer] INT NOT NULL,
	[Date] DATETIME NOT NULL
)
-----------------------------------------------------------------
CREATE TABLE [tbl_EvaluationStudentComments] (
	[ID_Teacher] INT NOT NULL FOREIGN KEY REFERENCES[tbl_Teachers]([ID]),
	[Username] VARCHAR(100) NOT NULL FOREIGN KEY REFERENCES[tbl_UserAccounts]([Username]),
	[ID_Schedule] INT NOT NULL FOREIGN KEY REFERENCES[tbl_EvaluationStudentSchedule]([ID]),
	[Comment_Pros] VARCHAR (2000) NOT NULL,
	[Comment_Cons] VARCHAR (2000) NOT NULL,
	[Date] DATETIME NOT NULL
)
-----------------------------------------------------------------
CREATE TABLE [tbl_EvaluationPrincipalResult] (
	[ID_Teacher] INT NOT NULL FOREIGN KEY REFERENCES[tbl_Teachers]([ID]),
	[ID_Schedule] INT NOT NULL FOREIGN KEY REFERENCES[tbl_EvaluationPrincipalSchedule]([ID]),
	[ID_Question] INT NOT NULL FOREIGN KEY REFERENCES[tbl_EvaluationPrincipalQuestion]([ID_Question]),
	[Answer] INT NOT NULL
)
-----------------------------------------------------------------

CREATE TABLE [tbl_EvaluationPrincipalResultOverall] (
	[ID_Schedule] INT NOT NULL FOREIGN KEY REFERENCES[tbl_EvaluationPrincipalSchedule]([ID]),
	[ID_Teacher] INT NOT NULL FOREIGN KEY REFERENCES[tbl_Teachers]([ID]),
	[Total_Attributes] DECIMAL,
	[Total_Instructional] DECIMAL,
	[Total_ClassManage] DECIMAL,
	[Total_Assessment] DECIMAL,
	[WTotal_Attributes] DECIMAL,
	[WTotal_Instructional] DECIMAL,
	[WTotal_ClassManage] DECIMAL,
	[WTotal_Assessment] DECIMAL,
	[Overall] DECIMAL
)

--CREATE TABLE [tbl_EvaluationQuestions] (
--    [Question] varchar(300)  NOT NULL UNIQUE ,
--    [Criteria] varchar(100)  NOT NULL FOREIGN KEY REFERENCES [tbl_EvaluationStudentCriteria]([Criteria])
--)

--CREATE TABLE [tbl_EvaluationPrincipal] (
--	[ID_Evaluation] int identity (1,1) NOT NULL PRIMARY KEY,
--    [ID_Teacher] int NOT NULL FOREIGN KEY REFERENCES [tbl_Teachers]([ID]),
--    [Teacher_Attributes] decimal NOT NULL,
--    [Instructional_Delivery] decimal  NOT NULL ,
--    [Classroom_Management_Learning_Environment] decimal  NOT NULL ,
--    [Assessment_Of_Learning] decimal  NOT NULL ,
--    [Overall] decimal  NOT NULL ,
--    [Date_Assessment] datetime  NOT NULL 
--)

--CREATE TABLE [tbl_EvaluationStudent] (
--    [ID_Evaluation] int IDENTITY(1,1) NOT NULL PRIMARY KEY ,
--	-- Error 2
--	-- i added primary key constraint to [ID_Evaluation] to reference it on [tbl_StudentAnswers] table
--	-- edited on 7/16/2018
--    [ID_Student] varchar(200)  NOT NULL FOREIGN KEY REFERENCES [tbl_Students]([ID_No]) ,
--	-- Error 3
--	-- the original data type of [ID_Student] is int but i changed it to varchar since the
--	-- foreign key data type in [tbl_Students].[ID_No] is varchar(200)
--	-- edited on 7/16/2018
--    [ID_Teacher] int  NOT NULL FOREIGN KEY REFERENCES [tbl_Teachers]([ID]) ,
--    [Date_Evaluated] datetime  NOT NULL ,
--    [Teachers_Characteristics] decimal  NOT NULL ,
--    [Instructional_Delivery] decimal  NOT NULL ,
--    [Management_of_Learning_Environment] decimal  NOT NULL ,
--    [Assessment_of_Learning] decimal  NOT NULL ,
--    [Total] decimal  NOT NULL 
--)

--CREATE TABLE [tbl_StudentAnswers] (
--    [ID_Evaluation] int  NOT NULL FOREIGN KEY REFERENCES [tbl_EvaluationStudent]([ID_Evaluation]),
--    [ID_Student] varchar(200)  NOT NULL FOREIGN KEY REFERENCES [tbl_Students]([ID_No]),
--	-- i changed the data type, see Error 3 (similar)
--	-- Error 4
--	-- i also changed the reference, from [tbl_EvaluationStudent]([ID_Student])
--	-- to [tbl_Students]([ID_No])
--	-- sql wont allow referencing another foreign key
--	-- edited on 7/16/2018
--    [ID_Teacher] int  NOT NULL FOREIGN KEY REFERENCES [tbl_Teachers]([ID]) ,
--	-- the only change here is the reference, same with Error 4
--	-- edited on 7/16/2018
--    [Area] varchar(100)  NOT NULL ,
--    [Question] varchar(300)  NOT NULL FOREIGN KEY REFERENCES [tbl_EvaluationQuestions]([Question]) ,
--    [Answer] int  NOT NULL 
--)

DELETE FROM tbl_UserAccounts
DELETE FROM tbl_Students
DELETE FROM tbl_TeachersHandledSection
DELETE FROM	tbl_Teachers 
DELETE FROM tbl_Section
DELETE FROM tbl_EvaluationPrincipalQuestion
DELETE FROM tbl_EvaluationPrincipalID

SELECT * FROM tbl_EvaluationPrincipalQuestion
SELECT * FROM tbl_EvaluationPrincipalID WHERE ID_Evaluation <= 2

DBCC CHECKIDENT('tbl_EvaluationSchedule', RESEED, 0)