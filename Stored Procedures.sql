USE [db_FacultyEvaluationSystem]
--STORED PROCEDURES--

CREATE PROCEDURE sp_InsertSection

	@Section varchar(50),
    @Grade_Level varchar(50)

AS
BEGIN
	INSERT INTO tbl_Section
	VALUES (@Section, @Grade_Level)
END
GO

--

CREATE PROCEDURE sp_ViewSection
AS
BEGIN
	SELECT * FROM tbl_Section
END
GO

--

CREATE PROCEDURE sp_ViewStudents
AS
BEGIN
	SELECT * FROM tbl_Students
END
GO

--

CREATE PROCEDURE sp_ViewTeachers
AS
BEGIN
	SELECT * FROM tbl_Teachers
END
GO

--

CREATE PROCEDURE sp_InsertAccount

	@username varchar(100),
    @password varchar(500),
	@usertype varchar(100)

AS
BEGIN
	INSERT INTO tbl_UserAccounts
	VALUES (@username, @password, @usertype)
END
GO

--

CREATE PROCEDURE sp_InsertStudents

	@id VARCHAR(200),
	@firstname VARCHAR(200),
	@mi VARCHAR(5),
	@lastname VARCHAR(200),
	@username VARCHAR(100),
	@grade_level VARCHAR(50),
	@section VARCHAR(50),
	@schoolyear VARCHAR(100)

AS
BEGIN
	INSERT INTO tbl_Students
	VALUES(@id, @firstname, @mi, @lastname, @username, @grade_level, @section, @schoolyear)
END
GO

--

CREATE PROCEDURE sp_InsertTeachers
	@firstname VARCHAR(200),
	@lastname VARCHAR(200),
	@schoolyear VARCHAR(100)
AS
BEGIN
	INSERT INTO tbl_Teachers
	VALUES (@firstname, @lastname, @schoolyear)
END
GO

--

CREATE PROCEDURE sp_InsertTeachersHandledSection
	@id int,
	@section VARCHAR(50),
	@subject VARCHAR(100)
AS
BEGIN
	INSERT INTO tbl_TeachersHandledSection
	VALUES (@id, @section, @subject)
END
GO

--
CREATE PROCEDURE sp_ReturnTeacherID
	-- Add the parameters for the stored procedure here
	@firstname VARCHAR(200),
	@lastname VARCHAR(200)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @teacherID INT
	SELECT @teacherID = ID FROM tbl_Teachers WHERE First_Name = @firstname AND Last_Name = @lastname
	RETURN @teacherID
	
END
GO

--

CREATE PROCEDURE sp_InsertAdmin

	@username VARCHAR(100),
	@password VARCHAR(500)

AS
BEGIN

	INSERT INTO tbl_UserAccounts
	VALUES (@username, @password, 'Admin')

END
GO

--

CREATE PROCEDURE sp_ViewStudentsByGradeLevel

	@gradelevel VARCHAR(50)

AS
BEGIN

	SELECT * FROM tbl_Students WHERE Grade_Level = @gradelevel

END
GO

--

CREATE PROCEDURE sp_ViewStudentsBySection

	@section VARCHAR(50)

AS
BEGIN

	SELECT * FROM tbl_Students WHERE Section = @section

END
GO

--

CREATE PROCEDURE sp_ViewSectionByGradeLevel

	@gradelevel VARCHAR(50)

AS
BEGIN

	SELECT * FROM tbl_Section WHERE Grade_Level = @gradelevel

END
GO

--

CREATE PROCEDURE sp_ViewTeachersHandledSectionByTeachersID
	@teacherID INT
AS
BEGIN
	SELECT        dbo.tbl_TeachersHandledSection.Section, dbo.tbl_TeachersHandledSection.Subject, dbo.tbl_Section.Grade_Level
	FROM            dbo.tbl_TeachersHandledSection INNER JOIN
                         dbo.tbl_Teachers ON dbo.tbl_TeachersHandledSection.ID_Teacher = dbo.tbl_Teachers.ID INNER JOIN
                         dbo.tbl_Section ON dbo.tbl_TeachersHandledSection.Section = dbo.tbl_Section.Section WHERE dbo.tbl_TeachersHandledSection.ID_Teacher = @teacherID
END
GO

--

CREATE PROCEDURE sp_ReturnTeachersHandledSectionID
	@teacherID INT,
	@section VARCHAR(50),
	@subject VARCHAR(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @handlerID INT
	SELECT @handlerID = ID_Handler FROM tbl_TeachersHandledSection WHERE ID_Teacher= @teacherID AND Section = @section AND Subject = @subject
	RETURN @handlerID
END
GO

--

CREATE PROCEDURE sp_ViewTeachersHandledSectionByID
	@id int
AS
BEGIN
	SELECT        dbo.tbl_Section.Grade_Level, dbo.tbl_TeachersHandledSection.*
FROM            dbo.tbl_TeachersHandledSection INNER JOIN
                         dbo.tbl_Section ON dbo.tbl_TeachersHandledSection.Section = dbo.tbl_Section.Section WHERE ID_Handler = @id
END
GO

--

CREATE PROCEDURE sp_UpdateTeachersHandledSection
	@section VARCHAR(50),
	@subject VARCHAR(100),
	@handlerid int
AS
BEGIN
	UPDATE tbl_TeachersHandledSection SET Section = @section, Subject = @subject WHERE ID_Handler = @handlerid
END
GO

--

CREATE PROCEDURE sp_SearchAllTeachers
	@searchKeyword VARCHAR(200)
AS
BEGIN
	
	SELECT * FROM tbl_Teachers WHERE First_Name LIKE '%' + @searchKeyword + '%' OR Last_Name LIKE '%' + @searchKeyword + '%'
END
GO

--

CREATE PROCEDURE sp_SearchTeachersByGradeLevel
	@gradelevel VARCHAR(50),
	@searchKeyword VARCHAR(200)
AS
BEGIN
	SELECT        dbo.tbl_Section.Grade_Level, dbo.tbl_TeachersHandledSection.Subject, dbo.tbl_TeachersHandledSection.Section, dbo.tbl_Teachers.Last_Name, 
                         dbo.tbl_Teachers.First_Name
	FROM            dbo.tbl_Teachers INNER JOIN
                         dbo.tbl_TeachersHandledSection ON dbo.tbl_Teachers.ID = dbo.tbl_TeachersHandledSection.ID_Teacher INNER JOIN
                         dbo.tbl_Section ON dbo.tbl_TeachersHandledSection.Section = dbo.tbl_Section.Section WHERE (First_Name LIKE '%' + @searchKeyword + '%' OR Last_Name LIKE '%' + @searchKeyword + '%' OR tbl_Section.Section LIKE '%' + @searchKeyword + '%' OR tbl_TeachersHandledSection.Subject LIKE '%' + @searchKeyword + '%') AND (tbl_Section.Grade_Level = @gradelevel)
END
GO

--

CREATE PROCEDURE sp_SearchTeachersBySection
	@section VARCHAR(50),
	@searchKeyword VARCHAR(200)
AS
BEGIN
	SELECT        dbo.tbl_Section.Grade_Level, dbo.tbl_TeachersHandledSection.Subject, dbo.tbl_TeachersHandledSection.Section, dbo.tbl_Teachers.Last_Name, 
                         dbo.tbl_Teachers.First_Name
	FROM            dbo.tbl_Teachers INNER JOIN
                         dbo.tbl_TeachersHandledSection ON dbo.tbl_Teachers.ID = dbo.tbl_TeachersHandledSection.ID_Teacher INNER JOIN
                         dbo.tbl_Section ON dbo.tbl_TeachersHandledSection.Section = dbo.tbl_Section.Section WHERE (First_Name LIKE '%' + @searchKeyword + '%' OR Last_Name LIKE '%' + @searchKeyword + '%' OR tbl_Section.Grade_Level LIKE '%' + @searchKeyword + '%' OR tbl_TeachersHandledSection.Subject LIKE '%' + @searchKeyword + '%') AND (tbl_Section.Section = @section)
END
GO


--

CREATE PROCEDURE sp_SearchAllStudents
	@searchKeyword VARCHAR(200)
AS
BEGIN
	
	SELECT * FROM tbl_Students WHERE ID_No LIKE '%' + @searchKeyword + '%' OR First_Name LIKE '%' + @searchKeyword + '%' OR Last_Name LIKE '%' + @searchKeyword + '%' OR Username LIKE '%' + @searchKeyword + '%' OR Grade_Level LIKE '%' + @searchKeyword + '%' OR Section LIKE '%' + @searchKeyword + '%' OR School_Year LIKE '%' + @searchKeyword + '%'

END
GO

--

CREATE PROCEDURE sp_SearchAllStudentsByGradeLevel
	@searchKeyword VARCHAR(200),
	@gradelevel VARCHAR(50)
AS
BEGIN
	
	SELECT * FROM tbl_Students WHERE (ID_No LIKE '%' + @searchKeyword + '%' OR First_Name LIKE '%' + @searchKeyword + '%' OR Last_Name LIKE '%' + @searchKeyword + '%' OR Username LIKE '%' + @searchKeyword + '%' OR Section LIKE '%' + @searchKeyword + '%' OR School_Year LIKE '%' + @searchKeyword + '%') AND (Grade_Level = @gradelevel)

END
GO

--

CREATE PROCEDURE sp_SearchAllStudentsBySection
	@searchKeyword VARCHAR(200),
	@section VARCHAR(50)
AS
BEGIN
	
	SELECT * FROM tbl_Students WHERE (ID_No LIKE '%' + @searchKeyword + '%' OR First_Name LIKE '%' + @searchKeyword + '%' OR Last_Name LIKE '%' + @searchKeyword + '%' OR Username LIKE '%' + @searchKeyword + '%' OR School_Year LIKE '%' + @searchKeyword + '%') AND (Section = @section)

END
GO

--

CREATE PROCEDURE sp_ViewTeachersByGradeLevel
	@gradelevel VARCHAR(50)
AS
BEGIN
	SELECT        dbo.tbl_Section.Grade_Level, dbo.tbl_TeachersHandledSection.Subject, dbo.tbl_TeachersHandledSection.Section, dbo.tbl_Teachers.Last_Name, 
                         dbo.tbl_Teachers.First_Name
	FROM            dbo.tbl_Teachers INNER JOIN
                         dbo.tbl_TeachersHandledSection ON dbo.tbl_Teachers.ID = dbo.tbl_TeachersHandledSection.ID_Teacher INNER JOIN
                         dbo.tbl_Section ON dbo.tbl_TeachersHandledSection.Section = dbo.tbl_Section.Section WHERE tbl_Section.Grade_Level = @gradelevel
END
GO

--

CREATE PROCEDURE sp_ViewTeachersBySection
	@section VARCHAR(50)
AS
BEGIN
	SELECT        dbo.tbl_Section.Grade_Level, dbo.tbl_TeachersHandledSection.Subject, dbo.tbl_TeachersHandledSection.Section, dbo.tbl_Teachers.Last_Name, 
                         dbo.tbl_Teachers.First_Name
	FROM            dbo.tbl_Teachers INNER JOIN
                         dbo.tbl_TeachersHandledSection ON dbo.tbl_Teachers.ID = dbo.tbl_TeachersHandledSection.ID_Teacher INNER JOIN
                         dbo.tbl_Section ON dbo.tbl_TeachersHandledSection.Section = dbo.tbl_Section.Section WHERE tbl_Section.section = @section
END
GO

--

CREATE PROCEDURE sp_ViewEvaluationPrincipalID
AS
BEGIN
	SELECT * FROM tbl_EvaluationPrincipalID
END
GO

--

CREATE PROCEDURE sp_ViewEvaluationPrincipalIDbyID
	@id int
AS
BEGIN
	SELECT * FROM tbl_EvaluationPrincipalID WHERE ID_Evaluation = @id
END
GO

--

CREATE PROCEDURE sp_InsertEvaluationID
	@date DATE
AS
BEGIN
	INSERT INTO tbl_EvaluationPrincipalID VALUES (@date, 'Inactive')
END
GO

--

CREATE PROCEDURE sp_InsertPrincipalQuestion
	@id INT,
	@criteria varchar(100),
	@questions varchar(2000)
AS
BEGIN
	INSERT INTO tbl_EvaluationPrincipalQuestion VALUES (@id, @questions, @criteria)
END
GO

--

CREATE PROCEDURE sp_UpdatePrincipalQuestion
	@id INT,
	@criteria varchar(100),
	@questions varchar(2000)
AS
BEGIN
	UPDATE tbl_EvaluationPrincipalQuestion SET Questions = @questions, Criteria = @criteria WHERE ID_Question = @id
END
GO

--
--dima kita sa pikas dgv inig cell click
CREATE PROCEDURE sp_ViewEvalutationQuestionsByID
	@id int
AS
BEGIN
	SELECT * FROM tbl_EvaluationPrincipalQuestion WHERE ID_Evaluation = @id
END
GO

--

CREATE PROCEDURE sp_ViewEvaluationQuestionsByID_ByCriteria
	@id int,
	@criteria varchar(100)
AS
BEGIN
	SELECT * FROM tbl_EvaluationPrincipalQuestion WHERE ID_Evaluation = @id AND Criteria = @criteria
END
GO

--

CREATE PROCEDURE sp_ViewEvaluationStudentID
AS
BEGIN
	SELECT * FROM tbl_EvaluationStudentID
END
GO

--

CREATE PROCEDURE sp_InsertEvaluationStudentID
	@date DATE
AS
BEGIN
	INSERT INTO tbl_EvaluationStudentID VALUES (@date, 'Inactive')
END
GO

--

CREATE PROCEDURE sp_ViewEvaluationStudentQuestionsByID
	@id int
AS
BEGIN
	SELECT        dbo.tbl_EvaluationStudentQuestions.*, dbo.tbl_EvaluationStudentCriteria.Criteria
FROM            dbo.tbl_EvaluationStudentQuestions INNER JOIN
                         dbo.tbl_EvaluationStudentCriteria ON dbo.tbl_EvaluationStudentQuestions.ID_Criteria = dbo.tbl_EvaluationStudentCriteria.ID_Criteria INNER JOIN
                         dbo.tbl_EvaluationStudentID ON dbo.tbl_EvaluationStudentCriteria.ID_Evaluation = dbo.tbl_EvaluationStudentID.ID_Evaluation WHERE tbl_EvaluationStudentID.ID_Evaluation = @id
END
GO

--

CREATE PROCEDURE sp_ViewEvaluationStudentCriteriaByEvaluationID
	@id int
AS
BEGIN

	SELECT        dbo.tbl_EvaluationStudentCriteria.*
	FROM            dbo.tbl_EvaluationStudentCriteria INNER JOIN
				dbo.tbl_EvaluationStudentID ON dbo.tbl_EvaluationStudentCriteria.ID_Evaluation = dbo.tbl_EvaluationStudentID.ID_Evaluation WHERE tbl_EvaluationStudentID.ID_Evaluation = @id
END
GO

--

CREATE PROCEDURE sp_ViewEvaluationStudentCriteriaByCriteriaID
	@id int
AS
BEGIN

	SELECT        dbo.tbl_EvaluationStudentCriteria.*
	FROM            dbo.tbl_EvaluationStudentCriteria INNER JOIN
				dbo.tbl_EvaluationStudentID ON dbo.tbl_EvaluationStudentCriteria.ID_Evaluation = dbo.tbl_EvaluationStudentID.ID_Evaluation WHERE tbl_EvaluationStudentCriteria.ID_Criteria = @id
END
GO

--

CREATE PROCEDURE sp_InsertEvaluationStudentCriteria
	@ID INT,
	@criteria VARCHAR(100),
	@percentage decimal
AS
BEGIN

	INSERT INTO tbl_EvaluationStudentCriteria VALUES (@ID, @criteria, @percentage)

END
GO

--

CREATE PROCEDURE sp_InsertEvaluationStudentQuestion
	@ID int,
	@question varchar(2000)
AS
BEGIN
	INSERT INTO tbl_EvaluationStudentQuestions VALUES (@ID, @question)
END
GO

--

CREATE PROCEDURE sp_ViewEvaluationSchedulePrincipal
AS
BEGIN
	SELECT * FROM tbl_EvaluationPrincipalSchedule
END
GO

--

CREATE PROCEDURE sp_ViewEvaluationScheduleStudent
AS
BEGIN
	SELECT * FROM tbl_EvaluationStudentSchedule
END
GO

---------------------TO BE AMBOT---------------------------

--CREATE PROCEDURE sp_InsertEvaluationSchedule
--	@ID_EvalStud int,
--	@ID_EvalPrin int,
--	@date_start date,
--	@date_end date,
--	@status varchar(10),
--	@schoolyear varchar(100)
--AS
--BEGIN
--	INSERT INTO tbl_EvaluationSchedule VALUES (@ID_EvalStud, @ID_EvalStud, @date_start, @date_end, @schoolyear, @status)
--END
--GO
--------------------------------------------------------------

CREATE PROCEDURE sp_InsertEvaluationSchedulePrincipal
	@ID_EvalPrin int,
	@date_start date,
	@date_end date,
	@status varchar(10),
	@schoolyear varchar(100)
AS
BEGIN
	INSERT INTO tbl_EvaluationPrincipalSchedule VALUES (@ID_EvalPrin, @date_start, @date_end, @schoolyear, @status)
END
GO

---

CREATE PROCEDURE sp_UpdateEvaluationPrincipalID_Status
	@ID_EvalPrin int,
	@status varchar(10)
AS
BEGIN
	UPDATE tbl_EvaluationPrincipalID SET Status = @status
	WHERE ID_Evaluation = @ID_EvalPrin
END
GO

---

CREATE PROCEDURE sp_InsertEvaluationScheduleStudent
	@ID_EvalStud int,
	@date_start date,
	@date_end date,
	@status varchar(10),
	@schoolyear varchar(100)
AS
BEGIN
	INSERT INTO tbl_EvaluationStudentSchedule VALUES (@ID_EvalStud, @date_start, @date_end, @schoolyear, @status)
END
GO

---

CREATE PROCEDURE sp_UpdateEvaluationStudentID_Status
	@ID_EvalStud int,
	@status varchar(10)
AS
BEGIN
	UPDATE tbl_EvaluationStudentID SET Status = @status
	WHERE ID_Evaluation = @ID_EvalStud
END
GO

---

--Added update proc for studQuestion
CREATE PROCEDURE sp_UpdateEvaluationStudentQuestion
	@ID int,
	@question varchar(2000),
	@ID_criteria int
AS
BEGIN
	UPDATE tbl_EvaluationStudentQuestions SET Question = @question, ID_Criteria = @ID_criteria WHERE ID_Question = @ID
END
GO

---

--Added update proc for studCriteria
CREATE PROCEDURE sp_UpdateEvaluationStudentCriteria
	@ID int,
	@criteria varchar(100),
	@percentage decimal
AS
BEGIN
    UPDATE tbl_EvaluationStudentCriteria SET Criteria = @criteria, Percentage = @percentage WHERE ID_Criteria = @ID
END
GO


--Added delete proc for studCriteria
CREATE PROCEDURE sp_DeleteEvaluationStudentCriteria
	@ID int
AS
BEGIN
	DELETE FROM tbl_EvaluationStudentCriteria WHERE ID_Criteria = @id
END
GO

---

--Not sure unsay i update with this one 
--CREATE PROCEDURE sp_UpdateEvaluationSchedule
--	@ID int,
--	@
--AS
--BEGIN
	
--END
--GO

---




CREATE PROCEDURE sp_ViewStudentsBySchoolYear
	@schoolyear varchar(100)
AS
BEGIN
	SELECT * FROM tbl_Students WHERE School_Year = @schoolyear

END
GO

--

CREATE PROCEDURE sp_ViewStudentsByGradeLevel_SchoolYear
	@schoolyear varchar(100),
	@gradelevel varchar(100)
AS
BEGIN
	SELECT * FROM tbl_Students WHERE Grade_Level = @gradelevel AND School_Year = @schoolyear

END
GO

--

CREATE PROCEDURE sp_ViewStudentsByGradeLevel_Section_SchoolYear
	@schoolyear varchar(100),
	@gradelevel varchar(100),
	@section varchar(50)
AS
BEGIN
	SELECT * FROM tbl_Students WHERE Grade_Level = @gradelevel AND School_Year = @schoolyear AND Section = @section

END
GO


--

--Added update proc for [tbl_EvaluationPrincipalSchedule] (klyde, 9-2-18)
--Added status - jorex limot kos date
--Revised - jorex 9/6/18
CREATE PROCEDURE sp_UpdatePrincipalEvaluationSchedule
	@id int,
	@Date_Start date,
	@Date_End date,
	@status_Sched varchar(30),
	@status_ID varchar(10)
AS
BEGIN
	UPDATE [tbl_EvaluationPrincipalSchedule]
		SET [Date_Start] = @Date_Start, [Date_End] = @Date_End, [Status] = @status_Sched
			WHERE [ID] = @id
	DECLARE @ID_eval INT
	SELECT @ID_eval = ID_EvaluationPrincipal FROM tbl_EvaluationPrincipalSchedule WHERE [Date_Start] = @Date_Start AND [Date_End] = @Date_End AND [Status] = @status_Sched AND [ID] = @id
	UPDATE [tbl_EvaluationPrincipalID]
		SET [Status] = @status_ID
			WHERE [ID_Evaluation] = @id
END
GO

--check for ongoing principal evaluation schedule
CREATE PROCEDURE sp_CheckPrincipalSchedule
	@id INT,
	@date DATE
AS
BEGIN
	
	DECLARE @dateScheduleDateStart DATE 
	DECLARE @dateScheduleDateEnd DATE 

	SELECT @dateScheduleDateStart = tbl_EvaluationPrincipalSchedule.Date_Start FROM tbl_EvaluationPrincipalSchedule WHERE ID = @id
	SELECT @dateScheduleDateEnd = tbl_EvaluationPrincipalSchedule.Date_End FROM tbl_EvaluationPrincipalSchedule WHERE ID = @id

	IF(@date >= @dateScheduleDateStart AND @date <= @dateScheduleDateEnd)
		BEGIN
			RETURN 1		
		END
	ELSE
		BEGIN
			RETURN 0
		END

END
GO
--check for ongoing student evaluation schedule
CREATE PROCEDURE sp_CheckStudentSchedule
	@id INT,
	@date DATE
AS
BEGIN
	
	DECLARE @dateScheduleDateStart DATE 
	DECLARE @dateScheduleDateEnd DATE 

	SELECT @dateScheduleDateStart = tbl_EvaluationStudentSchedule.Date_Start FROM tbl_EvaluationStudentSchedule WHERE ID = @id
	SELECT @dateScheduleDateEnd = tbl_EvaluationStudentSchedule.Date_End FROM tbl_EvaluationStudentSchedule WHERE ID = @id

	IF(@date >= @dateScheduleDateStart AND @date <= @dateScheduleDateEnd)
		BEGIN
			RETURN 1		
		END
	ELSE
		BEGIN
			RETURN 0
		END

END
GO

--

CREATE PROCEDURE sp_UpdateStudentEvaluationSchedule
	@id int,
	@Date_Start date,
	@Date_End date,
	@status_Sched varchar(30),
	@status_ID varchar(10)
AS
BEGIN
	UPDATE [tbl_EvaluationStudentSchedule]
		SET [Date_Start] = @Date_Start, [Date_End] = @Date_End, [Status] = @status_Sched
			WHERE [ID] = @id
	DECLARE @ID_eval INT
	SELECT @ID_eval = ID_EvaluationStudent FROM tbl_EvaluationStudentSchedule WHERE [Date_Start] = @Date_Start AND [Date_End] = @Date_End AND [Status] = @status_Sched AND [ID] = @id
	UPDATE [tbl_EvaluationStudentID]
		SET [Status] = @status_ID
			WHERE [ID_Evaluation] = @id
END
GO

--

--STUDENT EVALUATION STORED PROCS--

CREATE PROCEDURE sp_ViewTeachers_Distinct_BySchoolID_SchoolYear
	@id VARCHAR(30),
	@schoolyear VARCHAR(30)
AS
BEGIN
	SELECT    DISTINCT    dbo.tbl_Teachers.*
	FROM            dbo.tbl_Students INNER JOIN
							 dbo.tbl_Section ON dbo.tbl_Students.Section = dbo.tbl_Section.Section INNER JOIN
							 dbo.tbl_TeachersHandledSection ON dbo.tbl_Section.Section = dbo.tbl_TeachersHandledSection.Section INNER JOIN
							 dbo.tbl_Teachers ON dbo.tbl_TeachersHandledSection.ID_Teacher = dbo.tbl_Teachers.ID WHERE tbl_Students.ID_No = @id AND tbl_Teachers.School_Year = @schoolyear
END
GO 

--

CREATE PROCEDURE sp_ViewTeachers_ByID
	@id INT
AS
BEGIN
	SELECT * FROM tbl_Teachers WHERE ID = @id
END
GO

--


---LOGIN SHIT [[trial]]---
CREATE PROCEDURE sp_Login
	@username VARCHAR(100),
	@password VARCHAR(500)
AS
BEGIN
	SELECT * FROM tbl_UserAccounts WHERE Username = @username AND Password = @password
END
GO

--

CREATE PROCEDURE sp_ViewStudent_TopOne_BySchoolYear
	@id VARCHAR(30)
AS
BEGIN

	SELECT TOP 1 * FROM tbl_Students WHERE ID_No = @id ORDER BY School_Year DESC

END
GO

--

CREATE PROCEDURE sp_SelectOngoingStudentEval
AS
BEGIN	
	SELECT TOP 1 * FROM tbl_EvaluationStudentSchedule WHERE Status = 'Ongoing'
END
GO

--

CREATE PROCEDURE sp_ViewEvaluationStudentQuestions_ByID
	@id INT
AS
BEGIN
	SELECT        dbo.tbl_EvaluationStudentQuestions.*
	FROM            dbo.tbl_EvaluationStudentQuestions INNER JOIN
                         dbo.tbl_EvaluationStudentCriteria ON dbo.tbl_EvaluationStudentQuestions.ID_Criteria = dbo.tbl_EvaluationStudentCriteria.ID_Criteria INNER JOIN
                         dbo.tbl_EvaluationStudentID ON dbo.tbl_EvaluationStudentCriteria.ID_Evaluation = dbo.tbl_EvaluationStudentID.ID_Evaluation
	WHERE	tbl_EvaluationStudentID.ID_Evaluation = @id
END
GO

--

CREATE PROCEDURE sp_InsertEvaluationStudentResult 
	@ID_TEACHER INT,
	@USERNAME VARCHAR(100),
	@ID_SCHED INT,
	@ID_QUEST INT,
	@ANSWER INT
AS
BEGIN
	INSERT INTO tbl_EvaluationStudentResult 
	VALUES (@ID_TEACHER, @USERNAME, @ID_SCHED, @ID_QUEST, @ANSWER)
END
GO

--

CREATE PROCEDURE sp_InsertEvaluationStudentComment
	@ID_TEACHER INT,
	@USERNAME VARCHAR(100),
	@ID_SCHEDULE INT,
	@COMMENT_PRO VARCHAR(2000),
	@COMMENT_CON VARCHAR(2000),
	@DATE DATETIME
AS
BEGIN
	INSERT INTO tbl_EvaluationStudentComments
	VALUES (@ID_TEACHER, @USERNAME, @ID_SCHEDULE, @COMMENT_PRO, @COMMENT_CON, @DATE)
END
GO

--

CREATE PROCEDURE sp_ViewEvaluationResultByUser_ScheduleID
	@USERNAME VARCHAR(100),
	@ID INT
AS
BEGIN
	SELECT * FROM tbl_EvaluationStudentResult WHERE Username = @USERNAME AND ID_Schedule = @ID
END
GO

--

CREATE PROCEDURE sp_SelectOngoingPrincipalEval
AS
BEGIN	
	SELECT TOP 1 * FROM tbl_EvaluationPrincipalSchedule WHERE Status = 'Ongoing'
END
GO

--

CREATE PROCEDURE sp_ViewTeachersBySchoolYear
	@schoolYear VARCHAR(50)
AS
BEGIN	
	SELECT * FROM tbl_Teachers WHERE School_Year = @schoolYear
END
GO

--

CREATE PROCEDURE sp_ViewEvaluationResultByTeacherID_ScheduleID
	@IDTeacher INT,
	@IDSchedule INT
AS
BEGIN
	SELECT * FROM tbl_EvaluationPrincipalResult WHERE ID_Teacher = @IDTeacher AND ID_Schedule = @IDSchedule
END
GO

--

CREATE PROCEDURE sp_ViewEvaluationPrincipalScheduleByID
	@IDSchedule INT
AS
BEGIN
	SELECT * FROM tbl_EvaluationPrincipalSchedule WHERE ID = @IDSchedule
END
GO

--

CREATE PROCEDURE sp_InsertEvaluationPrincipalResult
	@IDSchedule INT,
	@IDTeacher INT,
	@IDQuestion INT,
	@Answer INT
AS
BEGIN
	INSERT INTO tbl_EvaluationPrincipalResult
	VALUES (@IDTeacher, @IDSchedule, @IDQuestion, @Answer, CURRENT_TIMESTAMP)
END
GO

--

CREATE PROCEDURE sp_ViewFinishedPrincipalEval
AS
BEGIN	
	SELECT * FROM tbl_EvaluationPrincipalSchedule WHERE Status = 'Finished'
END
GO

--

CREATE PROCEDURE sp_ViewFinishedStudentEval
AS
BEGIN	
	SELECT * FROM tbl_EvaluationStudentSchedule WHERE Status = 'Finished'
END
GO

--EXPERIMENTAL--
CREATE PROCEDURE sp_ViewPrincipalEvaluationResultFinishedByTeacherID
	@id_Teacher INT,
	@id_Sched INT
AS
BEGIN
	SELECT       tbl_Teachers.ID, dbo.tbl_Teachers.Last_Name + ', ' + tbl_Teachers.First_Name AS Name, dbo.tbl_EvaluationPrincipalQuestion.Criteria, SUM(dbo.tbl_EvaluationPrincipalResult.Answer) AS Score, COUNT(Criteria) AS Total 
	FROM            dbo.tbl_EvaluationPrincipalResult INNER JOIN
							 dbo.tbl_EvaluationPrincipalSchedule ON dbo.tbl_EvaluationPrincipalResult.ID_Schedule = dbo.tbl_EvaluationPrincipalSchedule.ID INNER JOIN
							 dbo.tbl_EvaluationPrincipalID ON dbo.tbl_EvaluationPrincipalSchedule.ID_EvaluationPrincipal = dbo.tbl_EvaluationPrincipalID.ID_Evaluation INNER JOIN
							 dbo.tbl_EvaluationPrincipalQuestion ON dbo.tbl_EvaluationPrincipalResult.ID_Question = dbo.tbl_EvaluationPrincipalQuestion.ID_Question AND 
							 dbo.tbl_EvaluationPrincipalID.ID_Evaluation = dbo.tbl_EvaluationPrincipalQuestion.ID_Evaluation INNER JOIN
							 dbo.tbl_Teachers ON dbo.tbl_EvaluationPrincipalResult.ID_Teacher = dbo.tbl_Teachers.ID
							 WHERE tbl_Teachers.ID = @id_Teacher AND tbl_EvaluationPrincipalSchedule.ID = @id_Sched
							 GROUP BY dbo.tbl_EvaluationPrincipalQuestion.Criteria, dbo.tbl_Teachers.ID, tbl_Teachers.First_Name, tbl_Teachers.Last_Name
END
GO
--

CREATE PROCEDURE sp_ViewComments_ByTeacherID_ScheduleID
	@id_Teacher INT,
	@id_Sched INT
AS
BEGIN
	SELECT        dbo.tbl_EvaluationStudentComments.*, dbo.tbl_EvaluationStudentSchedule.Date_Start, dbo.tbl_EvaluationStudentSchedule.Date_End, dbo.tbl_Teachers.First_Name, 
                         dbo.tbl_Teachers.Last_Name, dbo.tbl_Teachers.School_Year
FROM            dbo.tbl_EvaluationStudentComments INNER JOIN
                         dbo.tbl_Teachers ON dbo.tbl_EvaluationStudentComments.ID_Teacher = dbo.tbl_Teachers.ID INNER JOIN
                         dbo.tbl_EvaluationStudentSchedule ON dbo.tbl_EvaluationStudentComments.ID_Schedule = dbo.tbl_EvaluationStudentSchedule.ID WHERE ID_Schedule = @id_Sched AND ID_Teacher= @id_Teacher
END
GO

--

CREATE PROCEDURE sp_UpdateAccount_ByUsername
	@username VARCHAR(50),
	@new_Username VARCHAR(100),
	@new_Password VARCHAR(100)
AS
BEGIN
	UPDATE tbl_UserAccounts SET Username = @new_Username, Password = @new_Password
	WHERE Username = @username
END
GO

--

CREATE PROCEDURE sp_ViewStudentsThatHaveEvaluated_BySchedID
	@id_Sched INT
AS
BEGIN
	SELECT        DISTINCT dbo.tbl_Students.First_Name, dbo.tbl_Students.Middle_Initial, 
							 dbo.tbl_Students.Last_Name, dbo.tbl_Students.Grade_Level, dbo.tbl_Students.Section
	FROM            dbo.tbl_Students LEFT JOIN
							 dbo.tbl_UserAccounts ON dbo.tbl_Students.Username = dbo.tbl_UserAccounts.Username INNER JOIN
							 dbo.tbl_EvaluationStudentResult ON dbo.tbl_UserAccounts.Username = dbo.tbl_EvaluationStudentResult.Username
	WHERE			tbl_EvaluationStudentResult.ID_Schedule = @id_Sched
END
GO 

--

CREATE PROCEDURE sp_ViewStudentsThatHaventEvaluated_BySchedID
	@id_Sched INT
AS
BEGIN
	SELECT        DISTINCT dbo.tbl_Students.First_Name, dbo.tbl_Students.Middle_Initial, 
							 dbo.tbl_Students.Last_Name, dbo.tbl_Students.Grade_Level, dbo.tbl_Students.Section
	FROM            dbo.tbl_Students INNER JOIN
							 dbo.tbl_UserAccounts ON dbo.tbl_Students.Username = dbo.tbl_UserAccounts.Username LEFT JOIN
							 dbo.tbl_EvaluationStudentResult ON dbo.tbl_UserAccounts.Username = dbo.tbl_EvaluationStudentResult.Username
	WHERE			tbl_UserAccounts.Username NOT IN (
		SELECT Username FROM tbl_EvaluationStudentResult WHERE tbl_EvaluationStudentResult.ID_Schedule = @id_Sched
	)
END
GO 

--

CREATE PROCEDURE sp_InsertEvaluationPrincipalResultOverall
	@ID_Schedule INT,
	@ID_Teacher INT,
	@Total_Attributes DECIMAL,
	@Total_Instructional DECIMAL,
	@Total_ClassManage DECIMAL,
	@Total_Assessment DECIMAL,
	@WTotal_Attributes DECIMAL,
	@WTotal_Instructional DECIMAL,
	@WTotal_ClassManage DECIMAL,
	@WTotal_Assessment DECIMAL
AS
BEGIN
	INSERT INTO tbl_EvaluationPrincipalResultOverall
	VALUES (@ID_Schedule, @ID_Teacher, @Total_Attributes, @Total_Assessment, @Total_ClassManage,
	@Total_Assessment, @WTotal_Attributes, @WTotal_Instructional, @WTotal_ClassManage, @WTotal_Assessment, (@WTotal_Attributes+@WTotal_Instructional+@WTotal_ClassManage+@WTotal_Assessment))
END
GO

--

CREATE PROCEDURE sp_ViewEvaluationPrincipalResultOverall_BySchedIDTeacherID
	@ID_Schedule INT,
	@ID_Teacher INT
AS
BEGIN
	SELECT * FROM tbl_EvaluationPrincipalResultOverall
	WHERE ID_Schedule = @ID_Schedule AND ID_Teacher = @ID_Teacher
END
GO

--

ALTER PROCEDURE sp_ViewEvaluationPrincipalInnerJoin
	@ID_Schedule INT,
	@ID_Teacher INT
AS
BEGIN
	SELECT        dbo.tbl_EvaluationPrincipalResultOverall2.*, dbo.tbl_Teachers.Last_Name, dbo.tbl_Teachers.First_Name, dbo.tbl_Teachers.School_Year
	FROM            dbo.tbl_EvaluationPrincipalResultOverall2 INNER JOIN
                         dbo.tbl_Teachers ON dbo.tbl_EvaluationPrincipalResultOverall2.ID_Teacher = dbo.tbl_Teachers.ID
	WHERE tbl_EvaluationPrincipalResultOverall2.ID_Teacher = @ID_Teacher AND tbl_EvaluationPrincipalResultOverall2.ID_Schedule = @ID_Schedule
END
GO
--

CREATE PROCEDURE sp_InsertEvaluationPrincipalResultOverall2
	@ID_Schedule INT,
	@ID_Teacher INT,
	@Total_Attributes FLOAT,
	@Total_Instructional FLOAT,
	@Total_ClassManage FLOAT,
	@Total_Assessment FLOAT,
	@Date DATETIME
AS
BEGIN
	INSERT INTO tbl_EvaluationPrincipalResultOverall2
	VALUES (@ID_Schedule, @ID_Teacher, ROUND(@Total_Attributes,2), ROUND(@Total_Assessment,2), ROUND(@Total_ClassManage,2),
	ROUND(@Total_Assessment,2), @Date, ROUND(@Total_Attributes+@Total_Instructional+@Total_ClassManage+@Total_Assessment,2))
END
GO
--
select * from tbl_UserAccounts

--
-- FORGOT PASSWORD -----


CREATE PROCEDURE sp_ForgotPassword
	@new_Username VARCHAR(100),
	@new_Password VARCHAR(100)
AS
BEGIN
	UPDATE tbl_UserAccounts SET Username = @new_Username, Password = @new_Password
	WHERE UserType = 'Admin'
END
GO

----STUDENTS EVALUATION -----

	--- from principla evaluation
	SELECT       tbl_Teachers.ID, dbo.tbl_Teachers.Last_Name + ', ' + tbl_Teachers.First_Name AS Name, dbo.tbl_EvaluationPrincipalQuestion.Criteria, SUM(dbo.tbl_EvaluationPrincipalResult.Answer) AS Score, COUNT(Criteria) AS Total 
	FROM            dbo.tbl_EvaluationPrincipalResult INNER JOIN
							 dbo.tbl_EvaluationPrincipalSchedule ON dbo.tbl_EvaluationPrincipalResult.ID_Schedule = dbo.tbl_EvaluationPrincipalSchedule.ID INNER JOIN
							 dbo.tbl_EvaluationPrincipalID ON dbo.tbl_EvaluationPrincipalSchedule.ID_EvaluationPrincipal = dbo.tbl_EvaluationPrincipalID.ID_Evaluation INNER JOIN
							 dbo.tbl_EvaluationPrincipalQuestion ON dbo.tbl_EvaluationPrincipalResult.ID_Question = dbo.tbl_EvaluationPrincipalQuestion.ID_Question AND 
							 dbo.tbl_EvaluationPrincipalID.ID_Evaluation = dbo.tbl_EvaluationPrincipalQuestion.ID_Evaluation INNER JOIN
							 dbo.tbl_Teachers ON dbo.tbl_EvaluationPrincipalResult.ID_Teacher = dbo.tbl_Teachers.ID
							 WHERE tbl_Teachers.ID = 1 AND tbl_EvaluationPrincipalSchedule.ID = 1
							 GROUP BY dbo.tbl_EvaluationPrincipalQuestion.Criteria, dbo.tbl_Teachers.ID, tbl_Teachers.First_Name, tbl_Teachers.Last_Name
							 select * from tbl_EvaluationPrincipalResult
		---- latest student evaluation 
							 SELECT        dbo.tbl_UserAccounts.Username, dbo.tbl_Students.Last_Name, dbo.tbl_Students.First_Name, SUM(dbo.tbl_EvaluationStudentResult.Answer), 
                         dbo.tbl_EvaluationStudentCriteria.Criteria
FROM            dbo.tbl_EvaluationStudentCriteria INNER JOIN
                         dbo.tbl_EvaluationStudentID ON dbo.tbl_EvaluationStudentCriteria.ID_Evaluation = dbo.tbl_EvaluationStudentID.ID_Evaluation INNER JOIN
                         dbo.tbl_EvaluationStudentQuestions ON dbo.tbl_EvaluationStudentCriteria.ID_Criteria = dbo.tbl_EvaluationStudentQuestions.ID_Criteria INNER JOIN
                         dbo.tbl_EvaluationStudentResult ON dbo.tbl_EvaluationStudentQuestions.ID_Question = dbo.tbl_EvaluationStudentResult.ID_Question INNER JOIN
                         dbo.tbl_EvaluationStudentSchedule ON dbo.tbl_EvaluationStudentID.ID_Evaluation = dbo.tbl_EvaluationStudentSchedule.ID_EvaluationStudent AND 
                         dbo.tbl_EvaluationStudentResult.ID_Schedule = dbo.tbl_EvaluationStudentSchedule.ID INNER JOIN
                         dbo.tbl_UserAccounts ON dbo.tbl_EvaluationStudentResult.Username = dbo.tbl_UserAccounts.Username INNER JOIN
                         dbo.tbl_Students ON dbo.tbl_UserAccounts.Username = dbo.tbl_Students.Username
						 WHERE tbl_UserAccounts.Username = '18-9000074' group by tbl_UserAccounts.Username, tbl_EvaluationStudentCriteria.Criteria, dbo.tbl_Students.Last_Name, dbo.tbl_Students.First_Name
		----  lain nasad after realization
		SELECT        dbo.tbl_EvaluationStudentResult.ID_Teacher, SUM(dbo.tbl_EvaluationStudentResult.Answer) AS Score, dbo.tbl_Students.Section, dbo.tbl_EvaluationStudentCriteria.Criteria
FROM            dbo.tbl_EvaluationStudentCriteria INNER JOIN
                         dbo.tbl_EvaluationStudentID ON dbo.tbl_EvaluationStudentCriteria.ID_Evaluation = dbo.tbl_EvaluationStudentID.ID_Evaluation INNER JOIN
                         dbo.tbl_EvaluationStudentQuestions ON dbo.tbl_EvaluationStudentCriteria.ID_Criteria = dbo.tbl_EvaluationStudentQuestions.ID_Criteria INNER JOIN
                         dbo.tbl_EvaluationStudentResult ON dbo.tbl_EvaluationStudentQuestions.ID_Question = dbo.tbl_EvaluationStudentResult.ID_Question INNER JOIN
                         dbo.tbl_EvaluationStudentSchedule ON dbo.tbl_EvaluationStudentID.ID_Evaluation = dbo.tbl_EvaluationStudentSchedule.ID_EvaluationStudent AND 
                         dbo.tbl_EvaluationStudentResult.ID_Schedule = dbo.tbl_EvaluationStudentSchedule.ID INNER JOIN
                         dbo.tbl_UserAccounts ON dbo.tbl_EvaluationStudentResult.Username = dbo.tbl_UserAccounts.Username INNER JOIN
                         dbo.tbl_Students ON dbo.tbl_UserAccounts.Username = dbo.tbl_Students.Username
where section = 'Zeus'
group by dbo.tbl_EvaluationStudentCriteria.Criteria, dbo.tbl_EvaluationStudentResult.ID_Teacher,dbo.tbl_Students.Section
		----- asdasd
		SELECT        dbo.tbl_EvaluationStudentCriteria.Criteria, dbo.tbl_Students.Section, dbo.tbl_EvaluationStudentResult.ID_Teacher, SUM(dbo.tbl_EvaluationStudentResult.Answer)
FROM            dbo.tbl_Teachers INNER JOIN
                         dbo.tbl_EvaluationStudentCriteria INNER JOIN
                         dbo.tbl_EvaluationStudentID ON dbo.tbl_EvaluationStudentCriteria.ID_Evaluation = dbo.tbl_EvaluationStudentID.ID_Evaluation INNER JOIN
                         dbo.tbl_EvaluationStudentQuestions ON dbo.tbl_EvaluationStudentCriteria.ID_Criteria = dbo.tbl_EvaluationStudentQuestions.ID_Criteria INNER JOIN
                         dbo.tbl_EvaluationStudentResult ON dbo.tbl_EvaluationStudentQuestions.ID_Question = dbo.tbl_EvaluationStudentResult.ID_Question INNER JOIN
                         dbo.tbl_EvaluationStudentSchedule ON dbo.tbl_EvaluationStudentID.ID_Evaluation = dbo.tbl_EvaluationStudentSchedule.ID_EvaluationStudent AND 
                         dbo.tbl_EvaluationStudentResult.ID_Schedule = dbo.tbl_EvaluationStudentSchedule.ID ON 
                         dbo.tbl_Teachers.ID = dbo.tbl_EvaluationStudentResult.ID_Teacher INNER JOIN
                         dbo.tbl_UserAccounts ON dbo.tbl_EvaluationStudentResult.Username = dbo.tbl_UserAccounts.Username INNER JOIN
                         dbo.tbl_Students INNER JOIN
                         dbo.tbl_Section ON dbo.tbl_Students.Section = dbo.tbl_Section.Section ON dbo.tbl_UserAccounts.Username = dbo.tbl_Students.Username
WHERE dbo.tbl_EvaluationStudentResult.ID_Teacher = 1
GROUP BY dbo.tbl_Students.Section, dbo.tbl_EvaluationStudentCriteria.Criteria, dbo.tbl_EvaluationStudentResult.ID_Teacher
				-----
	SELECT        dbo.tbl_EvaluationStudentCriteria.Criteria, dbo.tbl_Students.Section, dbo.tbl_EvaluationStudentResult.ID_Teacher, dbo.tbl_EvaluationStudentResult.Answer
FROM            dbo.tbl_Teachers INNER JOIN
                         dbo.tbl_EvaluationStudentCriteria INNER JOIN
                         dbo.tbl_EvaluationStudentID ON dbo.tbl_EvaluationStudentCriteria.ID_Evaluation = dbo.tbl_EvaluationStudentID.ID_Evaluation INNER JOIN
                         dbo.tbl_EvaluationStudentQuestions ON dbo.tbl_EvaluationStudentCriteria.ID_Criteria = dbo.tbl_EvaluationStudentQuestions.ID_Criteria INNER JOIN
                         dbo.tbl_EvaluationStudentResult ON dbo.tbl_EvaluationStudentQuestions.ID_Question = dbo.tbl_EvaluationStudentResult.ID_Question INNER JOIN
                         dbo.tbl_EvaluationStudentSchedule ON dbo.tbl_EvaluationStudentID.ID_Evaluation = dbo.tbl_EvaluationStudentSchedule.ID_EvaluationStudent AND 
                         dbo.tbl_EvaluationStudentResult.ID_Schedule = dbo.tbl_EvaluationStudentSchedule.ID ON 
                         dbo.tbl_Teachers.ID = dbo.tbl_EvaluationStudentResult.ID_Teacher INNER JOIN
                         dbo.tbl_UserAccounts ON dbo.tbl_EvaluationStudentResult.Username = dbo.tbl_UserAccounts.Username INNER JOIN
                         dbo.tbl_Students INNER JOIN
                         dbo.tbl_Section ON dbo.tbl_Students.Section = dbo.tbl_Section.Section ON dbo.tbl_UserAccounts.Username = dbo.tbl_Students.Username
WHERE dbo.tbl_EvaluationStudentResult.ID_Teacher = 1




-------------- okay na unta ----------------
ALTER PROCEDURE sp_ViewStudentEvaluationResultFinishedByTeacherID
	@id_Student varchar(200),
	@id_Sched INT
AS
BEGIN
--SELECT       tbl_Teachers.ID, dbo.tbl_Teachers.Last_Name + ', ' + tbl_Teachers.First_Name AS Name, dbo.tbl_EvaluationPrincipalQuestion.Criteria, SUM(dbo.tbl_EvaluationPrincipalResult.Answer) AS Scor, COUNT(Criteria) AS Total 
--	SELECT         dbo.tbl_EvaluationStudentResult.Username, dbo.tbl_Students.Last_Name + ', ' + dbo.tbl_Students.First_Name AS Name, dbo.tbl_EvaluationStudentCriteria.Criteria, SUM(dbo.tbl_EvaluationStudentResult.Answer) AS Score
	SELECT	dbo.tbl_UserAccounts.Username AS Student_ID,
				dbo.tbl_Students.Last_Name + ', ' + dbo.tbl_Students.First_Name AS Name,
					SUM(dbo.tbl_EvaluationStudentResult.Answer) AS Score,
                         COUNT(dbo.tbl_EvaluationStudentCriteria.Criteria) AS Total,
						 dbo.tbl_EvaluationStudentCriteria.Criteria, dbo.tbl_EvaluationStudentCriteria.Percentage,
						 (SUM(dbo.tbl_EvaluationStudentResult.Answer) / COUNT(dbo.tbl_EvaluationStudentCriteria.Criteria)) * (dbo.tbl_EvaluationStudentCriteria.Percentage/100) AS Result
	FROM	dbo.tbl_EvaluationStudentCriteria
				INNER JOIN dbo.tbl_EvaluationStudentID
					ON dbo.tbl_EvaluationStudentCriteria.ID_Evaluation = dbo.tbl_EvaluationStudentID.ID_Evaluation
				INNER JOIN dbo.tbl_EvaluationStudentQuestions
					ON dbo.tbl_EvaluationStudentCriteria.ID_Criteria = dbo.tbl_EvaluationStudentQuestions.ID_Criteria
				INNER JOIN dbo.tbl_EvaluationStudentResult
					ON dbo.tbl_EvaluationStudentQuestions.ID_Question = dbo.tbl_EvaluationStudentResult.ID_Question
				INNER JOIN dbo.tbl_EvaluationStudentSchedule
					ON dbo.tbl_EvaluationStudentID.ID_Evaluation = dbo.tbl_EvaluationStudentSchedule.ID_EvaluationStudent
					AND dbo.tbl_EvaluationStudentResult.ID_Schedule = dbo.tbl_EvaluationStudentSchedule.ID
				INNER JOIN dbo.tbl_UserAccounts
					ON dbo.tbl_EvaluationStudentResult.Username = dbo.tbl_UserAccounts.Username
				INNER JOIN dbo.tbl_Students
					ON dbo.tbl_UserAccounts.Username = dbo.tbl_Students.Username
	--WHERE tbl_UserAccounts.Username = @id_Student AND tbl_EvaluationStudentResult.ID_Schedule = @id_Sched
	GROUP BY tbl_UserAccounts.Username, tbl_EvaluationStudentCriteria.Criteria, dbo.tbl_Students.Last_Name, dbo.tbl_Students.First_Name, tbl_EvaluationStudentCriteria.Percentage
END
GO
-------------- okay na unta ----------------




-- 
ALTER PROCEDURE [dbo].[sp_ViewStudentEvaluationResultFinishedByTeacherIDBySection]
	@id_teacher int,
	@id_sched int,
	@numOfSect int
AS
BEGIN
declare @asdf int
set @asdf = @numOfSect
	SELECT	dbo.tbl_EvaluationStudentCriteria.Criteria, tbl_EvaluationStudentSchedule.Date_Start,tbl_EvaluationStudentSchedule.Date_End,dbo.tbl_Teachers.Last_Name, dbo.tbl_Teachers.First_Name,
				dbo.tbl_Students.Section,
					dbo.tbl_EvaluationStudentResult.ID_Teacher,
						SUM(dbo.tbl_EvaluationStudentResult.Answer) as Score,
							COUNT(dbo.tbl_EvaluationStudentCriteria.Criteria) as QuantityofQuestions,
						 dbo.tbl_EvaluationStudentCriteria.Criteria, dbo.tbl_EvaluationStudentCriteria.Percentage,
						 SUM(dbo.tbl_EvaluationStudentResult.Answer) * dbo.tbl_EvaluationStudentCriteria.Percentage/1000.00 AS Result,
						 @asdf as noOfSect
	FROM	dbo.tbl_Teachers
				INNER JOIN dbo.tbl_EvaluationStudentCriteria
				INNER JOIN dbo.tbl_EvaluationStudentID
					ON dbo.tbl_EvaluationStudentCriteria.ID_Evaluation = dbo.tbl_EvaluationStudentID.ID_Evaluation
				INNER JOIN dbo.tbl_EvaluationStudentQuestions
					ON dbo.tbl_EvaluationStudentCriteria.ID_Criteria = dbo.tbl_EvaluationStudentQuestions.ID_Criteria
				INNER JOIN dbo.tbl_EvaluationStudentResult
					ON dbo.tbl_EvaluationStudentQuestions.ID_Question = dbo.tbl_EvaluationStudentResult.ID_Question
				INNER JOIN dbo.tbl_EvaluationStudentSchedule
					ON dbo.tbl_EvaluationStudentID.ID_Evaluation = dbo.tbl_EvaluationStudentSchedule.ID_EvaluationStudent
					AND dbo.tbl_EvaluationStudentResult.ID_Schedule = dbo.tbl_EvaluationStudentSchedule.ID
					ON dbo.tbl_Teachers.ID = dbo.tbl_EvaluationStudentResult.ID_Teacher
				INNER JOIN dbo.tbl_UserAccounts
					ON dbo.tbl_EvaluationStudentResult.Username = dbo.tbl_UserAccounts.Username
				INNER JOIN dbo.tbl_Students
				INNER JOIN dbo.tbl_Section
					ON dbo.tbl_Students.Section = dbo.tbl_Section.Section
					ON dbo.tbl_UserAccounts.Username = dbo.tbl_Students.Username
	WHERE	dbo.tbl_EvaluationStudentResult.ID_Teacher = @id_teacher AND dbo.tbl_EvaluationStudentResult.ID_Schedule = @id_sched 
	GROUP BY	dbo.tbl_Students.Section, dbo.tbl_EvaluationStudentCriteria.Criteria, dbo.tbl_EvaluationStudentResult.ID_Teacher, dbo.tbl_EvaluationStudentCriteria.Percentage, dbo.tbl_Teachers.Last_Name, dbo.tbl_Teachers.First_Name,
	tbl_EvaluationStudentSchedule.Date_Start, tbl_EvaluationStudentSchedule.Date_End
	ORDER BY	dbo.tbl_Students.Section
END
GO
-------------------------nahan na ko mamatay
CREATE PROCEDURE sp_CountNumOfEvaluatedSection
	@id_teacher int,
	@id_sched int
AS
BEGIN

	declare @num int
	select @num = COUNT(DISTINCT dbo.tbl_Section.Section) FROM dbo.tbl_EvaluationStudentResult INNER JOIN
							 dbo.tbl_UserAccounts ON dbo.tbl_EvaluationStudentResult.Username = dbo.tbl_UserAccounts.Username INNER JOIN
							 dbo.tbl_Students ON dbo.tbl_UserAccounts.Username = dbo.tbl_Students.Username INNER JOIN
							 dbo.tbl_Section ON dbo.tbl_Students.Section = dbo.tbl_Section.Section
							 WHERE	dbo.tbl_EvaluationStudentResult.ID_Teacher = @id_teacher AND dbo.tbl_EvaluationStudentResult.ID_Schedule = @id_sched
	return @num	

END
GO







---- trash -----
select * from tbl_EvaluationPrincipalResult where ID_Teacher = 1


select tbl_EvaluationStudentCriteria.Criteria, SUM(tbl_EvaluationStudentResult.Answer) AS total, count(Criteria) from tbl_EvaluationStudentResult INNER JOIN tbl_EvaluationStudentQuestions
on tbl_EvaluationStudentQuestions.ID_Question = tbl_EvaluationStudentResult.ID_Question INNER JOIN tbl_EvaluationStudentCriteria
on tbl_EvaluationStudentCriteria.ID_Criteria = tbl_EvaluationStudentQuestions.ID_Criteria
 WHERE Username = '18-9000074' group by dbo.tbl_EvaluationStudentResult.ID_Question, tbl_EvaluationStudentResult.ID_Teacher, tbl_EvaluationStudentResult.Username, tbl_EvaluationStudentResult.ID_Schedule,
 tbl_EvaluationStudentResult.Answer, tbl_EvaluationStudentCriteria.Criteria

 select *from tbl_EvaluationStudentResult 
 WHERE Username = '18-9000074'
 group by dbo.tbl_EvaluationStudentResult.ID_Question, tbl_EvaluationStudentResult.ID_Teacher, tbl_EvaluationStudentResult.Username, tbl_EvaluationStudentResult.ID_Schedule,
 tbl_EvaluationStudentResult.Answer, tbl_EvaluationStudentCriteria.Criteria




 select dbo.tbl_Students.Last_Name from tbl_EvaluationStudentResult inner join tbl_Students on tbl_Students.ID_No = tbl_EvaluationStudentResult.Username WHERE tbl_EvaluationStudentResult.Username = '18-9000074'

--


	SELECT       tbl_Teachers.ID, dbo.tbl_Teachers.Last_Name + ', ' + tbl_Teachers.First_Name AS Name, 
	COUNT(CASE WHEN tbl_EvaluationPrincipalSchedule.ID = 1 THEN 1 ELSE null END) * 4,
		SUM(CASE WHEN tbl_EvaluationPrincipalSchedule.ID = 1 THEN tbl_EvaluationPrincipalResult.Answer ELSE 0 END),
	cast(ROUND(SUM(CASE WHEN tbl_EvaluationPrincipalSchedule.ID = 1 THEN tbl_EvaluationPrincipalResult.Answer ELSE 0 END) / (COUNT(CASE WHEN tbl_EvaluationPrincipalSchedule.ID = 1 THEN 1 ELSE null END) * 4.0),2) as float)
	FROM            dbo.tbl_EvaluationPrincipalResult INNER JOIN
							 dbo.tbl_EvaluationPrincipalSchedule ON dbo.tbl_EvaluationPrincipalResult.ID_Schedule = dbo.tbl_EvaluationPrincipalSchedule.ID INNER JOIN
							 dbo.tbl_EvaluationPrincipalID ON dbo.tbl_EvaluationPrincipalSchedule.ID_EvaluationPrincipal = dbo.tbl_EvaluationPrincipalID.ID_Evaluation INNER JOIN
							 dbo.tbl_EvaluationPrincipalQuestion ON dbo.tbl_EvaluationPrincipalResult.ID_Question = dbo.tbl_EvaluationPrincipalQuestion.ID_Question AND 
							 dbo.tbl_EvaluationPrincipalID.ID_Evaluation = dbo.tbl_EvaluationPrincipalQuestion.ID_Evaluation INNER JOIN
							 dbo.tbl_Teachers ON dbo.tbl_EvaluationPrincipalResult.ID_Teacher = dbo.tbl_Teachers.ID
							 WHERE tbl_Teachers.ID = 1 AND tbl_EvaluationPrincipalSchedule.ID = 1
							 GROUP BY dbo.tbl_EvaluationPrincipalQuestion.Criteria, dbo.tbl_Teachers.ID, tbl_Teachers.First_Name, tbl_Teachers.Last_Name



------------------------------------------------------------END OF STORED PROC-----------------------------------------------------------------------
delete from tbl_useraccounts where username = 'admin'	

SELECT * FROM tbl_UserAccounts
SELECT * FROM tbl_Teachers
SELECT * FROM tbl_Students
SELECT * FROM tbl_EvaluationPrincipalResult
SELECT * FROM tbl_TeachersHandledSection
SELECT * FROM tbl_Section ORDER BY
SELECT * FROM tbl_EvaluationPrincipalSchedule
SELECT * FROM tbl_EvaluationStudentSchedule
SELECT * FROM tbl_EvaluationStudentResult
SELECT * FROM tbl_EvaluationStudentCriteria
SELECT * FROM tbl_EvaluationStudentQuestions
SELECT * FROM tbl_EvaluationStudentID
DELETE FROM tbl_UserAccounts
DELETE FROM tbl_Students
DELETE FROM tbl_TeachersHandledSection
DELETE FROM	tbl_Teachers 
DELETE FROM tbl_Section
DELETE FROM tbl_EvaluationStudentResult
DELETE FROM tbl_EvaluationPrincipalQuestion
DELETE FROM tbl_EvaluationPrincipalID
DELETE FROM tbl_EvaluationStudentResult
DELETE FROM tbl_EvaluationStudentSchedule
DELETE FROM tbl_EvaluationStudentComments
DELETE FROM tbl_EvaluationStudentCriteria
DELETE FROM tbl_EvaluationStudentID
DELETE FROM tbl_EvaluationStudentQuestions

	DECLARE @dateScheculePrin DATE
	SELECT @dateScheculePrin = Date FROM tbl_EvaluationPrincipalID WHERE ID_Evaluation = 1
	
	IF('2018-09-04' >= @dateSchedulePrin)
		BEGIN
	
			IF('2018-09-04'  <= @dateSchedulePrin)
				BEGIN
					RETURN 1
				END
	
		END




select * from tbl_students where id_no like '%1%' AND section = 'kamote'


SELECT * FROM tbl_EvaluationStudentResult WHERE Username = '18-9000074' AND ID_Schedule = 1